
# td
Some cool tools for not cool TestDirector.

## client
A TestDirector client implemented by Python.

## gen_code
Generator client code from `OTA_API_Reference.chm` or database.

## server
A TestDirector RESTful API Server. #TODO

## tools
Some td tools for my own use.
