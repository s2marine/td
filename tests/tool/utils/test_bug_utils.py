from unittest import TestCase

from s2marine.td.tools.utils import bug_utils
from tests import g


class TestBugUtils(TestCase):
    def test_reset(self):
        bug = g.conn.bug_factory()[14307]
        bug_utils.reset_bug(bug)
