import atexit

from s2marine.td.client.connection import Connection
from s2marine.td.default_conf import *

conn = Connection(remote=False)
conn.connect(username, password, project, td_url)

atexit.register(conn.disconnect)
