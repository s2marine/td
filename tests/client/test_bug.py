from unittest import TestCase

from tests import g


class TestBug(TestCase):
    def setUp(self):
        self.conn = g.conn
        self.bug = self.conn.bug_factory()[1]
        self.attach_bug = self.conn.bug_factory()[26]

    def test_id(self):
        self.assertEqual(self.bug.id, 1)

    def test_attachment_factory(self):
        attachment_factory = self.attach_bug.impl.get__attachments()
        for attach in attachment_factory.new_list(''):
            attach.get__attachment_storage()
            attach.load(True, 'C:\\aaa')
        print(attachment_factory)

    def test_has_attachment(self):
        self.fail()

    def test_is_auto_post(self):
        self.fail()

    def test_set_auto_post(self):
        self.fail()

    def test_post(self):
        self.fail()

    def test_undo(self):
        self.fail()

    def test_history(self):
        self.fail()

    def test_lock(self):
        self.fail()

    def test_unlock(self):
        self.fail()

    def test_update_status(self):
        self.fail()

    def test_update_by(self):
        self.fail()

    def test_invalid(self):
        self.fail()
