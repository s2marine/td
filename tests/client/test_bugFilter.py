from unittest import TestCase

from s2marine.td.client.field import Field
from s2marine.td.client.utils.filter_field import OrderDirection
from s2marine.td.tools.utils.bug_field import BugFields
from tests import g


class TestBugFilter(TestCase):
    def setUp(self):
        self.conn = g.conn
        self.bug_filter = self.conn.bug_factory().filter()

    def test_fields(self):
        self.assertIsInstance(self.bug_filter.fields()[0], Field)

    def test_query(self):
        self.bug_filter[BugFields.缺陷ID] = "<= 10"
        self.bug_filter.set_order(BugFields.缺陷ID, 1, OrderDirection.desc)
        bugs = self.bug_filter.query()
        self.assertEqual(len(bugs), 10)
        self.assertEqual(bugs[0][BugFields.缺陷ID], 10)

        self.bug_filter.clear()
        self.bug_filter[BugFields.缺陷ID] = "<= 30"
        bugs = self.bug_filter.query()
        self.assertEqual(len(bugs), 30)
        self.assertEqual(bugs[0][BugFields.缺陷ID], 1)
