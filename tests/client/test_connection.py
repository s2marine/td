from unittest import TestCase

from s2marine.td.client.bug import BugFactory
from s2marine.td.client.command import Command
from s2marine.td.client.connection import Connection
from s2marine.td.default_conf import password, project, td_url, username
from tests import g


class TestConnection(TestCase):
    def setUp(self):
        self.conn = g.conn

    def test_connect(self):
        conn = Connection(remote=True)
        conn.connect(username, password, project, td_url)
        self.assertTrue(conn.is_connected)
        conn.disconnect()
        self.assertFalse(conn.is_connected)

    def test_username(self):
        self.assertEqual(self.conn.username, username)

    def test_command(self):
        self.assertIsInstance(self.conn.command(), Command)

    def test_bug_factory(self):
        self.assertIsInstance(self.conn.bug_factory(), BugFactory)
