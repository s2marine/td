from unittest import TestCase

from s2marine.td.client.bug import BugFilter
from s2marine.td.client.history import History
from tests import g


class TestBugFactory(TestCase):
    def setUp(self):
        self.conn = g.conn
        self.bug_factory = g.conn.bug_factory()

    def test__getitem__(self):
        self.assertEqual(self.bug_factory[1].id, 1)

    def test_filter(self):
        self.assertIsInstance(self.bug_factory.filter(), BugFilter)

    def test_history(self):
        self.assertIsInstance(self.bug_factory.history(), History)
