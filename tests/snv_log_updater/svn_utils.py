from datetime import datetime
from unittest import TestCase

from s2marine.td.svn_log_updater import svn_utils
from s2marine.td.svn_log_updater.utils import SVNChangeCommit, SVNCommit


class TestSVNUtils(TestCase):

    def test_merge_changes(self):
        commit1 = SVNCommit(revision=1, author='me', date=datetime.now(), msg='')
        commit2 = SVNCommit(revision=2, author='me', date=datetime.now(), msg='')
        commit3 = SVNCommit(revision=3, author='me', date=datetime.now(), msg='should removed')
        commit41 = SVNCommit(revision=4, author='me', date=datetime.now(), msg='should override')
        commit42 = SVNCommit(revision=4, author='me', date=datetime.now(), msg='overrided')

        changes = SVNChangeCommit(changes=[commit1, commit3, commit41], removes=[99])
        merge_frm = SVNChangeCommit(changes=[commit2, commit42], removes=[3])

        svn_utils.merge_changes(changes, merge_frm)

        self.assertTrue(len(changes.changes), 2)
        self.assertIs(changes.changes[0], commit1)
        self.assertIn(commit2, changes.changes)

        self.assertIn(commit42, changes.changes)
        self.assertListEqual(changes.removes, [99, 3])
