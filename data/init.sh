#!/bin/bash

7z x OTA_API_Reference.chm -oOTA_API_Reference

if [ -d "OTA_API_Reference" ]; then
  cd OTA_API_Reference
  find . -not -name 'TDAPIOLELib*.html' -delete
  cd ..
fi