from setuptools import setup, find_packages

setup(
    name='s2marine.td',
    version='0.1.0',
    packages=find_packages(),
    author='s2marine',
    description='Some cool tools for not cool TestDirector',
    license='GPL3',
    keywords='TestDirector',
    install_requires=['comtypes'],
    extras_require={
        'gen_code': ['beautifulsoup4', 'html5lib', 'jinja2', 'pyperclip'],
        'svn_log_updater': ['beautifulsoup4', 'jinja2']
    }
)
