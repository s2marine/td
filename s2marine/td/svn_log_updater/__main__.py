import sys
import time
from getopt import getopt

from s2marine.td.client.connection import Connection
from s2marine.td.client.utils.exception import ITEM_DOES_NOT_EXISTS, BusinessException
from s2marine.td.svn_log_updater import td_utils, svn_utils, save_utils, config
from s2marine.td.svn_log_updater.logger import updater_logger
from s2marine.td.svn_log_updater.utils import SVNConfig


def redo_failed():
    if not config.FAILED_TDS:
        return

    success_tds = []

    conn = Connection()
    conn.connect(config.TD_USER, config.TD_PASS, config.TD_PROJECT, config.TD_URL)
    for td, changes in config.FAILED_TDS.items():
        updater_logger.debug(f'main: redo update td: {td}')
        try:
            td_utils.update_bug_comment(conn, td, changes)
            success_tds.append(td)
        except BusinessException as e:
            updater_logger.exception(f'main: failed redo update td: {td}, {e.message}')
            if e.code == ITEM_DOES_NOT_EXISTS:
                success_tds.append(td)
        except:
            updater_logger.exception(f'main: failed redo update td: {td}')

    for td in success_tds:
        del config.FAILED_TDS[td]

    if success_tds:
        save_utils.save_failed_queue()

    conn.disconnect()


def main():
    commit_group_by_td = svn_utils.get_changes_group_by_td()
    if not commit_group_by_td:
        return

    conn = Connection()
    conn.connect(config.TD_USER, config.TD_PASS, config.TD_PROJECT, config.TD_URL)
    all_success = True
    for td, changes in commit_group_by_td.items():
        if td in config.FAILED_TDS:
            updater_logger.debug(f'main: merge change to failed: {td}')
            svn_utils.merge_changes(config.FAILED_TDS[td], changes)
            continue
        updater_logger.debug(f'main: update td: {td}')

        try:
            td_utils.update_bug_comment(conn, td, changes)
        except BusinessException as e:
            updater_logger.exception(f'main: failed redo update td: {td}, {e.message}')
            if e.code != ITEM_DOES_NOT_EXISTS:
                all_success = False
                config.FAILED_TDS[td] = changes
        except:
            updater_logger.exception(f'main: failed update td: {td}')
            all_success = False
            config.FAILED_TDS[td] = changes

    conn.disconnect()

    if all_success and not config.FAILED_TDS:
        save_utils.save_all()
    else:
        save_utils.save_failed_queue()


def init():
    read_opt()
    save_utils.load_all()


def read_opt():
    optlist, args = getopt(sys.argv[1:], 'd', ['svn-user=', 'svn-url=',
                                               'td-url=', 'td-project=', 'td-user=', 'td-pass='])
    for opt, value in optlist:
        if opt == '--svn-user':
            config.USERS.append(value)
        elif opt == '--svn-url':
            config.SVN_ROOTS.append(SVNConfig(value))
        elif opt == '--td-url':
            config.TD_URL = value
        elif opt == '--td-project':
            config.TD_PROJECT = value
        elif opt == '--td-user':
            config.TD_USER = value
        elif opt == '--td-pass':
            config.TD_PASS = value
        elif opt == '-d':
            config.DRY_RUN = True

    updater_logger.debug(f'main: read opt: users: {config.USERS}, \n'
                         f'svn configs: {config.SVN_ROOTS}, \n'
                         f'td config: {config.TD_URL} {config.TD_PROJECT} {config.TD_USER}')


if __name__ == '__main__':
    init()
    while True:
        main()
        time.sleep(60)
        redo_failed()
