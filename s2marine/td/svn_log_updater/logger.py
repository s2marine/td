import logging

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('comtypes').setLevel(logging.ERROR)

updater_logger = logging.getLogger('td.svn_info_updater')
updater_logger.addHandler(logging.FileHandler('update.log'))
