import dataclasses
import datetime
from dataclasses import dataclass, field
from typing import List, Dict, Set


@dataclass
class SVNConfig:
    url: str
    max_revision: int = 0
    commits: 'Dict[int, SVNCommit]' = field(default_factory=dict)


@dataclass(eq=True, frozen=True)
class CommitPath:
    kind: str
    action: str
    path: str

    @staticmethod
    def to_dict(obj):
        if isinstance(obj, list):
            return [CommitPath.to_dict(o) for o in obj]
        return dataclasses.asdict(obj)

    @staticmethod
    def from_dict(data):
        if isinstance(data, list):
            return [CommitPath.to_dict(o) for o in data]
        return CommitPath(**data)


@dataclass
class SVNCommit:
    revision: int
    author: str
    date: datetime.datetime
    msg: str
    tds: 'Set[int]' = field(default_factory=set)
    paths: 'List[CommitPath]' = None

    remove_tds: 'Set[int]' = field(default_factory=set)

    @staticmethod
    def to_dict(obj):
        if isinstance(obj, list):
            return [SVNCommit.to_dict(o) for o in obj]
        data = dataclasses.asdict(obj)
        data['tds'] = list(data['tds']) if 'tds' in data else None
        data['date'] = data['date'].timestamp() if 'date' in data else None
        del data['remove_tds']
        return data

    @staticmethod
    def from_dict(data):
        if isinstance(data, list):
            return [SVNCommit.from_dict(o) for o in data]
        data['tds'] = set(data['tds']) if 'tds' in data else None
        data['paths'] = [CommitPath.from_dict(path) for path in data['paths']] if 'paths' in data else None
        data['date'] = datetime.datetime.fromtimestamp(data['date']) if 'date' in data else None
        return SVNCommit(**data)


@dataclass
class SVNChangeCommit:
    changes: 'List[SVNCommit]' = field(default_factory=list)
    removes: 'List[int]' = field(default_factory=list)
