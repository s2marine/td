import json
import os
import pickle
from pathlib import Path
from typing import Dict, List

from s2marine.td.svn_log_updater import config
from s2marine.td.svn_log_updater.logger import updater_logger
from s2marine.td.svn_log_updater.utils import SVNCommit

db_file = Path(os.getcwd()).joinpath('db.json')
failed_queue_file = Path(os.getcwd()).joinpath('failed.data')
td_commit_dir = Path(os.getcwd()).joinpath('tds')


def save_all():
    save_progress()
    save_failed_queue()


def save_progress():
    if not db_file.exists():
        db_file.touch()

    data = {svn_root.url: svn_root.max_revision for svn_root in config.SVN_ROOTS}
    data_str = json.dumps(data)

    with open(db_file, 'w', encoding='UTF-8') as db:
        db.write(data_str)
    updater_logger.debug(f'data: save progress success: {data_str}')


def save_failed_queue():
    if not failed_queue_file.exists():
        return

    with open(failed_queue_file, 'wb') as f:
        pickle.dump(config.FAILED_TDS, f)

    updater_logger.debug(f'data: save failed success: {config.FAILED_TDS}')


def save_td_commit(td: 'int', commits: 'List[SVNCommit]'):
    if not td_commit_dir.exists():
        td_commit_dir.mkdir()
    td_commit_file = td_commit_dir.joinpath(str(td))
    if not td_commit_file.exists():
        td_commit_file.mkdir()

    commits = SVNCommit.to_dict(commits)
    data_str = json.dumps(commits)

    with open(td_commit_file, 'w', encoding='UTF-8') as db:
        db.write(data_str)
    updater_logger.debug(f'data: save td commit success: {td}')


def load_all():
    load_progress()
    load_failed_queue()


def load_progress():
    if not db_file.is_file():
        return

    with db_file.open('r', encoding='UTF-8') as db:
        data_str = db.read()

    try:
        saved_data: 'Dict[str, int]' = json.loads(data_str)
    except Exception as ignored:
        print(ignored)
        return

    updater_logger.debug(f'data: load progress success: {saved_data}')
    for svn_root in config.SVN_ROOTS:
        if svn_root.url in saved_data:
            svn_root.max_revision = saved_data[svn_root.url]


def load_failed_queue():
    if not failed_queue_file.exists():
        return

    with open(failed_queue_file, 'rb') as f:
        config.FAILED_TDS = pickle.load(f)

    updater_logger.debug(f'data: load failed success: {config.FAILED_TDS}')


def load_td_commit(td: 'int') -> 'List[SVNCommit]':
    td_commit_file = td_commit_dir.joinpath(str(td))
    if not td_commit_file.is_file():
        return []

    with td_commit_file.open('r', encoding='UTF-8') as db:
        data_str = db.read()

    try:
        saved_data: 'List[Dict]' = json.loads(data_str)
    except Exception as ignored:
        print(ignored)
        return []

    updater_logger.debug(f'data: load td commit success: {td}')
    return SVNCommit.from_dict(saved_data)
