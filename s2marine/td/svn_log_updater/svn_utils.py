import datetime
import re
import subprocess
from typing import List, Dict

from bs4 import BeautifulSoup

from s2marine.td.svn_log_updater.config import USERS, SVN_ROOTS, SVN_COMMAND
from s2marine.td.svn_log_updater.logger import updater_logger
from s2marine.td.svn_log_updater.utils import SVNCommit, CommitPath, SVNConfig, SVNChangeCommit


def get_changes_group_by_td():
    all_changes = get_changes()
    ret = group_changes_by_td(all_changes)
    updater_logger.debug(f'svn: changes group by td : {ret}')
    return ret


def get_changes() -> 'List[SVNCommit]':
    changes = []
    for conf in SVN_ROOTS:
        changes += get_svn_change(conf)
    return changes


def group_changes_by_td(commits: 'List[SVNCommit]') -> 'Dict[int, SVNChangeCommit]':
    ret: 'Dict[int, SVNChangeCommit]' = {}

    for commit in commits:
        for td in commit.tds:
            if td not in ret:
                ret[td] = SVNChangeCommit()
            ret[td].changes.append(commit)
        for td in commit.remove_tds:
            if td not in ret:
                ret[td] = SVNChangeCommit()
            ret[td].removes.append(commit.revision)

    return ret


def get_svn_change(svn_config: 'SVNConfig') -> 'List[SVNCommit]':
    commits = query_svn_log(svn_config.url)
    changes = []

    for commit in commits:
        revision = commit.revision
        if revision <= svn_config.max_revision and revision not in svn_config.commits:
            svn_config.commits[revision] = commit
            continue

        if revision not in svn_config.commits:
            updater_logger.debug(f'svn: new commit: {revision}')
            svn_config.max_revision = max(revision, svn_config.max_revision)
            svn_config.commits[revision] = commit
            changes.append(commit)
            continue
        ori_commit = svn_config.commits[revision]

        # msg change but td no
        if ori_commit.msg != commit.msg and ori_commit.tds == commit.tds:
            updater_logger.debug(f'svn: msg change: {ori_commit.msg} -> {commit.msg}')
            ori_commit.msg = commit.msg
            changes.append(commit)

        # msg change and td change
        elif ori_commit.msg != commit.msg and ori_commit.tds != commit.tds:
            updater_logger.debug(f'svn: td change: {ori_commit.tds} -> {commit.tds}')
            ori_commit.msg = commit.msg
            commit.removed_tds = ori_commit.tds - commit.tds
            ori_commit.tds = commit.tds
            changes.append(ori_commit)

    return changes


td_re = re.compile('TD(\d+)')


def query_svn_log(svn_url: 'str') -> 'List[SVNCommit]':
    updater_logger.debug(f'svn: start query svn log : {svn_url}')
    from_date = (datetime.date.today() - datetime.timedelta(days=30)).strftime('%Y-%m-%d')

    cmd = f'{SVN_COMMAND} log --xml -v --search TD'.split(' ')
    cmd += f'-r {{{from_date}}}:HEAD'.split(' ')
    cmd += [svn_url]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    out, err = proc.communicate()

    soup = BeautifulSoup(out.decode('utf-8'), 'html.parser')

    logs = []
    for alog in soup.find_all('logentry'):
        author = alog.find('author').text
        msg = alog.find('msg').text
        if author not in USERS:
            continue
        tds = td_re.findall(msg)
        if not tds:
            continue

        revision = int(alog['revision'])
        entry_date = datetime.datetime.strptime(alog.find('date').text[:19], '%Y-%m-%dT%H:%M:%S')
        tds = set(map(int, tds))

        log = SVNCommit(revision, author, entry_date, msg, tds)
        log.paths = [CommitPath(p['kind'], p['action'], p.text) for p in alog.find_all('path')]
        logs.append(log)

    return logs


def merge_changes(changes: 'SVNChangeCommit', merge_frm: 'SVNChangeCommit'):
    change_group_by_revision = {change.revision: change
                                for change in changes.changes
                                if change.revision not in merge_frm.removes}
    for change in merge_frm.changes:
        change_group_by_revision[change.revision] = change

    changes.changes = list(change_group_by_revision.values())
    changes.removes += merge_frm.removes
