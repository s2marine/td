from typing import Dict, TYPE_CHECKING, List

if TYPE_CHECKING:
    from s2marine.td.svn_log_updater.utils import SVNChangeCommit, SVNConfig

SVN_COMMAND = 'svn'

SVN_ROOTS: 'List[SVNConfig]' = []
USERS = []
DRY_RUN = False

FAILED_TDS: 'Dict[int, SVNChangeCommit]' = {}

TD_URL = None
TD_PROJECT = None
TD_USER = None
TD_PASS = None
