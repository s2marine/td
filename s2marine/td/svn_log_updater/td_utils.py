import json
from datetime import datetime
from typing import List

from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader

from s2marine.td.client.connection import Connection
from s2marine.td.svn_log_updater import config, save_utils
from s2marine.td.svn_log_updater.logger import updater_logger
from s2marine.td.svn_log_updater.utils import SVNCommit, SVNChangeCommit
from s2marine.td.tools.utils.bug_field import BugFields


def analyze_td_desc(description: 'str') -> ('BeautifulSoup', 'BeautifulSoup'):
    description = description if description else ''
    soup = BeautifulSoup(description, 'html.parser')
    body = soup.find('body')
    if not body:
        html = soup.new_tag('html')
        body = soup.new_tag('body')
        html.append(body)
        soup.append(html)

    prefix: 'BeautifulSoup' = body.find('b', text='-' * 100)
    if not prefix:
        prefix = soup.new_tag('b')
        prefix.append(soup.new_string('-' * 100))
        body.append(prefix)

    suffix: 'BeautifulSoup' = prefix.find_next('b', text='-' * 100)
    if not suffix:
        suffix = soup.new_tag('b')
        suffix.append(soup.new_string('-' * 100))
        body.append(suffix)
        body.append(soup.new_tag('br'))

    next_tag: 'BeautifulSoup' = prefix.next_sibling
    while next_tag and next_tag != suffix:
        next_tag = next_tag.next_sibling
        next_tag.previous_sibling.extract()

    return soup, prefix


def analyze_td_data(ori_commits: 'List[SVNCommit]', changes: 'List[SVNCommit]', remove_revision: 'List[int]') -> 'List[SVNCommit]':
    ori_group_by_revision = {ori.revision: ori for ori in ori_commits if ori.revision not in remove_revision}

    for change in changes:
        if change.revision in ori_group_by_revision:
            ori_group_by_revision[change.revision].msg = change.msg
        else:
            ori_group_by_revision[change.revision] = change

    return [commit for commit in ori_group_by_revision.values()]


def commits_to_html(prefix: 'BeautifulSoup', commits: 'List[SVNCommit]'):
    paths = sorted(list(set([path for commit in commits for path in commit.paths])), key=lambda x: [x.path, x.action])
    data = SVNCommit.to_dict(commits)
    data = json.dumps(data)

    env = Environment(loader=FileSystemLoader('.'))
    tpl = env.get_template('comments.tpl')
    html = tpl.render(commits=commits, paths=paths, data=data,
                      first=commits[0].date.strftime('%Y-%m-%d %H:%M:%S'),
                      last=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    soup = BeautifulSoup(html, 'html.parser')
    br = soup.new_tag('br')
    prefix.insert_after(br)
    br.insert_after(soup)


def update_bug_comment(conn: 'Connection', td: 'int', changes: 'SVNChangeCommit'):
    if config.DRY_RUN:
        updater_logger.debug(f'td: dry update: {td}')
        return

    bug = conn.bug_factory()[td]
    desc = bug[BugFields.开发注释]
    ori_commits = save_utils.load_td_commit(td)
    soup, prefix = analyze_td_desc(desc)
    result_commit = analyze_td_data(ori_commits, changes.changes, changes.removes)
    commits_to_html(prefix, result_commit)
    bug[BugFields.开发注释] = str(soup)
    bug.post()
    save_utils.save_td_commit(td, result_commit)
