<font>first commit: {{first}} | last update: {{last}}</font><br/>
{% for commit in commits %}
    <font>r{{ commit.revision }} | {{ commit.author }} | {{ commit.date }} | </font>
    <font>{{ commit.msg.replace('\r', '<br/>') }}</font><br/>
{% endfor %}
<br/>
<br/>
{% for path in paths %}
    <font>{{ path.action }} {{ path.path }}</font><br/>
{% endfor %}
<font color="#FBF8EE">{{data}}</font><br/>
