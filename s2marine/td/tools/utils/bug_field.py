from s2marine.td.client.utils.value_enum import ValueEnums, SystemFiled


class BugFields(ValueEnums):
    B角测试 = SystemFiled('BG_USER_01', 'B角测试', edit_style='UserCombo')
    B角测试工时 = SystemFiled('BG_USER_12', 'B角测试工时')
    标题 = SystemFiled('BG_SUMMARY', '标题', required=True)
    测试负责人 = SystemFiled('BG_USER_21', '测试负责人', edit_style='UserCombo')
    测试工时 = SystemFiled('BG_USER_22', '测试工时')
    测试类型 = SystemFiled('BG_USER_20', '测试类型', edit_style='TreeCombo', root_id=110)
    测试用例 = SystemFiled('BG_USER_25', '测试用例', field_type='memo')
    产品模块 = SystemFiled('BG_USER_09', '产品模块', edit_style='TreeCombo', root_id=108)
    单元测试重点 = SystemFiled('BG_USER_27', '单元测试重点', field_type='memo')
    发现版本 = SystemFiled('BG_DETECTION_VERSION', '发现版本', edit_style='ListCombo', root_id=35)
    关闭日期 = SystemFiled('BG_CLOSING_DATE', '关闭日期', field_type='date', edit_style='DateCombo')
    关联需求单 = SystemFiled('BG_USER_11', '关联需求单')
    环境类型 = SystemFiled('BG_PROJECT', '环境类型', edit_style='TreeCombo', root_id=25)
    计划关闭版本 = SystemFiled('BG_PLANNED_CLOSING_VER', '计划关闭版本', edit_style='ListCombo', root_id=35)
    计划结束时间 = SystemFiled('BG_USER_03', '计划结束时间', edit_style='DateCombo')
    计划解决工时 = SystemFiled('BG_ESTIMATED_FIX_TIME', '计划解决工时', field_type='number')
    解决人 = SystemFiled('BG_RESPONSIBLE', '解决人', edit_style='UserCombo')
    界面体现位置 = SystemFiled('BG_USER_26', '界面体现位置', field_type='memo')
    开发二工时 = SystemFiled('BG_USER_19', '开发二工时')
    开发二解决人 = SystemFiled('BG_USER_18', '开发二解决人', edit_style='UserCombo')
    开发一工时 = SystemFiled('BG_ACTUAL_FIX_TIME', '开发一工时', field_type='number')
    开发一解决人 = SystemFiled('BG_USER_17', '开发一解决人', edit_style='UserCombo')
    开发注释 = SystemFiled('BG_DEV_COMMENTS', '开发注释', field_type='memo')
    客户名称 = SystemFiled('BG_USER_14', '客户名称')
    描述信息 = SystemFiled('BG_DESCRIPTION', '描述信息', field_type='memo')
    缺陷ID = SystemFiled('BG_BUG_ID', '缺陷ID', field_type='number', editable=False)
    缺陷发现的阶段 = SystemFiled('BG_USER_06', '缺陷发现的阶段', edit_style='TreeCombo', root_id=82)
    缺陷引入的活动 = SystemFiled('BG_USER_07', '缺陷引入的活动', edit_style='TreeCombo', root_id=77)
    设计解决人 = SystemFiled('BG_USER_16', '设计解决人', edit_style='UserCombo')
    实际关闭版本 = SystemFiled('BG_CLOSING_VERSION', '实际关闭版本', edit_style='ListCombo', root_id=35)
    实际结束时间 = SystemFiled('BG_USER_05', '实际结束时间', edit_style='DateCombo')
    实际设计工时 = SystemFiled('BG_USER_10', '实际设计工时')
    实际需求工时 = SystemFiled('BG_USER_04', '实际需求工时')
    是否可再现 = SystemFiled('BG_REPRODUCIBLE', '是否可再现', edit_style='ListCombo', root_id=1)
    属性 = SystemFiled('BG_USER_08', '属性', edit_style='TreeCombo', root_id=91)
    提交人 = SystemFiled('BG_DETECTED_BY', '提交人', edit_style='UserCombo', required=True)
    提交日期 = SystemFiled('BG_DETECTION_DATE', '提交日期', field_type='date', edit_style='DateCombo', required=True)
    需求解决人 = SystemFiled('BG_USER_15', '需求解决人', edit_style='UserCombo')
    严重性 = SystemFiled('BG_SEVERITY', '严重性', edit_style='ListCombo', root_id=36)
    影响客户 = SystemFiled('BG_USER_13', '影响客户')
    优先级 = SystemFiled('BG_PRIORITY', '优先级', edit_style='ListCombo', root_id=5)
    责任人 = SystemFiled('BG_USER_02', '责任人', edit_style='UserCombo')
    状态 = SystemFiled('BG_STATUS', '状态', edit_style='ListCombo', root_id=4)
    最后变动时间 = SystemFiled('BG_VTS', '最后变动时间', editable=False)
    Subject = SystemFiled('BG_SUBJECT', 'Subject',
                          field_type='number', edit_style='TreeCombo',
                          editable=False, is_by_code=True, root_id=2)


class BugStatus(ValueEnums):
    Closed = 'Closed'
    Open = 'Open'
    New = 'New'
    Fixed = 'Fixed'
    Rejected = 'Rejected'
    Reopen = 'Reopen'
    Configed = 'Configed'
    Defer = 'Defer'
    precfg = 'precfg'
    Validate = 'Validate'
    Verify = 'Verify'


class BugType(ValueEnums):
    变更 = '变更'
    缺陷 = '缺陷'
    任务 = '任务'
