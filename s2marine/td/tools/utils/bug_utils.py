from datetime import datetime
from typing import Optional

from s2marine.td.client.bug import Bug, BugFactory
from s2marine.td.client.utils.filter_field import OrderDirection
from s2marine.td.client.utils.value_enum import SystemFiled
from s2marine.td.tools.utils.bug_field import BugFields, BugStatus

special_fields = [BugFields.状态]


def update_status(bug: 'Bug', value: 'BugStatus') -> 'bool':
    if isinstance(value, BugStatus):
        value = value.value
    command = bug.conn.command()
    command.sql = f"update td.BUG set BG_STATUS = '{value}' where BG_BUG_ID = '{bug.id}'"
    return command.execute() > 0


def reset_bug(bug: 'Bug'):
    for fieldEnum in BugFields:
        field: 'SystemFiled' = fieldEnum.value
        if field.editable and not field.required and field not in special_fields:
            print(field.name)
            bug[fieldEnum] = None
        bug[BugFields.提交人] = bug.conn.username
        bug[BugFields.提交日期] = datetime.now().strftime('%Y/%m/%d')


def get_free_bug(factory: 'BugFactory', create: bool = True) -> 'Optional[Bug]':
    empty_td = __get_empty_bug(factory)
    if empty_td:
        reset_bug(empty_td)
        return empty_td

    abandon_td = __get_abandon_bug(factory)
    if abandon_td:
        reset_bug(abandon_td)
        return abandon_td

    if create:
        new_td = factory.add_bug()
        reset_bug(new_td)
        return new_td


def __get_abandon_bug(factory: 'BugFactory') -> 'Optional[Bug]':
    bug_filter = factory.filter()
    bug_filter[BugFields.提交人] = factory.conn.username
    bug_filter[BugFields.标题] = "='' OR *feidan* OR *feiqi*"
    bug_filter[BugFields.状态] = f'={BugStatus.New} OR {BugStatus.Open}'
    bug_filter.set_order(BugFields.缺陷ID, 1, OrderDirection.desc)
    bug_list = bug_filter.query()
    return bug_list[0] if bug_list else None


def __get_empty_bug(factory: 'BugFactory') -> Optional['Bug']:
    bug_filter = factory.filter()
    bug_filter[BugFields.标题] = "=''"
    bug_filter[BugFields.提交人] = "=''"
    bug_filter[BugFields.状态] = f'={BugStatus.New} OR {BugStatus.Open}'
    bug_filter.set_order(BugFields.缺陷ID, 1, OrderDirection.desc)
    bug_list = bug_filter.query()
    return bug_list[0] if bug_list else None
