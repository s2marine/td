import re
from typing import List

from s2marine.td.client.bug import Bug
from s2marine.td.client.connection import Connection
from s2marine.td.client.utils.exception import BusinessException
from s2marine.td.default_conf import *
from s2marine.td.tools.utils.bug_field import BugFields, BugStatus

from_td = 14100
target_version = ''


def split_title(bug: 'Bug'):
    title = bug[BugFields.标题]
    m = re.match('\('
                 '任务(?P<master>\d+)\[\d+\]'
                 ',\s'
                 '变更(?P<hotfix>.+)'
                 '\)'
                 '(?P<title>.*)', title)
    if not m:
        return None, None
    hotfix = m.group('hotfix')
    hotfix = re.sub('\[.*?\]', '', hotfix)
    return m.group('master'), re.split(',\s', hotfix)


def query_hotfix_versions(conn: 'Connection', project_name: str = None) -> 'List[str]':
    bug_filter = conn.bug_factory().filter()
    bug_filter[BugFields.标题] = '版本依赖及发布文档'
    bug_filter[BugFields.状态] = BugStatus.Open
    bug_filter[BugFields.最后变动时间] = '[ThisWeek]'
    if project_name:
        bug_filter[BugFields.计划关闭版本] = f'"{project_name}*"'
    ret: 'List[str]' = [bug[BugFields.计划关闭版本] for bug in bug_filter.query()]
    return ret


if __name__ == '__main__':
    conn = Connection()
    conn.connect(username, password, project, td_url)

    from_bug = conn.bug_factory()[from_td]
    master_td, hotfix_tds = split_title(from_bug)
    master_bug = conn.bug_factory()[master_td]
    hotfix_bugs = [conn.bug_factory()[hotfix_td] for hotfix_td in hotfix_tds]
    print(master_td, hotfix_tds)

    master_version: str = master_bug[BugFields.计划关闭版本]
    project = master_version.split(' ')[0]

    hotfix_versions = [hotfix[BugFields.计划关闭版本] for hotfix in hotfix_bugs]
    open_versions = [version for version in query_hotfix_versions(conn, project) if version not in hotfix_versions]

    target_version = open_versions[0] if len(open_versions) == 1 else target_version
    if not target_version:
        raise BusinessException(f'没有找到唯一分支版本: {open_versions}')

    conn.disconnect()
