from s2marine.td.client.connection import Connection
from s2marine.td.default_conf import username, password, project, td_url

'''
update td.SYSTEM_FIELD
   set SF_IS_ACTIVE        = 'Y', -- N
       SF_IS_HISTORY       = 'Y', -- N
       SF_USER_COLUMN_TYPE = 'char', -- None
       SF_USER_LABEL       = '关联单号', -- None
       SF_GRANT_MODIFY     = '2031' -- 1007
 where SF_TABLE_NAME = 'BUG'
   and SF_COLUMN_NAME = 'BG_USER_23'
'''
if __name__ == '__main__':
    conn = Connection()
    conn.connect(username, password, project, td_url)

    query_table = conn.command()
    sql = '''
    select * from td.SYSTEM_FIELD l where l.SF_TABLE_NAME = 'BUG'
    '''

    query_table.sql = sql
    rowset = query_table.query()
    print(rowset.recordset.get__record_count())
    i = 0
    for i in range(rowset.recordset.get__col_count()):
        print(rowset.recordset.get__col_name(i), end=', ')
    print()

    for row in rowset:
        for col in row:
            print(col, end=', ')
        print()

    print(i)

    conn.disconnect()
