from s2marine.td.client.connection import Connection
from s2marine.td.default_conf import *
from s2marine.td.tools.utils import bug_utils
from s2marine.td.tools.utils.bug_field import BugStatus

td = 14131

if __name__ == '__main__':
    conn = Connection(remote=True)
    conn.connect(username, password, project, td_url)

    bug_factory = conn.bug_factory()
    bug = bug_factory[td]
    bug.lock()
    bug_utils.update_status(bug, BugStatus.Open)
    bug.post()
    bug.unlock()

    conn.disconnect()
