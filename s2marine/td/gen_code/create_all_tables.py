from pprint import pprint

from s2marine.td.client.impl.direct.td_connection_impl import TDConnectionImpl
from s2marine.td.client.utils.td_constant import TD_OBJ
from s2marine.td.default_conf import *

data_path = '../../../data/'

if __name__ == '__main__':
    from comtypes.client import CreateObject

    com_obj = CreateObject(TD_OBJ)
    conn = TDConnectionImpl(com_obj)
    conn.init_connection_ex(td_url)
    conn.connect_project(project, username, password)

    query_table = conn.get__command()
    sql = """
select t.TABLE_SCHEMA,
       t.TABLE_NAME,
       c.COLUMN_NAME,
       c.DATA_TYPE,
       c.CHARACTER_MAXIMUM_LENGTH,
       f.SF_USER_LABEL
  from INFORMATION_SCHEMA.TABLES             t
       inner join INFORMATION_SCHEMA.COLUMNS c
           on t.TABLE_SCHEMA = c.TABLE_SCHEMA
                and t.TABLE_NAME = c.TABLE_NAME
       left join  TD.SYSTEM_FIELD            f
           on t.TABLE_NAME = f.SF_TABLE_NAME
                and c.COLUMN_NAME = f.SF_COLUMN_NAME
 where t.TABLE_SCHEMA = 'td'
 """

    query_table.set__command_text(sql)
    recordset = query_table.execute()

    rows = []
    for _ in range(recordset.get__record_count()):
        row = {}
        for i in range(recordset.get__col_count()):
            row[recordset.get__col_name(i)] = recordset.get__field_value(i)
        rows.append(row)
        recordset.next()
    pprint(rows)

    tables = {}
    for row in rows:
        if row['TABLE_NAME'] in tables:
            tables[row['TABLE_NAME']].append(row)
        else:
            tables[row['TABLE_NAME']] = [row]

    pprint(tables)
    f = open(data_path + 'create_all_tables.sql', 'w', encoding='UTF-8')
    for table_name, cols in tables.items():
        f.write(f'create table td.{table_name} (\n')
        col_strs = []
        for col in cols:
            col_str = '    '
            col_str += col["COLUMN_NAME"] + ' '
            col_str += col["DATA_TYPE"] + ' '
            if col['CHARACTER_MAXIMUM_LENGTH']:
                col_str += f'({col["CHARACTER_MAXIMUM_LENGTH"]}) '
            if col['SF_USER_LABEL']:
                col_str += f'comment \'{col["SF_USER_LABEL"]}\' '
            col_strs.append(col_str)
        f.write(', \n'.join(col_strs))
        f.write('\n);\n\n')
