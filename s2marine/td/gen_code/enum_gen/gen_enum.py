from pathlib import Path

import pyperclip
from jinja2 import Environment, FileSystemLoader

from s2marine.td.client.connection import Connection
from s2marine.td.client.intf.i_field_property import IFieldProperty
from s2marine.td.client.utils.value_enum import SystemFiled
from s2marine.td.default_conf import password, project, td_url, username

td_tools_root = '../../tools/'


def warp_quote(obj) -> str:
    if obj is None:
        return 'None'
    elif isinstance(obj, str):
        return f"'{obj}'"
    else:
        return str(obj)


def get_bug_fields(conn):
    fields = []
    bug_filter = conn.bug_factory().filter()
    for field in bug_filter.fields():
        if not field.property.impl.is__is_active():
            continue
        prop: 'IFieldProperty' = field.property.impl
        fields.append(
            SystemFiled(
                name=prop.get__db_column_name(),
                label=prop.get__user_label().replace(' ', ''),
                field_type=prop.get__db_column_type(),
                required=prop.is__is_required(),
                editable=prop.is__is_edit(),
                edit_style=prop.get__edit_style(),
                root_id=prop.get__root().get__node_id() if prop.get__root() else None,
                is_by_code=prop.is__is_by_code()
            )
        )
    return fields


def get_list(conn: 'Connection', node_id):
    list_values = []
    query_table = conn.command()
    sql = f'''
select L.AL_DESCRIPTION
from td.ALL_LISTS L
where l.AL_FATHER_ID = '{node_id}'
    '''
    query_table.sql = sql
    rowset = query_table.query()
    for row in rowset:
        list_values.append(row['AL_DESCRIPTION'])

    return list_values


def main():
    conn = Connection()
    conn.connect(username, password, project, td_url)

    fields = get_bug_fields(conn)
    statuses = get_list(conn, '4')
    types = get_list(conn, '91')

    for field in fields:
        if field.name == 'BG_SEVERITY':
            field.required = False

    env = Environment(loader=FileSystemLoader('.'))
    env.filters['wq'] = warp_quote
    tpl = env.get_template('fields_tpl.tpl')
    code = tpl.render(fields=fields, statuses=statuses, types=types)
    f = Path(td_tools_root).joinpath('utils', 'bug_field.py')
    print(f)
    f.touch(exist_ok=True)
    f.open('w', encoding='UTF-8').write(code)
    pyperclip.copy(code)

    conn.disconnect()


if __name__ == '__main__':
    main()
