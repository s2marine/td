from s2marine.td.client.utils.value_enum import ValueEnums, SystemFiled


class BugFields(ValueEnums):
{%- for field in fields %}
    {{field.label}} = SystemFiled({{field.name|wq}}, {{field.label|wq}}
    {%- if field.field_type != 'char' %}, field_type='{{field.field_type}}'{% endif %}
    {%- if field.edit_style %}, edit_style='{{field.edit_style}}'{% endif %}
    {{-'' if not field.required else ', required=True'}}
    {{-'' if field.editable else ', editable=False'}}
    {{-'' if not field.is_by_code else ', is_by_code=True'}}
    {%- if field.root_id %}, root_id={{field.root_id}}{% endif %})
{%- endfor %}


class BugStatus(ValueEnums):
{%- for status in statuses %}
    {{status}} = '{{status}}'
{%- endfor %}


class BugType(ValueEnums):
{%- for type in types %}
    {{type}} = '{{type}}'
{%- endfor %}

