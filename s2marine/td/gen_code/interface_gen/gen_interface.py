import os
import pickle
import shutil
from pathlib import Path
from typing import List, Tuple

from jinja2 import Environment, PackageLoader

from s2marine.td.gen_code.interface_gen import gen_utils
from s2marine.td.gen_code.interface_gen.custom_code import custom_code
from s2marine.td.gen_code.interface_gen.gen_utils import *

data_path = '../../../../data/'
cache_file = data_path + 'cache.db'
api_html_root = data_path + 'OTA_API_Reference/'

client_path = '../../client/'
intf_root = client_path + 'intf/'
direct_root = client_path + 'impl/direct/'
host_root = client_path + 'impl/process/host/'
remote_root = client_path + 'impl/process/remote/'


def main():
    all_class_metaes: 'Dict[str, ClassMeta]' = {}
    gen_package_dir(intf_root)
    gen_package_dir(direct_root)
    gen_package_dir(host_root)
    gen_package_dir(remote_root)

    if not os.path.isfile(cache_file):
        analyze_api('TDConnection', all_class_metaes)
        analyze_api('HistoryRecord', all_class_metaes)
        analyze_api('Attachment', all_class_metaes)

        for class_meta in all_class_metaes.values():
            for prop in class_meta.properties.values():
                if prop.type.name == 'T' and prop.name in all_class_metaes:
                    prop.type.name = prop.name

        pickle.dump(all_class_metaes, open(cache_file, 'wb'))
    else:
        all_class_metaes = pickle.load(open(cache_file, 'rb'))

    custom_code(all_class_metaes)

    gen_utils.ALL_API_NAMES += all_class_metaes.keys()
    for _, class_meta in all_class_metaes.items():
        generator_class(class_meta)


def gen_package_dir(path_str: 'str'):
    path = Path(path_str)
    shutil.rmtree(path_str, ignore_errors=True)
    path.mkdir(parents=True)

    while path.name != 'client':
        path.joinpath('__init__.py').touch()
        path = path.parent


def analyze_api(api_name: str, all_metaes: 'Dict[str, ClassMeta]'):
    api_file = api_html_root + 'TDAPIOLELib~' + api_name + '.html'
    soup = BeautifulSoup(open(api_file), 'html.parser')

    class_meta = ClassMeta()
    all_metaes[api_name] = class_meta

    title = soup.find('title').text
    if title.endswith(' Object'):
        class_meta.name = title[0: - len(' Object')]
    elif title.endswith(' Collection'):
        class_meta.name = title[0: - len(' Collection')]
    else:
        print(f'API analyze title failed: {title}')
        return

    class_meta.desc = node_next_text(soup.find('h4', string='Description'), 'p')

    for table in soup.find_all('table', {'class': 'FilteredItemListTable'}):
        if table.find_previous('h4').text == 'Public Methods':
            for a in table.find_all('a'):
                method_meta = analyze_method(a['href'], all_metaes)
                method_meta.owner = class_meta
                class_meta.methods[method_meta.name] = method_meta
        elif table.find_previous('h4').text == 'Public Properties':
            for a in table.find_all('a'):
                prop = analyze_property(a['href'], all_metaes)
                prop.owner = class_meta
                class_meta.properties[prop.name] = prop


def analyze_property(link: str, all_metaes: 'Dict[str, ClassMeta]') -> 'PropertyMeta':
    method_file = api_html_root + link
    soup = BeautifulSoup(open(method_file), 'html5lib')

    prop_meta = PropertyMeta()
    prop_meta.name = soup.find('title').text[0: - len(' Property')]
    prop_meta.desc = node_next_text(soup.find('h4', string='Description'), 'p')

    read_only_str = node_next_text(soup.find('h4', text='Property type'), 'p')
    if read_only_str == 'Read-only property':
        prop_meta.read_only = True
    elif read_only_str == 'Read-write property':
        prop_meta.read_only = False

    curr_node = ret_type_node = soup.find('h4', string='Get or Set Value Type')
    prop_meta.ret_desc = node_next_text(ret_type_node, 'p')

    if curr_node and curr_node.next_sibling.name == 'p':
        curr_node = curr_node.next_sibling
        for a in curr_node.find_all('a'):
            api_name = extract_api_name(a.text)
            if api_name:
                if api_name not in all_metaes:
                    analyze_api(api_name, all_metaes)
                prop_meta.type.name = api_name

    param_node = soup.find('h4', string=re.compile('Parameters'))
    if param_node:
        for dt in param_node.find_next('dl').find_all('dt'):
            param_meta = ParamMeta()
            param_meta.name = dt.text
            param_meta.desc = dt.find_next('dd').text

            if param_meta.name in prop_meta.params:
                prop_meta.params[param_meta.name].desc = param_meta.desc
            else:
                prop_meta.params[param_meta.name] = param_meta

    syntax_node = soup.find('h4', string=re.compile('Syntax'))
    if syntax_node:
        params, prop_type = analyze_syntax(syntax_node)
        prop_meta.type.name = prop_meta.type.name if prop_meta.type.name else prop_type.name
        prop_meta.type.is_list = prop_type.is_list

        for param_meta in params:
            if param_meta.name in prop_meta.params:
                prop_meta.params[param_meta.name].default = param_meta.default
                prop_meta.params[param_meta.name].type = param_meta.type
            else:
                prop_meta.params[param_meta.name] = param_meta

    prop_meta.type = try_extract_type(prop_meta.type, prop_meta.desc)
    for param in prop_meta.params.values():
        param.type = try_extract_type(param.type, param.desc)

    return prop_meta


def analyze_method(link: str, all_metaes: 'Dict[str, ClassMeta]') -> 'MethodMeta':
    method_file = api_html_root + link
    soup = BeautifulSoup(open(method_file), 'html5lib')

    method_meta = MethodMeta()
    method_meta.name = soup.find('title').text[0: - len(' Method')]
    method_meta.desc = node_next_text(soup.find('h4', string='Description'), 'p')
    remark = node_next_text(soup.find('h4', string='Remarks'), 'p')
    method_meta.desc += '\n' + remark

    curr_node = ret_type_node = soup.find('h4', string='Return Type')
    method_meta.ret_desc = node_next_text(ret_type_node, 'p')

    if curr_node and curr_node.next_sibling.name == 'p':
        curr_node = curr_node.next_sibling
        for a in curr_node.find_all('a'):
            api_name = extract_api_name(a.text)
            if api_name and api_name not in all_metaes:
                analyze_api(api_name, all_metaes)
                method_meta.ret_type.name = api_name

    param_node = soup.find('h4', string=re.compile('Parameters'))
    if param_node:
        for dt in param_node.find_next('dl').find_all('dt'):
            param_meta = ParamMeta()
            param_meta.name = dt.text
            param_meta.desc = dt.find_next('dd').text

            if param_meta.name in method_meta.params:
                method_meta.params[param_meta.name].desc = param_meta.desc
            else:
                method_meta.params[param_meta.name] = param_meta

    syntax_node = soup.find('h4', string=re.compile('Syntax'))
    if syntax_node:
        params, ret_type = analyze_syntax(syntax_node)
        method_meta.ret_type.name = method_meta.ret_type.name if method_meta.ret_type.name else ret_type.name
        method_meta.ret_type.is_list = ret_type.is_list

        for param_meta in params:
            if param_meta.name in method_meta.params:
                method_meta.params[param_meta.name].default = param_meta.default
                method_meta.params[param_meta.name].type = param_meta.type
            else:
                method_meta.params[param_meta.name] = param_meta

    method_meta.ret_type = try_extract_type(method_meta.ret_type, method_meta.desc)
    for param in method_meta.params.values():
        param.type = try_extract_type(param.type, param.desc)

    return method_meta


def analyze_syntax(syntax_node: 'BeautifulSoup') -> 'Tuple[List[ParamMeta], TypeMeta]':
    syntax_node.clear()
    syntax_text = syntax_node.parent.text.strip()

    params = []
    ret_type = TypeMeta()

    for line in syntax_text.split('\n'):
        m = re.search('(?P<field>\w+) As (?P<as_type>\w+)(?: = )?(?P<default_value>\w+)?', line)
        if not m:
            continue

        param_type = ParamMeta()
        param_type.name = m.group('field')
        param_type.default_value = m.group('default_value')
        param_type.type = analyze_type(m.group('as_type'))

        params.append(param_type)

    last_line = syntax_text.split('\n')[-1]
    m = re.search(' As (?P<as_type>\w+)', last_line)
    if m:
        ret_type = analyze_type(m.group('as_type'))

    return params, ret_type


def analyze_type(as_type: str) -> 'TypeMeta':
    type_meta = TypeMeta()
    if as_type == 'List':
        type_meta.is_list = True
    if as_type not in TYPE_MAP:
        print(f'unable to find return type: {as_type}')
    as_type = TYPE_MAP[as_type] if as_type in TYPE_MAP else as_type
    type_meta.name = as_type

    return type_meta


def generator_class(class_meta: 'ClassMeta'):
    env = Environment(loader=PackageLoader('s2marine.td.gen_code.interface_gen', package_path='tpl'))
    intf_tpl = env.get_template('intf_tpl.tpl')
    impl_tpl = env.get_template('impl_tpl.tpl')
    host_tpl = env.get_template('host_tpl.tpl')
    remote_tpl = env.get_template('remote_tpl.tpl')

    intf_code = intf_tpl.render(class_meta=class_meta)
    impl_code = impl_tpl.render(class_meta=class_meta)
    host_code = host_tpl.render(class_meta=class_meta)
    remote_code = remote_tpl.render(class_meta=class_meta)

    open(intf_root + class_meta.intf_file + '.py', 'w').write(intf_code)
    open(direct_root + class_meta.impl_file + '.py', 'w').write(impl_code)
    open(host_root + class_meta.host_file + '.py', 'w').write(host_code)
    open(remote_root + class_meta.remote_file + '.py', 'w').write(remote_code)


if __name__ == '__main__':
    main()
