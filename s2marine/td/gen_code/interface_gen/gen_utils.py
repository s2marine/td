import re
from dataclasses import dataclass, field
from json import JSONEncoder
from typing import Dict

from bs4 import BeautifulSoup

ALL_API_NAMES = []


class MyJSONEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def underscore(word: str):
    ret = ''
    for i in range(len(word)):
        if i == len(word) - 1:
            ret += word[i].lower()
        elif i < len(word) - 2 and word[i].isupper() and word[i + 1].isupper() and word[i + 2].islower():
            ret += word[i].lower() + '_'
        elif word[i].islower() and word[i + 1].isupper():
            ret += word[i].lower() + '_'
        else:
            ret += word[i].lower()
    return ret


@dataclass
class TypeMeta:
    name: str = None
    is_list: bool = False

    @property
    def intf_file(self):
        return 'i_' + underscore(self.name)

    @property
    def intf_name(self):
        return 'I' + self.name if self.is_td_type else self.name

    @property
    def type_name(self):
        return f'List[{self.intf_name}]' if self.is_list else self.intf_name

    @property
    def impl_file(self):
        return underscore(self.name) + '_impl'

    @property
    def impl_name(self):
        return self.name + 'Impl'

    @property
    def host_file(self):
        return 'host_' + underscore(self.name)

    @property
    def host_name(self):
        return 'Host' + self.name

    @property
    def remote_file(self):
        return 'remote_' + underscore(self.name)

    @property
    def remote_name(self):
        return 'Remote' + self.name

    @property
    def is_td_type(self):
        return self.name in ALL_API_NAMES


@dataclass
class ParamMeta:
    name: str = ''
    desc: str = ''
    default: str = ''
    type: 'TypeMeta' = field(default_factory=TypeMeta)

    @property
    def param_name(self):
        return underscore(self.name)

    @property
    def param_desc(self):
        return self.desc.strip()


@dataclass
class MethodMeta:
    owner: 'ClassMeta' = None
    name: str = ''
    desc: str = ''
    params: 'Dict[str, ParamMeta]' = field(default_factory=dict)
    ret_type: 'TypeMeta' = field(default_factory=TypeMeta)
    ret_desc: str = ''

    @property
    def method_name(self):
        return underscore(self.name)

    @property
    def method_params(self):
        params = []
        for param in self.params.values():
            params.append(f", {param.param_name}: '{param.type.type_name}'")
        return ''.join(params)

    @property
    def method_params_on_call(self):
        params = []
        for param in self.params.values():
            params.append(f"{param.param_name}")
        return ', '.join(params)

    @property
    def method_ret_type(self):
        if self.ret_type.name is None:
            return ''
        return f" -> '{self.ret_type.type_name}'" if self.ret_type.type_name else ''

    @property
    def method_desc(self):
        return self.desc.strip()

    @property
    def method_ret_desc(self):
        return self.ret_desc.strip()

    @property
    def impl_content(self):
        content = ''
        if self.ret_type.name and self.ret_type.is_td_type and self.ret_type.name != self.owner.name:
            content += f"from s2marine.td.client.impl.direct.{self.ret_type.impl_file} " \
                       f"import {self.ret_type.impl_name}\n        "
        if self.ret_type.name:
            content += 'return '
        if self.ret_type.is_list and self.ret_type.is_td_type:
            content += f"[{self.ret_type.impl_name}(com_obj) " \
                       f"for com_obj in self.com_obj.{self.name}({self.method_params_on_call})]"
        else:
            if self.ret_type.is_td_type:
                content += f"{self.ret_type.impl_name}("
            content += f"self.com_obj.{self.name}({self.method_params_on_call})"
            if self.ret_type.is_td_type:
                content += f") if self.com_obj.{self.name} else None"
        return content

    @property
    def remote_content(self):
        content = ''
        if self.ret_type.name and self.ret_type.is_td_type and self.ret_type.name != self.owner.name:
            content += f"from s2marine.td.client.impl.process.remote.{self.ret_type.remote_file} " \
                       f"import {self.ret_type.remote_name}\n        "
        if self.ret_type.name:
            content += 'return '
        if self.ret_type.is_list and self.ret_type.is_td_type:
            content += f"[{self.ret_type.remote_name}(com_obj) " \
                       f"for com_obj in self.com_obj.{self.name}({self.method_params_on_call})]"
        else:
            if self.ret_type.is_td_type:
                content += f"{self.ret_type.remote_name}("
            content += f"self.com_obj.{self.name}({self.method_params_on_call})"
            if self.ret_type.is_td_type:
                content += f")"
        return content

    @property
    def document(self):
        doc = '"""\n'
        doc += self.method_desc + '\n'
        if self.params:
            doc += '\n'
        for param in self.params.values():
            doc += f':param {param.param_name}: {param.param_desc}\n'
        if self.ret_type.name:
            doc += f':return: {self.method_ret_desc}\n'
        doc += '"""'
        return doc.replace('\n', '\n        ')


@dataclass
class PropertyMeta:
    owner: 'ClassMeta' = None
    name: str = ''
    desc: str = ''
    params: 'Dict[str, ParamMeta]' = field(default_factory=dict)
    type: 'TypeMeta' = field(default_factory=TypeMeta)
    read_only: bool = False

    @property
    def prop_name(self):
        return underscore(self.name)

    @property
    def get_method_name(self):
        return f'is__{self.prop_name}' if self.type.name == 'bool' else f'get__{self.prop_name}'

    @property
    def set_method_name(self):
        return f'set__{self.prop_name}'

    @property
    def get_method_params(self):
        params = []
        for param in self.params.values():
            params.append(f", {param.param_name}: '{param.type.type_name}'")
        return ''.join(params)

    @property
    def get_method_params_on_call(self):
        params = []
        for param in self.params.values():
            params.append(f"{param.param_name}")
        return ', '.join(params)

    @property
    def get_method_ret_type(self):
        return f" -> '{self.type.type_name}'"

    @property
    def set_method_params(self):
        params = []
        for param in self.params.values():
            params.append(f", {param.param_name}: '{param.type.type_name}'")
        params.append(f", value: '{self.type.type_name}'")
        return ''.join(params)

    @property
    def set_method_params_on_call(self):
        params = []
        for param in self.params.values():
            params.append(f"{param.param_name}")
        params.append('value')
        return ', '.join(params)

    @property
    def get_method_impl_content(self):
        content = ''
        if self.type.is_td_type and self.type.name != self.owner.name:
            content += f"from s2marine.td.client.impl.direct.{self.type.impl_file} " \
                       f"import {self.type.impl_name}\n        "
        content += 'return '
        if self.type.is_list and self.type.is_td_type:
            content += f"[{self.type.impl_name}(com_obj) for com_obj in self.com_obj.{self.name}"
            if self.params:
                content += f"[{self.get_method_params_on_call}]"
            content += "]"
        else:
            if self.type.is_td_type:
                content += f"{self.type.impl_name}("
            content += f"self.com_obj.{self.name}"
            if self.params:
                content += f"[{self.get_method_params_on_call}]"
            if self.type.is_td_type:
                content += f") if self.com_obj.{self.name} else None"
        return content

    @property
    def get_method_remote_content(self):
        content = ''
        if self.type.is_td_type and self.type.name != self.owner.name:
            content += f"from s2marine.td.client.impl.process.remote.{self.type.remote_file} " \
                       f"import {self.type.remote_name}\n        "
        content += 'return '
        if self.type.is_list and self.type.is_td_type:
            content += f"[{self.type.remote_name}(com_obj) for com_obj in self.com_obj.{self.name}"
            if self.params:
                content += f"[{self.get_method_params_on_call}]"
            content += "]"
        else:
            if self.type.is_td_type:
                content += f"{self.type.remote_name}("
            content += f"self.com_obj.{self.name}"
            if self.params:
                content += f"[{self.get_method_params_on_call}]"
            if self.type.is_td_type:
                content += f")"
        return content

    @property
    def set_method_impl_content(self):
        content = f"self.com_obj.{self.name}"
        if self.params:
            content += f"[{self.get_method_params_on_call}]"
        content += f" = value"
        return content

    @property
    def get_method_doc(self):
        doc = '"""\n'
        doc += self.desc + '\n'
        if self.params:
            doc += '\n'
        for param in self.params.values():
            doc += f":param {param.param_name}: '{param.desc}'\n"
        doc += '"""'
        return doc.replace('\n', '\n        ')

    @property
    def set_method_doc(self):
        doc = '"""\n'
        doc += self.desc + '\n'
        if self.params:
            doc += '\n'
        for param in self.params.values():
            doc += f":param {param.param_name}: '{param.desc}'\n"
        doc += f":param value: \n"
        doc += '"""'
        return doc.replace('\n', '\n        ')


@dataclass
class ClassMeta:
    name: str = ''
    desc: str = ''
    methods: 'Dict[str, MethodMeta]' = field(default_factory=dict)
    properties: 'Dict[str, PropertyMeta]' = field(default_factory=dict)

    @property
    def intf_file(self):
        return 'i_' + underscore(self.name)

    @property
    def intf_name(self):
        return 'I' + self.name

    @property
    def impl_file(self):
        return underscore(self.name) + '_impl'

    @property
    def impl_name(self):
        return self.name + 'Impl'

    @property
    def host_file(self):
        return 'host_' + underscore(self.name)

    @property
    def host_name(self):
        return 'Host' + self.name

    @property
    def remote_file(self):
        return 'remote_' + underscore(self.name)

    @property
    def remote_name(self):
        return 'Remote' + self.name

    @property
    def prop_list(self):
        return self.properties.values()

    @property
    def method_list(self):
        return self.methods.values()

    @property
    def document(self):
        doc = '"""\n'
        doc += self.desc + '\n'
        doc += '"""'
        return doc.replace('\n', '\n    ')


TYPE_MAP = {
    'Variant': 'Any',
    'String': 'str',
    'Boolean': 'bool',
    'Long': 'int',
    'ULong': 'int',
    'Integer': 'int',
    'Date': 'float',
    'List': 'T',
    'Object': 'T',
}


def node_next_text(start_node: 'BeautifulSoup', next_type='p') -> str:
    node_strs = []
    curr_node = start_node
    while curr_node and curr_node.next_sibling.name == next_type:
        curr_node = curr_node.next_sibling
        node_strs.append(curr_node.text.strip())

    return '\n'.join(node_strs)


def extract_api_name(text: str) -> str:
    text = text.replace('\n', ' ')
    if ' Object' in text:
        return text.split(' ')[0]


def try_extract_type(ori_type: 'TypeMeta', desc: str) -> 'TypeMeta':
    m = re.search('An? (\S*) Object.', desc)
    if m:
        return TypeMeta(name=m.group(1), is_list=False)
    m = re.search('A list of (\S*) Objects.', desc)
    if m:
        return TypeMeta(name=m.group(1), is_list=True)

    return ori_type
