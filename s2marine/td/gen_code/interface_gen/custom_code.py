import copy
from typing import Dict

from s2marine.td.gen_code.interface_gen.gen_utils import ClassMeta


def custom_code(all_metaes: 'Dict[str, ClassMeta]'):
    td_filter = all_metaes['TDFilter']
    td_filter.properties['Fields'].type.name = 'TDField'
    del all_metaes['TDFilter']

    all_metaes['BugFilter'] = copy.deepcopy(td_filter)
    all_metaes['BugFilter'].name = 'BugFilter'
    all_metaes['BugFilter'].methods['NewList'].ret_type.name = 'Bug'
    all_metaes['BugFactory'].properties['Filter'].type.name = 'BugFilter'

    all_metaes['HistoryFilter'] = copy.deepcopy(td_filter)
    all_metaes['HistoryFilter'].name = 'HistoryFilter'
    all_metaes['HistoryFilter'].methods['NewList'].ret_type.name = 'HistoryRecord'
    all_metaes['History'].properties['Filter'].type.name = 'HistoryFilter'
    all_metaes['History'].methods['NewList'].ret_type.name = 'HistoryRecord'

    all_metaes['AttachmentFilter'] = copy.deepcopy(td_filter)
    all_metaes['AttachmentFilter'].name = 'AttachmentFilter'
    all_metaes['AttachmentFilter'].methods['NewList'].ret_type.name = 'Attachment'
    all_metaes['AttachmentFactory'].properties['Filter'].type.name = 'AttachmentFilter'
    all_metaes['AttachmentFactory'].properties['AttachmentStorage'].type.name = 'ExtendedStorage'
    all_metaes['AttachmentFactory'].properties['Item'].type.name = 'Attachment'
    all_metaes['AttachmentFactory'].methods['AddItem'].ret_type.name = 'Attachment'
    all_metaes['AttachmentFactory'].methods['NewList'].ret_type.name = 'Attachment'

    all_metaes['Attachment'].properties['AttachmentStorage'].type.name = 'ExtendedStorage'
