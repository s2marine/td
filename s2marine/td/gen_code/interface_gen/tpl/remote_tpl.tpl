from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.{{ class_meta.impl_file }} import {{ class_meta.impl_name }}

if TYPE_CHECKING:
    pass
    {% for prop in class_meta.prop_list %}{% if prop.type.is_td_type %}
    from s2marine.td.client.intf.{{ prop.type.intf_file }} import {{ prop.type.intf_name }}{% endif %}{% endfor %}
    {% for method in class_meta.method_list %}{% if method.ret_type.is_td_type %}
    from s2marine.td.client.intf.{{ method.ret_type.intf_file }} import {{ method.ret_type.intf_name }}{% endif %}{% endfor %}

class {{ class_meta.remote_name }}({{ class_meta.impl_name }}, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        {{ class_meta.impl_name }}.__init__(self, com_obj)
    {% for prop in class_meta.prop_list %}{% if prop.type.is_td_type %}
    def {{ prop.get_method_name }}(self{{ prop.get_method_params }}){{ prop.get_method_ret_type }}:
        {{ prop.get_method_remote_content }}
    {% endif %}{% endfor %}
    {% for method in class_meta.method_list %}{% if method.ret_type.is_td_type %}
    def {{ method.method_name }}(self{{ method.method_params }}){{ method.method_ret_type }}:
        {{ method.remote_content }}

    {% endif %}{% endfor %}

