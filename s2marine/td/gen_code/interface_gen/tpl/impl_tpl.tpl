from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.{{ class_meta.intf_file }} import {{ class_meta.intf_name }}
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    {% for prop in class_meta.prop_list %}{% if prop.type.is_td_type %}
    from s2marine.td.client.intf.{{ prop.type.intf_file }} import {{ prop.type.intf_name }}{% endif %}{% endfor %}
    {% for method in class_meta.method_list %}{% if method.ret_type.is_td_type %}
    from s2marine.td.client.intf.{{ method.ret_type.intf_file }} import {{ method.ret_type.intf_name }}{% endif %}{% endfor %}


class {{ class_meta.impl_name }}({{ class_meta.intf_name }}):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    {% for prop in class_meta.prop_list %}
    @catch_com_error
    def {{ prop.get_method_name }}(self{{ prop.get_method_params }}){{ prop.get_method_ret_type }}:
        {{ prop.get_method_impl_content }}
    {% if not prop.read_only %}
    @catch_com_error
    def {{ prop.set_method_name }}(self{{ prop.set_method_params }}):
        {{ prop.set_method_impl_content }}
    {% endif %}{% endfor %}
    {% for method in class_meta.method_list -%}
    @catch_com_error
    def {{ method.method_name }}(self{{ method.method_params }}){{ method.method_ret_type }}:
        {{ method.impl_content }}

    {% endfor %}

