from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    {% for prop in class_meta.prop_list %}{% if prop.type.is_td_type %}
    from s2marine.td.client.intf.{{ prop.type.intf_file }} import {{ prop.type.intf_name }}{% endif %}{% endfor %}
    {% for method in class_meta.method_list %}{% if method.ret_type.is_td_type %}
    from s2marine.td.client.intf.{{ method.ret_type.intf_file }} import {{ method.ret_type.intf_name }}{% endif %}{% endfor %}


class {{ class_meta.intf_name }}(metaclass=ABCMeta):
    {{ class_meta.document }}
    {% for prop in class_meta.prop_list %}
    @abstractmethod
    def {{ prop.get_method_name }}(self{{ prop.get_method_params }}){{ prop.get_method_ret_type }}:
        {{ prop.get_method_doc }}
        pass
    {% if not prop.read_only %}
    @abstractmethod
    def {{ prop.set_method_name }}(self{{ prop.set_method_params }}):
        {{ prop.set_method_doc }}
        pass
    {% endif %}{% endfor %}
    {% for method in class_meta.method_list -%}
    @abstractmethod
    def {{ method.method_name }}(self{{ method.method_params }}){{ method.method_ret_type }}:
        {{ method.document }}
        pass

    {% endfor %}
