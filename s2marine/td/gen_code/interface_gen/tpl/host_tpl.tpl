from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.{{ class_meta.intf_file }} import {{ class_meta.intf_name }}

if TYPE_CHECKING:
    pass
    {% for prop in class_meta.prop_list %}{% if prop.type.is_td_type %}
    from s2marine.td.client.intf.{{ prop.type.intf_file }} import {{ prop.type.intf_name }}{% endif %}{% endfor %}
    {% for method in class_meta.method_list %}{% if method.ret_type.is_td_type %}
    from s2marine.td.client.intf.{{ method.ret_type.intf_file }} import {{ method.ret_type.intf_name }}{% endif %}{% endfor %}


class {{ class_meta.host_name }}({{ class_meta.intf_name }}):

    def __init__(self, pipe):
        self._pipe = pipe

    {% for prop in class_meta.prop_list %}
    @pipe_request({% if prop.type.is_td_type %}ret_pkg='{{ prop.type.host_file }}', ret_class='{{ prop.type.host_name }}'{% endif %})
    def {{ prop.get_method_name }}(self{{ prop.get_method_params }}){{ prop.get_method_ret_type }}:
        pass
    {% if not prop.read_only %}
    @pipe_request()
    def {{ prop.set_method_name }}(self{{ prop.set_method_params }}):
        pass
    {% endif %}{% endfor %}
    {% for method in class_meta.method_list -%}
    @pipe_request({% if method.ret_type.is_td_type %}ret_pkg='{{ method.ret_type.host_file }}', ret_class='{{ method.ret_type.host_name }}'{% endif %})
    def {{ method.method_name }}(self{{ method.method_params }}){{ method.method_ret_type }}:
        pass

    {% endfor %}


