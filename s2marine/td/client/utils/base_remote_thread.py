import _ctypes
import threading
from multiprocessing import Pipe

from s2marine.td.client.utils.process_utils import PipeRequest, PipeResponse


class BaseRemoteThread(threading.Thread):
    host_pipe: 'Pipe'
    remote_pipe: 'Pipe'

    def __init__(self):
        super().__init__()
        host_pipe, remote_pipe = Pipe()
        self.host_pipe = host_pipe
        self.remote_pipe = remote_pipe

    def run(self):
        while True:
            try:
                req: 'PipeRequest' = self.remote_pipe.recv()
            except BrokenPipeError:
                break
            except EOFError:
                break

            try:
                attr = getattr(self, req.action)
                if callable(attr):
                    result = attr(*req.args, **req.kwargs)
                else:
                    result = attr

                if isinstance(result, list):
                    result = [self.trans(i) for i in result]
                else:
                    result = self.trans(result)

                resp = PipeResponse(result=result)
                self.remote_pipe.send(resp)
            except _ctypes.COMError as e:
                resp = PipeResponse(status='error', code=e.args[0], message=e.args[2][0])
                self.remote_pipe.send(resp)

    @staticmethod
    def trans(result):
        if isinstance(result, BaseRemoteThread):
            result.start()
            return result.host_pipe
        else:
            return result
