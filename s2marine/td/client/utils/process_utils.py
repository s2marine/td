import importlib
from multiprocessing import Pipe

import comtypes
from comtypes.client import CreateObject

from s2marine.td.client.utils.exception import BusinessException
from s2marine.td.client.utils.td_constant import TD_OBJ


def create_process(start_pipe: 'Pipe'):
    from s2marine.td.client.impl.process.remote.remote_td_connection import RemoteTDConnection

    comtypes.CoUninitialize()
    comtypes.CoInitializeEx(comtypes.COINIT_MULTITHREADED)

    remote = RemoteTDConnection(CreateObject(TD_OBJ))
    start_pipe.send(remote.host_pipe)
    remote.start()


class PipeRequest:
    def __init__(self, action, args=None, kwargs=None):
        self.action = action
        self.args = args or []
        self.kwargs = kwargs or {}


class PipeResponse:
    def __init__(self, status='ok', code=None, message=None, result=None):
        self.status = status
        self.code = code
        self.message = message
        self.result = result


def pipe_request(ret_pkg=None, ret_class=None):
    def _inner(func):
        def __inner(*args):
            self = args[0]
            req = PipeRequest(func.__name__, args[1:])
            try:
                self._pipe.send(req)
                resp: PipeResponse = self._pipe.recv()
            except BrokenPipeError as e:
                raise Exception('远端发生错误') from e
            except EOFError as e:
                raise Exception('远端发生错误') from e

            if resp.status == 'error':
                raise BusinessException(code=resp.code, message=resp.message)

            if ret_pkg is None or ret_class is None:
                return resp.result

            _ret_pkg = importlib.import_module(f's2marine.td.client.impl.process.host.{ret_pkg}')
            _ret_class = getattr(_ret_pkg, ret_class)

            if isinstance(resp.result, list):
                return [_ret_class(i) for i in resp.result]
            else:
                return _ret_class(resp.result)

        return __inner

    return _inner
