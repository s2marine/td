import _ctypes


class BusinessException(Exception):
    code: int = None
    message: str = None

    def __init__(self, code: int = None, message: str = None, com_error: '_ctypes.COMError' = None):
        if com_error:
            self.code = com_error.args[0]
            self.message = com_error.args[2][0]
            super().__init__(self, com_error)
            self.with_traceback(com_error.__traceback__)
        else:
            self.code = code
            self.message = message
            super().__init__(self, message)

    def __repr__(self):
        return f'{self.code}, {self.message}'


def catch_com_error(func):
    def _inner(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except _ctypes.COMError as e:
            raise BusinessException(com_error=e) from None

    return _inner


ITEM_DOES_NOT_EXISTS = -2147220084
