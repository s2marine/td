from dataclasses import dataclass
from enum import Enum
from typing import Any, TypeVar, Optional


@dataclass
class SystemFiled:
    name: str
    label: str
    field_type: str = 'char'
    edit_style: 'Optional[str]' = None
    required: bool = False
    editable: bool = True
    root_id: 'Optional[int]' = None
    is_by_code: bool = False

    @property
    def val(self):
        return self.name


class ValueEnums(Enum):
    def __eq__(self, other):
        if isinstance(other, ValueEnums):
            return self.value == other.value
        else:
            return self.value == other

    def __hash__(self):
        return hash(self.value)

    @property
    def enum_val(self):
        if isinstance(self.value, str):
            return self.value
        else:
            return self.value.val

    @property
    def enum_obj(self):
        return self.value


FieldKey = TypeVar('FieldKey', ValueEnums, str)
FieldValue = TypeVar('FieldValue', ValueEnums, Any)
