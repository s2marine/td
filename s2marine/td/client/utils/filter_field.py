from s2marine.td.client.utils.value_enum import ValueEnums


class OrderDirection(ValueEnums):
    ase = 0
    desc = 1

