import logging

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('comtypes').setLevel(logging.ERROR)

td_logger = logging.getLogger('td.client')
