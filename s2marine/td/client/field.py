from s2marine.td.client.intf.i_field_property import IFieldProperty
from s2marine.td.client.intf.i_td_field import ITDField


class Field:
    def __init__(self, impl: 'ITDField'):
        self.impl = impl

    @property
    def name(self):
        return self.impl.get__name()

    @property
    def type(self):
        return self.impl.get__type()

    @property
    def property(self) -> 'FieldProperty':
        return FieldProperty(self.impl.get__property())


class FieldProperty:
    def __init__(self, impl: 'IFieldProperty'):
        self.impl = impl

    @property
    def column_name(self):
        return self.impl.get__db_column_name()

    @property
    def column_type(self):
        return self.impl.get__db_column_type()

    @property
    def table_name(self):
        return self.impl.get__db_table_name()

    @property
    def user_label(self):
        return self.impl.get__user_label()

    @property
    def edit_style(self) -> 'str':
        return self.impl.get__edit_style()

    @property
    def field_size(self) -> 'int':
        return self.impl.get__field_size()

    @property
    def is_active(self) -> 'bool':
        return self.impl.is__is_active()

    @property
    def is_by_code(self) -> 'bool':
        return self.impl.is__is_by_code()

    @property
    def can_filter(self) -> 'bool':
        return self.impl.is__is_can_filter()

    @property
    def is_customizable(self) -> 'bool':
        return self.impl.is__is_customizable()

    @property
    def is_editable(self) -> 'bool':
        return self.impl.is__is_edit()

    @property
    def is_history(self) -> 'bool':
        return self.impl.is__is_history()

    @property
    def is_keep_value(self) -> 'bool':
        return self.impl.is__is_keep_value()

    @property
    def is_key(self) -> 'bool':
        return self.impl.is__is_key()

    @property
    def is_mail(self) -> 'bool':
        return self.impl.is__is_mail()

    @property
    def is_modify(self) -> 'bool':
        return self.impl.is__is_modify()

    @property
    def is_multi_value(self) -> 'bool':
        return self.impl.is__is_multi_value()

    @property
    def required(self) -> 'bool':
        return self.impl.is__is_required()

    @property
    def is_searchable(self) -> 'bool':
        return self.impl.is__is_searchable()

    @property
    def is_system(self) -> 'bool':
        return self.impl.is__is_system()

    @property
    def requires_verify(self) -> 'bool':
        return self.impl.is__is_verify()

    @property
    def read_only(self) -> 'bool':
        return self.impl.is__read_only()

    @property
    def user_column_type(self) -> 'str':
        return self.impl.get__user_column_type()
