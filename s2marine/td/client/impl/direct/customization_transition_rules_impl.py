from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule


class CustomizationTransitionRulesImpl(ICustomizationTransitionRules):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__count(self) -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.direct.customization_transition_rule_impl import CustomizationTransitionRuleImpl
        return CustomizationTransitionRuleImpl(self.com_obj.Count) if self.com_obj.Count else None
    
    @catch_com_error
    def get__entity_name(self) -> 'str':
        return self.com_obj.EntityName
    
    @catch_com_error
    def get__field(self) -> 'ICustomizationField':
        from s2marine.td.client.impl.direct.customization_field_impl import CustomizationFieldImpl
        return CustomizationFieldImpl(self.com_obj.Field) if self.com_obj.Field else None
    
    @catch_com_error
    def get__group(self) -> 'ICustomizationUsersGroup':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return CustomizationUsersGroupImpl(self.com_obj.Group) if self.com_obj.Group else None
    
    @catch_com_error
    def get__transition_rule(self, position: 'int') -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.direct.customization_transition_rule_impl import CustomizationTransitionRuleImpl
        return CustomizationTransitionRuleImpl(self.com_obj.TransitionRule[position]) if self.com_obj.TransitionRule else None
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def add_transition_rule(self) -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.direct.customization_transition_rule_impl import CustomizationTransitionRuleImpl
        return CustomizationTransitionRuleImpl(self.com_obj.AddTransitionRule()) if self.com_obj.AddTransitionRule else None

    @catch_com_error
    def remove_transition_rule(self, rule: 'Any'):
        self.com_obj.RemoveTransitionRule(rule)

    
