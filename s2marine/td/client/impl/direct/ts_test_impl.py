from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_ts_test import ITSTest
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_test_set import ITestSet
    


class TSTestImpl(ITSTest):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__execution_params(self) -> 'str':
        return self.com_obj.ExecutionParams
    
    @catch_com_error
    def set__execution_params(self, value: 'str'):
        self.com_obj.ExecutionParams = value
    
    @catch_com_error
    def get__execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.direct.exec_settings_impl import ExecSettingsImpl
        return ExecSettingsImpl(self.com_obj.ExecutionSettings) if self.com_obj.ExecutionSettings else None
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def is__has_coverage(self) -> 'bool':
        return self.com_obj.HasCoverage
    
    @catch_com_error
    def is__has_steps(self) -> 'bool':
        return self.com_obj.HasSteps
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__host_name(self) -> 'str':
        return self.com_obj.HostName
    
    @catch_com_error
    def set__host_name(self, value: 'str'):
        self.com_obj.HostName = value
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def get__instance(self) -> 'int':
        return self.com_obj.Instance
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__last_run(self) -> 'IRun':
        from s2marine.td.client.impl.direct.run_impl import RunImpl
        return RunImpl(self.com_obj.LastRun) if self.com_obj.LastRun else None
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def get__params(self) -> 'IStepParams':
        from s2marine.td.client.impl.direct.step_params_impl import StepParamsImpl
        return StepParamsImpl(self.com_obj.Params) if self.com_obj.Params else None
    
    @catch_com_error
    def get__run_factory(self) -> 'IRunFactory':
        from s2marine.td.client.impl.direct.run_factory_impl import RunFactoryImpl
        return RunFactoryImpl(self.com_obj.RunFactory) if self.com_obj.RunFactory else None
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def set__status(self, value: 'str'):
        self.com_obj.Status = value
    
    @catch_com_error
    def get__test(self) -> 'ITest':
        from s2marine.td.client.impl.direct.test_impl import TestImpl
        return TestImpl(self.com_obj.Test) if self.com_obj.Test else None
    
    @catch_com_error
    def get__test_id(self) -> 'Any':
        return self.com_obj.TestId
    
    @catch_com_error
    def get__test_name(self) -> 'str':
        return self.com_obj.TestName
    
    @catch_com_error
    def get__test_set(self) -> 'ITestSet':
        from s2marine.td.client.impl.direct.test_set_impl import TestSetImpl
        return TestSetImpl(self.com_obj.TestSet) if self.com_obj.TestSet else None
    
    @catch_com_error
    def get__type(self) -> 'str':
        return self.com_obj.Type
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
