from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test_set import ITestSet
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_condition_factory import IConditionFactory
    from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    
    from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler


class TestSetImpl(ITestSet):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__condition_factory(self) -> 'IConditionFactory':
        from s2marine.td.client.impl.direct.condition_factory_impl import ConditionFactoryImpl
        return ConditionFactoryImpl(self.com_obj.ConditionFactory) if self.com_obj.ConditionFactory else None
    
    @catch_com_error
    def get__exec_event_notify_by_mail_settings(self) -> 'IExecEventNotifyByMailSettings':
        from s2marine.td.client.impl.direct.exec_event_notify_by_mail_settings_impl import ExecEventNotifyByMailSettingsImpl
        return ExecEventNotifyByMailSettingsImpl(self.com_obj.ExecEventNotifyByMailSettings) if self.com_obj.ExecEventNotifyByMailSettings else None
    
    @catch_com_error
    def get__execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.direct.exec_settings_impl import ExecSettingsImpl
        return ExecSettingsImpl(self.com_obj.ExecutionSettings) if self.com_obj.ExecutionSettings else None
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def set__status(self, value: 'str'):
        self.com_obj.Status = value
    
    @catch_com_error
    def get__test_default_execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.direct.exec_settings_impl import ExecSettingsImpl
        return ExecSettingsImpl(self.com_obj.TestDefaultExecutionSettings) if self.com_obj.TestDefaultExecutionSettings else None
    
    @catch_com_error
    def get__test_set_folder(self) -> 'ITestSetFolder':
        from s2marine.td.client.impl.direct.test_set_folder_impl import TestSetFolderImpl
        return TestSetFolderImpl(self.com_obj.TestSetFolder) if self.com_obj.TestSetFolder else None
    
    @catch_com_error
    def set__test_set_folder(self, value: 'ITestSetFolder'):
        self.com_obj.TestSetFolder = value
    
    @catch_com_error
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        from s2marine.td.client.impl.direct.ts_test_factory_impl import TSTestFactoryImpl
        return TSTestFactoryImpl(self.com_obj.TSTestFactory) if self.com_obj.TSTestFactory else None
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildPerfGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraph(group_by_field, sum_of_field, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraphEx(group_by_field, sum_of_field, by_history, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildSummaryGraph(x_axis_field, group_by_field, sum_of_field, max_cols, filter, force_refresh, show_full_path)

    @catch_com_error
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildTrendGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def check_test_instances(self, test_i_ds: 'str') -> 'str':
        return self.com_obj.CheckTestInstances(test_i_ds)

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def purge_executions(self):
        self.com_obj.PurgeExecutions()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def reset_test_set(self, delete_runs: 'bool'):
        self.com_obj.ResetTestSet(delete_runs)

    @catch_com_error
    def start_execution(self, server_name: 'str') -> 'ITSScheduler':
        from s2marine.td.client.impl.direct.ts_scheduler_impl import TSSchedulerImpl
        return TSSchedulerImpl(self.com_obj.StartExecution(server_name)) if self.com_obj.StartExecution else None

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
