from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_file_data import IFileData
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class FileDataImpl(IFileData):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def items(self) -> 'List[T]':
        return self.com_obj.Items()

    @catch_com_error
    def modify_date(self) -> 'float':
        return self.com_obj.ModifyDate()

    @catch_com_error
    def size(self) -> 'int':
        return self.com_obj.Size()

    @catch_com_error
    def type(self) -> 'int':
        return self.com_obj.Type()

    
