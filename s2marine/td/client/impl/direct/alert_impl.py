from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_alert import IAlert
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class AlertImpl(IAlert):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__alert_date(self) -> 'float':
        return self.com_obj.AlertDate
    
    @catch_com_error
    def get__alert_type(self) -> 'int':
        return self.com_obj.AlertType
    
    @catch_com_error
    def get__description(self) -> 'str':
        return self.com_obj.Description
    
    @catch_com_error
    def get__id(self) -> 'int':
        return self.com_obj.ID
    
    @catch_com_error
    def get__subject(self) -> 'str':
        return self.com_obj.Subject
    
    @catch_com_error
    def is__unread(self) -> 'bool':
        return self.com_obj.Unread
    
    @catch_com_error
    def set__unread(self, value: 'bool'):
        self.com_obj.Unread = value
    
    
