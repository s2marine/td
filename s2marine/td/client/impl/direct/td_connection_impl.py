from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_td_connection import ITDConnection
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_action_permission import IActionPermission
    from s2marine.td.client.intf.i_alert_manager import IAlertManager
    from s2marine.td.client.intf.i_audit_property_factory import IAuditPropertyFactory
    from s2marine.td.client.intf.i_audit_record_factory import IAuditRecordFactory
    from s2marine.td.client.intf.i_bug_factory import IBugFactory
    from s2marine.td.client.intf.i_command import ICommand
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_customization import ICustomization
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_graph_builder import IGraphBuilder
    from s2marine.td.client.intf.i_host_factory import IHostFactory
    from s2marine.td.client.intf.i_host_group_factory import IHostGroupFactory
    from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions
    from s2marine.td.client.intf.i_product_info import IProductInfo
    from s2marine.td.client.intf.i_project_properties import IProjectProperties
    from s2marine.td.client.intf.i_req_factory import IReqFactory
    from s2marine.td.client.intf.i_rule_manager import IRuleManager
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test_factory import ITestFactory
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager
    from s2marine.td.client.intf.i_text_parser import ITextParser
    from s2marine.td.client.intf.i_tree_manager import ITreeManager
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_vcs import IVCS
    


class TDConnectionImpl(ITDConnection):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__action_permission(self) -> 'IActionPermission':
        from s2marine.td.client.impl.direct.action_permission_impl import ActionPermissionImpl
        return ActionPermissionImpl(self.com_obj.ActionPermission) if self.com_obj.ActionPermission else None
    
    @catch_com_error
    def get__alert_manager(self) -> 'IAlertManager':
        from s2marine.td.client.impl.direct.alert_manager_impl import AlertManagerImpl
        return AlertManagerImpl(self.com_obj.AlertManager) if self.com_obj.AlertManager else None
    
    @catch_com_error
    def get__audit_property_factory(self) -> 'IAuditPropertyFactory':
        from s2marine.td.client.impl.direct.audit_property_factory_impl import AuditPropertyFactoryImpl
        return AuditPropertyFactoryImpl(self.com_obj.AuditPropertyFactory) if self.com_obj.AuditPropertyFactory else None
    
    @catch_com_error
    def get__audit_record_factory(self) -> 'IAuditRecordFactory':
        from s2marine.td.client.impl.direct.audit_record_factory_impl import AuditRecordFactoryImpl
        return AuditRecordFactoryImpl(self.com_obj.AuditRecordFactory) if self.com_obj.AuditRecordFactory else None
    
    @catch_com_error
    def get__bug_factory(self) -> 'IBugFactory':
        from s2marine.td.client.impl.direct.bug_factory_impl import BugFactoryImpl
        return BugFactoryImpl(self.com_obj.BugFactory) if self.com_obj.BugFactory else None
    
    @catch_com_error
    def get__checkout_repository(self) -> 'str':
        return self.com_obj.CheckoutRepository
    
    @catch_com_error
    def get__command(self) -> 'ICommand':
        from s2marine.td.client.impl.direct.command_impl import CommandImpl
        return CommandImpl(self.com_obj.Command) if self.com_obj.Command else None
    
    @catch_com_error
    def get__common_settings(self) -> 'ISettings':
        from s2marine.td.client.impl.direct.settings_impl import SettingsImpl
        return SettingsImpl(self.com_obj.CommonSettings) if self.com_obj.CommonSettings else None
    
    @catch_com_error
    def is__connected(self) -> 'bool':
        return self.com_obj.Connected
    
    @catch_com_error
    def get__customization(self) -> 'ICustomization':
        from s2marine.td.client.impl.direct.customization_impl import CustomizationImpl
        return CustomizationImpl(self.com_obj.Customization) if self.com_obj.Customization else None
    
    @catch_com_error
    def get__db_name(self) -> 'str':
        return self.com_obj.DBName
    
    @catch_com_error
    def get__db_type(self) -> 'str':
        return self.com_obj.DBType
    
    @catch_com_error
    def get__directory_path(self, n_type: 'int') -> 'str':
        return self.com_obj.DirectoryPath[n_type]
    
    @catch_com_error
    def get__domain_name(self) -> 'str':
        return self.com_obj.DomainName
    
    @catch_com_error
    def get__domains_list(self) -> 'List[T]':
        return self.com_obj.DomainsList
    
    @catch_com_error
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.ExtendedStorage) if self.com_obj.ExtendedStorage else None
    
    @catch_com_error
    def get__fields(self, data_type: 'str') -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields[data_type]]
    
    @catch_com_error
    def get__graph_builder(self) -> 'IGraphBuilder':
        from s2marine.td.client.impl.direct.graph_builder_impl import GraphBuilderImpl
        return GraphBuilderImpl(self.com_obj.GraphBuilder) if self.com_obj.GraphBuilder else None
    
    @catch_com_error
    def get__host_factory(self) -> 'IHostFactory':
        from s2marine.td.client.impl.direct.host_factory_impl import HostFactoryImpl
        return HostFactoryImpl(self.com_obj.HostFactory) if self.com_obj.HostFactory else None
    
    @catch_com_error
    def get__host_group_factory(self) -> 'IHostGroupFactory':
        from s2marine.td.client.impl.direct.host_group_factory_impl import HostGroupFactoryImpl
        return HostGroupFactoryImpl(self.com_obj.HostGroupFactory) if self.com_obj.HostGroupFactory else None
    
    @catch_com_error
    def is__ignore_html_format(self) -> 'bool':
        return self.com_obj.IgnoreHtmlFormat
    
    @catch_com_error
    def set__ignore_html_format(self, value: 'bool'):
        self.com_obj.IgnoreHtmlFormat = value
    
    @catch_com_error
    def is__is_search_supported(self) -> 'bool':
        return self.com_obj.IsSearchSupported
    
    @catch_com_error
    def is__logged_in(self) -> 'bool':
        return self.com_obj.LoggedIn
    
    @catch_com_error
    def get__mail_conditions(self) -> 'ITDMailConditions':
        from s2marine.td.client.impl.direct.td_mail_conditions_impl import TDMailConditionsImpl
        return TDMailConditionsImpl(self.com_obj.MailConditions) if self.com_obj.MailConditions else None
    
    @catch_com_error
    def get__product_info(self) -> 'IProductInfo':
        from s2marine.td.client.impl.direct.product_info_impl import ProductInfoImpl
        return ProductInfoImpl(self.com_obj.ProductInfo) if self.com_obj.ProductInfo else None
    
    @catch_com_error
    def is__project_connected(self) -> 'bool':
        return self.com_obj.ProjectConnected
    
    @catch_com_error
    def get__project_name(self) -> 'str':
        return self.com_obj.ProjectName
    
    @catch_com_error
    def get__project_properties(self) -> 'IProjectProperties':
        from s2marine.td.client.impl.direct.project_properties_impl import ProjectPropertiesImpl
        return ProjectPropertiesImpl(self.com_obj.ProjectProperties) if self.com_obj.ProjectProperties else None
    
    @catch_com_error
    def get__projects_list(self) -> 'List[T]':
        return self.com_obj.ProjectsList
    
    @catch_com_error
    def get__projects_list_ex(self, domain_name: 'str') -> 'List[T]':
        return self.com_obj.ProjectsListEx[domain_name]
    
    @catch_com_error
    def get__report_role(self) -> 'str':
        return self.com_obj.ReportRole
    
    @catch_com_error
    def get__req_factory(self) -> 'IReqFactory':
        from s2marine.td.client.impl.direct.req_factory_impl import ReqFactoryImpl
        return ReqFactoryImpl(self.com_obj.ReqFactory) if self.com_obj.ReqFactory else None
    
    @catch_com_error
    def get__rules(self) -> 'IRuleManager':
        from s2marine.td.client.impl.direct.rule_manager_impl import RuleManagerImpl
        return RuleManagerImpl(self.com_obj.Rules) if self.com_obj.Rules else None
    
    @catch_com_error
    def get__run_factory(self) -> 'IRunFactory':
        from s2marine.td.client.impl.direct.run_factory_impl import RunFactoryImpl
        return RunFactoryImpl(self.com_obj.RunFactory) if self.com_obj.RunFactory else None
    
    @catch_com_error
    def get__server_name(self) -> 'str':
        return self.com_obj.ServerName
    
    @catch_com_error
    def get__server_time(self) -> 'float':
        return self.com_obj.ServerTime
    
    @catch_com_error
    def get__server_url(self) -> 'str':
        return self.com_obj.ServerURL
    
    @catch_com_error
    def get__td_params(self, request: 'str') -> 'str':
        return self.com_obj.TDParams[request]
    
    @catch_com_error
    def get__test_factory(self) -> 'ITestFactory':
        from s2marine.td.client.impl.direct.test_factory_impl import TestFactoryImpl
        return TestFactoryImpl(self.com_obj.TestFactory) if self.com_obj.TestFactory else None
    
    @catch_com_error
    def get__test_repository(self) -> 'str':
        return self.com_obj.TestRepository
    
    @catch_com_error
    def get__test_set_factory(self) -> 'ITestSetFactory':
        from s2marine.td.client.impl.direct.test_set_factory_impl import TestSetFactoryImpl
        return TestSetFactoryImpl(self.com_obj.TestSetFactory) if self.com_obj.TestSetFactory else None
    
    @catch_com_error
    def get__test_set_tree_manager(self) -> 'ITestSetTreeManager':
        from s2marine.td.client.impl.direct.test_set_tree_manager_impl import TestSetTreeManagerImpl
        return TestSetTreeManagerImpl(self.com_obj.TestSetTreeManager) if self.com_obj.TestSetTreeManager else None
    
    @catch_com_error
    def get__text_param(self) -> 'ITextParser':
        from s2marine.td.client.impl.direct.text_parser_impl import TextParserImpl
        return TextParserImpl(self.com_obj.TextParam) if self.com_obj.TextParam else None
    
    @catch_com_error
    def get__tree_manager(self) -> 'ITreeManager':
        from s2marine.td.client.impl.direct.tree_manager_impl import TreeManagerImpl
        return TreeManagerImpl(self.com_obj.TreeManager) if self.com_obj.TreeManager else None
    
    @catch_com_error
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        from s2marine.td.client.impl.direct.ts_test_factory_impl import TSTestFactoryImpl
        return TSTestFactoryImpl(self.com_obj.TSTestFactory) if self.com_obj.TSTestFactory else None
    
    @catch_com_error
    def get__user_groups_list(self) -> 'List[T]':
        return self.com_obj.UserGroupsList
    
    @catch_com_error
    def get__user_name(self) -> 'str':
        return self.com_obj.UserName
    
    @catch_com_error
    def get__user_settings(self) -> 'ISettings':
        from s2marine.td.client.impl.direct.settings_impl import SettingsImpl
        return SettingsImpl(self.com_obj.UserSettings) if self.com_obj.UserSettings else None
    
    @catch_com_error
    def get__users_list(self) -> 'List[T]':
        return self.com_obj.UsersList
    
    @catch_com_error
    def is__using_progress(self) -> 'bool':
        return self.com_obj.UsingProgress
    
    @catch_com_error
    def set__using_progress(self, value: 'bool'):
        self.com_obj.UsingProgress = value
    
    @catch_com_error
    def get__vcs(self) -> 'IVCS':
        from s2marine.td.client.impl.direct.vcs_impl import VCSImpl
        return VCSImpl(self.com_obj.VCS) if self.com_obj.VCS else None
    
    @catch_com_error
    def get__vcs_db_repository(self) -> 'str':
        return self.com_obj.VcsDbRepository
    
    @catch_com_error
    def get__visible_domains(self) -> 'List[T]':
        return self.com_obj.VisibleDomains
    
    @catch_com_error
    def get__visible_projects(self, domain_name: 'str') -> 'List[T]':
        return self.com_obj.VisibleProjects[domain_name]
    
    @catch_com_error
    def change_password(self, old_password: 'str', new_password: 'str'):
        self.com_obj.ChangePassword(old_password, new_password)

    @catch_com_error
    def connect(self, domain_name: 'str', project_name: 'str'):
        self.com_obj.Connect(domain_name, project_name)

    @catch_com_error
    def connect_project(self, project_name: 'str', user_name: 'str', password: 'str'):
        self.com_obj.ConnectProject(project_name, user_name, password)

    @catch_com_error
    def connect_project_ex(self, domain_name: 'str', project_name: 'str', user_name: 'str', password: 'str'):
        self.com_obj.ConnectProjectEx(domain_name, project_name, user_name, password)

    @catch_com_error
    def disconnect(self):
        self.com_obj.Disconnect()

    @catch_com_error
    def disconnect_project(self):
        self.com_obj.DisconnectProject()

    @catch_com_error
    def get_licenses(self, licenses_type: 'int', p_val: 'str'):
        self.com_obj.GetLicenses(licenses_type, p_val)

    @catch_com_error
    def get_license_status(self, client_type: 'int', in_use: 'int', max: 'int'):
        self.com_obj.GetLicenseStatus(client_type, in_use, max)

    @catch_com_error
    def get_td_version(self, pbs_major_version: 'str', pbs_build_num: 'str'):
        self.com_obj.GetTDVersion(pbs_major_version, pbs_build_num)

    @catch_com_error
    def init_connection_ex(self, server_name: 'str'):
        self.com_obj.InitConnectionEx(server_name)

    @catch_com_error
    def login(self, user_name: 'str', password: 'str'):
        self.com_obj.Login(user_name, password)

    @catch_com_error
    def logout(self):
        self.com_obj.Logout()

    @catch_com_error
    def purge_runs(self, test_set_filter: 'str', keep_last: 'int', date_unit: 'Any', unit_count: 'int', steps_only: 'bool'):
        self.com_obj.PurgeRuns(test_set_filter, keep_last, date_unit, unit_count, steps_only)

    @catch_com_error
    def release_connection(self):
        self.com_obj.ReleaseConnection()

    @catch_com_error
    def send_mail(self, send_to: 'str', send_from: 'str', subject: 'str', message: 'str', attach_array: 'Any', bs_format: 'str'):
        self.com_obj.SendMail(send_to, send_from, subject, message, attach_array, bs_format)

    
