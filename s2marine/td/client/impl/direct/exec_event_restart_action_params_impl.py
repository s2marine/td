from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_exec_event_restart_action_params import IExecEventRestartActionParams
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ExecEventRestartActionParamsImpl(IExecEventRestartActionParams):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__cleanup_test(self) -> 'Any':
        return self.com_obj.CleanupTest
    
    @catch_com_error
    def set__cleanup_test(self, value: 'Any'):
        self.com_obj.CleanupTest = value
    
    @catch_com_error
    def get__number_of_retries(self) -> 'int':
        return self.com_obj.NumberOfRetries
    
    @catch_com_error
    def set__number_of_retries(self, value: 'int'):
        self.com_obj.NumberOfRetries = value
    
    @catch_com_error
    def get__on_exec_event_scheduler_action(self) -> 'int':
        return self.com_obj.OnExecEventSchedulerAction
    
    
