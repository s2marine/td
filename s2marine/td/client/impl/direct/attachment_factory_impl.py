from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_attachment_filter import IAttachmentFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_attachment import IAttachment
    
    from s2marine.td.client.intf.i_attachment import IAttachment
    from s2marine.td.client.intf.i_attachment import IAttachment


class AttachmentFactoryImpl(IAttachmentFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachment_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.AttachmentStorage) if self.com_obj.AttachmentStorage else None
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self) -> 'IAttachmentFilter':
        from s2marine.td.client.impl.direct.attachment_filter_impl import AttachmentFilterImpl
        return AttachmentFilterImpl(self.com_obj.Filter) if self.com_obj.Filter else None
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'IAttachment':
        from s2marine.td.client.impl.direct.attachment_impl import AttachmentImpl
        return AttachmentImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'IAttachment':
        from s2marine.td.client.impl.direct.attachment_impl import AttachmentImpl
        return AttachmentImpl(self.com_obj.AddItem(item_data)) if self.com_obj.AddItem else None

    @catch_com_error
    def factory_properties(self, owner_type: 'str', owner_key: 'Any'):
        self.com_obj.FactoryProperties(owner_type, owner_key)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[IAttachment]':
        from s2marine.td.client.impl.direct.attachment_impl import AttachmentImpl
        return [AttachmentImpl(com_obj) for com_obj in self.com_obj.NewList(filter)]

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    
