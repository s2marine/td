from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class TDMailConditionsImpl(ITDMailConditions):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__condition(self, name: 'str', is_user_condition: 'bool') -> 'str':
        return self.com_obj.Condition[name, is_user_condition]
    
    @catch_com_error
    def set__condition(self, name: 'str', is_user_condition: 'bool', value: 'str'):
        self.com_obj.Condition[name, is_user_condition] = value
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__item_list(self, get_real_names: 'bool') -> 'List[T]':
        return self.com_obj.ItemList[get_real_names]
    
    @catch_com_error
    def close(self, need_to_save: 'bool'):
        self.com_obj.Close(need_to_save)

    @catch_com_error
    def delete_condition(self, name: 'str', is_user_condition: 'bool'):
        self.com_obj.DeleteCondition(name, is_user_condition)

    @catch_com_error
    def load(self):
        self.com_obj.Load()

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
