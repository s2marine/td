from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_bug_filter import IBugFilter
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_bug import IBug


class BugFilterImpl(IBugFilter):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__case_sensitive(self, field_name: 'str') -> 'bool':
        return self.com_obj.CaseSensitive[field_name]
    
    @catch_com_error
    def set__case_sensitive(self, field_name: 'str', value: 'bool'):
        self.com_obj.CaseSensitive[field_name] = value
    
    @catch_com_error
    def get__data_type(self) -> 'str':
        return self.com_obj.DataType
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self, field_name: 'str') -> 'str':
        return self.com_obj.Filter[field_name]
    
    @catch_com_error
    def set__filter(self, field_name: 'str', value: 'str'):
        self.com_obj.Filter[field_name] = value
    
    @catch_com_error
    def get__order(self, field_name: 'str') -> 'int':
        return self.com_obj.Order[field_name]
    
    @catch_com_error
    def set__order(self, field_name: 'str', value: 'int'):
        self.com_obj.Order[field_name] = value
    
    @catch_com_error
    def get__order_direction(self, field_name: 'str') -> 'int':
        return self.com_obj.OrderDirection[field_name]
    
    @catch_com_error
    def set__order_direction(self, field_name: 'str', value: 'int'):
        self.com_obj.OrderDirection[field_name] = value
    
    @catch_com_error
    def get__text(self) -> 'str':
        return self.com_obj.Text
    
    @catch_com_error
    def set__text(self, value: 'str'):
        self.com_obj.Text = value
    
    @catch_com_error
    def get__x_filter(self, entity_type: 'str') -> 'str':
        return self.com_obj.XFilter[entity_type]
    
    @catch_com_error
    def set__x_filter(self, entity_type: 'str', value: 'str'):
        self.com_obj.XFilter[entity_type] = value
    
    @catch_com_error
    def clear(self):
        self.com_obj.Clear()

    @catch_com_error
    def get_x_filter(self, join_entities: 'str', inclusive: 'bool') -> 'str':
        return self.com_obj.GetXFilter(join_entities, inclusive)

    @catch_com_error
    def is_clear(self) -> 'bool':
        return self.com_obj.IsClear()

    @catch_com_error
    def new_list(self) -> 'List[IBug]':
        from s2marine.td.client.impl.direct.bug_impl import BugImpl
        return [BugImpl(com_obj) for com_obj in self.com_obj.NewList()]

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def set_x_filter(self, join_entities: 'str', inclusive: 'bool', filter_text: 'str'):
        self.com_obj.SetXFilter(join_entities, inclusive, filter_text)

    
