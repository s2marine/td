from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_version_item import IVersionItem
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class VersionItemImpl(IVersionItem):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__comments(self) -> 'str':
        return self.com_obj.Comments
    
    @catch_com_error
    def get__date(self) -> 'str':
        return self.com_obj.Date
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__time(self) -> 'str':
        return self.com_obj.Time
    
    @catch_com_error
    def get__user(self) -> 'str':
        return self.com_obj.User
    
    @catch_com_error
    def get__version(self) -> 'str':
        return self.com_obj.Version
    
    
