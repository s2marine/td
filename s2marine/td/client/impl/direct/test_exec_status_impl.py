from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class TestExecStatusImpl(ITestExecStatus):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__message(self) -> 'str':
        return self.com_obj.Message
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def get__test_id(self) -> 'int':
        return self.com_obj.TestId
    
    @catch_com_error
    def get__test_instance(self) -> 'int':
        return self.com_obj.TestInstance
    
    @catch_com_error
    def get__ts_test_id(self) -> 'Any':
        return self.com_obj.TSTestId
    
    
