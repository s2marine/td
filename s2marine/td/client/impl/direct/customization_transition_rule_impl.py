from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class CustomizationTransitionRuleImpl(ICustomizationTransitionRule):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__destination_value(self) -> 'str':
        return self.com_obj.DestinationValue
    
    @catch_com_error
    def set__destination_value(self, value: 'str'):
        self.com_obj.DestinationValue = value
    
    @catch_com_error
    def get__source_value(self) -> 'str':
        return self.com_obj.SourceValue
    
    @catch_com_error
    def set__source_value(self, value: 'str'):
        self.com_obj.SourceValue = value
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    
