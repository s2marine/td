from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_graph_definition import IGraphDefinition
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class GraphDefinitionImpl(IGraphDefinition):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__filter(self) -> 'TDFilter':
        return self.com_obj.Filter
    
    @catch_com_error
    def set__filter(self, value: 'TDFilter'):
        self.com_obj.Filter = value
    
    @catch_com_error
    def get__module(self) -> 'int':
        return self.com_obj.Module
    
    @catch_com_error
    def get__property(self, prop: 'int') -> 'Any':
        return self.com_obj.Property[prop]
    
    @catch_com_error
    def set__property(self, prop: 'int', value: 'Any'):
        self.com_obj.Property[prop] = value
    
    @catch_com_error
    def get__type(self) -> 'int':
        return self.com_obj.Type
    
    
