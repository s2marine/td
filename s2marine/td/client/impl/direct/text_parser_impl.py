from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_text_parser import ITextParser
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class TextParserImpl(ITextParser):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def is__param_exist(self, param_name: 'str') -> 'bool':
        return self.com_obj.ParamExist[param_name]
    
    @catch_com_error
    def get__param_name(self, n_position: 'int') -> 'str':
        return self.com_obj.ParamName[n_position]
    
    @catch_com_error
    def get__param_type(self, v_param: 'Any') -> 'str':
        return self.com_obj.ParamType[v_param]
    
    @catch_com_error
    def get__param_value(self, v_param: 'Any') -> 'str':
        return self.com_obj.ParamValue[v_param]
    
    @catch_com_error
    def set__param_value(self, v_param: 'Any', value: 'str'):
        self.com_obj.ParamValue[v_param] = value
    
    @catch_com_error
    def get__text(self) -> 'str':
        return self.com_obj.Text
    
    @catch_com_error
    def set__text(self, value: 'str'):
        self.com_obj.Text = value
    
    @catch_com_error
    def get__type(self, v_param: 'Any') -> 'int':
        return self.com_obj.Type[v_param]
    
    @catch_com_error
    def clear_param(self, v_param: 'Any'):
        self.com_obj.ClearParam(v_param)

    @catch_com_error
    def evaluate_text(self):
        self.com_obj.EvaluateText()

    @catch_com_error
    def initialize(self, start_close: 'str', end_close: 'str', type_close: 'str', max_len: 'int', default_type: 'str'):
        self.com_obj.Initialize(start_close, end_close, type_close, max_len, default_type)

    
