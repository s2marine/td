from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode


class TestSetFolderImpl(ITestSetFolder):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def get__attribute(self) -> 'int':
        return self.com_obj.Attribute
    
    @catch_com_error
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.Child[index]) if self.com_obj.Child else None
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def get__depth_type(self) -> 'int':
        return self.com_obj.DepthType
    
    @catch_com_error
    def get__description(self) -> 'str':
        return self.com_obj.Description
    
    @catch_com_error
    def set__description(self, value: 'str'):
        self.com_obj.Description = value
    
    @catch_com_error
    def get__father(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.Father) if self.com_obj.Father else None
    
    @catch_com_error
    def get__father_disp(self) -> 'T':
        return self.com_obj.FatherDisp
    
    @catch_com_error
    def get__father_id(self) -> 'int':
        return self.com_obj.FatherID
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__node_id(self) -> 'int':
        return self.com_obj.NodeID
    
    @catch_com_error
    def get__path(self) -> 'str':
        return self.com_obj.Path
    
    @catch_com_error
    def get__sub_nodes(self) -> 'List[T]':
        return self.com_obj.SubNodes
    
    @catch_com_error
    def get__test_set_factory(self) -> 'ITestSetFactory':
        from s2marine.td.client.impl.direct.test_set_factory_impl import TestSetFactoryImpl
        return TestSetFactoryImpl(self.com_obj.TestSetFactory) if self.com_obj.TestSetFactory else None
    
    @catch_com_error
    def get__view_order(self) -> 'int':
        return self.com_obj.ViewOrder
    
    @catch_com_error
    def set__view_order(self, value: 'int'):
        self.com_obj.ViewOrder = value
    
    @catch_com_error
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.AddNode(node_name)) if self.com_obj.AddNode else None

    @catch_com_error
    def add_node_disp(self, node_name: 'str') -> 'T':
        return self.com_obj.AddNodeDisp(node_name)

    @catch_com_error
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.FindChildNode(child_name)) if self.com_obj.FindChildNode else None

    @catch_com_error
    def find_children(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        return self.com_obj.FindChildren(pattern, match_case, filter)

    @catch_com_error
    def find_test_instances(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        return self.com_obj.FindTestInstances(pattern, match_case, filter)

    @catch_com_error
    def find_test_sets(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        return self.com_obj.FindTestSets(pattern, match_case, filter)

    @catch_com_error
    def move(self, father: 'Any'):
        self.com_obj.Move(father)

    @catch_com_error
    def new_list(self) -> 'List[T]':
        return self.com_obj.NewList()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def remove_node(self, node: 'Any'):
        self.com_obj.RemoveNode(node)

    @catch_com_error
    def remove_node_ex(self, node: 'Any', delete_test_sets: 'bool'):
        self.com_obj.RemoveNodeEx(node, delete_test_sets)

    
