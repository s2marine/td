from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_list import ICustomizationList
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode


class CustomizationListImpl(ICustomizationList):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__deleted(self) -> 'bool':
        return self.com_obj.Deleted
    
    @catch_com_error
    def set__deleted(self, value: 'bool'):
        self.com_obj.Deleted = value
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def get__root_node(self) -> 'ICustomizationListNode':
        from s2marine.td.client.impl.direct.customization_list_node_impl import CustomizationListNodeImpl
        return CustomizationListNodeImpl(self.com_obj.RootNode) if self.com_obj.RootNode else None
    
    @catch_com_error
    def find(self, val: 'Any') -> 'ICustomizationListNode':
        from s2marine.td.client.impl.direct.customization_list_node_impl import CustomizationListNodeImpl
        return CustomizationListNodeImpl(self.com_obj.Find(val)) if self.com_obj.Find else None

    
