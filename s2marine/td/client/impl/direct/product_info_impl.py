from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_product_info import IProductInfo
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ProductInfoImpl(IProductInfo):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__bpt_version(self) -> 'int':
        return self.com_obj.BPTVersion
    
    @catch_com_error
    def get__qc_version(self, pn_major_version: 'int', pn_minor_version: 'int') -> 'int':
        return self.com_obj.QCVersion[pn_major_version, pn_minor_version]
    
    
