from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_field_property import IFieldProperty
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class FieldPropertyImpl(IFieldProperty):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__db_column_name(self) -> 'str':
        return self.com_obj.DBColumnName
    
    @catch_com_error
    def get__db_column_type(self) -> 'str':
        return self.com_obj.DBColumnType
    
    @catch_com_error
    def get__db_table_name(self) -> 'str':
        return self.com_obj.DBTableName
    
    @catch_com_error
    def get__edit_mask(self) -> 'str':
        return self.com_obj.EditMask
    
    @catch_com_error
    def get__edit_style(self) -> 'str':
        return self.com_obj.EditStyle
    
    @catch_com_error
    def get__field_size(self) -> 'int':
        return self.com_obj.FieldSize
    
    @catch_com_error
    def is__is_active(self) -> 'bool':
        return self.com_obj.IsActive
    
    @catch_com_error
    def is__is_by_code(self) -> 'bool':
        return self.com_obj.IsByCode
    
    @catch_com_error
    def is__is_can_filter(self) -> 'bool':
        return self.com_obj.IsCanFilter
    
    @catch_com_error
    def is__is_can_group(self) -> 'bool':
        return self.com_obj.IsCanGroup
    
    @catch_com_error
    def is__is_customizable(self) -> 'bool':
        return self.com_obj.IsCustomizable
    
    @catch_com_error
    def is__is_edit(self) -> 'bool':
        return self.com_obj.IsEdit
    
    @catch_com_error
    def is__is_history(self) -> 'bool':
        return self.com_obj.IsHistory
    
    @catch_com_error
    def is__is_keep_value(self) -> 'bool':
        return self.com_obj.IsKeepValue
    
    @catch_com_error
    def is__is_key(self) -> 'bool':
        return self.com_obj.IsKey
    
    @catch_com_error
    def is__is_mail(self) -> 'bool':
        return self.com_obj.IsMail
    
    @catch_com_error
    def is__is_modify(self) -> 'bool':
        return self.com_obj.IsModify
    
    @catch_com_error
    def is__is_multi_value(self) -> 'bool':
        return self.com_obj.IsMultiValue
    
    @catch_com_error
    def is__is_required(self) -> 'bool':
        return self.com_obj.IsRequired
    
    @catch_com_error
    def is__is_searchable(self) -> 'bool':
        return self.com_obj.IsSearchable
    
    @catch_com_error
    def is__is_system(self) -> 'bool':
        return self.com_obj.IsSystem
    
    @catch_com_error
    def is__is_to_sum(self) -> 'bool':
        return self.com_obj.IsToSum
    
    @catch_com_error
    def is__is_verify(self) -> 'bool':
        return self.com_obj.IsVerify
    
    @catch_com_error
    def is__is_version_control(self) -> 'bool':
        return self.com_obj.IsVersionControl
    
    @catch_com_error
    def is__is_visible_in_new_bug(self) -> 'bool':
        return self.com_obj.IsVisibleInNewBug
    
    @catch_com_error
    def is__read_only(self) -> 'bool':
        return self.com_obj.ReadOnly
    
    @catch_com_error
    def get__root(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.Root) if self.com_obj.Root else None
    
    @catch_com_error
    def get__user_column_type(self) -> 'str':
        return self.com_obj.UserColumnType
    
    @catch_com_error
    def get__user_label(self) -> 'str':
        return self.com_obj.UserLabel
    
    
