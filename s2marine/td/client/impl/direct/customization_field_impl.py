from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_field import ICustomizationField
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class CustomizationFieldImpl(ICustomizationField):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__authorized_modify_for_groups(self) -> 'List[ICustomizationUsersGroup]':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return [CustomizationUsersGroupImpl(com_obj) for com_obj in self.com_obj.AuthorizedModifyForGroups]
    
    @catch_com_error
    def get__authorized_owner_sensible_for_groups(self) -> 'List[T]':
        return self.com_obj.AuthorizedOwnerSensibleForGroups
    
    @catch_com_error
    def get__authorized_visible_for_groups(self) -> 'List[T]':
        return self.com_obj.AuthorizedVisibleForGroups
    
    @catch_com_error
    def get__authorized_visible_in_new_bug_for_groups(self) -> 'List[T]':
        return self.com_obj.AuthorizedVisibleInNewBugForGroups
    
    @catch_com_error
    def is__can_make_multi_value(self) -> 'bool':
        return self.com_obj.CanMakeMultiValue
    
    @catch_com_error
    def is__can_make_searchable(self) -> 'bool':
        return self.com_obj.CanMakeSearchable
    
    @catch_com_error
    def get__column_name(self) -> 'str':
        return self.com_obj.ColumnName
    
    @catch_com_error
    def get__column_type(self) -> 'str':
        return self.com_obj.ColumnType
    
    @catch_com_error
    def set__column_type(self, value: 'str'):
        self.com_obj.ColumnType = value
    
    @catch_com_error
    def get__default_value(self) -> 'str':
        return self.com_obj.DefaultValue
    
    @catch_com_error
    def set__default_value(self, value: 'str'):
        self.com_obj.DefaultValue = value
    
    @catch_com_error
    def get__edit_mask(self) -> 'str':
        return self.com_obj.EditMask
    
    @catch_com_error
    def set__edit_mask(self, value: 'str'):
        self.com_obj.EditMask = value
    
    @catch_com_error
    def get__field_size(self) -> 'int':
        return self.com_obj.FieldSize
    
    @catch_com_error
    def set__field_size(self, value: 'int'):
        self.com_obj.FieldSize = value
    
    @catch_com_error
    def is__grant_modify_for_group(self, group: 'Any') -> 'bool':
        return self.com_obj.GrantModifyForGroup[group]
    
    @catch_com_error
    def set__grant_modify_for_group(self, group: 'Any', value: 'bool'):
        self.com_obj.GrantModifyForGroup[group] = value
    
    @catch_com_error
    def get__grant_modify_mask(self) -> 'int':
        return self.com_obj.GrantModifyMask
    
    @catch_com_error
    def set__grant_modify_mask(self, value: 'int'):
        self.com_obj.GrantModifyMask = value
    
    @catch_com_error
    def is__is_active(self) -> 'bool':
        return self.com_obj.IsActive
    
    @catch_com_error
    def set__is_active(self, value: 'bool'):
        self.com_obj.IsActive = value
    
    @catch_com_error
    def is__is_by_code(self) -> 'bool':
        return self.com_obj.IsByCode
    
    @catch_com_error
    def set__is_by_code(self, value: 'bool'):
        self.com_obj.IsByCode = value
    
    @catch_com_error
    def is__is_can_group(self) -> 'bool':
        return self.com_obj.IsCanGroup
    
    @catch_com_error
    def set__is_can_group(self, value: 'bool'):
        self.com_obj.IsCanGroup = value
    
    @catch_com_error
    def is__is_customizable(self) -> 'bool':
        return self.com_obj.IsCustomizable
    
    @catch_com_error
    def set__is_customizable(self, value: 'bool'):
        self.com_obj.IsCustomizable = value
    
    @catch_com_error
    def is__is_edit(self) -> 'bool':
        return self.com_obj.IsEdit
    
    @catch_com_error
    def set__is_edit(self, value: 'bool'):
        self.com_obj.IsEdit = value
    
    @catch_com_error
    def is__is_history(self) -> 'bool':
        return self.com_obj.IsHistory
    
    @catch_com_error
    def set__is_history(self, value: 'bool'):
        self.com_obj.IsHistory = value
    
    @catch_com_error
    def is__is_mail(self) -> 'bool':
        return self.com_obj.IsMail
    
    @catch_com_error
    def set__is_mail(self, value: 'bool'):
        self.com_obj.IsMail = value
    
    @catch_com_error
    def is__is_multi_value(self) -> 'bool':
        return self.com_obj.IsMultiValue
    
    @catch_com_error
    def set__is_multi_value(self, value: 'bool'):
        self.com_obj.IsMultiValue = value
    
    @catch_com_error
    def is__is_required(self) -> 'bool':
        return self.com_obj.IsRequired
    
    @catch_com_error
    def set__is_required(self, value: 'bool'):
        self.com_obj.IsRequired = value
    
    @catch_com_error
    def is__is_searchable(self) -> 'bool':
        return self.com_obj.IsSearchable
    
    @catch_com_error
    def set__is_searchable(self, value: 'bool'):
        self.com_obj.IsSearchable = value
    
    @catch_com_error
    def is__is_transition_logic(self) -> 'bool':
        return self.com_obj.IsTransitionLogic
    
    @catch_com_error
    def set__is_transition_logic(self, value: 'bool'):
        self.com_obj.IsTransitionLogic = value
    
    @catch_com_error
    def is__is_verify(self) -> 'bool':
        return self.com_obj.IsVerify
    
    @catch_com_error
    def set__is_verify(self, value: 'bool'):
        self.com_obj.IsVerify = value
    
    @catch_com_error
    def is__is_virtual(self) -> 'bool':
        return self.com_obj.IsVirtual
    
    @catch_com_error
    def get__is_visible_in_new_bug(self) -> 'int':
        return self.com_obj.IsVisibleInNewBug
    
    @catch_com_error
    def set__is_visible_in_new_bug(self, value: 'int'):
        self.com_obj.IsVisibleInNewBug = value
    
    @catch_com_error
    def get__list(self) -> 'ICustomizationList':
        from s2marine.td.client.impl.direct.customization_list_impl import CustomizationListImpl
        return CustomizationListImpl(self.com_obj.List) if self.com_obj.List else None
    
    @catch_com_error
    def set__list(self, value: 'ICustomizationList'):
        self.com_obj.List = value
    
    @catch_com_error
    def is__new_created(self) -> 'bool':
        return self.com_obj.NewCreated
    
    @catch_com_error
    def set__new_created(self, value: 'bool'):
        self.com_obj.NewCreated = value
    
    @catch_com_error
    def is__owner_sensible_for_group(self, group: 'Any') -> 'bool':
        return self.com_obj.OwnerSensibleForGroup[group]
    
    @catch_com_error
    def set__owner_sensible_for_group(self, group: 'Any', value: 'bool'):
        self.com_obj.OwnerSensibleForGroup[group] = value
    
    @catch_com_error
    def get__owner_sensible_mask(self) -> 'int':
        return self.com_obj.OwnerSensibleMask
    
    @catch_com_error
    def set__owner_sensible_mask(self, value: 'int'):
        self.com_obj.OwnerSensibleMask = value
    
    @catch_com_error
    def get__root_id(self) -> 'Any':
        return self.com_obj.RootId
    
    @catch_com_error
    def set__root_id(self, value: 'Any'):
        self.com_obj.RootId = value
    
    @catch_com_error
    def get__table_name(self) -> 'str':
        return self.com_obj.TableName
    
    @catch_com_error
    def get__type(self) -> 'int':
        return self.com_obj.Type
    
    @catch_com_error
    def set__type(self, value: 'int'):
        self.com_obj.Type = value
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def get__user_column_type(self) -> 'str':
        return self.com_obj.UserColumnType
    
    @catch_com_error
    def set__user_column_type(self, value: 'str'):
        self.com_obj.UserColumnType = value
    
    @catch_com_error
    def get__user_label(self) -> 'str':
        return self.com_obj.UserLabel
    
    @catch_com_error
    def set__user_label(self, value: 'str'):
        self.com_obj.UserLabel = value
    
    @catch_com_error
    def is__version_control(self) -> 'bool':
        return self.com_obj.VersionControl
    
    @catch_com_error
    def set__version_control(self, value: 'bool'):
        self.com_obj.VersionControl = value
    
    @catch_com_error
    def is__visible_for_group(self, group: 'Any') -> 'bool':
        return self.com_obj.VisibleForGroup[group]
    
    @catch_com_error
    def set__visible_for_group(self, group: 'Any', value: 'bool'):
        self.com_obj.VisibleForGroup[group] = value
    
    @catch_com_error
    def get__visible_for_groups(self) -> 'int':
        return self.com_obj.VisibleForGroups
    
    @catch_com_error
    def set__visible_for_groups(self, value: 'int'):
        self.com_obj.VisibleForGroups = value
    
    @catch_com_error
    def is__visible_in_new_bug_for_group(self, group: 'Any') -> 'bool':
        return self.com_obj.VisibleInNewBugForGroup[group]
    
    @catch_com_error
    def set__visible_in_new_bug_for_group(self, group: 'Any', value: 'bool'):
        self.com_obj.VisibleInNewBugForGroup[group] = value
    
    
