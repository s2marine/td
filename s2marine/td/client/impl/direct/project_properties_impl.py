from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_project_properties import IProjectProperties
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ProjectPropertiesImpl(IProjectProperties):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def is__is_param(self, param_name: 'str') -> 'bool':
        return self.com_obj.IsParam[param_name]
    
    @catch_com_error
    def get__param_name(self, param_index: 'int') -> 'str':
        return self.com_obj.ParamName[param_index]
    
    @catch_com_error
    def get__param_value(self, v_param: 'Any') -> 'str':
        return self.com_obj.ParamValue[v_param]
    
    
