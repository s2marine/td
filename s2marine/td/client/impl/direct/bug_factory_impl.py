from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_bug_factory import IBugFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_bug_filter import IBugFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_bug import IBug
    
    from s2marine.td.client.intf.i_bug import IBug
    from s2marine.td.client.intf.i_graph import IGraph


class BugFactoryImpl(IBugFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self) -> 'IBugFilter':
        from s2marine.td.client.impl.direct.bug_filter_impl import BugFilterImpl
        return BugFilterImpl(self.com_obj.Filter) if self.com_obj.Filter else None
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'IBug':
        from s2marine.td.client.impl.direct.bug_impl import BugImpl
        return BugImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'IBug':
        from s2marine.td.client.impl.direct.bug_impl import BugImpl
        return BugImpl(self.com_obj.AddItem(item_data)) if self.com_obj.AddItem else None

    @catch_com_error
    def build_age_graph(self, group_by_field: 'str', sum_of_field: 'str', max_age: 'int', max_cols: 'int', filter: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'IGraph':
        from s2marine.td.client.impl.direct.graph_impl import GraphImpl
        return GraphImpl(self.com_obj.BuildAgeGraph(group_by_field, sum_of_field, max_age, max_cols, filter, force_refresh, show_full_path)) if self.com_obj.BuildAgeGraph else None

    @catch_com_error
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraph(group_by_field, sum_of_field, by_history, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildSummaryGraph(x_axis_field, group_by_field, sum_of_field, max_cols, filter, force_refresh, show_full_path)

    @catch_com_error
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildTrendGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def find_similar_bugs(self, pattern: 'str', similarity_ratio: 'int') -> 'List[T]':
        return self.com_obj.FindSimilarBugs(pattern, similarity_ratio)

    @catch_com_error
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(items, send_to, send_cc, option, subject, comment)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[T]':
        return self.com_obj.NewList(filter)

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    
