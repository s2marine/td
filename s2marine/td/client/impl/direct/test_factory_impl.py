from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test_factory import ITestFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    


class TestFactoryImpl(ITestFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self) -> 'TDFilter':
        return self.com_obj.Filter
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'ITest':
        from s2marine.td.client.impl.direct.test_impl import TestImpl
        return TestImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def get__repository_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.RepositoryStorage) if self.com_obj.RepositoryStorage else None
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'T':
        return self.com_obj.AddItem(item_data)

    @catch_com_error
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildPerfGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraph(group_by_field, sum_of_field, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraphEx(group_by_field, sum_of_field, by_history, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildSummaryGraph(x_axis_field, group_by_field, sum_of_field, max_cols, filter, force_refresh, show_full_path)

    @catch_com_error
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildTrendGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(items, send_to, send_cc, option, subject, comment)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[T]':
        return self.com_obj.NewList(filter)

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    
