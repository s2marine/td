from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ExecEventNotifyByMailSettingsImpl(IExecEventNotifyByMailSettings):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__e_mail_to(self) -> 'str':
        return self.com_obj.EMailTo
    
    @catch_com_error
    def set__e_mail_to(self, value: 'str'):
        self.com_obj.EMailTo = value
    
    @catch_com_error
    def is__enabled(self, event_type: 'int') -> 'bool':
        return self.com_obj.Enabled[event_type]
    
    @catch_com_error
    def set__enabled(self, event_type: 'int', value: 'bool'):
        self.com_obj.Enabled[event_type] = value
    
    @catch_com_error
    def get__user_message(self) -> 'str':
        return self.com_obj.UserMessage
    
    @catch_com_error
    def set__user_message(self, value: 'str'):
        self.com_obj.UserMessage = value
    
    @catch_com_error
    def save(self, auto_post: 'bool'):
        self.com_obj.Save(auto_post)

    
