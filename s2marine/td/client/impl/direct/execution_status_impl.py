from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_execution_status import IExecutionStatus
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus
    
    from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo


class ExecutionStatusImpl(IExecutionStatus):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get___new_enum(self) -> 'Unknown':
        return self.com_obj._NewEnum
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def is__finished(self) -> 'bool':
        return self.com_obj.Finished
    
    @catch_com_error
    def get__item(self, index: 'int') -> 'ITestExecStatus':
        from s2marine.td.client.impl.direct.test_exec_status_impl import TestExecStatusImpl
        return TestExecStatusImpl(self.com_obj.Item[index]) if self.com_obj.Item else None
    
    @catch_com_error
    def events_list(self) -> 'List[IExecEventInfo]':
        from s2marine.td.client.impl.direct.exec_event_info_impl import ExecEventInfoImpl
        return [ExecEventInfoImpl(com_obj) for com_obj in self.com_obj.EventsList()]

    @catch_com_error
    def refresh_exec_status_info(self, test_data: 'Any', force: 'bool'):
        self.com_obj.RefreshExecStatusInfo(test_data, force)

    
