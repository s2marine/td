from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class CustomizationUsersGroupsImpl(ICustomizationUsersGroups):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__group(self, name: 'str') -> 'ICustomizationUsersGroup':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return CustomizationUsersGroupImpl(self.com_obj.Group[name]) if self.com_obj.Group else None
    
    @catch_com_error
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return [CustomizationUsersGroupImpl(com_obj) for com_obj in self.com_obj.Groups]
    
    @catch_com_error
    def add_group(self, name: 'str') -> 'T':
        return self.com_obj.AddGroup(name)

    @catch_com_error
    def remove_group(self, name: 'str'):
        self.com_obj.RemoveGroup(name)

    
