from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ExecEventActionParamsImpl(IExecEventActionParams):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__on_exec_event_scheduler_action(self) -> 'int':
        return self.com_obj.OnExecEventSchedulerAction
    
    
