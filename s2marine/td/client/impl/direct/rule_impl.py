from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_rule import IRule
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class RuleImpl(IRule):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__description(self) -> 'str':
        return self.com_obj.Description
    
    @catch_com_error
    def get__id(self) -> 'int':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_active(self) -> 'bool':
        return self.com_obj.IsActive
    
    @catch_com_error
    def set__is_active(self, value: 'bool'):
        self.com_obj.IsActive = value
    
    @catch_com_error
    def is__to_mail(self) -> 'bool':
        return self.com_obj.ToMail
    
    @catch_com_error
    def set__to_mail(self, value: 'bool'):
        self.com_obj.ToMail = value
    
    @catch_com_error
    def post(self):
        self.com_obj.Post()

    
