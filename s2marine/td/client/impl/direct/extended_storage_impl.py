from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_file_data import IFileData
    


class ExtendedStorageImpl(IExtendedStorage):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__action_finished(self) -> 'int':
        return self.com_obj.ActionFinished
    
    @catch_com_error
    def get__client_path(self) -> 'str':
        return self.com_obj.ClientPath
    
    @catch_com_error
    def set__client_path(self, value: 'str'):
        self.com_obj.ClientPath = value
    
    @catch_com_error
    def get__root(self) -> 'IFileData':
        from s2marine.td.client.impl.direct.file_data_impl import FileDataImpl
        return FileDataImpl(self.com_obj.Root) if self.com_obj.Root else None
    
    @catch_com_error
    def get__server_path(self) -> 'str':
        return self.com_obj.ServerPath
    
    @catch_com_error
    def set__server_path(self, value: 'str'):
        self.com_obj.ServerPath = value
    
    @catch_com_error
    def cancel(self):
        self.com_obj.Cancel()

    @catch_com_error
    def delete(self, f_sys_filter: 'str', n_delete_type: 'int'):
        self.com_obj.Delete(f_sys_filter, n_delete_type)

    @catch_com_error
    def get_last_error(self):
        self.com_obj.GetLastError()

    @catch_com_error
    def load(self, f_sys_filter: 'str', synchronize: 'bool') -> 'str':
        return self.com_obj.Load(f_sys_filter, synchronize)

    @catch_com_error
    def load_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]', p_non_fatal_error_occured: 'bool') -> 'str':
        return self.com_obj.LoadEx(f_sys_filter, synchronize, p_val, p_non_fatal_error_occured)

    @catch_com_error
    def progress(self, total: 'int', current: 'int') -> 'str':
        return self.com_obj.Progress(total, current)

    @catch_com_error
    def save(self, f_sys_filter: 'str', synchronize: 'bool'):
        self.com_obj.Save(f_sys_filter, synchronize)

    @catch_com_error
    def save_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]') -> 'bool':
        return self.com_obj.SaveEx(f_sys_filter, synchronize, p_val)

    
