from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_settings import ISettings
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class SettingsImpl(ISettings):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__enum_items(self) -> 'List[T]':
        return self.com_obj.EnumItems
    
    @catch_com_error
    def is__is_system(self, name: 'str') -> 'bool':
        return self.com_obj.IsSystem[name]
    
    @catch_com_error
    def get__value(self, name: 'str') -> 'str':
        return self.com_obj.Value[name]
    
    @catch_com_error
    def set__value(self, name: 'str', value: 'str'):
        self.com_obj.Value[name] = value
    
    @catch_com_error
    def close(self):
        self.com_obj.Close()

    @catch_com_error
    def delete_category(self, category: 'str'):
        self.com_obj.DeleteCategory(category)

    @catch_com_error
    def delete_value(self, name: 'str'):
        self.com_obj.DeleteValue(name)

    @catch_com_error
    def open(self, category: 'str'):
        self.com_obj.Open(category)

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self, category: 'str'):
        self.com_obj.Refresh(category)

    
