from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_fields import ICustomizationFields
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField


class CustomizationFieldsImpl(ICustomizationFields):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__field(self, table_name: 'str', field_name: 'str') -> 'ICustomizationField':
        from s2marine.td.client.impl.direct.customization_field_impl import CustomizationFieldImpl
        return CustomizationFieldImpl(self.com_obj.Field[table_name, field_name]) if self.com_obj.Field else None
    
    @catch_com_error
    def is__field_exists(self, table_name: 'str', field_name: 'str') -> 'bool':
        return self.com_obj.FieldExists[table_name, field_name]
    
    @catch_com_error
    def get__fields(self, table_name: 'str') -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields[table_name]]
    
    @catch_com_error
    def add_active_field(self, table_name: 'str') -> 'ICustomizationField':
        from s2marine.td.client.impl.direct.customization_field_impl import CustomizationFieldImpl
        return CustomizationFieldImpl(self.com_obj.AddActiveField(table_name)) if self.com_obj.AddActiveField else None

    @catch_com_error
    def add_active_memo_field(self, table_name: 'str') -> 'T':
        return self.com_obj.AddActiveMemoField(table_name)

    
