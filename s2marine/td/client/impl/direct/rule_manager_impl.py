from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_rule_manager import IRuleManager
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_rule import IRule


class RuleManagerImpl(IRuleManager):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__rules(self, need_refresh: 'bool') -> 'List[T]':
        return self.com_obj.Rules[need_refresh]
    
    @catch_com_error
    def get_rule(self, id: 'int') -> 'IRule':
        from s2marine.td.client.impl.direct.rule_impl import RuleImpl
        return RuleImpl(self.com_obj.GetRule(id)) if self.com_obj.GetRule else None

    
