from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_condition_factory import IConditionFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_condition import ICondition
    
    from s2marine.td.client.intf.i_condition import ICondition


class ConditionFactoryImpl(IConditionFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__filter(self) -> 'TDFilter':
        return self.com_obj.Filter
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'ICondition':
        from s2marine.td.client.impl.direct.condition_impl import ConditionImpl
        return ConditionImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'ICondition':
        from s2marine.td.client.impl.direct.condition_impl import ConditionImpl
        return ConditionImpl(self.com_obj.AddItem(item_data)) if self.com_obj.AddItem else None

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[T]':
        return self.com_obj.NewList(filter)

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    @catch_com_error
    def save(self):
        self.com_obj.Save()

    
