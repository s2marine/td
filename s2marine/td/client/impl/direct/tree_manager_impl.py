from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_tree_manager import ITreeManager
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class TreeManagerImpl(ITreeManager):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__node_by_id(self, node_id: 'int') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.NodeById[node_id]) if self.com_obj.NodeById else None
    
    @catch_com_error
    def get__node_by_path(self, path: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.NodeByPath[path]) if self.com_obj.NodeByPath else None
    
    @catch_com_error
    def get__node_path(self, node_id: 'int') -> 'str':
        return self.com_obj.NodePath[node_id]
    
    @catch_com_error
    def get__root_list(self, val: 'int') -> 'List[T]':
        return self.com_obj.RootList[val]
    
    @catch_com_error
    def get__tree_root(self, root_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.TreeRoot[root_name]) if self.com_obj.TreeRoot else None
    
    
