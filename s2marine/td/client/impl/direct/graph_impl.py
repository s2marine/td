from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_graph import IGraph
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class GraphImpl(IGraph):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__col_count(self) -> 'int':
        return self.com_obj.ColCount
    
    @catch_com_error
    def get__col_name(self, col: 'int') -> 'str':
        return self.com_obj.ColName[col]
    
    @catch_com_error
    def get__col_total(self, col: 'int') -> 'int':
        return self.com_obj.ColTotal[col]
    
    @catch_com_error
    def get__data(self, col: 'int', row: 'int') -> 'int':
        return self.com_obj.Data[col, row]
    
    @catch_com_error
    def get__graph_total(self) -> 'int':
        return self.com_obj.GraphTotal
    
    @catch_com_error
    def get__max_value(self) -> 'int':
        return self.com_obj.MaxValue
    
    @catch_com_error
    def get__row_count(self) -> 'int':
        return self.com_obj.RowCount
    
    @catch_com_error
    def get__row_name(self, row: 'int') -> 'str':
        return self.com_obj.RowName[row]
    
    @catch_com_error
    def get__row_total(self, row: 'int') -> 'int':
        return self.com_obj.RowTotal[row]
    
    @catch_com_error
    def get__start_date(self) -> 'float':
        return self.com_obj.StartDate
    
    @catch_com_error
    def drill_down(self, p_areas: 'Any', m_areas: 'Any') -> 'List[T]':
        return self.com_obj.DrillDown(p_areas, m_areas)

    @catch_com_error
    def multi_drill_down(self, areas: 'Any') -> 'List[T]':
        return self.com_obj.MultiDrillDown(areas)

    
