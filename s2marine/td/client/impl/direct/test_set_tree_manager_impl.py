from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    


class TestSetTreeManagerImpl(ITestSetTreeManager):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__node_by_id(self, node_id: 'int') -> 'ITestSetFolder':
        from s2marine.td.client.impl.direct.test_set_folder_impl import TestSetFolderImpl
        return TestSetFolderImpl(self.com_obj.NodeById[node_id]) if self.com_obj.NodeById else None
    
    @catch_com_error
    def get__node_by_path(self, path: 'str') -> 'ITestSetFolder':
        from s2marine.td.client.impl.direct.test_set_folder_impl import TestSetFolderImpl
        return TestSetFolderImpl(self.com_obj.NodeByPath[path]) if self.com_obj.NodeByPath else None
    
    @catch_com_error
    def get__root(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl
        return SysTreeNodeImpl(self.com_obj.Root) if self.com_obj.Root else None
    
    @catch_com_error
    def get__unattached(self) -> 'ITestSetFolder':
        from s2marine.td.client.impl.direct.test_set_folder_impl import TestSetFolderImpl
        return TestSetFolderImpl(self.com_obj.Unattached) if self.com_obj.Unattached else None
    
    
