from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_step_params import IStepParams
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class StepParamsImpl(IStepParams):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def is__param_exist(self, param_name: 'str') -> 'bool':
        return self.com_obj.ParamExist[param_name]
    
    @catch_com_error
    def get__param_name(self, n_position: 'int') -> 'str':
        return self.com_obj.ParamName[n_position]
    
    @catch_com_error
    def get__param_value(self, v_param: 'Any') -> 'str':
        return self.com_obj.ParamValue[v_param]
    
    @catch_com_error
    def set__param_value(self, v_param: 'Any', value: 'str'):
        self.com_obj.ParamValue[v_param] = value
    
    @catch_com_error
    def get__type(self, v_param: 'Any') -> 'int':
        return self.com_obj.Type[v_param]
    
    @catch_com_error
    def add_param(self, param_name: 'str', param_type: 'str'):
        self.com_obj.AddParam(param_name, param_type)

    @catch_com_error
    def clear_param(self, v_param: 'Any'):
        self.com_obj.ClearParam(v_param)

    @catch_com_error
    def delete_param(self, param_name: 'str'):
        self.com_obj.DeleteParam(param_name)

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def save(self):
        self.com_obj.Save()

    
