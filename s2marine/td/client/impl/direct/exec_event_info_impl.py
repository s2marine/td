from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ExecEventInfoImpl(IExecEventInfo):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__event_date(self) -> 'str':
        return self.com_obj.EventDate
    
    @catch_com_error
    def get__event_time(self) -> 'str':
        return self.com_obj.EventTime
    
    @catch_com_error
    def get__event_type(self) -> 'int':
        return self.com_obj.EventType
    
    
