from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_bug import IBug
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    


class BugImpl(IBug):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__assigned_to(self) -> 'str':
        return self.com_obj.AssignedTo
    
    @catch_com_error
    def set__assigned_to(self, value: 'str'):
        self.com_obj.AssignedTo = value
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__detected_by(self) -> 'str':
        return self.com_obj.DetectedBy
    
    @catch_com_error
    def set__detected_by(self, value: 'str'):
        self.com_obj.DetectedBy = value
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__priority(self) -> 'str':
        return self.com_obj.Priority
    
    @catch_com_error
    def set__priority(self, value: 'str'):
        self.com_obj.Priority = value
    
    @catch_com_error
    def get__project(self) -> 'str':
        return self.com_obj.Project
    
    @catch_com_error
    def set__project(self, value: 'str'):
        self.com_obj.Project = value
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def set__status(self, value: 'str'):
        self.com_obj.Status = value
    
    @catch_com_error
    def get__summary(self) -> 'str':
        return self.com_obj.Summary
    
    @catch_com_error
    def set__summary(self, value: 'str'):
        self.com_obj.Summary = value
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def find_similar_bugs(self, similarity_ratio: 'int') -> 'List[T]':
        return self.com_obj.FindSimilarBugs(similarity_ratio)

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(send_to, send_cc, option, subject, comment)

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
