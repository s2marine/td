from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_history import IHistory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history_filter import IHistoryFilter
    
    from s2marine.td.client.intf.i_history_record import IHistoryRecord


class HistoryImpl(IHistory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__filter(self) -> 'IHistoryFilter':
        from s2marine.td.client.impl.direct.history_filter_impl import HistoryFilterImpl
        return HistoryFilterImpl(self.com_obj.Filter) if self.com_obj.Filter else None
    
    @catch_com_error
    def clear_history(self, filter: 'str'):
        self.com_obj.ClearHistory(filter)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[IHistoryRecord]':
        from s2marine.td.client.impl.direct.history_record_impl import HistoryRecordImpl
        return [HistoryRecordImpl(com_obj) for com_obj in self.com_obj.NewList(filter)]

    
