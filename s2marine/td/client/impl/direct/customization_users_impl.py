from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_users import ICustomizationUsers
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    


class CustomizationUsersImpl(ICustomizationUsers):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__user(self, name: 'str') -> 'ICustomizationUser':
        from s2marine.td.client.impl.direct.customization_user_impl import CustomizationUserImpl
        return CustomizationUserImpl(self.com_obj.User[name]) if self.com_obj.User else None
    
    @catch_com_error
    def get__users(self) -> 'List[ICustomizationUser]':
        from s2marine.td.client.impl.direct.customization_user_impl import CustomizationUserImpl
        return [CustomizationUserImpl(com_obj) for com_obj in self.com_obj.Users]
    
    @catch_com_error
    def add_site_authenticated_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', domain_authentication: 'str', group: 'Any'):
        self.com_obj.AddSiteAuthenticatedUser(user_name, full_name, email, description, phone, domain_authentication, group)

    @catch_com_error
    def add_site_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', group: 'Any'):
        self.com_obj.AddSiteUser(user_name, full_name, email, description, phone, group)

    @catch_com_error
    def add_user(self, name: 'str') -> 'T':
        return self.com_obj.AddUser(name)

    @catch_com_error
    def remove_user(self, name: 'str'):
        self.com_obj.RemoveUser(name)

    
