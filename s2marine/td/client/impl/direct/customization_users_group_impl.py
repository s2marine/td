from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser


class CustomizationUsersGroupImpl(ICustomizationUsersGroup):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__deleted(self) -> 'bool':
        return self.com_obj.Deleted
    
    @catch_com_error
    def set__deleted(self, value: 'bool'):
        self.com_obj.Deleted = value
    
    @catch_com_error
    def get__hide_filter(self, filter_type: 'str') -> 'str':
        return self.com_obj.HideFilter[filter_type]
    
    @catch_com_error
    def set__hide_filter(self, filter_type: 'str', value: 'str'):
        self.com_obj.HideFilter[filter_type] = value
    
    @catch_com_error
    def get__id(self) -> 'int':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_system(self) -> 'bool':
        return self.com_obj.IsSystem
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def add_user(self, user: 'Any'):
        self.com_obj.AddUser(user)

    @catch_com_error
    def remove_user(self, user: 'Any'):
        self.com_obj.RemoveUser(user)

    @catch_com_error
    def users_list(self) -> 'List[ICustomizationUser]':
        from s2marine.td.client.impl.direct.customization_user_impl import CustomizationUserImpl
        return [CustomizationUserImpl(com_obj) for com_obj in self.com_obj.UsersList()]

    
