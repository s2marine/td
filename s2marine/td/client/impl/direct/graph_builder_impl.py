from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_graph_builder import IGraphBuilder
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_graph_definition import IGraphDefinition


class GraphBuilderImpl(IGraphBuilder):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def build_graph(self, p_graph_def: 'IGraphDefinition') -> 'T':
        return self.com_obj.BuildGraph(p_graph_def)

    @catch_com_error
    def build_multiple_graphs(self, graph_defs: 'Any') -> 'List[T]':
        return self.com_obj.BuildMultipleGraphs(graph_defs)

    @catch_com_error
    def create_graph_definition(self, module: 'int', graph_type: 'int') -> 'IGraphDefinition':
        from s2marine.td.client.impl.direct.graph_definition_impl import GraphDefinitionImpl
        return GraphDefinitionImpl(self.com_obj.CreateGraphDefinition(module, graph_type)) if self.com_obj.CreateGraphDefinition else None

    
