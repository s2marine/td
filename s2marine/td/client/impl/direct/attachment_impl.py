from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_attachment import IAttachment
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    


class AttachmentImpl(IAttachment):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachment_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.AttachmentStorage) if self.com_obj.AttachmentStorage else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__data(self) -> 'Any':
        return self.com_obj.Data
    
    @catch_com_error
    def get__description(self) -> 'str':
        return self.com_obj.Description
    
    @catch_com_error
    def set__description(self, value: 'str'):
        self.com_obj.Description = value
    
    @catch_com_error
    def get__direct_link(self) -> 'str':
        return self.com_obj.DirectLink
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def get__file_name(self) -> 'str':
        return self.com_obj.FileName
    
    @catch_com_error
    def set__file_name(self, value: 'str'):
        self.com_obj.FileName = value
    
    @catch_com_error
    def get__file_size(self) -> 'int':
        return self.com_obj.FileSize
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__last_modified(self) -> 'float':
        return self.com_obj.LastModified
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self, view_format: 'int') -> 'str':
        return self.com_obj.Name[view_format]
    
    @catch_com_error
    def get__server_file_name(self) -> 'str':
        return self.com_obj.ServerFileName
    
    @catch_com_error
    def get__type(self) -> 'int':
        return self.com_obj.Type
    
    @catch_com_error
    def set__type(self, value: 'int'):
        self.com_obj.Type = value
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def load(self, synchronize: 'bool', root_path: 'str'):
        self.com_obj.Load(synchronize, root_path)

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def rename(self, new_name: 'str'):
        self.com_obj.Rename(new_name)

    @catch_com_error
    def save(self, synchronize: 'bool'):
        self.com_obj.Save(synchronize)

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
