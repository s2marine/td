from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_td_field import ITDField
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_field_property import IFieldProperty
    


class TDFieldImpl(ITDField):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def get__property(self) -> 'IFieldProperty':
        from s2marine.td.client.impl.direct.field_property_impl import FieldPropertyImpl
        return FieldPropertyImpl(self.com_obj.Property) if self.com_obj.Property else None
    
    @catch_com_error
    def get__type(self) -> 'int':
        return self.com_obj.Type
    
    @catch_com_error
    def is_valid_value(self, value: 'Any', p_object: 'T'):
        self.com_obj.IsValidValue(value, p_object)

    
