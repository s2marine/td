from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_action import ICustomizationAction
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class CustomizationActionImpl(ICustomizationAction):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return [CustomizationUsersGroupImpl(com_obj) for com_obj in self.com_obj.Groups]
    
    @catch_com_error
    def is__is_group_permited(self, group: 'Any') -> 'bool':
        return self.com_obj.IsGroupPermited[group]
    
    @catch_com_error
    def is__is_owner_group(self, group: 'Any') -> 'bool':
        return self.com_obj.IsOwnerGroup[group]
    
    @catch_com_error
    def is__is_user_permited(self, user: 'Any') -> 'bool':
        return self.com_obj.IsUserPermited[user]
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def get__owner_group(self) -> 'ICustomizationUsersGroup':
        from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl
        return CustomizationUsersGroupImpl(self.com_obj.OwnerGroup) if self.com_obj.OwnerGroup else None
    
    @catch_com_error
    def set__owner_group(self, value: 'ICustomizationUsersGroup'):
        self.com_obj.OwnerGroup = value
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def add_group(self, group: 'Any'):
        self.com_obj.AddGroup(group)

    @catch_com_error
    def remove_group(self, group: 'Any'):
        self.com_obj.RemoveGroup(group)

    
