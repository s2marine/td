from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_actions import ICustomizationActions
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    


class CustomizationActionsImpl(ICustomizationActions):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__action(self, name: 'str') -> 'ICustomizationAction':
        from s2marine.td.client.impl.direct.customization_action_impl import CustomizationActionImpl
        return CustomizationActionImpl(self.com_obj.Action[name]) if self.com_obj.Action else None
    
    @catch_com_error
    def get__actions(self) -> 'List[ICustomizationAction]':
        from s2marine.td.client.impl.direct.customization_action_impl import CustomizationActionImpl
        return [CustomizationActionImpl(com_obj) for com_obj in self.com_obj.Actions]
    
    
