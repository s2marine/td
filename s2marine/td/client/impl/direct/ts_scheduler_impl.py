from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_execution_status import IExecutionStatus
    


class TSSchedulerImpl(ITSScheduler):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__execution_log(self) -> 'str':
        return self.com_obj.ExecutionLog
    
    @catch_com_error
    def get__execution_status(self) -> 'IExecutionStatus':
        from s2marine.td.client.impl.direct.execution_status_impl import ExecutionStatusImpl
        return ExecutionStatusImpl(self.com_obj.ExecutionStatus) if self.com_obj.ExecutionStatus else None
    
    @catch_com_error
    def get__host_time_out(self) -> 'int':
        return self.com_obj.HostTimeOut
    
    @catch_com_error
    def set__host_time_out(self, value: 'int'):
        self.com_obj.HostTimeOut = value
    
    @catch_com_error
    def is__log_enabled(self) -> 'bool':
        return self.com_obj.LogEnabled
    
    @catch_com_error
    def set__log_enabled(self, value: 'bool'):
        self.com_obj.LogEnabled = value
    
    @catch_com_error
    def is__run_all_locally(self) -> 'bool':
        return self.com_obj.RunAllLocally
    
    @catch_com_error
    def set__run_all_locally(self, value: 'bool'):
        self.com_obj.RunAllLocally = value
    
    @catch_com_error
    def get__run_on_host(self, ts_test_id: 'Any') -> 'str':
        return self.com_obj.RunOnHost[ts_test_id]
    
    @catch_com_error
    def set__run_on_host(self, ts_test_id: 'Any', value: 'str'):
        self.com_obj.RunOnHost[ts_test_id] = value
    
    @catch_com_error
    def get__td_host_name(self) -> 'str':
        return self.com_obj.TdHostName
    
    @catch_com_error
    def set__td_host_name(self, value: 'str'):
        self.com_obj.TdHostName = value
    
    @catch_com_error
    def run(self, test_data: 'Any'):
        self.com_obj.Run(test_data)

    @catch_com_error
    def stop(self, test_data: 'Any'):
        self.com_obj.Stop(test_data)

    
