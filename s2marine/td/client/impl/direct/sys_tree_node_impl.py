from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode


class SysTreeNodeImpl(ISysTreeNode):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attribute(self) -> 'int':
        return self.com_obj.Attribute
    
    @catch_com_error
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        return SysTreeNodeImpl(self.com_obj.Child[index]) if self.com_obj.Child else None
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def get__depth_type(self) -> 'int':
        return self.com_obj.DepthType
    
    @catch_com_error
    def get__father(self) -> 'ISysTreeNode':
        return SysTreeNodeImpl(self.com_obj.Father) if self.com_obj.Father else None
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__node_id(self) -> 'int':
        return self.com_obj.NodeID
    
    @catch_com_error
    def get__path(self) -> 'str':
        return self.com_obj.Path
    
    @catch_com_error
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        return SysTreeNodeImpl(self.com_obj.AddNode(node_name)) if self.com_obj.AddNode else None

    @catch_com_error
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        return SysTreeNodeImpl(self.com_obj.FindChildNode(child_name)) if self.com_obj.FindChildNode else None

    @catch_com_error
    def find_children(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        return self.com_obj.FindChildren(pattern, match_case, filter)

    @catch_com_error
    def new_list(self) -> 'List[T]':
        return self.com_obj.NewList()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def remove_node(self, node: 'Any'):
        self.com_obj.RemoveNode(node)

    
