from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_run_factory import IRunFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    


class RunFactoryImpl(IRunFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self) -> 'TDFilter':
        return self.com_obj.Filter
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'IRun':
        from s2marine.td.client.impl.direct.run_impl import RunImpl
        return RunImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def get__unique_run_name(self) -> 'str':
        return self.com_obj.UniqueRunName
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'T':
        return self.com_obj.AddItem(item_data)

    @catch_com_error
    def delete_duplicate_runs(self, run_name: 'str'):
        self.com_obj.DeleteDuplicateRuns(run_name)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[T]':
        return self.com_obj.NewList(filter)

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    
