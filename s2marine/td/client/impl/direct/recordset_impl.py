from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_recordset import IRecordset
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class RecordsetImpl(IRecordset):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__bor(self) -> 'bool':
        return self.com_obj.BOR
    
    @catch_com_error
    def get__cache_size(self) -> 'int':
        return self.com_obj.CacheSize
    
    @catch_com_error
    def set__cache_size(self, value: 'int'):
        self.com_obj.CacheSize = value
    
    @catch_com_error
    def get__col_count(self) -> 'int':
        return self.com_obj.ColCount
    
    @catch_com_error
    def get__col_index(self, name: 'str') -> 'int':
        return self.com_obj.ColIndex[name]
    
    @catch_com_error
    def get__col_name(self, index: 'int') -> 'str':
        return self.com_obj.ColName[index]
    
    @catch_com_error
    def get__col_size(self, index: 'int') -> 'int':
        return self.com_obj.ColSize[index]
    
    @catch_com_error
    def get__col_type(self, index: 'int') -> 'int':
        return self.com_obj.ColType[index]
    
    @catch_com_error
    def is__eor(self) -> 'bool':
        return self.com_obj.EOR
    
    @catch_com_error
    def get__field_value(self, field_key: 'Any') -> 'Any':
        return self.com_obj.FieldValue[field_key]
    
    @catch_com_error
    def set__field_value(self, field_key: 'Any', value: 'Any'):
        self.com_obj.FieldValue[field_key] = value
    
    @catch_com_error
    def get__position(self) -> 'int':
        return self.com_obj.Position
    
    @catch_com_error
    def set__position(self, value: 'int'):
        self.com_obj.Position = value
    
    @catch_com_error
    def get__record_count(self) -> 'int':
        return self.com_obj.RecordCount
    
    @catch_com_error
    def clone(self) -> 'T':
        return self.com_obj.Clone()

    @catch_com_error
    def first(self):
        self.com_obj.First()

    @catch_com_error
    def last(self):
        self.com_obj.Last()

    @catch_com_error
    def next(self):
        self.com_obj.Next()

    @catch_com_error
    def prev(self):
        self.com_obj.Prev()

    @catch_com_error
    def refresh(self, range: 'int', low: 'int', high: 'int'):
        self.com_obj.Refresh(range, low, high)

    
