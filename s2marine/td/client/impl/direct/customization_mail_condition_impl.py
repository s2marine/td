from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class CustomizationMailConditionImpl(ICustomizationMailCondition):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__condition_text(self) -> 'str':
        return self.com_obj.ConditionText
    
    @catch_com_error
    def set__condition_text(self, value: 'str'):
        self.com_obj.ConditionText = value
    
    @catch_com_error
    def get__condition_type(self) -> 'int':
        return self.com_obj.ConditionType
    
    @catch_com_error
    def is__deleted(self) -> 'bool':
        return self.com_obj.Deleted
    
    @catch_com_error
    def set__deleted(self, value: 'bool'):
        self.com_obj.Deleted = value
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    
