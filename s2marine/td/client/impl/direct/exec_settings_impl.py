from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_exec_settings import IExecSettings
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams
    


class ExecSettingsImpl(IExecSettings):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__on_exec_event_scheduler_action_params(self, event_type: 'int') -> 'IExecEventActionParams':
        from s2marine.td.client.impl.direct.exec_event_action_params_impl import ExecEventActionParamsImpl
        return ExecEventActionParamsImpl(self.com_obj.OnExecEventSchedulerActionParams[event_type]) if self.com_obj.OnExecEventSchedulerActionParams else None
    
    @catch_com_error
    def get__on_exec_event_scheduler_action_type(self, event_type: 'int') -> 'int':
        return self.com_obj.OnExecEventSchedulerActionType[event_type]
    
    @catch_com_error
    def set__on_exec_event_scheduler_action_type(self, event_type: 'int', value: 'int'):
        self.com_obj.OnExecEventSchedulerActionType[event_type] = value
    
    @catch_com_error
    def get__planned_execution_date(self) -> 'Any':
        return self.com_obj.PlannedExecutionDate
    
    @catch_com_error
    def set__planned_execution_date(self, value: 'Any'):
        self.com_obj.PlannedExecutionDate = value
    
    @catch_com_error
    def get__planned_execution_time(self) -> 'Any':
        return self.com_obj.PlannedExecutionTime
    
    @catch_com_error
    def set__planned_execution_time(self, value: 'Any'):
        self.com_obj.PlannedExecutionTime = value
    
    @catch_com_error
    def get__planned_run_duration(self) -> 'Any':
        return self.com_obj.PlannedRunDuration
    
    @catch_com_error
    def set__planned_run_duration(self, value: 'Any'):
        self.com_obj.PlannedRunDuration = value
    
    
