from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_host_group import IHostGroup
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    


class HostGroupImpl(IHostGroup):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def add_host(self, val: 'Any'):
        self.com_obj.AddHost(val)

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def new_list(self) -> 'List[T]':
        return self.com_obj.NewList()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def remove_host(self, val: 'Any'):
        self.com_obj.RemoveHost(val)

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
