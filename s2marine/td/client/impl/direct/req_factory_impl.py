from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_req_factory import IReqFactory
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_req import IReq
    
    from s2marine.td.client.intf.i_req import IReq


class ReqFactoryImpl(IReqFactory):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__fetch_level(self, field_name: 'str') -> 'int':
        return self.com_obj.FetchLevel[field_name]
    
    @catch_com_error
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        self.com_obj.FetchLevel[field_name] = value
    
    @catch_com_error
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl
        return [TDFieldImpl(com_obj) for com_obj in self.com_obj.Fields]
    
    @catch_com_error
    def get__filter(self) -> 'TDFilter':
        return self.com_obj.Filter
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__item(self, item_key: 'Any') -> 'IReq':
        from s2marine.td.client.impl.direct.req_impl import ReqImpl
        return ReqImpl(self.com_obj.Item[item_key]) if self.com_obj.Item else None
    
    @catch_com_error
    def add_item(self, item_data: 'Any') -> 'T':
        return self.com_obj.AddItem(item_data)

    @catch_com_error
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildPerfGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_perf_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        return self.com_obj.BuildPerfGraphEx(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path, show_null_parents)

    @catch_com_error
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraph(group_by_field, sum_of_field, by_history, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        return self.com_obj.BuildProgressGraphEx(group_by_field, sum_of_field, by_history, major_skip, minor_skip, max_cols, filter, fr_date, force_refresh, show_full_path, show_null_parents)

    @catch_com_error
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildSummaryGraph(x_axis_field, group_by_field, sum_of_field, max_cols, filter, force_refresh, show_full_path)

    @catch_com_error
    def build_summary_graph_ex(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        return self.com_obj.BuildSummaryGraphEx(x_axis_field, group_by_field, sum_of_field, max_cols, filter, force_refresh, show_full_path, show_null_parents)

    @catch_com_error
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        return self.com_obj.BuildTrendGraph(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path)

    @catch_com_error
    def build_trend_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        return self.com_obj.BuildTrendGraphEx(group_by_field, sum_of_field, max_cols, filter, fr_date, force_refresh, show_full_path, show_null_parents)

    @catch_com_error
    def find(self, start_root_id: 'int', field_name: 'str', pattern: 'str', mode: 'int', limit: 'int') -> 'List[T]':
        return self.com_obj.Find(start_root_id, field_name, pattern, mode, limit)

    @catch_com_error
    def get_children_list(self, father_id: 'int') -> 'List[IReq]':
        from s2marine.td.client.impl.direct.req_impl import ReqImpl
        return [ReqImpl(com_obj) for com_obj in self.com_obj.GetChildrenList(father_id)]

    @catch_com_error
    def get_filtered_children_list(self, father_id: 'int', filter: 'TDFilter') -> 'List[T]':
        return self.com_obj.GetFilteredChildrenList(father_id, filter)

    @catch_com_error
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(items, send_to, send_cc, option, subject, comment)

    @catch_com_error
    def new_list(self, filter: 'str') -> 'List[T]':
        return self.com_obj.NewList(filter)

    @catch_com_error
    def remove_item(self, item_key: 'Any'):
        self.com_obj.RemoveItem(item_key)

    
