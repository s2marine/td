from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_req import IReq
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    
    from s2marine.td.client.intf.i_test import ITest


class ReqImpl(IReq):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def get__author(self) -> 'str':
        return self.com_obj.Author
    
    @catch_com_error
    def set__author(self, value: 'str'):
        self.com_obj.Author = value
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__comment(self) -> 'str':
        return self.com_obj.Comment
    
    @catch_com_error
    def set__comment(self, value: 'str'):
        self.com_obj.Comment = value
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def is__has_coverage(self) -> 'bool':
        return self.com_obj.HasCoverage
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__paragraph(self) -> 'str':
        return self.com_obj.Paragraph
    
    @catch_com_error
    def get__path(self) -> 'str':
        return self.com_obj.Path
    
    @catch_com_error
    def get__priority(self) -> 'str':
        return self.com_obj.Priority
    
    @catch_com_error
    def set__priority(self, value: 'str'):
        self.com_obj.Priority = value
    
    @catch_com_error
    def get__product(self) -> 'str':
        return self.com_obj.Product
    
    @catch_com_error
    def set__product(self, value: 'str'):
        self.com_obj.Product = value
    
    @catch_com_error
    def get__reviewed(self) -> 'str':
        return self.com_obj.Reviewed
    
    @catch_com_error
    def set__reviewed(self, value: 'str'):
        self.com_obj.Reviewed = value
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def get__type(self) -> 'str':
        return self.com_obj.Type
    
    @catch_com_error
    def set__type(self, value: 'str'):
        self.com_obj.Type = value
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def add_coverage(self, test_id: 'int', order: 'int') -> 'int':
        return self.com_obj.AddCoverage(test_id, order)

    @catch_com_error
    def add_coverage_by_filter(self, subject_id: 'int', order: 'int', test_filter: 'str') -> 'int':
        return self.com_obj.AddCoverageByFilter(subject_id, order, test_filter)

    @catch_com_error
    def add_coverage_ex(self, subject_id: 'int', order: 'int') -> 'int':
        return self.com_obj.AddCoverageEx(subject_id, order)

    @catch_com_error
    def get_cover_list(self, recursive: 'bool') -> 'List[ITest]':
        from s2marine.td.client.impl.direct.test_impl import TestImpl
        return [TestImpl(com_obj) for com_obj in self.com_obj.GetCoverList(recursive)]

    @catch_com_error
    def get_cover_list_by_filter(self, test_filter: 'str', recursive: 'bool') -> 'List[T]':
        return self.com_obj.GetCoverListByFilter(test_filter, recursive)

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(send_to, send_cc, option, subject, comment)

    @catch_com_error
    def move(self, new_father_id: 'int', new_order: 'int'):
        self.com_obj.Move(new_father_id, new_order)

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def remove_coverage(self, v_test: 'Any', recursive: 'bool'):
        self.com_obj.RemoveCoverage(v_test, recursive)

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
