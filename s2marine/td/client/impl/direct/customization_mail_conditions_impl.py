from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition


class CustomizationMailConditionsImpl(ICustomizationMailConditions):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__condition(self, name: 'str', condition_type: 'int') -> 'ICustomizationMailCondition':
        from s2marine.td.client.impl.direct.customization_mail_condition_impl import CustomizationMailConditionImpl
        return CustomizationMailConditionImpl(self.com_obj.Condition[name, condition_type]) if self.com_obj.Condition else None
    
    @catch_com_error
    def is__condition_exist(self, name: 'str', condition_type: 'int') -> 'bool':
        return self.com_obj.ConditionExist[name, condition_type]
    
    @catch_com_error
    def get__conditions(self) -> 'List[ICustomizationMailCondition]':
        from s2marine.td.client.impl.direct.customization_mail_condition_impl import CustomizationMailConditionImpl
        return [CustomizationMailConditionImpl(com_obj) for com_obj in self.com_obj.Conditions]
    
    @catch_com_error
    def add_condition(self, name: 'str', condition_type: 'int', condition_text: 'str') -> 'ICustomizationMailCondition':
        from s2marine.td.client.impl.direct.customization_mail_condition_impl import CustomizationMailConditionImpl
        return CustomizationMailConditionImpl(self.com_obj.AddCondition(name, condition_type, condition_text)) if self.com_obj.AddCondition else None

    @catch_com_error
    def remove_condition(self, name: 'str', condition_type: 'int'):
        self.com_obj.RemoveCondition(name, condition_type)

    
