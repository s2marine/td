from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_modules import ICustomizationModules
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class CustomizationModulesImpl(ICustomizationModules):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__description(self, module_id: 'int') -> 'str':
        return self.com_obj.Description[module_id]
    
    @catch_com_error
    def set__description(self, module_id: 'int', value: 'str'):
        self.com_obj.Description[module_id] = value
    
    @catch_com_error
    def get__guid(self, module_id: 'int') -> 'str':
        return self.com_obj.GUID[module_id]
    
    @catch_com_error
    def set__guid(self, module_id: 'int', value: 'str'):
        self.com_obj.GUID[module_id] = value
    
    @catch_com_error
    def is__is_visible_for_group(self, module_id: 'int', group: 'Any') -> 'bool':
        return self.com_obj.IsVisibleForGroup[module_id, group]
    
    @catch_com_error
    def set__is_visible_for_group(self, module_id: 'int', group: 'Any', value: 'bool'):
        self.com_obj.IsVisibleForGroup[module_id, group] = value
    
    @catch_com_error
    def get__name(self, module_id: 'int') -> 'str':
        return self.com_obj.Name[module_id]
    
    @catch_com_error
    def set__name(self, module_id: 'int', value: 'str'):
        self.com_obj.Name[module_id] = value
    
    @catch_com_error
    def get__visible(self, module_id: 'int') -> 'int':
        return self.com_obj.Visible[module_id]
    
    @catch_com_error
    def set__visible(self, module_id: 'int', value: 'int'):
        self.com_obj.Visible[module_id] = value
    
    @catch_com_error
    def get__visible_for_groups(self, module_id: 'int') -> 'List[T]':
        return self.com_obj.VisibleForGroups[module_id]
    
    
