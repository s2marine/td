from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_lists import ICustomizationLists
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class CustomizationListsImpl(ICustomizationLists):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def is__is_list_exist(self, list_name: 'str') -> 'bool':
        return self.com_obj.IsListExist[list_name]
    
    @catch_com_error
    def get__list(self, param: 'Any') -> 'ICustomizationList':
        from s2marine.td.client.impl.direct.customization_list_impl import CustomizationListImpl
        return CustomizationListImpl(self.com_obj.List[param]) if self.com_obj.List else None
    
    @catch_com_error
    def get__list_by_count(self, count: 'int') -> 'ICustomizationList':
        from s2marine.td.client.impl.direct.customization_list_impl import CustomizationListImpl
        return CustomizationListImpl(self.com_obj.ListByCount[count]) if self.com_obj.ListByCount else None
    
    @catch_com_error
    def add_list(self, name: 'str') -> 'T':
        return self.com_obj.AddList(name)

    @catch_com_error
    def remove_list(self, name: 'str'):
        self.com_obj.RemoveList(name)

    
