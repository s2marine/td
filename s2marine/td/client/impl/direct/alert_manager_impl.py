from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_alert_manager import IAlertManager
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_alert import IAlert
    from s2marine.td.client.intf.i_alert import IAlert
    


class AlertManagerImpl(IAlertManager):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__alert(self, id: 'int') -> 'IAlert':
        from s2marine.td.client.impl.direct.alert_impl import AlertImpl
        return AlertImpl(self.com_obj.Alert[id]) if self.com_obj.Alert else None
    
    @catch_com_error
    def get__alert_list(self, entity_type: 'str', need_refresh: 'bool') -> 'List[IAlert]':
        from s2marine.td.client.impl.direct.alert_impl import AlertImpl
        return [AlertImpl(com_obj) for com_obj in self.com_obj.AlertList[entity_type, need_refresh]]
    
    @catch_com_error
    def clean_all_alerts(self):
        self.com_obj.CleanAllAlerts()

    @catch_com_error
    def delete_alert(self, i_ds: 'Any'):
        self.com_obj.DeleteAlert(i_ds)

    
