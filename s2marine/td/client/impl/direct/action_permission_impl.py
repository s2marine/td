from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_action_permission import IActionPermission
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ActionPermissionImpl(IActionPermission):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__action_enabled(self, action_identity: 'Any', action_target: 'Any') -> 'bool':
        return self.com_obj.ActionEnabled[action_identity, action_target]
    
    
