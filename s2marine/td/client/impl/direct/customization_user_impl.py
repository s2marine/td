from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_user import ICustomizationUser
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class CustomizationUserImpl(ICustomizationUser):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__address(self) -> 'str':
        return self.com_obj.Address
    
    @catch_com_error
    def set__address(self, value: 'str'):
        self.com_obj.Address = value
    
    @catch_com_error
    def is__deleted(self) -> 'bool':
        return self.com_obj.Deleted
    
    @catch_com_error
    def set__deleted(self, value: 'bool'):
        self.com_obj.Deleted = value
    
    @catch_com_error
    def get__domain_authentication(self) -> 'str':
        return self.com_obj.DomainAuthentication
    
    @catch_com_error
    def set__domain_authentication(self, value: 'str'):
        self.com_obj.DomainAuthentication = value
    
    @catch_com_error
    def get__email(self) -> 'str':
        return self.com_obj.Email
    
    @catch_com_error
    def set__email(self, value: 'str'):
        self.com_obj.Email = value
    
    @catch_com_error
    def get__full_name(self) -> 'str':
        return self.com_obj.FullName
    
    @catch_com_error
    def set__full_name(self, value: 'str'):
        self.com_obj.FullName = value
    
    @catch_com_error
    def is__in_group(self, group_name: 'Any') -> 'bool':
        return self.com_obj.InGroup[group_name]
    
    @catch_com_error
    def set__in_group(self, group_name: 'Any', value: 'bool'):
        self.com_obj.InGroup[group_name] = value
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def get__password(self, rhs: 'str') -> 'str':
        return self.com_obj.Password[rhs]
    
    @catch_com_error
    def set__password(self, rhs: 'str', value: 'str'):
        self.com_obj.Password[rhs] = value
    
    @catch_com_error
    def get__phone(self) -> 'str':
        return self.com_obj.Phone
    
    @catch_com_error
    def set__phone(self, value: 'str'):
        self.com_obj.Phone = value
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def add_to_group(self, group: 'Any'):
        self.com_obj.AddToGroup(group)

    @catch_com_error
    def groups_list(self) -> 'List[T]':
        return self.com_obj.GroupsList()

    @catch_com_error
    def remove_from_group(self, group: 'Any'):
        self.com_obj.RemoveFromGroup(group)

    
