from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_vcs import IVCS
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    


class VCSImpl(IVCS):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__checkout_info(self) -> 'IVersionItem':
        from s2marine.td.client.impl.direct.version_item_impl import VersionItemImpl
        return VersionItemImpl(self.com_obj.CheckoutInfo) if self.com_obj.CheckoutInfo else None
    
    @catch_com_error
    def get__current_version(self) -> 'str':
        return self.com_obj.CurrentVersion
    
    @catch_com_error
    def is__is_checked_out(self) -> 'bool':
        return self.com_obj.IsCheckedOut
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__locked_by(self) -> 'str':
        return self.com_obj.LockedBy
    
    @catch_com_error
    def get__version(self) -> 'str':
        return self.com_obj.Version
    
    @catch_com_error
    def get__version_info(self, version: 'str') -> 'IVersionItem':
        from s2marine.td.client.impl.direct.version_item_impl import VersionItemImpl
        return VersionItemImpl(self.com_obj.VersionInfo[version]) if self.com_obj.VersionInfo else None
    
    @catch_com_error
    def get__versions(self) -> 'List[T]':
        return self.com_obj.Versions
    
    @catch_com_error
    def get__versions_ex(self) -> 'List[IVersionItem]':
        from s2marine.td.client.impl.direct.version_item_impl import VersionItemImpl
        return [VersionItemImpl(com_obj) for com_obj in self.com_obj.VersionsEx]
    
    @catch_com_error
    def add_object_to_vcs(self, version: 'str', comments: 'str'):
        self.com_obj.AddObjectToVcs(version, comments)

    @catch_com_error
    def check_in(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool'):
        self.com_obj.CheckIn(version, comments, remove, set_current)

    @catch_com_error
    def check_in_ex(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool', force_checkin: 'bool', reserved: 'int'):
        self.com_obj.CheckInEx(version, comments, remove, set_current, force_checkin, reserved)

    @catch_com_error
    def check_out(self, version: 'str', comment: 'str', lock: 'bool', read_only: 'bool', sync: 'bool'):
        self.com_obj.CheckOut(version, comment, lock, read_only, sync)

    @catch_com_error
    def delete_object_from_vcs(self):
        self.com_obj.DeleteObjectFromVCS()

    @catch_com_error
    def lock_vcs_object(self):
        self.com_obj.LockVcsObject()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def set_current_version(self, version: 'str'):
        self.com_obj.SetCurrentVersion(version)

    @catch_com_error
    def undo_checkout(self, remove: 'bool'):
        self.com_obj.UndoCheckout(remove)

    
