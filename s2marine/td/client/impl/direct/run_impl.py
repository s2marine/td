from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_run import IRun
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_step_factory import IStepFactory
    


class RunImpl(IRun):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__bp_step_param_factory(self) -> 'T':
        return self.com_obj.BPStepParamFactory
    
    @catch_com_error
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.ExtendedStorage) if self.com_obj.ExtendedStorage else None
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__params(self, source_mode: 'int') -> 'IStepParams':
        from s2marine.td.client.impl.direct.step_params_impl import StepParamsImpl
        return StepParamsImpl(self.com_obj.Params[source_mode]) if self.com_obj.Params else None
    
    @catch_com_error
    def get__result_location(self) -> 'str':
        return self.com_obj.ResultLocation
    
    @catch_com_error
    def get__status(self) -> 'str':
        return self.com_obj.Status
    
    @catch_com_error
    def set__status(self, value: 'str'):
        self.com_obj.Status = value
    
    @catch_com_error
    def get__step_factory(self) -> 'IStepFactory':
        from s2marine.td.client.impl.direct.step_factory_impl import StepFactoryImpl
        return StepFactoryImpl(self.com_obj.StepFactory) if self.com_obj.StepFactory else None
    
    @catch_com_error
    def get__test_id(self) -> 'int':
        return self.com_obj.TestId
    
    @catch_com_error
    def get__test_instance(self) -> 'int':
        return self.com_obj.TestInstance
    
    @catch_com_error
    def get__test_instance_id(self) -> 'int':
        return self.com_obj.TestInstanceID
    
    @catch_com_error
    def get__test_set_id(self) -> 'int':
        return self.com_obj.TestSetID
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def cancel_run(self):
        self.com_obj.CancelRun()

    @catch_com_error
    def copy_design_steps(self):
        self.com_obj.CopyDesignSteps()

    @catch_com_error
    def copy_steps_to_test(self):
        self.com_obj.CopyStepsToTest()

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def resolve_steps_parameters(self, update_local_cache: 'bool'):
        self.com_obj.ResolveStepsParameters(update_local_cache)

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
