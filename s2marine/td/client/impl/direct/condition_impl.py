from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_condition import ICondition
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class ConditionImpl(ICondition):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__description(self) -> 'str':
        return self.com_obj.Description
    
    @catch_com_error
    def set__description(self, value: 'str'):
        self.com_obj.Description = value
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def get__source(self) -> 'Any':
        return self.com_obj.Source
    
    @catch_com_error
    def get__target(self) -> 'Any':
        return self.com_obj.Target
    
    @catch_com_error
    def get__value(self) -> 'Any':
        return self.com_obj.Value
    
    @catch_com_error
    def set__value(self, value: 'Any'):
        self.com_obj.Value = value
    
    
