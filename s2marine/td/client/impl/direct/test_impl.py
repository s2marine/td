from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_test import ITest
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_design_step_factory import IDesignStepFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_vcs import IVCS
    


class TestImpl(ITest):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl
        return AttachmentFactoryImpl(self.com_obj.Attachments) if self.com_obj.Attachments else None
    
    @catch_com_error
    def is__auto_post(self) -> 'bool':
        return self.com_obj.AutoPost
    
    @catch_com_error
    def set__auto_post(self, value: 'bool'):
        self.com_obj.AutoPost = value
    
    @catch_com_error
    def get__checkout_path_if_exist(self) -> 'str':
        return self.com_obj.CheckoutPathIfExist
    
    @catch_com_error
    def get__design_step_factory(self) -> 'IDesignStepFactory':
        from s2marine.td.client.impl.direct.design_step_factory_impl import DesignStepFactoryImpl
        return DesignStepFactoryImpl(self.com_obj.DesignStepFactory) if self.com_obj.DesignStepFactory else None
    
    @catch_com_error
    def get__des_steps_num(self) -> 'int':
        return self.com_obj.DesStepsNum
    
    @catch_com_error
    def get__exec_date(self) -> 'float':
        return self.com_obj.ExecDate
    
    @catch_com_error
    def get__exec_status(self) -> 'str':
        return self.com_obj.ExecStatus
    
    @catch_com_error
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl
        return ExtendedStorageImpl(self.com_obj.ExtendedStorage) if self.com_obj.ExtendedStorage else None
    
    @catch_com_error
    def get__field(self, field_name: 'str') -> 'Any':
        return self.com_obj.Field[field_name]
    
    @catch_com_error
    def set__field(self, field_name: 'str', value: 'Any'):
        self.com_obj.Field[field_name] = value
    
    @catch_com_error
    def get__full_path(self) -> 'str':
        return self.com_obj.FullPath
    
    @catch_com_error
    def is__has_attachment(self) -> 'bool':
        return self.com_obj.HasAttachment
    
    @catch_com_error
    def is__has_coverage(self) -> 'bool':
        return self.com_obj.HasCoverage
    
    @catch_com_error
    def is__has_param(self) -> 'bool':
        return self.com_obj.HasParam
    
    @catch_com_error
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.direct.history_impl import HistoryImpl
        return HistoryImpl(self.com_obj.History) if self.com_obj.History else None
    
    @catch_com_error
    def get__id(self) -> 'Any':
        return self.com_obj.ID
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__last_run(self) -> 'IRun':
        from s2marine.td.client.impl.direct.run_impl import RunImpl
        return RunImpl(self.com_obj.LastRun) if self.com_obj.LastRun else None
    
    @catch_com_error
    def is__modified(self) -> 'bool':
        return self.com_obj.Modified
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__params(self) -> 'IStepParams':
        from s2marine.td.client.impl.direct.step_params_impl import StepParamsImpl
        return StepParamsImpl(self.com_obj.Params) if self.com_obj.Params else None
    
    @catch_com_error
    def is__template_test(self) -> 'bool':
        return self.com_obj.TemplateTest
    
    @catch_com_error
    def set__template_test(self, value: 'bool'):
        self.com_obj.TemplateTest = value
    
    @catch_com_error
    def get__type(self) -> 'str':
        return self.com_obj.Type
    
    @catch_com_error
    def set__type(self, value: 'str'):
        self.com_obj.Type = value
    
    @catch_com_error
    def get__vcs(self) -> 'IVCS':
        from s2marine.td.client.impl.direct.vcs_impl import VCSImpl
        return VCSImpl(self.com_obj.VCS) if self.com_obj.VCS else None
    
    @catch_com_error
    def is__virtual(self) -> 'bool':
        return self.com_obj.Virtual
    
    @catch_com_error
    def cover_requirement(self, req: 'Any', order: 'int', recursive: 'bool') -> 'int':
        return self.com_obj.CoverRequirement(req, order, recursive)

    @catch_com_error
    def get_cover_list(self) -> 'List[T]':
        return self.com_obj.GetCoverList()

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        self.com_obj.Mail(send_to, send_cc, option, subject, comment)

    @catch_com_error
    def post(self):
        self.com_obj.Post()

    @catch_com_error
    def refresh(self):
        self.com_obj.Refresh()

    @catch_com_error
    def remove_coverage(self, req: 'Any'):
        self.com_obj.RemoveCoverage(req)

    @catch_com_error
    def undo(self):
        self.com_obj.Undo()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
