from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules
    


class CustomizationPermissionsImpl(ICustomizationPermissions):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__can_add_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        return self.com_obj.CanAddItem[entity_name, group]
    
    @catch_com_error
    def set__can_add_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        self.com_obj.CanAddItem[entity_name, group] = value
    
    @catch_com_error
    def get__can_allow_attachment(self, entity_name: 'str', group: 'Any') -> 'int':
        return self.com_obj.CanAllowAttachment[entity_name, group]
    
    @catch_com_error
    def set__can_allow_attachment(self, entity_name: 'str', group: 'Any', value: 'int'):
        self.com_obj.CanAllowAttachment[entity_name, group] = value
    
    @catch_com_error
    def get__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'int':
        return self.com_obj.CanModifyField[entity_name, field, group]
    
    @catch_com_error
    def set__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'int'):
        self.com_obj.CanModifyField[entity_name, field, group] = value
    
    @catch_com_error
    def is__can_modify_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        return self.com_obj.CanModifyItem[entity_name, group]
    
    @catch_com_error
    def set__can_modify_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        self.com_obj.CanModifyItem[entity_name, group] = value
    
    @catch_com_error
    def get__can_remove_item(self, entity_name: 'str', group: 'Any') -> 'int':
        return self.com_obj.CanRemoveItem[entity_name, group]
    
    @catch_com_error
    def set__can_remove_item(self, entity_name: 'str', group: 'Any', value: 'int'):
        self.com_obj.CanRemoveItem[entity_name, group] = value
    
    @catch_com_error
    def is__can_use_owner_sensible(self, entity_name: 'str') -> 'bool':
        return self.com_obj.CanUseOwnerSensible[entity_name]
    
    @catch_com_error
    def is__has_attachment_field(self, entity_name: 'str') -> 'bool':
        return self.com_obj.HasAttachmentField[entity_name]
    
    @catch_com_error
    def is__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'bool':
        return self.com_obj.IsVisibleInNewBug[entity_name, field, group]
    
    @catch_com_error
    def set__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'bool'):
        self.com_obj.IsVisibleInNewBug[entity_name, field, group] = value
    
    @catch_com_error
    def get__transition_rules(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'ICustomizationTransitionRules':
        from s2marine.td.client.impl.direct.customization_transition_rules_impl import CustomizationTransitionRulesImpl
        return CustomizationTransitionRulesImpl(self.com_obj.TransitionRules[entity_name, field, group]) if self.com_obj.TransitionRules else None
    
    
