from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization import ICustomization
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_actions import ICustomizationActions
    from s2marine.td.client.intf.i_customization_fields import ICustomizationFields
    from s2marine.td.client.intf.i_customization_lists import ICustomizationLists
    from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions
    from s2marine.td.client.intf.i_customization_modules import ICustomizationModules
    from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions
    from s2marine.td.client.intf.i_customization_users import ICustomizationUsers
    from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups
    


class CustomizationImpl(ICustomization):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__actions(self) -> 'ICustomizationActions':
        from s2marine.td.client.impl.direct.customization_actions_impl import CustomizationActionsImpl
        return CustomizationActionsImpl(self.com_obj.Actions) if self.com_obj.Actions else None
    
    @catch_com_error
    def get__extended_udf_support(self) -> 'int':
        return self.com_obj.ExtendedUDFSupport
    
    @catch_com_error
    def get__fields(self) -> 'ICustomizationFields':
        from s2marine.td.client.impl.direct.customization_fields_impl import CustomizationFieldsImpl
        return CustomizationFieldsImpl(self.com_obj.Fields) if self.com_obj.Fields else None
    
    @catch_com_error
    def is__is_changed(self) -> 'bool':
        return self.com_obj.IsChanged
    
    @catch_com_error
    def is__is_locked(self) -> 'bool':
        return self.com_obj.IsLocked
    
    @catch_com_error
    def get__lists(self) -> 'ICustomizationLists':
        from s2marine.td.client.impl.direct.customization_lists_impl import CustomizationListsImpl
        return CustomizationListsImpl(self.com_obj.Lists) if self.com_obj.Lists else None
    
    @catch_com_error
    def get__mail_conditions(self) -> 'ICustomizationMailConditions':
        from s2marine.td.client.impl.direct.customization_mail_conditions_impl import CustomizationMailConditionsImpl
        return CustomizationMailConditionsImpl(self.com_obj.MailConditions) if self.com_obj.MailConditions else None
    
    @catch_com_error
    def get__modules(self) -> 'ICustomizationModules':
        from s2marine.td.client.impl.direct.customization_modules_impl import CustomizationModulesImpl
        return CustomizationModulesImpl(self.com_obj.Modules) if self.com_obj.Modules else None
    
    @catch_com_error
    def get__permissions(self) -> 'ICustomizationPermissions':
        from s2marine.td.client.impl.direct.customization_permissions_impl import CustomizationPermissionsImpl
        return CustomizationPermissionsImpl(self.com_obj.Permissions) if self.com_obj.Permissions else None
    
    @catch_com_error
    def get__users(self) -> 'ICustomizationUsers':
        from s2marine.td.client.impl.direct.customization_users_impl import CustomizationUsersImpl
        return CustomizationUsersImpl(self.com_obj.Users) if self.com_obj.Users else None
    
    @catch_com_error
    def get__users_groups(self) -> 'ICustomizationUsersGroups':
        from s2marine.td.client.impl.direct.customization_users_groups_impl import CustomizationUsersGroupsImpl
        return CustomizationUsersGroupsImpl(self.com_obj.UsersGroups) if self.com_obj.UsersGroups else None
    
    @catch_com_error
    def commit(self):
        self.com_obj.Commit()

    @catch_com_error
    def load(self):
        self.com_obj.Load()

    @catch_com_error
    def lock_object(self) -> 'bool':
        return self.com_obj.LockObject()

    @catch_com_error
    def rollback(self):
        self.com_obj.Rollback()

    @catch_com_error
    def un_lock_object(self):
        self.com_obj.UnLockObject()

    
