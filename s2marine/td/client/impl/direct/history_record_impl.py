from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_history_record import IHistoryRecord
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    


class HistoryRecordImpl(IHistoryRecord):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__change_date(self) -> 'float':
        return self.com_obj.ChangeDate
    
    @catch_com_error
    def get__changer(self) -> 'str':
        return self.com_obj.Changer
    
    @catch_com_error
    def get__field_name(self) -> 'str':
        return self.com_obj.FieldName
    
    @catch_com_error
    def get__item_key(self) -> 'Any':
        return self.com_obj.ItemKey
    
    @catch_com_error
    def get__new_value(self) -> 'Any':
        return self.com_obj.NewValue
    
    @catch_com_error
    def get__old_value(self) -> 'Any':
        return self.com_obj.OldValue
    
    
