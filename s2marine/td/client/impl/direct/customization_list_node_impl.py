from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class CustomizationListNodeImpl(ICustomizationListNode):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def is__can_add_child(self) -> 'bool':
        return self.com_obj.CanAddChild
    
    @catch_com_error
    def get__child(self, node_name: 'str') -> 'ICustomizationListNode':
        return CustomizationListNodeImpl(self.com_obj.Child[node_name]) if self.com_obj.Child else None
    
    @catch_com_error
    def get__children(self) -> 'List[T]':
        return self.com_obj.Children
    
    @catch_com_error
    def get__children_count(self) -> 'int':
        return self.com_obj.ChildrenCount
    
    @catch_com_error
    def is__deleted(self) -> 'bool':
        return self.com_obj.Deleted
    
    @catch_com_error
    def set__deleted(self, value: 'bool'):
        self.com_obj.Deleted = value
    
    @catch_com_error
    def get__father(self) -> 'ICustomizationListNode':
        return CustomizationListNodeImpl(self.com_obj.Father) if self.com_obj.Father else None
    
    @catch_com_error
    def set__father(self, value: 'ICustomizationListNode'):
        self.com_obj.Father = value
    
    @catch_com_error
    def get__id(self) -> 'int':
        return self.com_obj.ID
    
    @catch_com_error
    def set__id(self, value: 'int'):
        self.com_obj.ID = value
    
    @catch_com_error
    def get__list(self) -> 'ICustomizationList':
        from s2marine.td.client.impl.direct.customization_list_impl import CustomizationListImpl
        return CustomizationListImpl(self.com_obj.List) if self.com_obj.List else None
    
    @catch_com_error
    def get__name(self) -> 'str':
        return self.com_obj.Name
    
    @catch_com_error
    def set__name(self, value: 'str'):
        self.com_obj.Name = value
    
    @catch_com_error
    def get__order(self) -> 'int':
        return self.com_obj.Order
    
    @catch_com_error
    def set__order(self, value: 'int'):
        self.com_obj.Order = value
    
    @catch_com_error
    def is__read_only(self) -> 'bool':
        return self.com_obj.ReadOnly
    
    @catch_com_error
    def is__updated(self) -> 'bool':
        return self.com_obj.Updated
    
    @catch_com_error
    def set__updated(self, value: 'bool'):
        self.com_obj.Updated = value
    
    @catch_com_error
    def add_child(self, node: 'Any') -> 'T':
        return self.com_obj.AddChild(node)

    @catch_com_error
    def remove_child(self, node: 'Any'):
        self.com_obj.RemoveChild(node)

    
