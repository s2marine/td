from typing import List, Any, TYPE_CHECKING

from s2marine.td.client.intf.i_command import ICommand
from s2marine.td.client.utils.exception import catch_com_error

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_recordset import IRecordset


class CommandImpl(ICommand):

    def __init__(self, com_obj):
        self.com_obj = com_obj
    
    @catch_com_error
    def get__affected_rows(self) -> 'int':
        return self.com_obj.AffectedRows
    
    @catch_com_error
    def get__command_text(self) -> 'str':
        return self.com_obj.CommandText
    
    @catch_com_error
    def set__command_text(self, value: 'str'):
        self.com_obj.CommandText = value
    
    @catch_com_error
    def get__count(self) -> 'int':
        return self.com_obj.Count
    
    @catch_com_error
    def get__index_fields(self) -> 'str':
        return self.com_obj.IndexFields
    
    @catch_com_error
    def set__index_fields(self, value: 'str'):
        self.com_obj.IndexFields = value
    
    @catch_com_error
    def get__param_index(self, name: 'str') -> 'int':
        return self.com_obj.ParamIndex[name]
    
    @catch_com_error
    def get__param_name(self, index: 'int') -> 'str':
        return self.com_obj.ParamName[index]
    
    @catch_com_error
    def get__param_type(self, index: 'int') -> 'int':
        return self.com_obj.ParamType[index]
    
    @catch_com_error
    def get__param_value(self, key: 'Any') -> 'Any':
        return self.com_obj.ParamValue[key]
    
    @catch_com_error
    def set__param_value(self, key: 'Any', value: 'Any'):
        self.com_obj.ParamValue[key] = value
    
    @catch_com_error
    def add_param(self, name: 'str', initial_value: 'Any'):
        self.com_obj.AddParam(name, initial_value)

    @catch_com_error
    def delete_param(self, key: 'Any'):
        self.com_obj.DeleteParam(key)

    @catch_com_error
    def delete_params(self):
        self.com_obj.DeleteParams()

    @catch_com_error
    def execute(self) -> 'IRecordset':
        from s2marine.td.client.impl.direct.recordset_impl import RecordsetImpl
        return RecordsetImpl(self.com_obj.Execute()) if self.com_obj.Execute else None

    
