from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_product_info import IProductInfo

if TYPE_CHECKING:
    pass
    
    


class HostProductInfo(IProductInfo):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__bpt_version(self) -> 'int':
        pass
    
    @pipe_request()
    def get__qc_version(self, pn_major_version: 'int', pn_minor_version: 'int') -> 'int':
        pass
    
    

