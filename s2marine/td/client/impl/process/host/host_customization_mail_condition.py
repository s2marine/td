from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition

if TYPE_CHECKING:
    pass
    
    


class HostCustomizationMailCondition(ICustomizationMailCondition):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__condition_text(self) -> 'str':
        pass
    
    @pipe_request()
    def set__condition_text(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__condition_type(self) -> 'int':
        pass
    
    @pipe_request()
    def is__deleted(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__deleted(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    

