from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo

if TYPE_CHECKING:
    pass
    
    


class HostExecEventInfo(IExecEventInfo):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__event_date(self) -> 'str':
        pass
    
    @pipe_request()
    def get__event_time(self) -> 'str':
        pass
    
    @pipe_request()
    def get__event_type(self) -> 'int':
        pass
    
    

