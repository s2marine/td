from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_test_factory import ITestFactory

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    


class HostTestFactory(ITestFactory):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__fetch_level(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self) -> 'List[ITDField]':
        pass
    
    @pipe_request()
    def get__filter(self) -> 'TDFilter':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request(ret_pkg='host_test', ret_class='HostTest')
    def get__item(self, item_key: 'Any') -> 'ITest':
        pass
    
    @pipe_request(ret_pkg='host_extended_storage', ret_class='HostExtendedStorage')
    def get__repository_storage(self) -> 'IExtendedStorage':
        pass
    
    @pipe_request()
    def add_item(self, item_data: 'Any') -> 'T':
        pass

    @pipe_request()
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        pass

    @pipe_request()
    def new_list(self, filter: 'str') -> 'List[T]':
        pass

    @pipe_request()
    def remove_item(self, item_key: 'Any'):
        pass

    

