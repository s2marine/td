from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_text_parser import ITextParser

if TYPE_CHECKING:
    pass
    
    


class HostTextParser(ITextParser):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__param_exist(self, param_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def get__param_name(self, n_position: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__param_type(self, v_param: 'Any') -> 'str':
        pass
    
    @pipe_request()
    def get__param_value(self, v_param: 'Any') -> 'str':
        pass
    
    @pipe_request()
    def set__param_value(self, v_param: 'Any', value: 'str'):
        pass
    
    @pipe_request()
    def get__text(self) -> 'str':
        pass
    
    @pipe_request()
    def set__text(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__type(self, v_param: 'Any') -> 'int':
        pass
    
    @pipe_request()
    def clear_param(self, v_param: 'Any'):
        pass

    @pipe_request()
    def evaluate_text(self):
        pass

    @pipe_request()
    def initialize(self, start_close: 'str', end_close: 'str', type_close: 'str', max_len: 'int', default_type: 'str'):
        pass

    

