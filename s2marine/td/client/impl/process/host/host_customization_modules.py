from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_modules import ICustomizationModules

if TYPE_CHECKING:
    pass
    
    


class HostCustomizationModules(ICustomizationModules):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__description(self, module_id: 'int') -> 'str':
        pass
    
    @pipe_request()
    def set__description(self, module_id: 'int', value: 'str'):
        pass
    
    @pipe_request()
    def get__guid(self, module_id: 'int') -> 'str':
        pass
    
    @pipe_request()
    def set__guid(self, module_id: 'int', value: 'str'):
        pass
    
    @pipe_request()
    def is__is_visible_for_group(self, module_id: 'int', group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__is_visible_for_group(self, module_id: 'int', group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__name(self, module_id: 'int') -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, module_id: 'int', value: 'str'):
        pass
    
    @pipe_request()
    def get__visible(self, module_id: 'int') -> 'int':
        pass
    
    @pipe_request()
    def set__visible(self, module_id: 'int', value: 'int'):
        pass
    
    @pipe_request()
    def get__visible_for_groups(self, module_id: 'int') -> 'List[T]':
        pass
    
    

