from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule

if TYPE_CHECKING:
    pass
    
    


class HostCustomizationTransitionRule(ICustomizationTransitionRule):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__destination_value(self) -> 'str':
        pass
    
    @pipe_request()
    def set__destination_value(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__source_value(self) -> 'str':
        pass
    
    @pipe_request()
    def set__source_value(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    

