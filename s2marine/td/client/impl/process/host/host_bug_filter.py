from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_bug_filter import IBugFilter

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_bug import IBug


class HostBugFilter(IBugFilter):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__case_sensitive(self, field_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def set__case_sensitive(self, field_name: 'str', value: 'bool'):
        pass
    
    @pipe_request()
    def get__data_type(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self) -> 'List[ITDField]':
        pass
    
    @pipe_request()
    def get__filter(self, field_name: 'str') -> 'str':
        pass
    
    @pipe_request()
    def set__filter(self, field_name: 'str', value: 'str'):
        pass
    
    @pipe_request()
    def get__order(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__order(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request()
    def get__order_direction(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__order_direction(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request()
    def get__text(self) -> 'str':
        pass
    
    @pipe_request()
    def set__text(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__x_filter(self, entity_type: 'str') -> 'str':
        pass
    
    @pipe_request()
    def set__x_filter(self, entity_type: 'str', value: 'str'):
        pass
    
    @pipe_request()
    def clear(self):
        pass

    @pipe_request()
    def get_x_filter(self, join_entities: 'str', inclusive: 'bool') -> 'str':
        pass

    @pipe_request()
    def is_clear(self) -> 'bool':
        pass

    @pipe_request(ret_pkg='host_bug', ret_class='HostBug')
    def new_list(self) -> 'List[IBug]':
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def set_x_filter(self, join_entities: 'str', inclusive: 'bool', filter_text: 'str'):
        pass

    

