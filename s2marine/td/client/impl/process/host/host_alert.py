from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_alert import IAlert

if TYPE_CHECKING:
    pass
    
    


class HostAlert(IAlert):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__alert_date(self) -> 'float':
        pass
    
    @pipe_request()
    def get__alert_type(self) -> 'int':
        pass
    
    @pipe_request()
    def get__description(self) -> 'str':
        pass
    
    @pipe_request()
    def get__id(self) -> 'int':
        pass
    
    @pipe_request()
    def get__subject(self) -> 'str':
        pass
    
    @pipe_request()
    def is__unread(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__unread(self, value: 'bool'):
        pass
    
    

