from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_exec_settings import IExecSettings

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams
    


class HostExecSettings(IExecSettings):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_exec_event_action_params', ret_class='HostExecEventActionParams')
    def get__on_exec_event_scheduler_action_params(self, event_type: 'int') -> 'IExecEventActionParams':
        pass
    
    @pipe_request()
    def get__on_exec_event_scheduler_action_type(self, event_type: 'int') -> 'int':
        pass
    
    @pipe_request()
    def set__on_exec_event_scheduler_action_type(self, event_type: 'int', value: 'int'):
        pass
    
    @pipe_request()
    def get__planned_execution_date(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__planned_execution_date(self, value: 'Any'):
        pass
    
    @pipe_request()
    def get__planned_execution_time(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__planned_execution_time(self, value: 'Any'):
        pass
    
    @pipe_request()
    def get__planned_run_duration(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__planned_run_duration(self, value: 'Any'):
        pass
    
    

