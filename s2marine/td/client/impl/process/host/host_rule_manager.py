from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_rule_manager import IRuleManager

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_rule import IRule


class HostRuleManager(IRuleManager):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__rules(self, need_refresh: 'bool') -> 'List[T]':
        pass
    
    @pipe_request(ret_pkg='host_rule', ret_class='HostRule')
    def get_rule(self, id: 'int') -> 'IRule':
        pass

    

