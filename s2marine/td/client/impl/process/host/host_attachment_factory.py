from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_attachment_filter import IAttachmentFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_attachment import IAttachment
    
    from s2marine.td.client.intf.i_attachment import IAttachment
    from s2marine.td.client.intf.i_attachment import IAttachment


class HostAttachmentFactory(IAttachmentFactory):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_extended_storage', ret_class='HostExtendedStorage')
    def get__attachment_storage(self) -> 'IExtendedStorage':
        pass
    
    @pipe_request()
    def get__fetch_level(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self) -> 'List[ITDField]':
        pass
    
    @pipe_request(ret_pkg='host_attachment_filter', ret_class='HostAttachmentFilter')
    def get__filter(self) -> 'IAttachmentFilter':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request(ret_pkg='host_attachment', ret_class='HostAttachment')
    def get__item(self, item_key: 'Any') -> 'IAttachment':
        pass
    
    @pipe_request(ret_pkg='host_attachment', ret_class='HostAttachment')
    def add_item(self, item_data: 'Any') -> 'IAttachment':
        pass

    @pipe_request()
    def factory_properties(self, owner_type: 'str', owner_key: 'Any'):
        pass

    @pipe_request(ret_pkg='host_attachment', ret_class='HostAttachment')
    def new_list(self, filter: 'str') -> 'List[IAttachment]':
        pass

    @pipe_request()
    def remove_item(self, item_key: 'Any'):
        pass

    

