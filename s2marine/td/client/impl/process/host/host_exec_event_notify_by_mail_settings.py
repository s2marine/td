from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings

if TYPE_CHECKING:
    pass
    
    


class HostExecEventNotifyByMailSettings(IExecEventNotifyByMailSettings):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__e_mail_to(self) -> 'str':
        pass
    
    @pipe_request()
    def set__e_mail_to(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__enabled(self, event_type: 'int') -> 'bool':
        pass
    
    @pipe_request()
    def set__enabled(self, event_type: 'int', value: 'bool'):
        pass
    
    @pipe_request()
    def get__user_message(self) -> 'str':
        pass
    
    @pipe_request()
    def set__user_message(self, value: 'str'):
        pass
    
    @pipe_request()
    def save(self, auto_post: 'bool'):
        pass

    

