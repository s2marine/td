from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_settings import ISettings

if TYPE_CHECKING:
    pass
    
    


class HostSettings(ISettings):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__enum_items(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def is__is_system(self, name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def get__value(self, name: 'str') -> 'str':
        pass
    
    @pipe_request()
    def set__value(self, name: 'str', value: 'str'):
        pass
    
    @pipe_request()
    def close(self):
        pass

    @pipe_request()
    def delete_category(self, category: 'str'):
        pass

    @pipe_request()
    def delete_value(self, name: 'str'):
        pass

    @pipe_request()
    def open(self, category: 'str'):
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self, category: 'str'):
        pass

    

