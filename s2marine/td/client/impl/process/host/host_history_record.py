from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_history_record import IHistoryRecord

if TYPE_CHECKING:
    pass
    
    


class HostHistoryRecord(IHistoryRecord):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__change_date(self) -> 'float':
        pass
    
    @pipe_request()
    def get__changer(self) -> 'str':
        pass
    
    @pipe_request()
    def get__field_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__item_key(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__new_value(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__old_value(self) -> 'Any':
        pass
    
    

