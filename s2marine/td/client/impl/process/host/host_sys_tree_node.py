from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode


class HostSysTreeNode(ISysTreeNode):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__attribute(self) -> 'int':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        pass
    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def get__depth_type(self) -> 'int':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__father(self) -> 'ISysTreeNode':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__node_id(self) -> 'int':
        pass
    
    @pipe_request()
    def get__path(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        pass

    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        pass

    @pipe_request()
    def find_children(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        pass

    @pipe_request()
    def new_list(self) -> 'List[T]':
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def remove_node(self, node: 'Any'):
        pass

    

