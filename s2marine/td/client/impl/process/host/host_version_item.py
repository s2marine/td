from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_version_item import IVersionItem

if TYPE_CHECKING:
    pass
    
    


class HostVersionItem(IVersionItem):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__comments(self) -> 'str':
        pass
    
    @pipe_request()
    def get__date(self) -> 'str':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__time(self) -> 'str':
        pass
    
    @pipe_request()
    def get__user(self) -> 'str':
        pass
    
    @pipe_request()
    def get__version(self) -> 'str':
        pass
    
    

