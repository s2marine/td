from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_action import ICustomizationAction

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class HostCustomizationAction(ICustomizationAction):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        pass
    
    @pipe_request()
    def is__is_group_permited(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def is__is_owner_group(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def is__is_user_permited(self, user: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__owner_group(self) -> 'ICustomizationUsersGroup':
        pass
    
    @pipe_request()
    def set__owner_group(self, value: 'ICustomizationUsersGroup'):
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request()
    def add_group(self, group: 'Any'):
        pass

    @pipe_request()
    def remove_group(self, group: 'Any'):
        pass

    

