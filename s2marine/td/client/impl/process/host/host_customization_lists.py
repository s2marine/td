from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_lists import ICustomizationLists

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class HostCustomizationLists(ICustomizationLists):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_list_exist(self, list_name: 'str') -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_customization_list', ret_class='HostCustomizationList')
    def get__list(self, param: 'Any') -> 'ICustomizationList':
        pass
    
    @pipe_request(ret_pkg='host_customization_list', ret_class='HostCustomizationList')
    def get__list_by_count(self, count: 'int') -> 'ICustomizationList':
        pass
    
    @pipe_request()
    def add_list(self, name: 'str') -> 'T':
        pass

    @pipe_request()
    def remove_list(self, name: 'str'):
        pass

    

