from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_field_property import IFieldProperty

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class HostFieldProperty(IFieldProperty):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__db_column_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__db_column_type(self) -> 'str':
        pass
    
    @pipe_request()
    def get__db_table_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__edit_mask(self) -> 'str':
        pass
    
    @pipe_request()
    def get__edit_style(self) -> 'str':
        pass
    
    @pipe_request()
    def get__field_size(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_active(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_by_code(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_can_filter(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_can_group(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_customizable(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_edit(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_history(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_keep_value(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_key(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_mail(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_modify(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_multi_value(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_required(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_searchable(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_system(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_to_sum(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_verify(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_version_control(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_visible_in_new_bug(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__read_only(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__root(self) -> 'ISysTreeNode':
        pass
    
    @pipe_request()
    def get__user_column_type(self) -> 'str':
        pass
    
    @pipe_request()
    def get__user_label(self) -> 'str':
        pass
    
    

