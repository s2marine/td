from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_test import ITest

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_design_step_factory import IDesignStepFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_vcs import IVCS
    


class HostTest(ITest):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_attachment_factory', ret_class='HostAttachmentFactory')
    def get__attachments(self) -> 'IAttachmentFactory':
        pass
    
    @pipe_request()
    def is__auto_post(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__auto_post(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__checkout_path_if_exist(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_design_step_factory', ret_class='HostDesignStepFactory')
    def get__design_step_factory(self) -> 'IDesignStepFactory':
        pass
    
    @pipe_request()
    def get__des_steps_num(self) -> 'int':
        pass
    
    @pipe_request()
    def get__exec_date(self) -> 'float':
        pass
    
    @pipe_request()
    def get__exec_status(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_extended_storage', ret_class='HostExtendedStorage')
    def get__extended_storage(self) -> 'IExtendedStorage':
        pass
    
    @pipe_request()
    def get__field(self, field_name: 'str') -> 'Any':
        pass
    
    @pipe_request()
    def set__field(self, field_name: 'str', value: 'Any'):
        pass
    
    @pipe_request()
    def get__full_path(self) -> 'str':
        pass
    
    @pipe_request()
    def is__has_attachment(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__has_coverage(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__has_param(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_run', ret_class='HostRun')
    def get__last_run(self) -> 'IRun':
        pass
    
    @pipe_request()
    def is__modified(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_step_params', ret_class='HostStepParams')
    def get__params(self) -> 'IStepParams':
        pass
    
    @pipe_request()
    def is__template_test(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__template_test(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__type(self) -> 'str':
        pass
    
    @pipe_request()
    def set__type(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_vcs', ret_class='HostVCS')
    def get__vcs(self) -> 'IVCS':
        pass
    
    @pipe_request()
    def is__virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def cover_requirement(self, req: 'Any', order: 'int', recursive: 'bool') -> 'int':
        pass

    @pipe_request()
    def get_cover_list(self) -> 'List[T]':
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def remove_coverage(self, req: 'Any'):
        pass

    @pipe_request()
    def undo(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

