from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_execution_status import IExecutionStatus
    


class HostTSScheduler(ITSScheduler):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__execution_log(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_execution_status', ret_class='HostExecutionStatus')
    def get__execution_status(self) -> 'IExecutionStatus':
        pass
    
    @pipe_request()
    def get__host_time_out(self) -> 'int':
        pass
    
    @pipe_request()
    def set__host_time_out(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__log_enabled(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__log_enabled(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__run_all_locally(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__run_all_locally(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__run_on_host(self, ts_test_id: 'Any') -> 'str':
        pass
    
    @pipe_request()
    def set__run_on_host(self, ts_test_id: 'Any', value: 'str'):
        pass
    
    @pipe_request()
    def get__td_host_name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__td_host_name(self, value: 'str'):
        pass
    
    @pipe_request()
    def run(self, test_data: 'Any'):
        pass

    @pipe_request()
    def stop(self, test_data: 'Any'):
        pass

    

