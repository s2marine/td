from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_run_factory import IRunFactory

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    


class HostRunFactory(IRunFactory):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__fetch_level(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self) -> 'List[ITDField]':
        pass
    
    @pipe_request()
    def get__filter(self) -> 'TDFilter':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request(ret_pkg='host_run', ret_class='HostRun')
    def get__item(self, item_key: 'Any') -> 'IRun':
        pass
    
    @pipe_request()
    def get__unique_run_name(self) -> 'str':
        pass
    
    @pipe_request()
    def add_item(self, item_data: 'Any') -> 'T':
        pass

    @pipe_request()
    def delete_duplicate_runs(self, run_name: 'str'):
        pass

    @pipe_request()
    def new_list(self, filter: 'str') -> 'List[T]':
        pass

    @pipe_request()
    def remove_item(self, item_key: 'Any'):
        pass

    

