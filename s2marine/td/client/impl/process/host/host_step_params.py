from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_step_params import IStepParams

if TYPE_CHECKING:
    pass
    
    


class HostStepParams(IStepParams):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__param_exist(self, param_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def get__param_name(self, n_position: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__param_value(self, v_param: 'Any') -> 'str':
        pass
    
    @pipe_request()
    def set__param_value(self, v_param: 'Any', value: 'str'):
        pass
    
    @pipe_request()
    def get__type(self, v_param: 'Any') -> 'int':
        pass
    
    @pipe_request()
    def add_param(self, param_name: 'str', param_type: 'str'):
        pass

    @pipe_request()
    def clear_param(self, v_param: 'Any'):
        pass

    @pipe_request()
    def delete_param(self, param_name: 'str'):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def save(self):
        pass

    

