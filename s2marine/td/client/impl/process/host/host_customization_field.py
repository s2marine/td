from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_field import ICustomizationField

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class HostCustomizationField(ICustomizationField):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__authorized_modify_for_groups(self) -> 'List[ICustomizationUsersGroup]':
        pass
    
    @pipe_request()
    def get__authorized_owner_sensible_for_groups(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__authorized_visible_for_groups(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__authorized_visible_in_new_bug_for_groups(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def is__can_make_multi_value(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__can_make_searchable(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__column_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__column_type(self) -> 'str':
        pass
    
    @pipe_request()
    def set__column_type(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__default_value(self) -> 'str':
        pass
    
    @pipe_request()
    def set__default_value(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__edit_mask(self) -> 'str':
        pass
    
    @pipe_request()
    def set__edit_mask(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__field_size(self) -> 'int':
        pass
    
    @pipe_request()
    def set__field_size(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__grant_modify_for_group(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__grant_modify_for_group(self, group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__grant_modify_mask(self) -> 'int':
        pass
    
    @pipe_request()
    def set__grant_modify_mask(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__is_active(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_active(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_by_code(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_by_code(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_can_group(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_can_group(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_customizable(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_customizable(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_edit(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_edit(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_history(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_history(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_mail(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_mail(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_multi_value(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_multi_value(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_required(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_required(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_searchable(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_searchable(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_transition_logic(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_transition_logic(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_verify(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_verify(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__is_visible_in_new_bug(self) -> 'int':
        pass
    
    @pipe_request()
    def set__is_visible_in_new_bug(self, value: 'int'):
        pass
    
    @pipe_request(ret_pkg='host_customization_list', ret_class='HostCustomizationList')
    def get__list(self) -> 'ICustomizationList':
        pass
    
    @pipe_request()
    def set__list(self, value: 'ICustomizationList'):
        pass
    
    @pipe_request()
    def is__new_created(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__new_created(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__owner_sensible_for_group(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__owner_sensible_for_group(self, group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__owner_sensible_mask(self) -> 'int':
        pass
    
    @pipe_request()
    def set__owner_sensible_mask(self, value: 'int'):
        pass
    
    @pipe_request()
    def get__root_id(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__root_id(self, value: 'Any'):
        pass
    
    @pipe_request()
    def get__table_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__type(self) -> 'int':
        pass
    
    @pipe_request()
    def set__type(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__user_column_type(self) -> 'str':
        pass
    
    @pipe_request()
    def set__user_column_type(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__user_label(self) -> 'str':
        pass
    
    @pipe_request()
    def set__user_label(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__version_control(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__version_control(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__visible_for_group(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__visible_for_group(self, group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__visible_for_groups(self) -> 'int':
        pass
    
    @pipe_request()
    def set__visible_for_groups(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__visible_in_new_bug_for_group(self, group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__visible_in_new_bug_for_group(self, group: 'Any', value: 'bool'):
        pass
    
    

