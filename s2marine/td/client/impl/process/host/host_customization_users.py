from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_users import ICustomizationUsers

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    


class HostCustomizationUsers(ICustomizationUsers):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_user', ret_class='HostCustomizationUser')
    def get__user(self, name: 'str') -> 'ICustomizationUser':
        pass
    
    @pipe_request(ret_pkg='host_customization_user', ret_class='HostCustomizationUser')
    def get__users(self) -> 'List[ICustomizationUser]':
        pass
    
    @pipe_request()
    def add_site_authenticated_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', domain_authentication: 'str', group: 'Any'):
        pass

    @pipe_request()
    def add_site_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', group: 'Any'):
        pass

    @pipe_request()
    def add_user(self, name: 'str') -> 'T':
        pass

    @pipe_request()
    def remove_user(self, name: 'str'):
        pass

    

