from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_td_connection import ITDConnection

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_action_permission import IActionPermission
    from s2marine.td.client.intf.i_alert_manager import IAlertManager
    from s2marine.td.client.intf.i_audit_property_factory import IAuditPropertyFactory
    from s2marine.td.client.intf.i_audit_record_factory import IAuditRecordFactory
    from s2marine.td.client.intf.i_bug_factory import IBugFactory
    from s2marine.td.client.intf.i_command import ICommand
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_customization import ICustomization
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_graph_builder import IGraphBuilder
    from s2marine.td.client.intf.i_host_factory import IHostFactory
    from s2marine.td.client.intf.i_host_group_factory import IHostGroupFactory
    from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions
    from s2marine.td.client.intf.i_product_info import IProductInfo
    from s2marine.td.client.intf.i_project_properties import IProjectProperties
    from s2marine.td.client.intf.i_req_factory import IReqFactory
    from s2marine.td.client.intf.i_rule_manager import IRuleManager
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test_factory import ITestFactory
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager
    from s2marine.td.client.intf.i_text_parser import ITextParser
    from s2marine.td.client.intf.i_tree_manager import ITreeManager
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_vcs import IVCS
    


class HostTDConnection(ITDConnection):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_action_permission', ret_class='HostActionPermission')
    def get__action_permission(self) -> 'IActionPermission':
        pass
    
    @pipe_request(ret_pkg='host_alert_manager', ret_class='HostAlertManager')
    def get__alert_manager(self) -> 'IAlertManager':
        pass
    
    @pipe_request(ret_pkg='host_audit_property_factory', ret_class='HostAuditPropertyFactory')
    def get__audit_property_factory(self) -> 'IAuditPropertyFactory':
        pass
    
    @pipe_request(ret_pkg='host_audit_record_factory', ret_class='HostAuditRecordFactory')
    def get__audit_record_factory(self) -> 'IAuditRecordFactory':
        pass
    
    @pipe_request(ret_pkg='host_bug_factory', ret_class='HostBugFactory')
    def get__bug_factory(self) -> 'IBugFactory':
        pass
    
    @pipe_request()
    def get__checkout_repository(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_command', ret_class='HostCommand')
    def get__command(self) -> 'ICommand':
        pass
    
    @pipe_request(ret_pkg='host_settings', ret_class='HostSettings')
    def get__common_settings(self) -> 'ISettings':
        pass
    
    @pipe_request()
    def is__connected(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_customization', ret_class='HostCustomization')
    def get__customization(self) -> 'ICustomization':
        pass
    
    @pipe_request()
    def get__db_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__db_type(self) -> 'str':
        pass
    
    @pipe_request()
    def get__directory_path(self, n_type: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__domain_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__domains_list(self) -> 'List[T]':
        pass
    
    @pipe_request(ret_pkg='host_extended_storage', ret_class='HostExtendedStorage')
    def get__extended_storage(self) -> 'IExtendedStorage':
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self, data_type: 'str') -> 'List[ITDField]':
        pass
    
    @pipe_request(ret_pkg='host_graph_builder', ret_class='HostGraphBuilder')
    def get__graph_builder(self) -> 'IGraphBuilder':
        pass
    
    @pipe_request(ret_pkg='host_host_factory', ret_class='HostHostFactory')
    def get__host_factory(self) -> 'IHostFactory':
        pass
    
    @pipe_request(ret_pkg='host_host_group_factory', ret_class='HostHostGroupFactory')
    def get__host_group_factory(self) -> 'IHostGroupFactory':
        pass
    
    @pipe_request()
    def is__ignore_html_format(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__ignore_html_format(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__is_search_supported(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__logged_in(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_td_mail_conditions', ret_class='HostTDMailConditions')
    def get__mail_conditions(self) -> 'ITDMailConditions':
        pass
    
    @pipe_request(ret_pkg='host_product_info', ret_class='HostProductInfo')
    def get__product_info(self) -> 'IProductInfo':
        pass
    
    @pipe_request()
    def is__project_connected(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__project_name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_project_properties', ret_class='HostProjectProperties')
    def get__project_properties(self) -> 'IProjectProperties':
        pass
    
    @pipe_request()
    def get__projects_list(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__projects_list_ex(self, domain_name: 'str') -> 'List[T]':
        pass
    
    @pipe_request()
    def get__report_role(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_req_factory', ret_class='HostReqFactory')
    def get__req_factory(self) -> 'IReqFactory':
        pass
    
    @pipe_request(ret_pkg='host_rule_manager', ret_class='HostRuleManager')
    def get__rules(self) -> 'IRuleManager':
        pass
    
    @pipe_request(ret_pkg='host_run_factory', ret_class='HostRunFactory')
    def get__run_factory(self) -> 'IRunFactory':
        pass
    
    @pipe_request()
    def get__server_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__server_time(self) -> 'float':
        pass
    
    @pipe_request()
    def get__server_url(self) -> 'str':
        pass
    
    @pipe_request()
    def get__td_params(self, request: 'str') -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_test_factory', ret_class='HostTestFactory')
    def get__test_factory(self) -> 'ITestFactory':
        pass
    
    @pipe_request()
    def get__test_repository(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_test_set_factory', ret_class='HostTestSetFactory')
    def get__test_set_factory(self) -> 'ITestSetFactory':
        pass
    
    @pipe_request(ret_pkg='host_test_set_tree_manager', ret_class='HostTestSetTreeManager')
    def get__test_set_tree_manager(self) -> 'ITestSetTreeManager':
        pass
    
    @pipe_request(ret_pkg='host_text_parser', ret_class='HostTextParser')
    def get__text_param(self) -> 'ITextParser':
        pass
    
    @pipe_request(ret_pkg='host_tree_manager', ret_class='HostTreeManager')
    def get__tree_manager(self) -> 'ITreeManager':
        pass
    
    @pipe_request(ret_pkg='host_ts_test_factory', ret_class='HostTSTestFactory')
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        pass
    
    @pipe_request()
    def get__user_groups_list(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__user_name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_settings', ret_class='HostSettings')
    def get__user_settings(self) -> 'ISettings':
        pass
    
    @pipe_request()
    def get__users_list(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def is__using_progress(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__using_progress(self, value: 'bool'):
        pass
    
    @pipe_request(ret_pkg='host_vcs', ret_class='HostVCS')
    def get__vcs(self) -> 'IVCS':
        pass
    
    @pipe_request()
    def get__vcs_db_repository(self) -> 'str':
        pass
    
    @pipe_request()
    def get__visible_domains(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__visible_projects(self, domain_name: 'str') -> 'List[T]':
        pass
    
    @pipe_request()
    def change_password(self, old_password: 'str', new_password: 'str'):
        pass

    @pipe_request()
    def connect(self, domain_name: 'str', project_name: 'str'):
        pass

    @pipe_request()
    def connect_project(self, project_name: 'str', user_name: 'str', password: 'str'):
        pass

    @pipe_request()
    def connect_project_ex(self, domain_name: 'str', project_name: 'str', user_name: 'str', password: 'str'):
        pass

    @pipe_request()
    def disconnect(self):
        pass

    @pipe_request()
    def disconnect_project(self):
        pass

    @pipe_request()
    def get_licenses(self, licenses_type: 'int', p_val: 'str'):
        pass

    @pipe_request()
    def get_license_status(self, client_type: 'int', in_use: 'int', max: 'int'):
        pass

    @pipe_request()
    def get_td_version(self, pbs_major_version: 'str', pbs_build_num: 'str'):
        pass

    @pipe_request()
    def init_connection_ex(self, server_name: 'str'):
        pass

    @pipe_request()
    def login(self, user_name: 'str', password: 'str'):
        pass

    @pipe_request()
    def logout(self):
        pass

    @pipe_request()
    def purge_runs(self, test_set_filter: 'str', keep_last: 'int', date_unit: 'Any', unit_count: 'int', steps_only: 'bool'):
        pass

    @pipe_request()
    def release_connection(self):
        pass

    @pipe_request()
    def send_mail(self, send_to: 'str', send_from: 'str', subject: 'str', message: 'str', attach_array: 'Any', bs_format: 'str'):
        pass

    

