from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules
    


class HostCustomizationPermissions(ICustomizationPermissions):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__can_add_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__can_add_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__can_allow_attachment(self, entity_name: 'str', group: 'Any') -> 'int':
        pass
    
    @pipe_request()
    def set__can_allow_attachment(self, entity_name: 'str', group: 'Any', value: 'int'):
        pass
    
    @pipe_request()
    def get__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'int':
        pass
    
    @pipe_request()
    def set__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'int'):
        pass
    
    @pipe_request()
    def is__can_modify_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__can_modify_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__can_remove_item(self, entity_name: 'str', group: 'Any') -> 'int':
        pass
    
    @pipe_request()
    def set__can_remove_item(self, entity_name: 'str', group: 'Any', value: 'int'):
        pass
    
    @pipe_request()
    def is__can_use_owner_sensible(self, entity_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def is__has_attachment_field(self, entity_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def is__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'bool'):
        pass
    
    @pipe_request(ret_pkg='host_customization_transition_rules', ret_class='HostCustomizationTransitionRules')
    def get__transition_rules(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'ICustomizationTransitionRules':
        pass
    
    

