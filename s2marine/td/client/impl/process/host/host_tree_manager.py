from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_tree_manager import ITreeManager

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class HostTreeManager(ITreeManager):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__node_by_id(self, node_id: 'int') -> 'ISysTreeNode':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__node_by_path(self, path: 'str') -> 'ISysTreeNode':
        pass
    
    @pipe_request()
    def get__node_path(self, node_id: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__root_list(self, val: 'int') -> 'List[T]':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__tree_root(self, root_name: 'str') -> 'ISysTreeNode':
        pass
    
    

