from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams

if TYPE_CHECKING:
    pass
    
    


class HostExecEventActionParams(IExecEventActionParams):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__on_exec_event_scheduler_action(self) -> 'int':
        pass
    
    

