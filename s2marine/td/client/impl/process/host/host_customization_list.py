from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_list import ICustomizationList

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode


class HostCustomizationList(ICustomizationList):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__deleted(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__deleted(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_customization_list_node', ret_class='HostCustomizationListNode')
    def get__root_node(self) -> 'ICustomizationListNode':
        pass
    
    @pipe_request(ret_pkg='host_customization_list_node', ret_class='HostCustomizationListNode')
    def find(self, val: 'Any') -> 'ICustomizationListNode':
        pass

    

