from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization import ICustomization

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_actions import ICustomizationActions
    from s2marine.td.client.intf.i_customization_fields import ICustomizationFields
    from s2marine.td.client.intf.i_customization_lists import ICustomizationLists
    from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions
    from s2marine.td.client.intf.i_customization_modules import ICustomizationModules
    from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions
    from s2marine.td.client.intf.i_customization_users import ICustomizationUsers
    from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups
    


class HostCustomization(ICustomization):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_actions', ret_class='HostCustomizationActions')
    def get__actions(self) -> 'ICustomizationActions':
        pass
    
    @pipe_request()
    def get__extended_udf_support(self) -> 'int':
        pass
    
    @pipe_request(ret_pkg='host_customization_fields', ret_class='HostCustomizationFields')
    def get__fields(self) -> 'ICustomizationFields':
        pass
    
    @pipe_request()
    def is__is_changed(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_customization_lists', ret_class='HostCustomizationLists')
    def get__lists(self) -> 'ICustomizationLists':
        pass
    
    @pipe_request(ret_pkg='host_customization_mail_conditions', ret_class='HostCustomizationMailConditions')
    def get__mail_conditions(self) -> 'ICustomizationMailConditions':
        pass
    
    @pipe_request(ret_pkg='host_customization_modules', ret_class='HostCustomizationModules')
    def get__modules(self) -> 'ICustomizationModules':
        pass
    
    @pipe_request(ret_pkg='host_customization_permissions', ret_class='HostCustomizationPermissions')
    def get__permissions(self) -> 'ICustomizationPermissions':
        pass
    
    @pipe_request(ret_pkg='host_customization_users', ret_class='HostCustomizationUsers')
    def get__users(self) -> 'ICustomizationUsers':
        pass
    
    @pipe_request(ret_pkg='host_customization_users_groups', ret_class='HostCustomizationUsersGroups')
    def get__users_groups(self) -> 'ICustomizationUsersGroups':
        pass
    
    @pipe_request()
    def commit(self):
        pass

    @pipe_request()
    def load(self):
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def rollback(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

