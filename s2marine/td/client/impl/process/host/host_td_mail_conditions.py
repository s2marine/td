from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions

if TYPE_CHECKING:
    pass
    
    


class HostTDMailConditions(ITDMailConditions):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__condition(self, name: 'str', is_user_condition: 'bool') -> 'str':
        pass
    
    @pipe_request()
    def set__condition(self, name: 'str', is_user_condition: 'bool', value: 'str'):
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__item_list(self, get_real_names: 'bool') -> 'List[T]':
        pass
    
    @pipe_request()
    def close(self, need_to_save: 'bool'):
        pass

    @pipe_request()
    def delete_condition(self, name: 'str', is_user_condition: 'bool'):
        pass

    @pipe_request()
    def load(self):
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

