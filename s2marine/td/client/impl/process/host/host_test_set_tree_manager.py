from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    


class HostTestSetTreeManager(ITestSetTreeManager):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_test_set_folder', ret_class='HostTestSetFolder')
    def get__node_by_id(self, node_id: 'int') -> 'ITestSetFolder':
        pass
    
    @pipe_request(ret_pkg='host_test_set_folder', ret_class='HostTestSetFolder')
    def get__node_by_path(self, path: 'str') -> 'ITestSetFolder':
        pass
    
    @pipe_request(ret_pkg='host_sys_tree_node', ret_class='HostSysTreeNode')
    def get__root(self) -> 'ISysTreeNode':
        pass
    
    @pipe_request(ret_pkg='host_test_set_folder', ret_class='HostTestSetFolder')
    def get__unattached(self) -> 'ITestSetFolder':
        pass
    
    

