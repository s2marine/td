from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_file_data import IFileData

if TYPE_CHECKING:
    pass
    
    


class HostFileData(IFileData):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def items(self) -> 'List[T]':
        pass

    @pipe_request()
    def modify_date(self) -> 'float':
        pass

    @pipe_request()
    def size(self) -> 'int':
        pass

    @pipe_request()
    def type(self) -> 'int':
        pass

    

