from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser


class HostCustomizationUsersGroup(ICustomizationUsersGroup):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__deleted(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__deleted(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__hide_filter(self, filter_type: 'str') -> 'str':
        pass
    
    @pipe_request()
    def set__hide_filter(self, filter_type: 'str', value: 'str'):
        pass
    
    @pipe_request()
    def get__id(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_system(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request()
    def add_user(self, user: 'Any'):
        pass

    @pipe_request()
    def remove_user(self, user: 'Any'):
        pass

    @pipe_request(ret_pkg='host_customization_user', ret_class='HostCustomizationUser')
    def users_list(self) -> 'List[ICustomizationUser]':
        pass

    

