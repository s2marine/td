from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_graph import IGraph

if TYPE_CHECKING:
    pass
    
    


class HostGraph(IGraph):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__col_count(self) -> 'int':
        pass
    
    @pipe_request()
    def get__col_name(self, col: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__col_total(self, col: 'int') -> 'int':
        pass
    
    @pipe_request()
    def get__data(self, col: 'int', row: 'int') -> 'int':
        pass
    
    @pipe_request()
    def get__graph_total(self) -> 'int':
        pass
    
    @pipe_request()
    def get__max_value(self) -> 'int':
        pass
    
    @pipe_request()
    def get__row_count(self) -> 'int':
        pass
    
    @pipe_request()
    def get__row_name(self, row: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__row_total(self, row: 'int') -> 'int':
        pass
    
    @pipe_request()
    def get__start_date(self) -> 'float':
        pass
    
    @pipe_request()
    def drill_down(self, p_areas: 'Any', m_areas: 'Any') -> 'List[T]':
        pass

    @pipe_request()
    def multi_drill_down(self, areas: 'Any') -> 'List[T]':
        pass

    

