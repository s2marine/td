from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_recordset import IRecordset

if TYPE_CHECKING:
    pass
    
    


class HostRecordset(IRecordset):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__bor(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__cache_size(self) -> 'int':
        pass
    
    @pipe_request()
    def set__cache_size(self, value: 'int'):
        pass
    
    @pipe_request()
    def get__col_count(self) -> 'int':
        pass
    
    @pipe_request()
    def get__col_index(self, name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def get__col_name(self, index: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__col_size(self, index: 'int') -> 'int':
        pass
    
    @pipe_request()
    def get__col_type(self, index: 'int') -> 'int':
        pass
    
    @pipe_request()
    def is__eor(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__field_value(self, field_key: 'Any') -> 'Any':
        pass
    
    @pipe_request()
    def set__field_value(self, field_key: 'Any', value: 'Any'):
        pass
    
    @pipe_request()
    def get__position(self) -> 'int':
        pass
    
    @pipe_request()
    def set__position(self, value: 'int'):
        pass
    
    @pipe_request()
    def get__record_count(self) -> 'int':
        pass
    
    @pipe_request()
    def clone(self) -> 'T':
        pass

    @pipe_request()
    def first(self):
        pass

    @pipe_request()
    def last(self):
        pass

    @pipe_request()
    def next(self):
        pass

    @pipe_request()
    def prev(self):
        pass

    @pipe_request()
    def refresh(self, range: 'int', low: 'int', high: 'int'):
        pass

    

