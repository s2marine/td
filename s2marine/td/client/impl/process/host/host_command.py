from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_command import ICommand

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_recordset import IRecordset


class HostCommand(ICommand):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__affected_rows(self) -> 'int':
        pass
    
    @pipe_request()
    def get__command_text(self) -> 'str':
        pass
    
    @pipe_request()
    def set__command_text(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def get__index_fields(self) -> 'str':
        pass
    
    @pipe_request()
    def set__index_fields(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__param_index(self, name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def get__param_name(self, index: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__param_type(self, index: 'int') -> 'int':
        pass
    
    @pipe_request()
    def get__param_value(self, key: 'Any') -> 'Any':
        pass
    
    @pipe_request()
    def set__param_value(self, key: 'Any', value: 'Any'):
        pass
    
    @pipe_request()
    def add_param(self, name: 'str', initial_value: 'Any'):
        pass

    @pipe_request()
    def delete_param(self, key: 'Any'):
        pass

    @pipe_request()
    def delete_params(self):
        pass

    @pipe_request(ret_pkg='host_recordset', ret_class='HostRecordset')
    def execute(self) -> 'IRecordset':
        pass

    

