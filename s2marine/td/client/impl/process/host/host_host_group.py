from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_host_group import IHostGroup

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    


class HostHostGroup(IHostGroup):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_attachment_factory', ret_class='HostAttachmentFactory')
    def get__attachments(self) -> 'IAttachmentFactory':
        pass
    
    @pipe_request()
    def is__auto_post(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__auto_post(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__field(self, field_name: 'str') -> 'Any':
        pass
    
    @pipe_request()
    def set__field(self, field_name: 'str', value: 'Any'):
        pass
    
    @pipe_request()
    def is__has_attachment(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__modified(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def is__virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def add_host(self, val: 'Any'):
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def new_list(self) -> 'List[T]':
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def remove_host(self, val: 'Any'):
        pass

    @pipe_request()
    def undo(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

