from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_alert_manager import IAlertManager

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_alert import IAlert
    from s2marine.td.client.intf.i_alert import IAlert
    


class HostAlertManager(IAlertManager):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_alert', ret_class='HostAlert')
    def get__alert(self, id: 'int') -> 'IAlert':
        pass
    
    @pipe_request(ret_pkg='host_alert', ret_class='HostAlert')
    def get__alert_list(self, entity_type: 'str', need_refresh: 'bool') -> 'List[IAlert]':
        pass
    
    @pipe_request()
    def clean_all_alerts(self):
        pass

    @pipe_request()
    def delete_alert(self, i_ds: 'Any'):
        pass

    

