from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class HostCustomizationUsersGroups(ICustomizationUsersGroups):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__group(self, name: 'str') -> 'ICustomizationUsersGroup':
        pass
    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        pass
    
    @pipe_request()
    def add_group(self, name: 'str') -> 'T':
        pass

    @pipe_request()
    def remove_group(self, name: 'str'):
        pass

    

