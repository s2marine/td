from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_history import IHistory

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history_filter import IHistoryFilter
    
    from s2marine.td.client.intf.i_history_record import IHistoryRecord


class HostHistory(IHistory):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_history_filter', ret_class='HostHistoryFilter')
    def get__filter(self) -> 'IHistoryFilter':
        pass
    
    @pipe_request()
    def clear_history(self, filter: 'str'):
        pass

    @pipe_request(ret_pkg='host_history_record', ret_class='HostHistoryRecord')
    def new_list(self, filter: 'str') -> 'List[IHistoryRecord]':
        pass

    

