from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_attachment import IAttachment

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    


class HostAttachment(IAttachment):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_extended_storage', ret_class='HostExtendedStorage')
    def get__attachment_storage(self) -> 'IExtendedStorage':
        pass
    
    @pipe_request()
    def is__auto_post(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__auto_post(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__data(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__description(self) -> 'str':
        pass
    
    @pipe_request()
    def set__description(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__direct_link(self) -> 'str':
        pass
    
    @pipe_request()
    def get__field(self, field_name: 'str') -> 'Any':
        pass
    
    @pipe_request()
    def set__field(self, field_name: 'str', value: 'Any'):
        pass
    
    @pipe_request()
    def get__file_name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__file_name(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__file_size(self) -> 'int':
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__last_modified(self) -> 'float':
        pass
    
    @pipe_request()
    def is__modified(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self, view_format: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__server_file_name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__type(self) -> 'int':
        pass
    
    @pipe_request()
    def set__type(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def load(self, synchronize: 'bool', root_path: 'str'):
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def rename(self, new_name: 'str'):
        pass

    @pipe_request()
    def save(self, synchronize: 'bool'):
        pass

    @pipe_request()
    def undo(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

