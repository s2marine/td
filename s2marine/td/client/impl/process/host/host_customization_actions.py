from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_actions import ICustomizationActions

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    


class HostCustomizationActions(ICustomizationActions):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_action', ret_class='HostCustomizationAction')
    def get__action(self, name: 'str') -> 'ICustomizationAction':
        pass
    
    @pipe_request(ret_pkg='host_customization_action', ret_class='HostCustomizationAction')
    def get__actions(self) -> 'List[ICustomizationAction]':
        pass
    
    

