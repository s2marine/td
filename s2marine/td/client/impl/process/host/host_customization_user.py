from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_user import ICustomizationUser

if TYPE_CHECKING:
    pass
    
    


class HostCustomizationUser(ICustomizationUser):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__address(self) -> 'str':
        pass
    
    @pipe_request()
    def set__address(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__deleted(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__deleted(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__domain_authentication(self) -> 'str':
        pass
    
    @pipe_request()
    def set__domain_authentication(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__email(self) -> 'str':
        pass
    
    @pipe_request()
    def set__email(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__full_name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__full_name(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__in_group(self, group_name: 'Any') -> 'bool':
        pass
    
    @pipe_request()
    def set__in_group(self, group_name: 'Any', value: 'bool'):
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def get__password(self, rhs: 'str') -> 'str':
        pass
    
    @pipe_request()
    def set__password(self, rhs: 'str', value: 'str'):
        pass
    
    @pipe_request()
    def get__phone(self) -> 'str':
        pass
    
    @pipe_request()
    def set__phone(self, value: 'str'):
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request()
    def add_to_group(self, group: 'Any'):
        pass

    @pipe_request()
    def groups_list(self) -> 'List[T]':
        pass

    @pipe_request()
    def remove_from_group(self, group: 'Any'):
        pass

    

