from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_condition import ICondition

if TYPE_CHECKING:
    pass
    
    


class HostCondition(ICondition):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__description(self) -> 'str':
        pass
    
    @pipe_request()
    def set__description(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__source(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__target(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__value(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__value(self, value: 'Any'):
        pass
    
    

