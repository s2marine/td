from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus

if TYPE_CHECKING:
    pass
    
    


class HostTestExecStatus(ITestExecStatus):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__message(self) -> 'str':
        pass
    
    @pipe_request()
    def get__status(self) -> 'str':
        pass
    
    @pipe_request()
    def get__test_id(self) -> 'int':
        pass
    
    @pipe_request()
    def get__test_instance(self) -> 'int':
        pass
    
    @pipe_request()
    def get__ts_test_id(self) -> 'Any':
        pass
    
    

