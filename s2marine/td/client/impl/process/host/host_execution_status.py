from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_execution_status import IExecutionStatus

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus
    
    from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo


class HostExecutionStatus(IExecutionStatus):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get___new_enum(self) -> 'Unknown':
        pass
    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__finished(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_test_exec_status', ret_class='HostTestExecStatus')
    def get__item(self, index: 'int') -> 'ITestExecStatus':
        pass
    
    @pipe_request(ret_pkg='host_exec_event_info', ret_class='HostExecEventInfo')
    def events_list(self) -> 'List[IExecEventInfo]':
        pass

    @pipe_request()
    def refresh_exec_status_info(self, test_data: 'Any', force: 'bool'):
        pass

    

