from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_action_permission import IActionPermission

if TYPE_CHECKING:
    pass
    
    


class HostActionPermission(IActionPermission):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__action_enabled(self, action_identity: 'Any', action_target: 'Any') -> 'bool':
        pass
    
    

