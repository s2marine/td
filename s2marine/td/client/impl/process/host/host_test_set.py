from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_test_set import ITestSet

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_condition_factory import IConditionFactory
    from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    
    from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler


class HostTestSet(ITestSet):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_attachment_factory', ret_class='HostAttachmentFactory')
    def get__attachments(self) -> 'IAttachmentFactory':
        pass
    
    @pipe_request()
    def is__auto_post(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__auto_post(self, value: 'bool'):
        pass
    
    @pipe_request(ret_pkg='host_condition_factory', ret_class='HostConditionFactory')
    def get__condition_factory(self) -> 'IConditionFactory':
        pass
    
    @pipe_request(ret_pkg='host_exec_event_notify_by_mail_settings', ret_class='HostExecEventNotifyByMailSettings')
    def get__exec_event_notify_by_mail_settings(self) -> 'IExecEventNotifyByMailSettings':
        pass
    
    @pipe_request(ret_pkg='host_exec_settings', ret_class='HostExecSettings')
    def get__execution_settings(self) -> 'IExecSettings':
        pass
    
    @pipe_request()
    def get__field(self, field_name: 'str') -> 'Any':
        pass
    
    @pipe_request()
    def set__field(self, field_name: 'str', value: 'Any'):
        pass
    
    @pipe_request()
    def is__has_attachment(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__modified(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__status(self) -> 'str':
        pass
    
    @pipe_request()
    def set__status(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_exec_settings', ret_class='HostExecSettings')
    def get__test_default_execution_settings(self) -> 'IExecSettings':
        pass
    
    @pipe_request(ret_pkg='host_test_set_folder', ret_class='HostTestSetFolder')
    def get__test_set_folder(self) -> 'ITestSetFolder':
        pass
    
    @pipe_request()
    def set__test_set_folder(self, value: 'ITestSetFolder'):
        pass
    
    @pipe_request(ret_pkg='host_ts_test_factory', ret_class='HostTSTestFactory')
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        pass
    
    @pipe_request()
    def is__virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        pass

    @pipe_request()
    def check_test_instances(self, test_i_ds: 'str') -> 'str':
        pass

    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def purge_executions(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def reset_test_set(self, delete_runs: 'bool'):
        pass

    @pipe_request(ret_pkg='host_ts_scheduler', ret_class='HostTSScheduler')
    def start_execution(self, server_name: 'str') -> 'ITSScheduler':
        pass

    @pipe_request()
    def undo(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

