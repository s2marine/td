from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class HostCustomizationListNode(ICustomizationListNode):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def is__can_add_child(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_customization_list_node', ret_class='HostCustomizationListNode')
    def get__child(self, node_name: 'str') -> 'ICustomizationListNode':
        pass
    
    @pipe_request()
    def get__children(self) -> 'List[T]':
        pass
    
    @pipe_request()
    def get__children_count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__deleted(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__deleted(self, value: 'bool'):
        pass
    
    @pipe_request(ret_pkg='host_customization_list_node', ret_class='HostCustomizationListNode')
    def get__father(self) -> 'ICustomizationListNode':
        pass
    
    @pipe_request()
    def set__father(self, value: 'ICustomizationListNode'):
        pass
    
    @pipe_request()
    def get__id(self) -> 'int':
        pass
    
    @pipe_request()
    def set__id(self, value: 'int'):
        pass
    
    @pipe_request(ret_pkg='host_customization_list', ret_class='HostCustomizationList')
    def get__list(self) -> 'ICustomizationList':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__name(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__order(self) -> 'int':
        pass
    
    @pipe_request()
    def set__order(self, value: 'int'):
        pass
    
    @pipe_request()
    def is__read_only(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request()
    def add_child(self, node: 'Any') -> 'T':
        pass

    @pipe_request()
    def remove_child(self, node: 'Any'):
        pass

    

