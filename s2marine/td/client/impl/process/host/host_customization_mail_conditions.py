from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition


class HostCustomizationMailConditions(ICustomizationMailConditions):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_mail_condition', ret_class='HostCustomizationMailCondition')
    def get__condition(self, name: 'str', condition_type: 'int') -> 'ICustomizationMailCondition':
        pass
    
    @pipe_request()
    def is__condition_exist(self, name: 'str', condition_type: 'int') -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_customization_mail_condition', ret_class='HostCustomizationMailCondition')
    def get__conditions(self) -> 'List[ICustomizationMailCondition]':
        pass
    
    @pipe_request(ret_pkg='host_customization_mail_condition', ret_class='HostCustomizationMailCondition')
    def add_condition(self, name: 'str', condition_type: 'int', condition_text: 'str') -> 'ICustomizationMailCondition':
        pass

    @pipe_request()
    def remove_condition(self, name: 'str', condition_type: 'int'):
        pass

    

