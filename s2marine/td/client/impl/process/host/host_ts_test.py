from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_ts_test import ITSTest

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_test_set import ITestSet
    


class HostTSTest(ITSTest):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_attachment_factory', ret_class='HostAttachmentFactory')
    def get__attachments(self) -> 'IAttachmentFactory':
        pass
    
    @pipe_request()
    def is__auto_post(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__auto_post(self, value: 'bool'):
        pass
    
    @pipe_request()
    def get__execution_params(self) -> 'str':
        pass
    
    @pipe_request()
    def set__execution_params(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_exec_settings', ret_class='HostExecSettings')
    def get__execution_settings(self) -> 'IExecSettings':
        pass
    
    @pipe_request()
    def get__field(self, field_name: 'str') -> 'Any':
        pass
    
    @pipe_request()
    def set__field(self, field_name: 'str', value: 'Any'):
        pass
    
    @pipe_request()
    def is__has_attachment(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__has_coverage(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__has_steps(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request()
    def get__host_name(self) -> 'str':
        pass
    
    @pipe_request()
    def set__host_name(self, value: 'str'):
        pass
    
    @pipe_request()
    def get__id(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__instance(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_run', ret_class='HostRun')
    def get__last_run(self) -> 'IRun':
        pass
    
    @pipe_request()
    def is__modified(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_step_params', ret_class='HostStepParams')
    def get__params(self) -> 'IStepParams':
        pass
    
    @pipe_request(ret_pkg='host_run_factory', ret_class='HostRunFactory')
    def get__run_factory(self) -> 'IRunFactory':
        pass
    
    @pipe_request()
    def get__status(self) -> 'str':
        pass
    
    @pipe_request()
    def set__status(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_test', ret_class='HostTest')
    def get__test(self) -> 'ITest':
        pass
    
    @pipe_request()
    def get__test_id(self) -> 'Any':
        pass
    
    @pipe_request()
    def get__test_name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_test_set', ret_class='HostTestSet')
    def get__test_set(self) -> 'ITestSet':
        pass
    
    @pipe_request()
    def get__type(self) -> 'str':
        pass
    
    @pipe_request()
    def is__virtual(self) -> 'bool':
        pass
    
    @pipe_request()
    def lock_object(self) -> 'bool':
        pass

    @pipe_request()
    def post(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def undo(self):
        pass

    @pipe_request()
    def un_lock_object(self):
        pass

    

