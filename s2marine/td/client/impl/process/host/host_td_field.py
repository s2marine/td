from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_td_field import ITDField

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_field_property import IFieldProperty
    


class HostTDField(ITDField):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_field_property', ret_class='HostFieldProperty')
    def get__property(self) -> 'IFieldProperty':
        pass
    
    @pipe_request()
    def get__type(self) -> 'int':
        pass
    
    @pipe_request()
    def is_valid_value(self, value: 'Any', p_object: 'T'):
        pass

    

