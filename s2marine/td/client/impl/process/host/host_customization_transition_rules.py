from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule


class HostCustomizationTransitionRules(ICustomizationTransitionRules):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_transition_rule', ret_class='HostCustomizationTransitionRule')
    def get__count(self) -> 'ICustomizationTransitionRule':
        pass
    
    @pipe_request()
    def get__entity_name(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_customization_field', ret_class='HostCustomizationField')
    def get__field(self) -> 'ICustomizationField':
        pass
    
    @pipe_request(ret_pkg='host_customization_users_group', ret_class='HostCustomizationUsersGroup')
    def get__group(self) -> 'ICustomizationUsersGroup':
        pass
    
    @pipe_request(ret_pkg='host_customization_transition_rule', ret_class='HostCustomizationTransitionRule')
    def get__transition_rule(self, position: 'int') -> 'ICustomizationTransitionRule':
        pass
    
    @pipe_request()
    def is__updated(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__updated(self, value: 'bool'):
        pass
    
    @pipe_request(ret_pkg='host_customization_transition_rule', ret_class='HostCustomizationTransitionRule')
    def add_transition_rule(self) -> 'ICustomizationTransitionRule':
        pass

    @pipe_request()
    def remove_transition_rule(self, rule: 'Any'):
        pass

    

