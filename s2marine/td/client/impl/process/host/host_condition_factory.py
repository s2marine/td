from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_condition_factory import IConditionFactory

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_condition import ICondition
    
    from s2marine.td.client.intf.i_condition import ICondition


class HostConditionFactory(IConditionFactory):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__fetch_level(self, field_name: 'str') -> 'int':
        pass
    
    @pipe_request()
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        pass
    
    @pipe_request()
    def get__filter(self) -> 'TDFilter':
        pass
    
    @pipe_request(ret_pkg='host_history', ret_class='HostHistory')
    def get__history(self) -> 'IHistory':
        pass
    
    @pipe_request(ret_pkg='host_condition', ret_class='HostCondition')
    def get__item(self, item_key: 'Any') -> 'ICondition':
        pass
    
    @pipe_request(ret_pkg='host_condition', ret_class='HostCondition')
    def add_item(self, item_data: 'Any') -> 'ICondition':
        pass

    @pipe_request()
    def new_list(self, filter: 'str') -> 'List[T]':
        pass

    @pipe_request()
    def remove_item(self, item_key: 'Any'):
        pass

    @pipe_request()
    def save(self):
        pass

    

