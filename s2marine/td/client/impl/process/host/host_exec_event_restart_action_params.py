from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_exec_event_restart_action_params import IExecEventRestartActionParams

if TYPE_CHECKING:
    pass
    
    


class HostExecEventRestartActionParams(IExecEventRestartActionParams):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__cleanup_test(self) -> 'Any':
        pass
    
    @pipe_request()
    def set__cleanup_test(self, value: 'Any'):
        pass
    
    @pipe_request()
    def get__number_of_retries(self) -> 'int':
        pass
    
    @pipe_request()
    def set__number_of_retries(self, value: 'int'):
        pass
    
    @pipe_request()
    def get__on_exec_event_scheduler_action(self) -> 'int':
        pass
    
    

