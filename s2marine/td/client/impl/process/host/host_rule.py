from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_rule import IRule

if TYPE_CHECKING:
    pass
    
    


class HostRule(IRule):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__description(self) -> 'str':
        pass
    
    @pipe_request()
    def get__id(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_active(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__is_active(self, value: 'bool'):
        pass
    
    @pipe_request()
    def is__to_mail(self) -> 'bool':
        pass
    
    @pipe_request()
    def set__to_mail(self, value: 'bool'):
        pass
    
    @pipe_request()
    def post(self):
        pass

    

