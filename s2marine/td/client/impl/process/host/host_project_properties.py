from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_project_properties import IProjectProperties

if TYPE_CHECKING:
    pass
    
    


class HostProjectProperties(IProjectProperties):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__count(self) -> 'int':
        pass
    
    @pipe_request()
    def is__is_param(self, param_name: 'str') -> 'bool':
        pass
    
    @pipe_request()
    def get__param_name(self, param_index: 'int') -> 'str':
        pass
    
    @pipe_request()
    def get__param_value(self, v_param: 'Any') -> 'str':
        pass
    
    

