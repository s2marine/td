from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_extended_storage import IExtendedStorage

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_file_data import IFileData
    


class HostExtendedStorage(IExtendedStorage):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__action_finished(self) -> 'int':
        pass
    
    @pipe_request()
    def get__client_path(self) -> 'str':
        pass
    
    @pipe_request()
    def set__client_path(self, value: 'str'):
        pass
    
    @pipe_request(ret_pkg='host_file_data', ret_class='HostFileData')
    def get__root(self) -> 'IFileData':
        pass
    
    @pipe_request()
    def get__server_path(self) -> 'str':
        pass
    
    @pipe_request()
    def set__server_path(self, value: 'str'):
        pass
    
    @pipe_request()
    def cancel(self):
        pass

    @pipe_request()
    def delete(self, f_sys_filter: 'str', n_delete_type: 'int'):
        pass

    @pipe_request()
    def get_last_error(self):
        pass

    @pipe_request()
    def load(self, f_sys_filter: 'str', synchronize: 'bool') -> 'str':
        pass

    @pipe_request()
    def load_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]', p_non_fatal_error_occured: 'bool') -> 'str':
        pass

    @pipe_request()
    def progress(self, total: 'int', current: 'int') -> 'str':
        pass

    @pipe_request()
    def save(self, f_sys_filter: 'str', synchronize: 'bool'):
        pass

    @pipe_request()
    def save_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]') -> 'bool':
        pass

    

