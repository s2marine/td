from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_graph_definition import IGraphDefinition

if TYPE_CHECKING:
    pass
    
    


class HostGraphDefinition(IGraphDefinition):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def get__filter(self) -> 'TDFilter':
        pass
    
    @pipe_request()
    def set__filter(self, value: 'TDFilter'):
        pass
    
    @pipe_request()
    def get__module(self) -> 'int':
        pass
    
    @pipe_request()
    def get__property(self, prop: 'int') -> 'Any':
        pass
    
    @pipe_request()
    def set__property(self, prop: 'int', value: 'Any'):
        pass
    
    @pipe_request()
    def get__type(self) -> 'int':
        pass
    
    

