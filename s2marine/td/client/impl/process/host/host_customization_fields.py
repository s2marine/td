from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_customization_fields import ICustomizationFields

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField


class HostCustomizationFields(ICustomizationFields):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_customization_field', ret_class='HostCustomizationField')
    def get__field(self, table_name: 'str', field_name: 'str') -> 'ICustomizationField':
        pass
    
    @pipe_request()
    def is__field_exists(self, table_name: 'str', field_name: 'str') -> 'bool':
        pass
    
    @pipe_request(ret_pkg='host_td_field', ret_class='HostTDField')
    def get__fields(self, table_name: 'str') -> 'List[ITDField]':
        pass
    
    @pipe_request(ret_pkg='host_customization_field', ret_class='HostCustomizationField')
    def add_active_field(self, table_name: 'str') -> 'ICustomizationField':
        pass

    @pipe_request()
    def add_active_memo_field(self, table_name: 'str') -> 'T':
        pass

    

