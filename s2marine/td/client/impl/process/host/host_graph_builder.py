from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_graph_builder import IGraphBuilder

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_graph_definition import IGraphDefinition


class HostGraphBuilder(IGraphBuilder):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request()
    def build_graph(self, p_graph_def: 'IGraphDefinition') -> 'T':
        pass

    @pipe_request()
    def build_multiple_graphs(self, graph_defs: 'Any') -> 'List[T]':
        pass

    @pipe_request(ret_pkg='host_graph_definition', ret_class='HostGraphDefinition')
    def create_graph_definition(self, module: 'int', graph_type: 'int') -> 'IGraphDefinition':
        pass

    

