from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.process_utils import pipe_request

from s2marine.td.client.intf.i_vcs import IVCS

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    


class HostVCS(IVCS):

    def __init__(self, pipe):
        self._pipe = pipe

    
    @pipe_request(ret_pkg='host_version_item', ret_class='HostVersionItem')
    def get__checkout_info(self) -> 'IVersionItem':
        pass
    
    @pipe_request()
    def get__current_version(self) -> 'str':
        pass
    
    @pipe_request()
    def is__is_checked_out(self) -> 'bool':
        pass
    
    @pipe_request()
    def is__is_locked(self) -> 'bool':
        pass
    
    @pipe_request()
    def get__locked_by(self) -> 'str':
        pass
    
    @pipe_request()
    def get__version(self) -> 'str':
        pass
    
    @pipe_request(ret_pkg='host_version_item', ret_class='HostVersionItem')
    def get__version_info(self, version: 'str') -> 'IVersionItem':
        pass
    
    @pipe_request()
    def get__versions(self) -> 'List[T]':
        pass
    
    @pipe_request(ret_pkg='host_version_item', ret_class='HostVersionItem')
    def get__versions_ex(self) -> 'List[IVersionItem]':
        pass
    
    @pipe_request()
    def add_object_to_vcs(self, version: 'str', comments: 'str'):
        pass

    @pipe_request()
    def check_in(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool'):
        pass

    @pipe_request()
    def check_in_ex(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool', force_checkin: 'bool', reserved: 'int'):
        pass

    @pipe_request()
    def check_out(self, version: 'str', comment: 'str', lock: 'bool', read_only: 'bool', sync: 'bool'):
        pass

    @pipe_request()
    def delete_object_from_vcs(self):
        pass

    @pipe_request()
    def lock_vcs_object(self):
        pass

    @pipe_request()
    def refresh(self):
        pass

    @pipe_request()
    def set_current_version(self, version: 'str'):
        pass

    @pipe_request()
    def undo_checkout(self, remove: 'bool'):
        pass

    

