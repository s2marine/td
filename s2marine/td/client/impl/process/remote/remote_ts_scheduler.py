from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.ts_scheduler_impl import TSSchedulerImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_execution_status import IExecutionStatus
    

class RemoteTSScheduler(TSSchedulerImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TSSchedulerImpl.__init__(self, com_obj)
    
    def get__execution_status(self) -> 'IExecutionStatus':
        from s2marine.td.client.impl.process.remote.remote_execution_status import RemoteExecutionStatus
        return RemoteExecutionStatus(self.com_obj.ExecutionStatus)
    
    
