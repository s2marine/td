from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.exec_settings_impl import ExecSettingsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams
    

class RemoteExecSettings(ExecSettingsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecSettingsImpl.__init__(self, com_obj)
    
    def get__on_exec_event_scheduler_action_params(self, event_type: 'int') -> 'IExecEventActionParams':
        from s2marine.td.client.impl.process.remote.remote_exec_event_action_params import RemoteExecEventActionParams
        return RemoteExecEventActionParams(self.com_obj.OnExecEventSchedulerActionParams[event_type])
    
    
