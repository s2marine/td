from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.td_mail_conditions_impl import TDMailConditionsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteTDMailConditions(TDMailConditionsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TDMailConditionsImpl.__init__(self, com_obj)
    
    
