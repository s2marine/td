from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.td_field_impl import TDFieldImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_field_property import IFieldProperty
    

class RemoteTDField(TDFieldImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TDFieldImpl.__init__(self, com_obj)
    
    def get__property(self) -> 'IFieldProperty':
        from s2marine.td.client.impl.process.remote.remote_field_property import RemoteFieldProperty
        return RemoteFieldProperty(self.com_obj.Property)
    
    
