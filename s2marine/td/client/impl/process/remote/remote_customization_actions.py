from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_actions_impl import CustomizationActionsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    from s2marine.td.client.intf.i_customization_action import ICustomizationAction
    

class RemoteCustomizationActions(CustomizationActionsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationActionsImpl.__init__(self, com_obj)
    
    def get__action(self, name: 'str') -> 'ICustomizationAction':
        from s2marine.td.client.impl.process.remote.remote_customization_action import RemoteCustomizationAction
        return RemoteCustomizationAction(self.com_obj.Action[name])
    
    def get__actions(self) -> 'List[ICustomizationAction]':
        from s2marine.td.client.impl.process.remote.remote_customization_action import RemoteCustomizationAction
        return [RemoteCustomizationAction(com_obj) for com_obj in self.com_obj.Actions]
    
    
