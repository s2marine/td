from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.vcs_impl import VCSImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    

class RemoteVCS(VCSImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        VCSImpl.__init__(self, com_obj)
    
    def get__checkout_info(self) -> 'IVersionItem':
        from s2marine.td.client.impl.process.remote.remote_version_item import RemoteVersionItem
        return RemoteVersionItem(self.com_obj.CheckoutInfo)
    
    def get__version_info(self, version: 'str') -> 'IVersionItem':
        from s2marine.td.client.impl.process.remote.remote_version_item import RemoteVersionItem
        return RemoteVersionItem(self.com_obj.VersionInfo[version])
    
    def get__versions_ex(self) -> 'List[IVersionItem]':
        from s2marine.td.client.impl.process.remote.remote_version_item import RemoteVersionItem
        return [RemoteVersionItem(com_obj) for com_obj in self.com_obj.VersionsEx]
    
    
