from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_set_tree_manager_impl import TestSetTreeManagerImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    

class RemoteTestSetTreeManager(TestSetTreeManagerImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestSetTreeManagerImpl.__init__(self, com_obj)
    
    def get__node_by_id(self, node_id: 'int') -> 'ITestSetFolder':
        from s2marine.td.client.impl.process.remote.remote_test_set_folder import RemoteTestSetFolder
        return RemoteTestSetFolder(self.com_obj.NodeById[node_id])
    
    def get__node_by_path(self, path: 'str') -> 'ITestSetFolder':
        from s2marine.td.client.impl.process.remote.remote_test_set_folder import RemoteTestSetFolder
        return RemoteTestSetFolder(self.com_obj.NodeByPath[path])
    
    def get__root(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.Root)
    
    def get__unattached(self) -> 'ITestSetFolder':
        from s2marine.td.client.impl.process.remote.remote_test_set_folder import RemoteTestSetFolder
        return RemoteTestSetFolder(self.com_obj.Unattached)
    
    
