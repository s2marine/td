from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.extended_storage_impl import ExtendedStorageImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_file_data import IFileData
    

class RemoteExtendedStorage(ExtendedStorageImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExtendedStorageImpl.__init__(self, com_obj)
    
    def get__root(self) -> 'IFileData':
        from s2marine.td.client.impl.process.remote.remote_file_data import RemoteFileData
        return RemoteFileData(self.com_obj.Root)
    
    
