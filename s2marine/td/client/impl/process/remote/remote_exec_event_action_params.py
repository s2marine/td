from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.exec_event_action_params_impl import ExecEventActionParamsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteExecEventActionParams(ExecEventActionParamsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecEventActionParamsImpl.__init__(self, com_obj)
    
    
