from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.audit_property_factory_impl import AuditPropertyFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    

class RemoteAuditPropertyFactory(AuditPropertyFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        AuditPropertyFactoryImpl.__init__(self, com_obj)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    
