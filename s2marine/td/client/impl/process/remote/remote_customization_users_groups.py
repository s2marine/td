from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_users_groups_impl import CustomizationUsersGroupsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    

class RemoteCustomizationUsersGroups(CustomizationUsersGroupsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationUsersGroupsImpl.__init__(self, com_obj)
    
    def get__group(self, name: 'str') -> 'ICustomizationUsersGroup':
        from s2marine.td.client.impl.process.remote.remote_customization_users_group import RemoteCustomizationUsersGroup
        return RemoteCustomizationUsersGroup(self.com_obj.Group[name])
    
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        from s2marine.td.client.impl.process.remote.remote_customization_users_group import RemoteCustomizationUsersGroup
        return [RemoteCustomizationUsersGroup(com_obj) for com_obj in self.com_obj.Groups]
    
    
