from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.condition_factory_impl import ConditionFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_condition import ICondition
    
    from s2marine.td.client.intf.i_condition import ICondition

class RemoteConditionFactory(ConditionFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ConditionFactoryImpl.__init__(self, com_obj)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__item(self, item_key: 'Any') -> 'ICondition':
        from s2marine.td.client.impl.process.remote.remote_condition import RemoteCondition
        return RemoteCondition(self.com_obj.Item[item_key])
    
    
    def add_item(self, item_data: 'Any') -> 'ICondition':
        from s2marine.td.client.impl.process.remote.remote_condition import RemoteCondition
        return RemoteCondition(self.com_obj.AddItem(item_data))

    
