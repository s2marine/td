from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_impl import CustomizationImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_actions import ICustomizationActions
    from s2marine.td.client.intf.i_customization_fields import ICustomizationFields
    from s2marine.td.client.intf.i_customization_lists import ICustomizationLists
    from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions
    from s2marine.td.client.intf.i_customization_modules import ICustomizationModules
    from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions
    from s2marine.td.client.intf.i_customization_users import ICustomizationUsers
    from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups
    

class RemoteCustomization(CustomizationImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationImpl.__init__(self, com_obj)
    
    def get__actions(self) -> 'ICustomizationActions':
        from s2marine.td.client.impl.process.remote.remote_customization_actions import RemoteCustomizationActions
        return RemoteCustomizationActions(self.com_obj.Actions)
    
    def get__fields(self) -> 'ICustomizationFields':
        from s2marine.td.client.impl.process.remote.remote_customization_fields import RemoteCustomizationFields
        return RemoteCustomizationFields(self.com_obj.Fields)
    
    def get__lists(self) -> 'ICustomizationLists':
        from s2marine.td.client.impl.process.remote.remote_customization_lists import RemoteCustomizationLists
        return RemoteCustomizationLists(self.com_obj.Lists)
    
    def get__mail_conditions(self) -> 'ICustomizationMailConditions':
        from s2marine.td.client.impl.process.remote.remote_customization_mail_conditions import RemoteCustomizationMailConditions
        return RemoteCustomizationMailConditions(self.com_obj.MailConditions)
    
    def get__modules(self) -> 'ICustomizationModules':
        from s2marine.td.client.impl.process.remote.remote_customization_modules import RemoteCustomizationModules
        return RemoteCustomizationModules(self.com_obj.Modules)
    
    def get__permissions(self) -> 'ICustomizationPermissions':
        from s2marine.td.client.impl.process.remote.remote_customization_permissions import RemoteCustomizationPermissions
        return RemoteCustomizationPermissions(self.com_obj.Permissions)
    
    def get__users(self) -> 'ICustomizationUsers':
        from s2marine.td.client.impl.process.remote.remote_customization_users import RemoteCustomizationUsers
        return RemoteCustomizationUsers(self.com_obj.Users)
    
    def get__users_groups(self) -> 'ICustomizationUsersGroups':
        from s2marine.td.client.impl.process.remote.remote_customization_users_groups import RemoteCustomizationUsersGroups
        return RemoteCustomizationUsersGroups(self.com_obj.UsersGroups)
    
    
