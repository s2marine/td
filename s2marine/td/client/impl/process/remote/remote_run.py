from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.run_impl import RunImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_step_factory import IStepFactory
    

class RemoteRun(RunImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        RunImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.process.remote.remote_extended_storage import RemoteExtendedStorage
        return RemoteExtendedStorage(self.com_obj.ExtendedStorage)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__params(self, source_mode: 'int') -> 'IStepParams':
        from s2marine.td.client.impl.process.remote.remote_step_params import RemoteStepParams
        return RemoteStepParams(self.com_obj.Params[source_mode])
    
    def get__step_factory(self) -> 'IStepFactory':
        from s2marine.td.client.impl.process.remote.remote_step_factory import RemoteStepFactory
        return RemoteStepFactory(self.com_obj.StepFactory)
    
    
