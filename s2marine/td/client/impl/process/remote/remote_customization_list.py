from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_list_impl import CustomizationListImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode

class RemoteCustomizationList(CustomizationListImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationListImpl.__init__(self, com_obj)
    
    def get__root_node(self) -> 'ICustomizationListNode':
        from s2marine.td.client.impl.process.remote.remote_customization_list_node import RemoteCustomizationListNode
        return RemoteCustomizationListNode(self.com_obj.RootNode)
    
    
    def find(self, val: 'Any') -> 'ICustomizationListNode':
        from s2marine.td.client.impl.process.remote.remote_customization_list_node import RemoteCustomizationListNode
        return RemoteCustomizationListNode(self.com_obj.Find(val))

    
