from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_factory_impl import TestFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    

class RemoteTestFactory(TestFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestFactoryImpl.__init__(self, com_obj)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__item(self, item_key: 'Any') -> 'ITest':
        from s2marine.td.client.impl.process.remote.remote_test import RemoteTest
        return RemoteTest(self.com_obj.Item[item_key])
    
    def get__repository_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.process.remote.remote_extended_storage import RemoteExtendedStorage
        return RemoteExtendedStorage(self.com_obj.RepositoryStorage)
    
    
