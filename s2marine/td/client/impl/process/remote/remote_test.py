from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_impl import TestImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_design_step_factory import IDesignStepFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_vcs import IVCS
    

class RemoteTest(TestImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__design_step_factory(self) -> 'IDesignStepFactory':
        from s2marine.td.client.impl.process.remote.remote_design_step_factory import RemoteDesignStepFactory
        return RemoteDesignStepFactory(self.com_obj.DesignStepFactory)
    
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.process.remote.remote_extended_storage import RemoteExtendedStorage
        return RemoteExtendedStorage(self.com_obj.ExtendedStorage)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__last_run(self) -> 'IRun':
        from s2marine.td.client.impl.process.remote.remote_run import RemoteRun
        return RemoteRun(self.com_obj.LastRun)
    
    def get__params(self) -> 'IStepParams':
        from s2marine.td.client.impl.process.remote.remote_step_params import RemoteStepParams
        return RemoteStepParams(self.com_obj.Params)
    
    def get__vcs(self) -> 'IVCS':
        from s2marine.td.client.impl.process.remote.remote_vcs import RemoteVCS
        return RemoteVCS(self.com_obj.VCS)
    
    
