from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.step_params_impl import StepParamsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteStepParams(StepParamsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        StepParamsImpl.__init__(self, com_obj)
    
    
