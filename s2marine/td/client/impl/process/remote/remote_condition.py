from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.condition_impl import ConditionImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteCondition(ConditionImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ConditionImpl.__init__(self, com_obj)
    
    
