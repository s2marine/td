from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.tree_manager_impl import TreeManagerImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    

class RemoteTreeManager(TreeManagerImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TreeManagerImpl.__init__(self, com_obj)
    
    def get__node_by_id(self, node_id: 'int') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.NodeById[node_id])
    
    def get__node_by_path(self, path: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.NodeByPath[path])
    
    def get__tree_root(self, root_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.TreeRoot[root_name])
    
    
