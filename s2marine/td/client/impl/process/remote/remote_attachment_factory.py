from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.attachment_factory_impl import AttachmentFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_attachment_filter import IAttachmentFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_attachment import IAttachment
    
    from s2marine.td.client.intf.i_attachment import IAttachment
    from s2marine.td.client.intf.i_attachment import IAttachment

class RemoteAttachmentFactory(AttachmentFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        AttachmentFactoryImpl.__init__(self, com_obj)
    
    def get__attachment_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.process.remote.remote_extended_storage import RemoteExtendedStorage
        return RemoteExtendedStorage(self.com_obj.AttachmentStorage)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    def get__filter(self) -> 'IAttachmentFilter':
        from s2marine.td.client.impl.process.remote.remote_attachment_filter import RemoteAttachmentFilter
        return RemoteAttachmentFilter(self.com_obj.Filter)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__item(self, item_key: 'Any') -> 'IAttachment':
        from s2marine.td.client.impl.process.remote.remote_attachment import RemoteAttachment
        return RemoteAttachment(self.com_obj.Item[item_key])
    
    
    def add_item(self, item_data: 'Any') -> 'IAttachment':
        from s2marine.td.client.impl.process.remote.remote_attachment import RemoteAttachment
        return RemoteAttachment(self.com_obj.AddItem(item_data))

    
    def new_list(self, filter: 'str') -> 'List[IAttachment]':
        from s2marine.td.client.impl.process.remote.remote_attachment import RemoteAttachment
        return [RemoteAttachment(com_obj) for com_obj in self.com_obj.NewList(filter)]

    
