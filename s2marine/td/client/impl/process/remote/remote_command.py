from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.command_impl import CommandImpl

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_recordset import IRecordset

class RemoteCommand(CommandImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CommandImpl.__init__(self, com_obj)
    
    
    def execute(self) -> 'IRecordset':
        from s2marine.td.client.impl.process.remote.remote_recordset import RemoteRecordset
        return RemoteRecordset(self.com_obj.Execute())

    
