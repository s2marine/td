from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.file_data_impl import FileDataImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteFileData(FileDataImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        FileDataImpl.__init__(self, com_obj)
    
    
