from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.attachment_filter_impl import AttachmentFilterImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_attachment import IAttachment

class RemoteAttachmentFilter(AttachmentFilterImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        AttachmentFilterImpl.__init__(self, com_obj)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    
    def new_list(self) -> 'List[IAttachment]':
        from s2marine.td.client.impl.process.remote.remote_attachment import RemoteAttachment
        return [RemoteAttachment(com_obj) for com_obj in self.com_obj.NewList()]

    
