from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_mail_condition_impl import CustomizationMailConditionImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteCustomizationMailCondition(CustomizationMailConditionImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationMailConditionImpl.__init__(self, com_obj)
    
    
