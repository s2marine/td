from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.exec_event_notify_by_mail_settings_impl import ExecEventNotifyByMailSettingsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteExecEventNotifyByMailSettings(ExecEventNotifyByMailSettingsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecEventNotifyByMailSettingsImpl.__init__(self, com_obj)
    
    
