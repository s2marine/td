from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.history_record_impl import HistoryRecordImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteHistoryRecord(HistoryRecordImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        HistoryRecordImpl.__init__(self, com_obj)
    
    
