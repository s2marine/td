from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.ts_test_impl import TSTestImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_test_set import ITestSet
    

class RemoteTSTest(TSTestImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TSTestImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.process.remote.remote_exec_settings import RemoteExecSettings
        return RemoteExecSettings(self.com_obj.ExecutionSettings)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__last_run(self) -> 'IRun':
        from s2marine.td.client.impl.process.remote.remote_run import RemoteRun
        return RemoteRun(self.com_obj.LastRun)
    
    def get__params(self) -> 'IStepParams':
        from s2marine.td.client.impl.process.remote.remote_step_params import RemoteStepParams
        return RemoteStepParams(self.com_obj.Params)
    
    def get__run_factory(self) -> 'IRunFactory':
        from s2marine.td.client.impl.process.remote.remote_run_factory import RemoteRunFactory
        return RemoteRunFactory(self.com_obj.RunFactory)
    
    def get__test(self) -> 'ITest':
        from s2marine.td.client.impl.process.remote.remote_test import RemoteTest
        return RemoteTest(self.com_obj.Test)
    
    def get__test_set(self) -> 'ITestSet':
        from s2marine.td.client.impl.process.remote.remote_test_set import RemoteTestSet
        return RemoteTestSet(self.com_obj.TestSet)
    
    
