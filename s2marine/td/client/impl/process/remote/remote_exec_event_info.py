from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.exec_event_info_impl import ExecEventInfoImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteExecEventInfo(ExecEventInfoImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecEventInfoImpl.__init__(self, com_obj)
    
    
