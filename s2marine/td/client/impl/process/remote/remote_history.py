from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.history_impl import HistoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history_filter import IHistoryFilter
    
    from s2marine.td.client.intf.i_history_record import IHistoryRecord

class RemoteHistory(HistoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        HistoryImpl.__init__(self, com_obj)
    
    def get__filter(self) -> 'IHistoryFilter':
        from s2marine.td.client.impl.process.remote.remote_history_filter import RemoteHistoryFilter
        return RemoteHistoryFilter(self.com_obj.Filter)
    
    
    def new_list(self, filter: 'str') -> 'List[IHistoryRecord]':
        from s2marine.td.client.impl.process.remote.remote_history_record import RemoteHistoryRecord
        return [RemoteHistoryRecord(com_obj) for com_obj in self.com_obj.NewList(filter)]

    
