from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_set_impl import TestSetImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_condition_factory import IConditionFactory
    from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    
    from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler

class RemoteTestSet(TestSetImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestSetImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__condition_factory(self) -> 'IConditionFactory':
        from s2marine.td.client.impl.process.remote.remote_condition_factory import RemoteConditionFactory
        return RemoteConditionFactory(self.com_obj.ConditionFactory)
    
    def get__exec_event_notify_by_mail_settings(self) -> 'IExecEventNotifyByMailSettings':
        from s2marine.td.client.impl.process.remote.remote_exec_event_notify_by_mail_settings import RemoteExecEventNotifyByMailSettings
        return RemoteExecEventNotifyByMailSettings(self.com_obj.ExecEventNotifyByMailSettings)
    
    def get__execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.process.remote.remote_exec_settings import RemoteExecSettings
        return RemoteExecSettings(self.com_obj.ExecutionSettings)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__test_default_execution_settings(self) -> 'IExecSettings':
        from s2marine.td.client.impl.process.remote.remote_exec_settings import RemoteExecSettings
        return RemoteExecSettings(self.com_obj.TestDefaultExecutionSettings)
    
    def get__test_set_folder(self) -> 'ITestSetFolder':
        from s2marine.td.client.impl.process.remote.remote_test_set_folder import RemoteTestSetFolder
        return RemoteTestSetFolder(self.com_obj.TestSetFolder)
    
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        from s2marine.td.client.impl.process.remote.remote_ts_test_factory import RemoteTSTestFactory
        return RemoteTSTestFactory(self.com_obj.TSTestFactory)
    
    
    def start_execution(self, server_name: 'str') -> 'ITSScheduler':
        from s2marine.td.client.impl.process.remote.remote_ts_scheduler import RemoteTSScheduler
        return RemoteTSScheduler(self.com_obj.StartExecution(server_name))

    
