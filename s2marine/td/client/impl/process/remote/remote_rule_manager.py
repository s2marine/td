from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.rule_manager_impl import RuleManagerImpl

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_rule import IRule

class RemoteRuleManager(RuleManagerImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        RuleManagerImpl.__init__(self, com_obj)
    
    
    def get_rule(self, id: 'int') -> 'IRule':
        from s2marine.td.client.impl.process.remote.remote_rule import RemoteRule
        return RemoteRule(self.com_obj.GetRule(id))

    
