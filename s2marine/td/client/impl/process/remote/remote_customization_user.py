from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_user_impl import CustomizationUserImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteCustomizationUser(CustomizationUserImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationUserImpl.__init__(self, com_obj)
    
    
