from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.alert_manager_impl import AlertManagerImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_alert import IAlert
    from s2marine.td.client.intf.i_alert import IAlert
    

class RemoteAlertManager(AlertManagerImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        AlertManagerImpl.__init__(self, com_obj)
    
    def get__alert(self, id: 'int') -> 'IAlert':
        from s2marine.td.client.impl.process.remote.remote_alert import RemoteAlert
        return RemoteAlert(self.com_obj.Alert[id])
    
    def get__alert_list(self, entity_type: 'str', need_refresh: 'bool') -> 'List[IAlert]':
        from s2marine.td.client.impl.process.remote.remote_alert import RemoteAlert
        return [RemoteAlert(com_obj) for com_obj in self.com_obj.AlertList[entity_type, need_refresh]]
    
    
