from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.graph_impl import GraphImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteGraph(GraphImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        GraphImpl.__init__(self, com_obj)
    
    
