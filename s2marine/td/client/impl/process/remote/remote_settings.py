from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.settings_impl import SettingsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteSettings(SettingsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        SettingsImpl.__init__(self, com_obj)
    
    
