from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_transition_rules_impl import CustomizationTransitionRulesImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule

class RemoteCustomizationTransitionRules(CustomizationTransitionRulesImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationTransitionRulesImpl.__init__(self, com_obj)
    
    def get__count(self) -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.process.remote.remote_customization_transition_rule import RemoteCustomizationTransitionRule
        return RemoteCustomizationTransitionRule(self.com_obj.Count)
    
    def get__field(self) -> 'ICustomizationField':
        from s2marine.td.client.impl.process.remote.remote_customization_field import RemoteCustomizationField
        return RemoteCustomizationField(self.com_obj.Field)
    
    def get__group(self) -> 'ICustomizationUsersGroup':
        from s2marine.td.client.impl.process.remote.remote_customization_users_group import RemoteCustomizationUsersGroup
        return RemoteCustomizationUsersGroup(self.com_obj.Group)
    
    def get__transition_rule(self, position: 'int') -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.process.remote.remote_customization_transition_rule import RemoteCustomizationTransitionRule
        return RemoteCustomizationTransitionRule(self.com_obj.TransitionRule[position])
    
    
    def add_transition_rule(self) -> 'ICustomizationTransitionRule':
        from s2marine.td.client.impl.process.remote.remote_customization_transition_rule import RemoteCustomizationTransitionRule
        return RemoteCustomizationTransitionRule(self.com_obj.AddTransitionRule())

    
