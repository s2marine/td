from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.project_properties_impl import ProjectPropertiesImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteProjectProperties(ProjectPropertiesImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ProjectPropertiesImpl.__init__(self, com_obj)
    
    
