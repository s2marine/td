from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.text_parser_impl import TextParserImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteTextParser(TextParserImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TextParserImpl.__init__(self, com_obj)
    
    
