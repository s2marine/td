from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.execution_status_impl import ExecutionStatusImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus
    
    from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo

class RemoteExecutionStatus(ExecutionStatusImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecutionStatusImpl.__init__(self, com_obj)
    
    def get__item(self, index: 'int') -> 'ITestExecStatus':
        from s2marine.td.client.impl.process.remote.remote_test_exec_status import RemoteTestExecStatus
        return RemoteTestExecStatus(self.com_obj.Item[index])
    
    
    def events_list(self) -> 'List[IExecEventInfo]':
        from s2marine.td.client.impl.process.remote.remote_exec_event_info import RemoteExecEventInfo
        return [RemoteExecEventInfo(com_obj) for com_obj in self.com_obj.EventsList()]

    
