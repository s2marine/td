from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.recordset_impl import RecordsetImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteRecordset(RecordsetImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        RecordsetImpl.__init__(self, com_obj)
    
    
