from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.req_factory_impl import ReqFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_req import IReq
    
    from s2marine.td.client.intf.i_req import IReq

class RemoteReqFactory(ReqFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ReqFactoryImpl.__init__(self, com_obj)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__item(self, item_key: 'Any') -> 'IReq':
        from s2marine.td.client.impl.process.remote.remote_req import RemoteReq
        return RemoteReq(self.com_obj.Item[item_key])
    
    
    def get_children_list(self, father_id: 'int') -> 'List[IReq]':
        from s2marine.td.client.impl.process.remote.remote_req import RemoteReq
        return [RemoteReq(com_obj) for com_obj in self.com_obj.GetChildrenList(father_id)]

    
