from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_users_group_impl import CustomizationUsersGroupImpl

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser

class RemoteCustomizationUsersGroup(CustomizationUsersGroupImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationUsersGroupImpl.__init__(self, com_obj)
    
    
    def users_list(self) -> 'List[ICustomizationUser]':
        from s2marine.td.client.impl.process.remote.remote_customization_user import RemoteCustomizationUser
        return [RemoteCustomizationUser(com_obj) for com_obj in self.com_obj.UsersList()]

    
