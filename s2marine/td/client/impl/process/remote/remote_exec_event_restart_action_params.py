from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.exec_event_restart_action_params_impl import ExecEventRestartActionParamsImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteExecEventRestartActionParams(ExecEventRestartActionParamsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ExecEventRestartActionParamsImpl.__init__(self, com_obj)
    
    
