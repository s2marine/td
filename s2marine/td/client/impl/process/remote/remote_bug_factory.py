from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.bug_factory_impl import BugFactoryImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_bug_filter import IBugFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_bug import IBug
    
    from s2marine.td.client.intf.i_bug import IBug
    from s2marine.td.client.intf.i_graph import IGraph

class RemoteBugFactory(BugFactoryImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        BugFactoryImpl.__init__(self, com_obj)
    
    def get__fields(self) -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields]
    
    def get__filter(self) -> 'IBugFilter':
        from s2marine.td.client.impl.process.remote.remote_bug_filter import RemoteBugFilter
        return RemoteBugFilter(self.com_obj.Filter)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    def get__item(self, item_key: 'Any') -> 'IBug':
        from s2marine.td.client.impl.process.remote.remote_bug import RemoteBug
        return RemoteBug(self.com_obj.Item[item_key])
    
    
    def add_item(self, item_data: 'Any') -> 'IBug':
        from s2marine.td.client.impl.process.remote.remote_bug import RemoteBug
        return RemoteBug(self.com_obj.AddItem(item_data))

    
    def build_age_graph(self, group_by_field: 'str', sum_of_field: 'str', max_age: 'int', max_cols: 'int', filter: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'IGraph':
        from s2marine.td.client.impl.process.remote.remote_graph import RemoteGraph
        return RemoteGraph(self.com_obj.BuildAgeGraph(group_by_field, sum_of_field, max_age, max_cols, filter, force_refresh, show_full_path))

    
