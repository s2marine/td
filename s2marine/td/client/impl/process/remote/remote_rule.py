from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.rule_impl import RuleImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteRule(RuleImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        RuleImpl.__init__(self, com_obj)
    
    
