from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.bug_impl import BugImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    

class RemoteBug(BugImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        BugImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    
