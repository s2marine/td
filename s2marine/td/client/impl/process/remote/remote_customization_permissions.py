from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_permissions_impl import CustomizationPermissionsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules
    

class RemoteCustomizationPermissions(CustomizationPermissionsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationPermissionsImpl.__init__(self, com_obj)
    
    def get__transition_rules(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'ICustomizationTransitionRules':
        from s2marine.td.client.impl.process.remote.remote_customization_transition_rules import RemoteCustomizationTransitionRules
        return RemoteCustomizationTransitionRules(self.com_obj.TransitionRules[entity_name, field, group])
    
    
