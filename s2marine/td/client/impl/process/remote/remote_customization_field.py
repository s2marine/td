from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_field_impl import CustomizationFieldImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    

class RemoteCustomizationField(CustomizationFieldImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationFieldImpl.__init__(self, com_obj)
    
    def get__authorized_modify_for_groups(self) -> 'List[ICustomizationUsersGroup]':
        from s2marine.td.client.impl.process.remote.remote_customization_users_group import RemoteCustomizationUsersGroup
        return [RemoteCustomizationUsersGroup(com_obj) for com_obj in self.com_obj.AuthorizedModifyForGroups]
    
    def get__list(self) -> 'ICustomizationList':
        from s2marine.td.client.impl.process.remote.remote_customization_list import RemoteCustomizationList
        return RemoteCustomizationList(self.com_obj.List)
    
    
