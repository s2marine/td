from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_exec_status_impl import TestExecStatusImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteTestExecStatus(TestExecStatusImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestExecStatusImpl.__init__(self, com_obj)
    
    
