from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_list_node_impl import CustomizationListNodeImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    

class RemoteCustomizationListNode(CustomizationListNodeImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationListNodeImpl.__init__(self, com_obj)
    
    def get__child(self, node_name: 'str') -> 'ICustomizationListNode':
        return RemoteCustomizationListNode(self.com_obj.Child[node_name])
    
    def get__father(self) -> 'ICustomizationListNode':
        return RemoteCustomizationListNode(self.com_obj.Father)
    
    def get__list(self) -> 'ICustomizationList':
        from s2marine.td.client.impl.process.remote.remote_customization_list import RemoteCustomizationList
        return RemoteCustomizationList(self.com_obj.List)
    
    
