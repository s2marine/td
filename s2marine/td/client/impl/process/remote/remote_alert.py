from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.alert_impl import AlertImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteAlert(AlertImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        AlertImpl.__init__(self, com_obj)
    
    
