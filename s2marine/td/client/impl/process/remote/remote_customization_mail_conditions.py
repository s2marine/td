from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_mail_conditions_impl import CustomizationMailConditionsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition

class RemoteCustomizationMailConditions(CustomizationMailConditionsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationMailConditionsImpl.__init__(self, com_obj)
    
    def get__condition(self, name: 'str', condition_type: 'int') -> 'ICustomizationMailCondition':
        from s2marine.td.client.impl.process.remote.remote_customization_mail_condition import RemoteCustomizationMailCondition
        return RemoteCustomizationMailCondition(self.com_obj.Condition[name, condition_type])
    
    def get__conditions(self) -> 'List[ICustomizationMailCondition]':
        from s2marine.td.client.impl.process.remote.remote_customization_mail_condition import RemoteCustomizationMailCondition
        return [RemoteCustomizationMailCondition(com_obj) for com_obj in self.com_obj.Conditions]
    
    
    def add_condition(self, name: 'str', condition_type: 'int', condition_text: 'str') -> 'ICustomizationMailCondition':
        from s2marine.td.client.impl.process.remote.remote_customization_mail_condition import RemoteCustomizationMailCondition
        return RemoteCustomizationMailCondition(self.com_obj.AddCondition(name, condition_type, condition_text))

    
