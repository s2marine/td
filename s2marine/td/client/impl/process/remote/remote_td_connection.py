from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.td_connection_impl import TDConnectionImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_action_permission import IActionPermission
    from s2marine.td.client.intf.i_alert_manager import IAlertManager
    from s2marine.td.client.intf.i_audit_property_factory import IAuditPropertyFactory
    from s2marine.td.client.intf.i_audit_record_factory import IAuditRecordFactory
    from s2marine.td.client.intf.i_bug_factory import IBugFactory
    from s2marine.td.client.intf.i_command import ICommand
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_customization import ICustomization
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_graph_builder import IGraphBuilder
    from s2marine.td.client.intf.i_host_factory import IHostFactory
    from s2marine.td.client.intf.i_host_group_factory import IHostGroupFactory
    from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions
    from s2marine.td.client.intf.i_product_info import IProductInfo
    from s2marine.td.client.intf.i_project_properties import IProjectProperties
    from s2marine.td.client.intf.i_req_factory import IReqFactory
    from s2marine.td.client.intf.i_rule_manager import IRuleManager
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test_factory import ITestFactory
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager
    from s2marine.td.client.intf.i_text_parser import ITextParser
    from s2marine.td.client.intf.i_tree_manager import ITreeManager
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_vcs import IVCS
    

class RemoteTDConnection(TDConnectionImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TDConnectionImpl.__init__(self, com_obj)
    
    def get__action_permission(self) -> 'IActionPermission':
        from s2marine.td.client.impl.process.remote.remote_action_permission import RemoteActionPermission
        return RemoteActionPermission(self.com_obj.ActionPermission)
    
    def get__alert_manager(self) -> 'IAlertManager':
        from s2marine.td.client.impl.process.remote.remote_alert_manager import RemoteAlertManager
        return RemoteAlertManager(self.com_obj.AlertManager)
    
    def get__audit_property_factory(self) -> 'IAuditPropertyFactory':
        from s2marine.td.client.impl.process.remote.remote_audit_property_factory import RemoteAuditPropertyFactory
        return RemoteAuditPropertyFactory(self.com_obj.AuditPropertyFactory)
    
    def get__audit_record_factory(self) -> 'IAuditRecordFactory':
        from s2marine.td.client.impl.process.remote.remote_audit_record_factory import RemoteAuditRecordFactory
        return RemoteAuditRecordFactory(self.com_obj.AuditRecordFactory)
    
    def get__bug_factory(self) -> 'IBugFactory':
        from s2marine.td.client.impl.process.remote.remote_bug_factory import RemoteBugFactory
        return RemoteBugFactory(self.com_obj.BugFactory)
    
    def get__command(self) -> 'ICommand':
        from s2marine.td.client.impl.process.remote.remote_command import RemoteCommand
        return RemoteCommand(self.com_obj.Command)
    
    def get__common_settings(self) -> 'ISettings':
        from s2marine.td.client.impl.process.remote.remote_settings import RemoteSettings
        return RemoteSettings(self.com_obj.CommonSettings)
    
    def get__customization(self) -> 'ICustomization':
        from s2marine.td.client.impl.process.remote.remote_customization import RemoteCustomization
        return RemoteCustomization(self.com_obj.Customization)
    
    def get__extended_storage(self) -> 'IExtendedStorage':
        from s2marine.td.client.impl.process.remote.remote_extended_storage import RemoteExtendedStorage
        return RemoteExtendedStorage(self.com_obj.ExtendedStorage)
    
    def get__fields(self, data_type: 'str') -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields[data_type]]
    
    def get__graph_builder(self) -> 'IGraphBuilder':
        from s2marine.td.client.impl.process.remote.remote_graph_builder import RemoteGraphBuilder
        return RemoteGraphBuilder(self.com_obj.GraphBuilder)
    
    def get__host_factory(self) -> 'IHostFactory':
        from s2marine.td.client.impl.process.remote.remote_host_factory import RemoteHostFactory
        return RemoteHostFactory(self.com_obj.HostFactory)
    
    def get__host_group_factory(self) -> 'IHostGroupFactory':
        from s2marine.td.client.impl.process.remote.remote_host_group_factory import RemoteHostGroupFactory
        return RemoteHostGroupFactory(self.com_obj.HostGroupFactory)
    
    def get__mail_conditions(self) -> 'ITDMailConditions':
        from s2marine.td.client.impl.process.remote.remote_td_mail_conditions import RemoteTDMailConditions
        return RemoteTDMailConditions(self.com_obj.MailConditions)
    
    def get__product_info(self) -> 'IProductInfo':
        from s2marine.td.client.impl.process.remote.remote_product_info import RemoteProductInfo
        return RemoteProductInfo(self.com_obj.ProductInfo)
    
    def get__project_properties(self) -> 'IProjectProperties':
        from s2marine.td.client.impl.process.remote.remote_project_properties import RemoteProjectProperties
        return RemoteProjectProperties(self.com_obj.ProjectProperties)
    
    def get__req_factory(self) -> 'IReqFactory':
        from s2marine.td.client.impl.process.remote.remote_req_factory import RemoteReqFactory
        return RemoteReqFactory(self.com_obj.ReqFactory)
    
    def get__rules(self) -> 'IRuleManager':
        from s2marine.td.client.impl.process.remote.remote_rule_manager import RemoteRuleManager
        return RemoteRuleManager(self.com_obj.Rules)
    
    def get__run_factory(self) -> 'IRunFactory':
        from s2marine.td.client.impl.process.remote.remote_run_factory import RemoteRunFactory
        return RemoteRunFactory(self.com_obj.RunFactory)
    
    def get__test_factory(self) -> 'ITestFactory':
        from s2marine.td.client.impl.process.remote.remote_test_factory import RemoteTestFactory
        return RemoteTestFactory(self.com_obj.TestFactory)
    
    def get__test_set_factory(self) -> 'ITestSetFactory':
        from s2marine.td.client.impl.process.remote.remote_test_set_factory import RemoteTestSetFactory
        return RemoteTestSetFactory(self.com_obj.TestSetFactory)
    
    def get__test_set_tree_manager(self) -> 'ITestSetTreeManager':
        from s2marine.td.client.impl.process.remote.remote_test_set_tree_manager import RemoteTestSetTreeManager
        return RemoteTestSetTreeManager(self.com_obj.TestSetTreeManager)
    
    def get__text_param(self) -> 'ITextParser':
        from s2marine.td.client.impl.process.remote.remote_text_parser import RemoteTextParser
        return RemoteTextParser(self.com_obj.TextParam)
    
    def get__tree_manager(self) -> 'ITreeManager':
        from s2marine.td.client.impl.process.remote.remote_tree_manager import RemoteTreeManager
        return RemoteTreeManager(self.com_obj.TreeManager)
    
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        from s2marine.td.client.impl.process.remote.remote_ts_test_factory import RemoteTSTestFactory
        return RemoteTSTestFactory(self.com_obj.TSTestFactory)
    
    def get__user_settings(self) -> 'ISettings':
        from s2marine.td.client.impl.process.remote.remote_settings import RemoteSettings
        return RemoteSettings(self.com_obj.UserSettings)
    
    def get__vcs(self) -> 'IVCS':
        from s2marine.td.client.impl.process.remote.remote_vcs import RemoteVCS
        return RemoteVCS(self.com_obj.VCS)
    
    
