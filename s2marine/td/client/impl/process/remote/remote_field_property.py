from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.field_property_impl import FieldPropertyImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    

class RemoteFieldProperty(FieldPropertyImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        FieldPropertyImpl.__init__(self, com_obj)
    
    def get__root(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.Root)
    
    
