from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.req_impl import ReqImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    
    from s2marine.td.client.intf.i_test import ITest

class RemoteReq(ReqImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        ReqImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__history(self) -> 'IHistory':
        from s2marine.td.client.impl.process.remote.remote_history import RemoteHistory
        return RemoteHistory(self.com_obj.History)
    
    
    def get_cover_list(self, recursive: 'bool') -> 'List[ITest]':
        from s2marine.td.client.impl.process.remote.remote_test import RemoteTest
        return [RemoteTest(com_obj) for com_obj in self.com_obj.GetCoverList(recursive)]

    
