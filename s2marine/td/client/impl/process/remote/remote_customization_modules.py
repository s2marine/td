from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_modules_impl import CustomizationModulesImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteCustomizationModules(CustomizationModulesImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationModulesImpl.__init__(self, com_obj)
    
    
