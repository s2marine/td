from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.test_set_folder_impl import TestSetFolderImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode

class RemoteTestSetFolder(TestSetFolderImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        TestSetFolderImpl.__init__(self, com_obj)
    
    def get__attachments(self) -> 'IAttachmentFactory':
        from s2marine.td.client.impl.process.remote.remote_attachment_factory import RemoteAttachmentFactory
        return RemoteAttachmentFactory(self.com_obj.Attachments)
    
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.Child[index])
    
    def get__father(self) -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.Father)
    
    def get__test_set_factory(self) -> 'ITestSetFactory':
        from s2marine.td.client.impl.process.remote.remote_test_set_factory import RemoteTestSetFactory
        return RemoteTestSetFactory(self.com_obj.TestSetFactory)
    
    
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.AddNode(node_name))

    
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        from s2marine.td.client.impl.process.remote.remote_sys_tree_node import RemoteSysTreeNode
        return RemoteSysTreeNode(self.com_obj.FindChildNode(child_name))

    
