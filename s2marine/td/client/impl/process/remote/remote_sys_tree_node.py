from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.sys_tree_node_impl import SysTreeNodeImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode

class RemoteSysTreeNode(SysTreeNodeImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        SysTreeNodeImpl.__init__(self, com_obj)
    
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        return RemoteSysTreeNode(self.com_obj.Child[index])
    
    def get__father(self) -> 'ISysTreeNode':
        return RemoteSysTreeNode(self.com_obj.Father)
    
    
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        return RemoteSysTreeNode(self.com_obj.AddNode(node_name))

    
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        return RemoteSysTreeNode(self.com_obj.FindChildNode(child_name))

    
