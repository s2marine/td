from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_transition_rule_impl import CustomizationTransitionRuleImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteCustomizationTransitionRule(CustomizationTransitionRuleImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationTransitionRuleImpl.__init__(self, com_obj)
    
    
