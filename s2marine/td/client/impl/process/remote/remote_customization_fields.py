from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_fields_impl import CustomizationFieldsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField

class RemoteCustomizationFields(CustomizationFieldsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationFieldsImpl.__init__(self, com_obj)
    
    def get__field(self, table_name: 'str', field_name: 'str') -> 'ICustomizationField':
        from s2marine.td.client.impl.process.remote.remote_customization_field import RemoteCustomizationField
        return RemoteCustomizationField(self.com_obj.Field[table_name, field_name])
    
    def get__fields(self, table_name: 'str') -> 'List[ITDField]':
        from s2marine.td.client.impl.process.remote.remote_td_field import RemoteTDField
        return [RemoteTDField(com_obj) for com_obj in self.com_obj.Fields[table_name]]
    
    
    def add_active_field(self, table_name: 'str') -> 'ICustomizationField':
        from s2marine.td.client.impl.process.remote.remote_customization_field import RemoteCustomizationField
        return RemoteCustomizationField(self.com_obj.AddActiveField(table_name))

    
