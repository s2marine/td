from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.graph_builder_impl import GraphBuilderImpl

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_graph_definition import IGraphDefinition

class RemoteGraphBuilder(GraphBuilderImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        GraphBuilderImpl.__init__(self, com_obj)
    
    
    def create_graph_definition(self, module: 'int', graph_type: 'int') -> 'IGraphDefinition':
        from s2marine.td.client.impl.process.remote.remote_graph_definition import RemoteGraphDefinition
        return RemoteGraphDefinition(self.com_obj.CreateGraphDefinition(module, graph_type))

    
