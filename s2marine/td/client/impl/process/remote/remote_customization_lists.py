from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.customization_lists_impl import CustomizationListsImpl

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    

class RemoteCustomizationLists(CustomizationListsImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        CustomizationListsImpl.__init__(self, com_obj)
    
    def get__list(self, param: 'Any') -> 'ICustomizationList':
        from s2marine.td.client.impl.process.remote.remote_customization_list import RemoteCustomizationList
        return RemoteCustomizationList(self.com_obj.List[param])
    
    def get__list_by_count(self, count: 'int') -> 'ICustomizationList':
        from s2marine.td.client.impl.process.remote.remote_customization_list import RemoteCustomizationList
        return RemoteCustomizationList(self.com_obj.ListByCount[count])
    
    
