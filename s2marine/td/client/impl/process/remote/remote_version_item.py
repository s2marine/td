from typing import List, Any, TYPE_CHECKING
from s2marine.td.client.utils.base_remote_thread import BaseRemoteThread
from s2marine.td.client.impl.direct.version_item_impl import VersionItemImpl

if TYPE_CHECKING:
    pass
    
    

class RemoteVersionItem(VersionItemImpl, BaseRemoteThread):

    def __init__(self, com_obj):
        BaseRemoteThread.__init__(self)
        VersionItemImpl.__init__(self, com_obj)
    
    
