from typing import Dict, List, TYPE_CHECKING, Any

from s2marine.td.client.field import Field
from s2marine.td.client.history import History
from s2marine.td.client.intf.i_bug import IBug
from s2marine.td.client.intf.i_bug_factory import IBugFactory
from s2marine.td.client.intf.i_bug_filter import IBugFilter
from s2marine.td.client.utils.filter_field import OrderDirection
from s2marine.td.client.utils.value_enum import ValueEnums, FieldKey, FieldValue

if TYPE_CHECKING:
    from s2marine.td.client.connection import Connection


class BugFactory:
    def __init__(self, impl: 'IBugFactory', conn: 'Connection'):
        self.impl = impl
        self.conn = conn

    def add_bug(self):
        return Bug(self.impl.add_item(None), self.conn)

    def __getitem__(self, bug_id: 'int'):
        return Bug(self.impl.get__item(bug_id), self.conn)

    def filter(self):
        return BugFilter(self.impl.get__filter(), self.conn)

    def history(self):
        return History(self.impl.get__history())


class BugFilter:
    def __init__(self, impl: 'IBugFilter', conn: 'Connection'):
        self.impl = impl
        self.conn = conn

    def __getitem__(self, field: 'FieldKey'):
        if isinstance(field, ValueEnums):
            field = field.enum_val
        return self.impl.get__filter(field)

    def __setitem__(self, field: 'FieldKey', value: 'FieldValue'):
        if isinstance(field, ValueEnums):
            field = field.enum_val
        if isinstance(value, ValueEnums):
            value = value.enum_val
        self.impl.set__filter(field, value)

    def fields(self) -> 'List[Field]':
        return [Field(impl) for impl in self.impl.get__fields()]

    def set_order(self, field: 'FieldKey', priority: 'int', direction: 'OrderDirection'):
        if isinstance(field, ValueEnums):
            field = field.enum_val
        self.impl.set__order(field, priority)
        self.impl.set__order_direction(field, direction.value)

    def clear(self):
        self.impl.clear()

    def query(self) -> 'List[Bug]':
        return [Bug(impl, self.conn) for impl in self.impl.new_list()]


class Bug:
    def __init__(self, impl: 'IBug', conn: 'Connection'):
        self.impl = impl
        self.conn = conn

    @property
    def id(self):
        return self.impl.get__id()

    def attachment_factory(self):
        return self.impl.get__attachments()

    @property
    def has_attachment(self) -> 'bool':
        return self.impl.is__has_attachment()

    def is_auto_post(self):
        return self.impl.is__auto_post()

    def set_auto_post(self, value: 'bool'):
        return self.impl.set__auto_post(value)

    def post(self):
        self.impl.post()

    def undo(self):
        self.impl.undo()

    def __getitem__(self, field: 'FieldKey') -> 'Any':
        if isinstance(field, ValueEnums):
            field = field.enum_val
        return self.impl.get__field(field)

    def __setitem__(self, field: 'FieldKey', value: 'FieldValue'):
        if isinstance(field, ValueEnums):
            field = field.enum_val
        if isinstance(value, ValueEnums):
            value = value.enum_val
        self.impl.set__field(field, value)

    def history(self):
        return History(self.impl.get__history())

    def lock(self):
        self.impl.lock_object()

    def unlock(self):
        self.impl.un_lock_object()

    def update_by(self, by_bug: 'Dict[FieldKey, FieldValue]'):
        for field in by_bug.keys():
            if self[field] != by_bug[field]:
                self[field] = by_bug[field]
