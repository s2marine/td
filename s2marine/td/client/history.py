from typing import Union

from s2marine.td.client.intf.i_history import IHistory
from s2marine.td.client.intf.i_history_filter import IHistoryFilter
from s2marine.td.client.intf.i_history_record import IHistoryRecord


class History:
    def __init__(self, impl: 'IHistory'):
        self.impl = impl

    def filter(self):
        return HistoryFilter(self.impl.get__filter())

    def query(self, filter_text: 'str' = ''):
        return [HistoryRecord(impl) for impl in self.impl.new_list(filter_text)]


class HistoryFilter:
    def __init__(self, impl: 'IHistoryFilter'):
        self.impl = impl

    def __getitem__(self, field: 'Union[str]'):
        return self.impl.get__filter(field)

    def __setitem__(self, field: 'Union[str]', value: 'any'):
        self.impl.set__filter(field, value)

    def fields(self):
        return self.impl.get__fields()

    def set_order(self, field: 'Union[str]', priority: 'int', direction: 'int'):
        self.impl.set__order(field, priority)
        self.impl.set__order_direction(field, direction)

    def clear(self):
        self.impl.clear()

    def query(self):
        return [HistoryRecord(impl) for impl in self.impl.new_list()]


class HistoryRecord:
    def __init__(self, impl: 'IHistoryRecord'):
        self.impl = impl

    @property
    def changer(self):
        return self.impl.get__changer()

    @property
    def change_date(self):
        return self.impl.get__change_date()

    @property
    def field(self):
        return self.impl.get__field_name()

    @property
    def new_value(self):
        return self.impl.get__new_value()

    def __str__(self):
        return f'{self.changer} changed {self.field} ' \
               f'to {self.new_value} ' \
               f'at {self.change_date}'
