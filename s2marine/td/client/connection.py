from multiprocessing import Pipe, Process

from comtypes.client import CreateObject

from s2marine.td.client.bug import BugFactory
from s2marine.td.client.command import Command
from s2marine.td.client.impl.direct.td_connection_impl import TDConnectionImpl
from s2marine.td.client.impl.process.host.host_td_connection import HostTDConnection
from s2marine.td.client.utils.process_utils import create_process
from s2marine.td.client.utils.td_constant import TD_OBJ


class Connection:
    def __init__(self, remote=False):
        if remote:
            start_host_pipe, start_remote_pipe = Pipe()
            p = Process(target=create_process, args=(start_remote_pipe,))
            p.daemon = True
            p.start()
            host_pipe = start_host_pipe.recv()
            self.impl = HostTDConnection(host_pipe)
        else:
            com_obj = CreateObject(TD_OBJ)
            self.impl = TDConnectionImpl(com_obj)

    def connect(self, username, password, project, server_url):
        self.impl.init_connection_ex(server_url)
        self.impl.connect_project(project, username, password)

    @property
    def is_connected(self):
        return self.impl.is__connected()

    def disconnect(self):
        self.impl.release_connection()

    @property
    def username(self):
        return self.impl.get__user_name()

    def command(self):
        return Command(self.impl.get__command())

    def bug_factory(self):
        return BugFactory(self.impl.get__bug_factory(), self)

    def is_admin(self):
        return self.username == 's2marine'
