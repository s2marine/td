from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_actions import ICustomizationActions
    from s2marine.td.client.intf.i_customization_fields import ICustomizationFields
    from s2marine.td.client.intf.i_customization_lists import ICustomizationLists
    from s2marine.td.client.intf.i_customization_mail_conditions import ICustomizationMailConditions
    from s2marine.td.client.intf.i_customization_modules import ICustomizationModules
    from s2marine.td.client.intf.i_customization_permissions import ICustomizationPermissions
    from s2marine.td.client.intf.i_customization_users import ICustomizationUsers
    from s2marine.td.client.intf.i_customization_users_groups import ICustomizationUsersGroups
    


class ICustomization(metaclass=ABCMeta):
    """
    Services to perform customization tasks, such as adding users to user groups, defining user-defined fields, and defining user access privileges.
    """
    
    @abstractmethod
    def get__actions(self) -> 'ICustomizationActions':
        """
        The CustomizationActions object.
        """
        pass
    
    @abstractmethod
    def get__extended_udf_support(self) -> 'int':
        """
        Checks if the program can use the maximum allowable user-defined fields (99 user-defined fields and 3 memo fields).
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'ICustomizationFields':
        """
        The CustomizationFields object.
        """
        pass
    
    @abstractmethod
    def is__is_changed(self) -> 'bool':
        """
        Checks if there has been at least one change to a property or child object that has not been saved.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def get__lists(self) -> 'ICustomizationLists':
        """
        The CustomizationLists object.
        """
        pass
    
    @abstractmethod
    def get__mail_conditions(self) -> 'ICustomizationMailConditions':
        """
        The CustomizationMailConditions Object.
        """
        pass
    
    @abstractmethod
    def get__modules(self) -> 'ICustomizationModules':
        """
        The CustomizationModules object.
        """
        pass
    
    @abstractmethod
    def get__permissions(self) -> 'ICustomizationPermissions':
        """
        The CustomizationPermissions object.
        """
        pass
    
    @abstractmethod
    def get__users(self) -> 'ICustomizationUsers':
        """
        The CustomizationUsers object.
        """
        pass
    
    @abstractmethod
    def get__users_groups(self) -> 'ICustomizationUsersGroups':
        """
        The CustomizationUsersGroups object.
        """
        pass
    
    @abstractmethod
    def commit(self):
        """
        Posts all customization data changes to the project database.
        """
        pass

    @abstractmethod
    def load(self):
        """
        Loads all customization data from the server into the client cache. This includes actions, fields, lists, modules, permissions, users, and user groups.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def rollback(self):
        """
        Discards unsaved changes to properties and child objects.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    