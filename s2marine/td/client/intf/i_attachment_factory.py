from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_attachment_filter import IAttachmentFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_attachment import IAttachment
    
    from s2marine.td.client.intf.i_attachment import IAttachment
    from s2marine.td.client.intf.i_attachment import IAttachment


class IAttachmentFactory(metaclass=ABCMeta):
    """
    Services for managing attachments of the current field object.
    """
    
    @abstractmethod
    def get__attachment_storage(self) -> 'IExtendedStorage':
        """
        The ExtendedStorage object for downloading and uploading attachment files.
        """
        pass
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'IAttachmentFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'IAttachment':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'The attachment ID (long ). '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'IAttachment':
        """
        Creates a new item object.
        
        Passing NULL as the ItemData argument creates a virtual object, one that does not appear in the project database. After creating the item, use the relevant object properties to fill the object, then use the Post method to save the object in the database.
        
        :param item_data: When using IBaseFactory.AddItem with the AttachmentFactory, pass NULL for ItemData.
        :return: 
        """
        pass

    @abstractmethod
    def factory_properties(self, owner_type: 'str', owner_key: 'Any'):
        """
        Gets the owner type and key for the AttachmentFactory object.
        OwnerType is one of:
        "ALL_LISTS" for Subject "BUG" for Defect "CYCL_FOLD" for Test Set Folder "CYCLE" for Test Set "DESSTEPS" for Design Step "REQ" for Requirement "RUN" for Run "STEP" for Run Step "TEST" for Test "TESTCYCL" for Test in Test Set (Test Instance)
        
        :param owner_type: The owner object type.
        :param owner_key: The ID of the object to which this factory belongs.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[IAttachment]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: An Attachment.ID, a reference to an Attachment Object , or a Variant array of Attachment.IDs.
        """
        pass

    