from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class ICustomizationUsersGroups(metaclass=ABCMeta):
    """
    The collection of all CustomizationUsersGroup objects.
    """
    
    @abstractmethod
    def get__group(self, name: 'str') -> 'ICustomizationUsersGroup':
        """
        Gets the specified user group.
        
        :param name: 'The name of the group.'
        """
        pass
    
    @abstractmethod
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        """
        The list of CustomizationUsersGroup objects.
        """
        pass
    
    @abstractmethod
    def add_group(self, name: 'str') -> 'T':
        """
        Adds a new CustomizationUsersGroup to the collection.
        
        :param name: The name of the group to be added.
        :return: The new CustomizationUsersGroup Object.
        """
        pass

    @abstractmethod
    def remove_group(self, name: 'str'):
        """
        Removes a CustomizationUsersGroup object.
        
        :param name: The name of the group to be removed.
        """
        pass

    