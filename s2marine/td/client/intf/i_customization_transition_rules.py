from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule
    
    from s2marine.td.client.intf.i_customization_transition_rule import ICustomizationTransitionRule


class ICustomizationTransitionRules(metaclass=ABCMeta):
    """
    A collection of CustomizationTransitionRule objects applied to a specific field and user group.
    """
    
    @abstractmethod
    def get__count(self) -> 'ICustomizationTransitionRule':
        """
        The number of CustomizationTransitionRule objects contained by the current object.
        """
        pass
    
    @abstractmethod
    def get__entity_name(self) -> 'str':
        """
        The location (database table) of the field to which the transition rules in the current object are attached.
        """
        pass
    
    @abstractmethod
    def get__field(self) -> 'ICustomizationField':
        """
        The CustomizationField object to which the transition rules in the current object are attached.
        """
        pass
    
    @abstractmethod
    def get__group(self) -> 'ICustomizationUsersGroup':
        """
        The CustomizationUsersGroup object to which the transition rules in the current object are attached.
        """
        pass
    
    @abstractmethod
    def get__transition_rule(self, position: 'int') -> 'ICustomizationTransitionRule':
        """
        The CustomizationTransitionRule specified by its position in the list of transition rules.
        
        :param position: 'The position in the list of transition rules (1-based).'
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the object has been modified since the last synchronization with database.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the object has been modified since the last synchronization with database.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_transition_rule(self) -> 'ICustomizationTransitionRule':
        """
        Adds a new CustomizationTransitionRule to the current object.
        The new transition rule created with this method is open: it is possible to change the field from any value to any other value.  Thus, the rule has no effect until the source and destination values are set. To define the source and destination values, set the SourceValue and DestinationValue of the new CustomizationTransitionRule object.
        :return: The new CustomizationTransitionRule Object.
        """
        pass

    @abstractmethod
    def remove_transition_rule(self, rule: 'Any'):
        """
        Removes the specified transition rule from the current object.
        
        :param rule: Either the position (zero-based) in the CustomTransitionRules collection, or a CustomizationTransitionRule Object.
        """
        pass

    