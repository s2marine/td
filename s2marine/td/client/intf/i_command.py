from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_recordset import IRecordset


class ICommand(metaclass=ABCMeta):
    """
    Represents a database command.
    """
    
    @abstractmethod
    def get__affected_rows(self) -> 'int':
        """
        The number of table rows that were affected by the command.
        """
        pass
    
    @abstractmethod
    def get__command_text(self) -> 'str':
        """
        The text of the command.
        """
        pass
    
    @abstractmethod
    def set__command_text(self, value: 'str'):
        """
        The text of the command.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        Number of parameters in current command.
        """
        pass
    
    @abstractmethod
    def get__index_fields(self) -> 'str':
        """
        A comma-separated list of index fields for a SELECT command.
        """
        pass
    
    @abstractmethod
    def set__index_fields(self, value: 'str'):
        """
        A comma-separated list of index fields for a SELECT command.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__param_index(self, name: 'str') -> 'int':
        """
        The parameter index by parameter name.
        
        :param name: 'The parameter name.'
        """
        pass
    
    @abstractmethod
    def get__param_name(self, index: 'int') -> 'str':
        """
        The parameter name by parameter index. The index is 0-based.
        
        :param index: 'The parameter index.'
        """
        pass
    
    @abstractmethod
    def get__param_type(self, index: 'int') -> 'int':
        """
        The data type of the specified parameter.  The index is 0-based.
        
        :param index: 'The parameter index. The index is 0-based.'
        """
        pass
    
    @abstractmethod
    def get__param_value(self, key: 'Any') -> 'Any':
        """
        The parameter value.
        
        :param key: 'The parameter index or name. The index is 0-based.'
        """
        pass
    
    @abstractmethod
    def set__param_value(self, key: 'Any', value: 'Any'):
        """
        The parameter value.
        
        :param key: 'The parameter index or name. The index is 0-based.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_param(self, name: 'str', initial_value: 'Any'):
        """
        Adds new parameter to Command object.
        
        :param name: The name of the new parameter.
        :param initial_value: The initial value.
        """
        pass

    @abstractmethod
    def delete_param(self, key: 'Any'):
        """
        Deletes the specified parameter from Command object.
        
        :param key: The parameter index or name. The index is 0-based.
        """
        pass

    @abstractmethod
    def delete_params(self):
        """
        Deletes all parameters from Command object.
        """
        pass

    @abstractmethod
    def execute(self) -> 'IRecordset':
        """
        Executes the command in the CommandText property.
        :return: For a SELECT statement, returns a Recordset Object. Otherwise, returns nothing.
        """
        pass

    