from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class IFieldProperty(metaclass=ABCMeta):
    """
    Properties for object fields.
    """
    
    @abstractmethod
    def get__db_column_name(self) -> 'str':
        """
        The database column name.
        """
        pass
    
    @abstractmethod
    def get__db_column_type(self) -> 'str':
        """
        The database column type.
        """
        pass
    
    @abstractmethod
    def get__db_table_name(self) -> 'str':
        """
        The database table name.
        """
        pass
    
    @abstractmethod
    def get__edit_mask(self) -> 'str':
        """
        The input mask for string fields.
        """
        pass
    
    @abstractmethod
    def get__edit_style(self) -> 'str':
        """
        The user interface control type used to edit this field.
        """
        pass
    
    @abstractmethod
    def get__field_size(self) -> 'int':
        """
        The size of the field in the database (-1 for BLOB fields).
        """
        pass
    
    @abstractmethod
    def is__is_active(self) -> 'bool':
        """
        Checks if the field is can be displayed in the user interface.
        """
        pass
    
    @abstractmethod
    def is__is_by_code(self) -> 'bool':
        """
        Checks if the field saves the Tree Node ID (True), or the passed value (False).
        """
        pass
    
    @abstractmethod
    def is__is_can_filter(self) -> 'bool':
        """
        Checks if the field is displayed in the filter dialog box.
        """
        pass
    
    @abstractmethod
    def is__is_can_group(self) -> 'bool':
        """
        Checks if items of this type can form groups with others of the same type.
        """
        pass
    
    @abstractmethod
    def is__is_customizable(self) -> 'bool':
        """
        Checks if the field is displayed in the customization UI.
        """
        pass
    
    @abstractmethod
    def is__is_edit(self) -> 'bool':
        """
        Checks if the field can be edited.
        """
        pass
    
    @abstractmethod
    def is__is_history(self) -> 'bool':
        """
        Checks if change history is stored for the field.
        """
        pass
    
    @abstractmethod
    def is__is_keep_value(self) -> 'bool':
        """
        Checks if the last value for the field is stored.
        """
        pass
    
    @abstractmethod
    def is__is_key(self) -> 'bool':
        """
        Checks if this is a database key field.
        """
        pass
    
    @abstractmethod
    def is__is_mail(self) -> 'bool':
        """
        Checks if email is sent to the users on the notification list when this field changes.
        """
        pass
    
    @abstractmethod
    def is__is_modify(self) -> 'bool':
        """
        Checks if the user currently logged on the TDConnection is permitted to modify this field.
        """
        pass
    
    @abstractmethod
    def is__is_multi_value(self) -> 'bool':
        """
        Checks if the field can store multiple values.
        """
        pass
    
    @abstractmethod
    def is__is_required(self) -> 'bool':
        """
        Checks if this is a required field.
        """
        pass
    
    @abstractmethod
    def is__is_searchable(self) -> 'bool':
        """
        Checks if the content of the field is searched by the Search method.
        """
        pass
    
    @abstractmethod
    def is__is_system(self) -> 'bool':
        """
        Checks if this is a predefined system field.
        """
        pass
    
    @abstractmethod
    def is__is_to_sum(self) -> 'bool':
        """
        Checks if this field is summed for graphs.
        """
        pass
    
    @abstractmethod
    def is__is_verify(self) -> 'bool':
        """
        Checks if the field requires verification.
        """
        pass
    
    @abstractmethod
    def is__is_version_control(self) -> 'bool':
        """
        Checks if the field is under version control.
        """
        pass
    
    @abstractmethod
    def is__is_visible_in_new_bug(self) -> 'bool':
        """
        Checks if the field is visible in the New Bug form.
        """
        pass
    
    @abstractmethod
    def is__read_only(self) -> 'bool':
        """
        Checks if this is a read-only field.
        """
        pass
    
    @abstractmethod
    def get__root(self) -> 'ISysTreeNode':
        """
        The tree root connected to the field.
        """
        pass
    
    @abstractmethod
    def get__user_column_type(self) -> 'str':
        """
        The user column type: char, number, date, memo, or empty string.
        """
        pass
    
    @abstractmethod
    def get__user_label(self) -> 'str':
        """
        The user-defined label.
        """
        pass
    
    