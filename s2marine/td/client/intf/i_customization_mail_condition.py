from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ICustomizationMailCondition(metaclass=ABCMeta):
    """
    The configuration of a custom automatic mail notification.
    """
    
    @abstractmethod
    def get__condition_text(self) -> 'str':
        """
        The filter to determine when the notification is sent.
        """
        pass
    
    @abstractmethod
    def set__condition_text(self, value: 'str'):
        """
        The filter to determine when the notification is sent.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__condition_type(self) -> 'int':
        """
        The type of condition.
        """
        pass
    
    @abstractmethod
    def is__deleted(self) -> 'bool':
        """
        Indicates if the condition has been removed, but the deletion has not been posted to the server.
        """
        pass
    
    @abstractmethod
    def set__deleted(self, value: 'bool'):
        """
        Indicates if the condition has been removed, but the deletion has not been posted to the server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The name of the mail recipient or the field containing the name.
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the condition object has changed locally since last synchronized with the server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the condition object has changed locally since last synchronized with the server.
        :param value: 
        """
        pass
    
    