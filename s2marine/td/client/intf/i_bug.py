from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    


class IBug(metaclass=ABCMeta):
    """
    A defect.
    """
    
    @abstractmethod
    def get__assigned_to(self) -> 'str':
        """
        The name of the user to whom the defect is assigned.
        """
        pass
    
    @abstractmethod
    def set__assigned_to(self, value: 'str'):
        """
        The name of the user to whom the defect is assigned.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__detected_by(self) -> 'str':
        """
        The name of the user who detected the defect.
        """
        pass
    
    @abstractmethod
    def set__detected_by(self, value: 'str'):
        """
        The name of the user who detected the defect.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__priority(self) -> 'str':
        """
        The defect priority.
        """
        pass
    
    @abstractmethod
    def set__priority(self, value: 'str'):
        """
        The defect priority.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__project(self) -> 'str':
        """
        The testing project to which the defect belongs.
        """
        pass
    
    @abstractmethod
    def set__project(self, value: 'str'):
        """
        The testing project to which the defect belongs.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The defect status.
        """
        pass
    
    @abstractmethod
    def set__status(self, value: 'str'):
        """
        The defect status.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__summary(self) -> 'str':
        """
        A short description of the defect.
        """
        pass
    
    @abstractmethod
    def set__summary(self, value: 'str'):
        """
        A short description of the defect.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def find_similar_bugs(self, similarity_ratio: 'int') -> 'List[T]':
        """
        Searches the defect summaries for similarities to this defect.
        For more information on the similarity ratio, see 'Matching Defects' under 'Adding New Defects' in the Quality Center User's Guide.
        
        :param similarity_ratio: Specifies the similarity ratio (0-100).
        :return: A FactoryList of Bug objects.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        """
        Mails the IBaseFieldExMail field item.
        
        :param send_to: The recipient address or addresses. Addresses can be separated by a comma or a semicolon.
        :param send_cc: The recipients of a carbon copy of the mail. Addresses can be separated by a comma or a semicolon.
        :param option: The mailing options binary mask.Create the mask using values of TDMAIL_FLAGS.
        :param subject: The mail subject line.
        :param comment: A comment on the sent e-mail. If the TDMAIL_COMMENT_AS_BODY option is set, this will be sent as the message body.
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    