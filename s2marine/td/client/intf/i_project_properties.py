from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IProjectProperties(metaclass=ABCMeta):
    """
    Global project parameters and settings.
    """
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of parameters in the project.
        """
        pass
    
    @abstractmethod
    def is__is_param(self, param_name: 'str') -> 'bool':
        """
        Checks if a parameter of this name exists.
        
        :param param_name: 'The name of a project parameter'
        """
        pass
    
    @abstractmethod
    def get__param_name(self, param_index: 'int') -> 'str':
        """
        Gets the parameter name.
        
        :param param_index: 'The index of the parameter in the list of project properties (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__param_value(self, v_param: 'Any') -> 'str':
        """
        The value of the parameter.
        
        :param v_param: 'Either the name of the parameter or the index of the parameter in the list of project properties (zero-based).'
        """
        pass
    
    