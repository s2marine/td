from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_condition import ICondition
    
    from s2marine.td.client.intf.i_condition import ICondition


class IConditionFactory(metaclass=ABCMeta):
    """
    Services for managing conditions for executing test instances in an test set.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'ICondition':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'A long integer. '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'ICondition':
        """
        Creates a new item object.
        
        Passing NULL as the ItemData argument creates a virtual object, one that does not appear in the project database. After creating the item, use the relevant object properties to fill the object, then use the Post method to save the object in the database.
        
        :param item_data: There are two options for ItemData: 
        
        Null. Initially creating a virtual object with Null ensures that you cannot then Post until all required fields are initialized. 
        An array consisting of the following elements: 
        
        (0) Type - The condition type (long, required).  1 for run or 2 for time.
        (1) Source - For a run condition, the source condition ID (long). For a time condition, the date-time string. (required) 
        (2) Target - The target condition ID (long, required).
        (3) Value - The condition value. (required). For a time condition, the date-time string. For a run condition, either TDCOND_PASSED  or TDCOND_FAILED.
        (4) Desc - A description of the condition (string, optional).
        :return: A Condition Object.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: A Condition.ID (long), a reference to a Condition Object, or a variant array of Condition IDs.
        """
        pass

    @abstractmethod
    def save(self):
        """
        Saves all conditions.
        """
        pass

    