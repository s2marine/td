from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_bug_filter import IBugFilter
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_bug import IBug
    
    from s2marine.td.client.intf.i_bug import IBug
    from s2marine.td.client.intf.i_graph import IGraph


class IBugFactory(metaclass=ABCMeta):
    """
    Services for managing defect records.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'IBugFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'IBug':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'The defect ID (long). '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'IBug':
        """
        Creates a new item object.
        
        :param item_data: When creating defects, always pass Null as the ItemData argument.
        :return: A Bug Object.
        """
        pass

    @abstractmethod
    def build_age_graph(self, group_by_field: 'str', sum_of_field: 'str', max_age: 'int', max_cols: 'int', filter: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'IGraph':
        """
        Creates a graph that shows the lifetime of defects.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: The field whose values are summed for all entities in a group:One of Run 'Duration', Defect 'Actual Fix Time', or Defect 'Estimated Fix Time'.If the argument  is not used, the resulting value is the count of the entities in the group.
        :param max_age: The maximum age (in days) of defects included in the graph. A value of 0 signifies no age limit.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates a graph showing status at specific points: either defect accumulation or  estimated/actual time to fix.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: The field whose values are summed for all entities in a group:One of Run 'Duration', Defect 'Actual Fix Time', or Defect 'Estimated Fix Time'.If the argument  is not used, the resulting value is the count of the entities in the group.
        :param by_history: Determines whether to use history information to create the graph. Possible values are: 
        TRUE - Use history information. 
        FALSE - Do not use history information.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates a summary graph either of the number of defects or the estimated/actual amount of time to fix.
        
        :param x_axis_field: A database field representing the name of the graph x-axis.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: The field whose values are summed for all entities in a group:One of Run 'Duration', Defect 'Actual Fix Time', or Defect 'Estimated Fix Time'.If the argument  is not used, the resulting value is the count of the entities in the group.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates a graph that shows the history of changes to specific Defect fields for each time interval displayed.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: Not in use in BugFactory BuildTrendGraph.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def find_similar_bugs(self, pattern: 'str', similarity_ratio: 'int') -> 'List[T]':
        """
        Searches defect summaries and descriptions for matches to Pattern.
        For more information on the similarity ratio, see 'Matching Defects' under 'Adding New Defects' in the Quality Center User's Guide.
        
        :param pattern: The search text.
        :param similarity_ratio: Specifies the percentage of similarity (0-100).
        :return: A FactoryList of Bug objects.
        """
        pass

    @abstractmethod
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        """
        Mails the list of IBase Factory Items. 'Items' is a list of ID numbers.
        
        :param items: A list of IDs.
        :param send_to: A Quality Center user name, an e-mail address (not necessarily a Quality Center user), or a list of e-mail addresses. Addresses can be separated by a comma or a semi-colon.
        :param send_cc: The recipients of a carbon copy of the mail. Addresses can be separated by a comma or a semicolon.
        :param option: The mailing options binary mask.Create the mask using values of TDMAIL_FLAGS.
        :param subject: The mail subject line.
        :param comment: A comment on the sent e-mail. If the TDMAIL_COMMENT_AS_BODY option is set, this will be sent as the message body.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: The defect ID (long), a reference to the Bug Object, or a variant array of defect IDs.
        """
        pass

    