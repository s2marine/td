from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class ICustomizationLists(metaclass=ABCMeta):
    """
    The collection of all CustomizationList objects.
    """
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of lists in the CustomizationLists object.
        """
        pass
    
    @abstractmethod
    def is__is_list_exist(self, list_name: 'str') -> 'bool':
        """
        Checks if the specified customization list exists.
        
        :param list_name: 'The name of the list whose existence you want to check.'
        """
        pass
    
    @abstractmethod
    def get__list(self, param: 'Any') -> 'ICustomizationList':
        """
        Gets the CustomizationList specified by name or ID.
        
        :param param: 'The list name (string) or root ID (long). '
        """
        pass
    
    @abstractmethod
    def get__list_by_count(self, count: 'int') -> 'ICustomizationList':
        """
        Gets the CustomizationList specified by its index number.
        
        :param count: 'The index is 1-based.'
        """
        pass
    
    @abstractmethod
    def add_list(self, name: 'str') -> 'T':
        """
        Adds a new CustomizationList object.
        
        :param name: The name of the list to be added.
        :return: A CustomizationList Object.
        """
        pass

    @abstractmethod
    def remove_list(self, name: 'str'):
        """
        Removes a CustomizationList object.
        
        :param name: The name of the list to be removed.
        """
        pass

    