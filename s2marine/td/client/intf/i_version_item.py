from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IVersionItem(metaclass=ABCMeta):
    """
    Represents specific version information.
    """
    
    @abstractmethod
    def get__comments(self) -> 'str':
        """
        The check-in comments.
        """
        pass
    
    @abstractmethod
    def get__date(self) -> 'str':
        """
        The check-in date.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if the item is locked.
        """
        pass
    
    @abstractmethod
    def get__time(self) -> 'str':
        """
        The check-in time.
        """
        pass
    
    @abstractmethod
    def get__user(self) -> 'str':
        """
        The user who performed the check-in.
        """
        pass
    
    @abstractmethod
    def get__version(self) -> 'str':
        """
        The version checked in.
        """
        pass
    
    