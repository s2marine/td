from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IExecEventInfo(metaclass=ABCMeta):
    """
    The execution information of the scheduler. This object handles the actual, not the planned, information.
    """
    
    @abstractmethod
    def get__event_date(self) -> 'str':
        """
        The Execution Event Date.
        """
        pass
    
    @abstractmethod
    def get__event_time(self) -> 'str':
        """
        The Execution Event Time.
        """
        pass
    
    @abstractmethod
    def get__event_type(self) -> 'int':
        """
        The type of Execution Event.
        """
        pass
    
    