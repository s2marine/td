from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_req import IReq
    
    from s2marine.td.client.intf.i_req import IReq


class IReqFactory(metaclass=ABCMeta):
    """
    Services for managing requirements.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'IReq':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'The requirement ID (long). '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'T':
        """
        Creates a new item object.
        
        Passing NULL as the ItemData argument creates a virtual object, one that does not appear in the project database. After creating the item, use the relevant object properties to fill the object, then use the Post method to save the object in the database.
        
        :param item_data: There are three options for ItemData: 
        
        Null. Creating a virtual Req object with Null ensures that you cannot then Post until all required fields are initialized. 
        The parent requirement ID (Variant). 
        An array consisting of the following elements: 
        
        (0) FatherID - The parent requirement ID (long or string, required). 
        (1) Order - The new requirement position in the children list (long, optional). 
             Order is either: 
        
        
        A number representing the requirement position. 
        TDPOSITION_LAST [-4] - Default. Indicates that the requirement is inserted last.
        :return: 
        """
        pass

    @abstractmethod
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds Performance Graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_perf_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        """
        Builds Performance Graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :param show_null_parents: Only for Requirements mode. A value of True allows parent folders, which have no test coverage, from being viewed during Cover Status queries.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds Req progress graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param by_history: Determines whether to use history information to create the graph. Possible values are: 
        TRUE - Use history information. 
        FALSE - Do not use history information.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        """
        Builds Requirement progress graph. Extends BuildProgressGraph with ability to display Null parent folders.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param by_history: Determines whether to use history information to create the graph. Possible values are: 
        TRUE - Use history information. 
        FALSE - Do not use history information.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :param show_null_parents: Only for Requirements mode. A value of True allows parent folders, which have no test coverage, from being viewed during Cover Status queries.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds Req summary graph.
        
        :param x_axis_field: A database field representing the name of the graph x-axis.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_summary_graph_ex(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        """
        Builds Req summary graph. Extends BuildSummaryGraph with ability to display Null parent folders.
        
        :param x_axis_field: A database field representing the name of the graph x-axis.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :param show_null_parents: Only for Requirements mode. A value of True allows parent folders, which have no test coverage, from being viewed during Cover Status queries.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates graph that shows the number of defect status changes over time.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_trend_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool', show_null_parents: 'bool') -> 'T':
        """
        Creates graph that shows the number of defect status changes over time.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :param show_null_parents: Only for Requirements mode. A value of True allows parent folders, which have no test coverage, from being viewed during Cover Status queries.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def find(self, start_root_id: 'int', field_name: 'str', pattern: 'str', mode: 'int', limit: 'int') -> 'List[T]':
        """
        Gets a list of requirements containing the Pattern value in specified field.
        
        :param start_root_id: The ID of the root of the subtree from which to start the search.
        :param field_name: The name of the field to scan for the value.
        :param pattern: The search pattern. Regular expression are permitted in SQL format only.
        :param mode: A search option. Use values from the tagTDAPI_REQMODE Enumeration.
        :param limit: The maximum number of matches to report.
        :return: A list with format "ID,NAME".
        """
        pass

    @abstractmethod
    def get_children_list(self, father_id: 'int') -> 'List[IReq]':
        """
        Gets list of child requirements.
        
        :param father_id: The ID of the parent requirement.
        :return: A list of Req Objects.
        """
        pass

    @abstractmethod
    def get_filtered_children_list(self, father_id: 'int', filter: 'TDFilter') -> 'List[T]':
        """
        Gets filtered list of child requirements.
        If no filter is passed, all child requirements are included in the list.
        
        :param father_id: The Req.ID of the requirement whose child requirements are to be retreived.
        :param filter: A TDFilter Object that provides criteria for the child requirements
        :return: A list of Req Objects.
        """
        pass

    @abstractmethod
    def mail(self, items: 'Any', send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        """
        Mails the list of IBase Factory Items. 'Items' is a list of ID numbers.
        
        :param items: A list of IDs.
        :param send_to: A Quality Center user name, an e-mail address (not necessarily a Quality Center user), or a list of e-mail addresses. Addresses can be separated by a comma or a semi-colon.
        :param send_cc: The recipients of a carbon copy of the mail. Addresses can be separated by a comma or a semicolon.
        :param option: The mailing options binary mask.Create the mask using values of TDMAIL_FLAGS.
        :param subject: The mail subject line.
        :param comment: A comment on the sent e-mail. If the TDMAIL_COMMENT_AS_BODY option is set, this will be sent as the message body.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: The requirement ID (long), a reference to the Req Object or a Variant array of Reg.IDs.
        """
        pass

    