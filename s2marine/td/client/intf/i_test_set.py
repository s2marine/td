from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_condition_factory import IConditionFactory
    from s2marine.td.client.intf.i_exec_event_notify_by_mail_settings import IExecEventNotifyByMailSettings
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    
    from s2marine.td.client.intf.i_ts_scheduler import ITSScheduler


class ITestSet(metaclass=ABCMeta):
    """
    A group of tests designed to meet a specific testing goal.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__condition_factory(self) -> 'IConditionFactory':
        """
        The condition factory object for the current test set.
        """
        pass
    
    @abstractmethod
    def get__exec_event_notify_by_mail_settings(self) -> 'IExecEventNotifyByMailSettings':
        """
        Gets the execution mail notification settings for the current test set.
        """
        pass
    
    @abstractmethod
    def get__execution_settings(self) -> 'IExecSettings':
        """
        Gets the execution settings for the test set.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The test set name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The test set name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The test set status.
        """
        pass
    
    @abstractmethod
    def set__status(self, value: 'str'):
        """
        The test set status.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__test_default_execution_settings(self) -> 'IExecSettings':
        """
        Gets default execution settings for tests.
        """
        pass
    
    @abstractmethod
    def get__test_set_folder(self) -> 'ITestSetFolder':
        """
        The test set's parent node.
        """
        pass
    
    @abstractmethod
    def set__test_set_folder(self, value: 'ITestSetFolder'):
        """
        The test set's parent node.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        """
        The factory object for the current test set.
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def build_perf_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates Performance Graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph(self, group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds test set progress graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph_ex(self, group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds test set progress graph.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param by_history: Determines whether to use history information to create the graph. Possible values are: 
        TRUE - Use history information. 
        FALSE - Do not use history information.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_summary_graph(self, x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Builds test set summary graph.
        
        :param x_axis_field: A database field representing the name of the graph x-axis.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_trend_graph(self, group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates graph that shows the test set changes over time.
        
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def check_test_instances(self, test_i_ds: 'str') -> 'str':
        """
        Gets information about the instances of the specified test in this test set.
        
        :param test_i_ds: A Test.ID or a comma-separated list of IDs.
        :return: For each Test ID, an output line of format:
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def purge_executions(self):
        """
        Purges all the running executions within the test set.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def reset_test_set(self, delete_runs: 'bool'):
        """
        Sets the status of each test in the test set to 'No Run'.
        
        :param delete_runs: If TRUE, the method deletes all runs for all tests in the test set.
        """
        pass

    @abstractmethod
    def start_execution(self, server_name: 'str') -> 'ITSScheduler':
        """
        Starts the execution controller.
        
        The scheduler server is not necessarily the machine on which the tests will run, only the machine on which the scheduler runs. The remote test execution hosts can be set with properties of the TSScheduler Object.
        
        :param server_name: The name of the host for the scheduler. If an empty string is passed, localhost is used.
        :return: A TSScheduler Object.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    