from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IRecordset(metaclass=ABCMeta):
    """
    Represents the entire set of records resulting from an executed command. At any given time, a Recordset object refers to a single record within the record set as the current record.
    """
    
    @abstractmethod
    def is__bor(self) -> 'bool':
        """
        Checks if the record cursor is before the first record. True when the object is created, and after the Prev method was called when the first record was current.
        """
        pass
    
    @abstractmethod
    def get__cache_size(self) -> 'int':
        """
        The number of records in the recordset that are cached locally in memory.
        """
        pass
    
    @abstractmethod
    def set__cache_size(self, value: 'int'):
        """
        The number of records in the recordset that are cached locally in memory.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__col_count(self) -> 'int':
        """
        Gets the number of columns in the current Recordset object.
        """
        pass
    
    @abstractmethod
    def get__col_index(self, name: 'str') -> 'int':
        """
        Gets the index (zero-based) of a Recordset column specified by column name.
        
        :param name: 'The name of the column in the project database.'
        """
        pass
    
    @abstractmethod
    def get__col_name(self, index: 'int') -> 'str':
        """
        The name of the Recordset column specified by Index (zero-based).
        
        :param index: 'The index of the column (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__col_size(self, index: 'int') -> 'int':
        """
        The physical size as defined in the database of the fields in the column specified by Index (zero-based).
        
        :param index: 'The index of the column (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__col_type(self, index: 'int') -> 'int':
        """
        The data type of column.
        
        :param index: 'The index of the column (zero-based).'
        """
        pass
    
    @abstractmethod
    def is__eor(self) -> 'bool':
        """
        Checks if the record cursor is after the last record.
        """
        pass
    
    @abstractmethod
    def get__field_value(self, field_key: 'Any') -> 'Any':
        """
        The value for the specified field.
        
        :param field_key: 'The column index (long) or column name.'
        """
        pass
    
    @abstractmethod
    def set__field_value(self, field_key: 'Any', value: 'Any'):
        """
        The value for the specified field.
        
        :param field_key: 'The column index (long) or column name.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__position(self) -> 'int':
        """
        The current record position (0-based).
        """
        pass
    
    @abstractmethod
    def set__position(self, value: 'int'):
        """
        The current record position (0-based).
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__record_count(self) -> 'int':
        """
        The number of records in the Recordset.
        """
        pass
    
    @abstractmethod
    def clone(self) -> 'T':
        """
        Creates a duplicate of this Recordset object.
        :return: The new Recordset object.
        """
        pass

    @abstractmethod
    def first(self):
        """
        Moves to the first record and makes it the current record.
        """
        pass

    @abstractmethod
    def last(self):
        """
        Moves to the last record and makes it the current record.
        """
        pass

    @abstractmethod
    def next(self):
        """
        Moves to the next record and makes it the current record.
        """
        pass

    @abstractmethod
    def prev(self):
        """
        Moves to the previous record and makes it the current record.
        """
        pass

    @abstractmethod
    def refresh(self, range: 'int', low: 'int', high: 'int'):
        """
        Refreshes the Recordset, overwriting values in memory.
        
        :param range: For internal use only.
        :param low: For internal use only.
        :param high: For internal use only.
        """
        pass

    