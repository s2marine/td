from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_graph_definition import IGraphDefinition


class IGraphBuilder(metaclass=ABCMeta):
    """
    Services for creating graphs.
    """
    
    @abstractmethod
    def build_graph(self, p_graph_def: 'IGraphDefinition') -> 'T':
        """
        Build the graph.
        
        :param p_graph_def: A GraphDefinition Object.
        :return: A Graph Object.
        """
        pass

    @abstractmethod
    def build_multiple_graphs(self, graph_defs: 'Any') -> 'List[T]':
        """
        Build several graphs.
        
        :param graph_defs: A Variant array of GraphDefinition Objects.
        :return: A list of Graph Objects.
        """
        pass

    @abstractmethod
    def create_graph_definition(self, module: 'int', graph_type: 'int') -> 'IGraphDefinition':
        """
        Create an IGraphDefinition object.
        
        :param module: A value of the tagTDAPI_MODULE Enumeration.
        :param graph_type: A value of the tagTDAPI_GRAPH_TYPE Enumeration.
        :return: A GraphDefinition Object.
        """
        pass

    