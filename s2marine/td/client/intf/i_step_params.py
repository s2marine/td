from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IStepParams(metaclass=ABCMeta):
    """
    A collection of test parameters.
    """
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of parameters for the object.
        """
        pass
    
    @abstractmethod
    def is__param_exist(self, param_name: 'str') -> 'bool':
        """
        Checks if a parameter of this name exists.
        
        :param param_name: 'The name of the parameter.'
        """
        pass
    
    @abstractmethod
    def get__param_name(self, n_position: 'int') -> 'str':
        """
        The name of the parameter in the specified position in the StepParams collection (zero-based).
        
        :param n_position: 'The position (zero-based) of this parameter in the StepParams collection.'
        """
        pass
    
    @abstractmethod
    def get__param_value(self, v_param: 'Any') -> 'str':
        """
        The value of the specified parameter.
        
        :param v_param: 'Either the index (zero-based) in the StepParams collection, or the name of the parameter.'
        """
        pass
    
    @abstractmethod
    def set__param_value(self, v_param: 'Any', value: 'str'):
        """
        The value of the specified parameter.
        
        :param v_param: 'Either the index (zero-based) in the StepParams collection, or the name of the parameter.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__type(self, v_param: 'Any') -> 'int':
        """
        Gets the parameter type: predefined, null, or regular.
        
        :param v_param: 'Either the index (zero-based) in the StepParams collection, or the name of the parameter.'
        """
        pass
    
    @abstractmethod
    def add_param(self, param_name: 'str', param_type: 'str'):
        """
        Adds a new parameter to the object.
        
        :param param_name: The name of the new parameter.
        :param param_type: Always pass the string "Text".
        """
        pass

    @abstractmethod
    def clear_param(self, v_param: 'Any'):
        """
        Clears the parameter value by setting it to null.
        
        :param v_param: Either the index (zero-based) in the StepParams collection, or the name of 
        the parameter.
        """
        pass

    @abstractmethod
    def delete_param(self, param_name: 'str'):
        """
        Deletes the specified parameter.
        
        :param param_name: The name of the parameter to delete.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Refreshes the data from the database, discarding changes.
        """
        pass

    @abstractmethod
    def save(self):
        """
        Uploads the parameter definitions to the database.
        """
        pass

    