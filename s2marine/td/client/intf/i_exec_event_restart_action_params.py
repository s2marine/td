from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IExecEventRestartActionParams(metaclass=ABCMeta):
    """
    Information on actions to be taken during restart after completion of a test set.
    """
    
    @abstractmethod
    def get__cleanup_test(self) -> 'Any':
        """
        The test ID of the test to run before each rerun.
        """
        pass
    
    @abstractmethod
    def set__cleanup_test(self, value: 'Any'):
        """
        The test ID of the test to run before each rerun.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__number_of_retries(self) -> 'int':
        """
        The number of retries if a test fails.
        """
        pass
    
    @abstractmethod
    def set__number_of_retries(self, value: 'int'):
        """
        The number of retries if a test fails.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__on_exec_event_scheduler_action(self) -> 'int':
        """
        The action to be taken.
        """
        pass
    
    