from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IFileData(metaclass=ABCMeta):
    """
    Information about a folder or file.
    """
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The file or folder name.
        """
        pass
    
    @abstractmethod
    def items(self) -> 'List[T]':
        """
        The folder's child items.
        :return: 
        """
        pass

    @abstractmethod
    def modify_date(self) -> 'float':
        """
        The file's modify date.
        :return: 
        """
        pass

    @abstractmethod
    def size(self) -> 'int':
        """
        The file size in Kbytes.
        :return: 
        """
        pass

    @abstractmethod
    def type(self) -> 'int':
        """
        The type of object for which data is stored.
        :return: A value of the tagTDAPI_FDATA_TYPE Enumeration.
        """
        pass

    