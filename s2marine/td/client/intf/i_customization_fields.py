from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_customization_field import ICustomizationField


class ICustomizationFields(metaclass=ABCMeta):
    """
    The collection of all CustomizationField objects.
    """
    
    @abstractmethod
    def get__field(self, table_name: 'str', field_name: 'str') -> 'ICustomizationField':
        """
        The customization field object for the specified database table and field.
        
        :param table_name: 'The name of the database table.'
        :param field_name: 'The name of the field in the table.'
        """
        pass
    
    @abstractmethod
    def is__field_exists(self, table_name: 'str', field_name: 'str') -> 'bool':
        """
        Checks if the specified database field exists.
        
        :param table_name: 'The name of the database table.'
        :param field_name: 'The name of the field in the table.'
        """
        pass
    
    @abstractmethod
    def get__fields(self, table_name: 'str') -> 'List[ITDField]':
        """
        A list of all fields in the specified table.
        
        :param table_name: 'The name of the database table.'
        """
        pass
    
    @abstractmethod
    def add_active_field(self, table_name: 'str') -> 'ICustomizationField':
        """
        Finds the first free, inactive field in the specified table, signs the field as active, and returns the CustomizationFields object representing the field.
        
        :param table_name: The name of the table to which to add the field.
        :return: A CustomizationField Object.
        """
        pass

    @abstractmethod
    def add_active_memo_field(self, table_name: 'str') -> 'T':
        """
        Creates a new memo field in a table.
        
        :param table_name: The name of the table to which to add the field.
        :return: A CustomizationField Object.
        """
        pass

    