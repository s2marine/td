from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    
    from s2marine.td.client.intf.i_attachment import IAttachment


class IAttachmentFilter(metaclass=ABCMeta):
    """
    Services for creation of a filtered list of field objects without SQL.
    """
    
    @abstractmethod
    def is__case_sensitive(self, field_name: 'str') -> 'bool':
        """
        Indicates if the filter for the specified field is case sensitive. The default is False.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__case_sensitive(self, field_name: 'str', value: 'bool'):
        """
        Indicates if the filter for the specified field is case sensitive. The default is False.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__data_type(self) -> 'str':
        """
        The filtered entity type.
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of fields for the TDFilter's parent object.
        """
        pass
    
    @abstractmethod
    def get__filter(self, field_name: 'str') -> 'str':
        """
        The filter condition for the field specified by the FieldName.
        
        :param field_name: 'The name of a field of the entity to which the filter is applied.
        Only fields whose FieldProperty.IsCanFilter Property is true can be used.'
        """
        pass
    
    @abstractmethod
    def set__filter(self, field_name: 'str', value: 'str'):
        """
        The filter condition for the field specified by the FieldName.
        
        :param field_name: 'The name of a field of the entity to which the filter is applied.
        Only fields whose FieldProperty.IsCanFilter Property is true can be used.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__order(self, field_name: 'str') -> 'int':
        """
        The position of the specified field in the list of the sort fields. (zero-based)
        
        :param field_name: 'The name of the database field whose sort priority is to be retrieved or set.'
        """
        pass
    
    @abstractmethod
    def set__order(self, field_name: 'str', value: 'int'):
        """
        The position of the specified field in the list of the sort fields. (zero-based)
        
        :param field_name: 'The name of the database field whose sort priority is to be retrieved or set.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__order_direction(self, field_name: 'str') -> 'int':
        """
        The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__order_direction(self, field_name: 'str', value: 'int'):
        """
        The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__text(self) -> 'str':
        """
        The full fields filter as text.
        """
        pass
    
    @abstractmethod
    def set__text(self, value: 'str'):
        """
        The full fields filter as text.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__x_filter(self, entity_type: 'str') -> 'str':
        """
        The Intersection Filter according to the entity type.
        
        :param entity_type: 'Possible values: "BUG-REQ", "BUG-TEST", "BUG-TESTSET", "REQ-BUG", "REQ-TEST", "RUN-BUG", "TEST-BUG", "TEST-REQ", "TEST-TESTSET", "TEST-VER_CTRL", "TESTSET-BUG", "TESTSET-TEST", "TSTEST-BUG", and "TSTEST-REQ".'
        """
        pass
    
    @abstractmethod
    def set__x_filter(self, entity_type: 'str', value: 'str'):
        """
        The Intersection Filter according to the entity type.
        
        :param entity_type: 'Possible values: "BUG-REQ", "BUG-TEST", "BUG-TESTSET", "REQ-BUG", "REQ-TEST", "RUN-BUG", "TEST-BUG", "TEST-REQ", "TEST-TESTSET", "TEST-VER_CTRL", "TESTSET-BUG", "TESTSET-TEST", "TSTEST-BUG", and "TSTEST-REQ".'
        :param value: 
        """
        pass
    
    @abstractmethod
    def clear(self):
        """
        Clears the current filter.
        """
        pass

    @abstractmethod
    def get_x_filter(self, join_entities: 'str', inclusive: 'bool') -> 'str':
        """
        Gets the intersection filter specified by JoinEntities.
        
        :param join_entities: Possible values: "BUG-REQ", "BUG-TEST", "BUG-TESTSET", "REQ-BUG", "REQ-TEST", "RUN-BUG", "TEST-BUG", "TEST-REQ", "TEST-TESTSET", "TEST-VER_CTRL", "TESTSET-BUG", "TESTSET-TEST", "TSTEST-BUG", and "TSTEST-REQ".
        :param inclusive: Output variable.If True, the filter specifies the set of objects where the conditions for the first entity and for the second entity are all true.
        If False, the filter specifies the set of objects where the conditions for the first entity are true and the conditions for the second entity are not true.
        :return: The text representation of the filter.
        """
        pass

    @abstractmethod
    def is_clear(self) -> 'bool':
        """
        Checks whether the filter is clear, including cross-filter settings
        :return: 
        """
        pass

    @abstractmethod
    def new_list(self) -> 'List[IAttachment]':
        """
        Creates a new list from current filter.
        :return: 
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved data and refreshes the fetched list according to the current filter, overwriting values in memory.
        """
        pass

    @abstractmethod
    def set_x_filter(self, join_entities: 'str', inclusive: 'bool', filter_text: 'str'):
        """
        Sets the intersection filter, the filter for second entity.
        
        The best way to ensure that the Filter text is valid, is to create a TDFilter with the cross-filter conditions, and use its Text property as this value.
        
        :param join_entities: Possible values: "BUG-REQ", "BUG-TEST", "BUG-TESTSET", "REQ-BUG", "REQ-TEST", "RUN-BUG", "TEST-BUG", "TEST-REQ", "TEST-TESTSET", "TEST-VER_CTRL", "TESTSET-BUG", "TESTSET-TEST", "TSTEST-BUG", and "TSTEST-REQ".
        :param inclusive: If True, the filter specifies the set of objects where the conditions for the first entity and for the second entity are all true.
        If False, the filter specifies the set of objects where the conditions for the first entity are true and the conditions for the second entity are not true.
        :param filter_text: A TDFilter.Text or a string that would be a valid TDFilter.Text
        """
        pass

    