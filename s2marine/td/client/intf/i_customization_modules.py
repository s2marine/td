from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ICustomizationModules(metaclass=ABCMeta):
    """
    Services for managing the customization modules.
    """
    
    @abstractmethod
    def get__description(self, module_id: 'int') -> 'str':
        """
        The Module Description
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__description(self, module_id: 'int', value: 'str'):
        """
        The Module Description
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__guid(self, module_id: 'int') -> 'str':
        """
        The Module's globally unique ID.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__guid(self, module_id: 'int', value: 'str'):
        """
        The Module's globally unique ID.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_visible_for_group(self, module_id: 'int', group: 'Any') -> 'bool':
        """
        Indicates if the specified module is visible in the UI to the specified user group.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param group: 'The CustomizationUsersGroup.ID.'
        """
        pass
    
    @abstractmethod
    def set__is_visible_for_group(self, module_id: 'int', group: 'Any', value: 'bool'):
        """
        Indicates if the specified module is visible in the UI to the specified user group.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param group: 'The CustomizationUsersGroup.ID.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__name(self, module_id: 'int') -> 'str':
        """
        The Module Name.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__name(self, module_id: 'int', value: 'str'):
        """
        The Module Name.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__visible(self, module_id: 'int') -> 'int':
        """
        A bit-mask that indicates for which groups the module is visible in the user interface.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__visible(self, module_id: 'int', value: 'int'):
        """
        A bit-mask that indicates for which groups the module is visible in the user interface.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__visible_for_groups(self, module_id: 'int') -> 'List[T]':
        """
        The list of the user groups to which the specified module is visible in the UI.
        
        :param module_id: 'The module type. A value of the tagTDAPI_MODULE Enumeration.'
        """
        pass
    
    