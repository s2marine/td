from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_field_property import IFieldProperty
    


class ITDField(metaclass=ABCMeta):
    """
    Properties for a field.
    """
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The database name of the current field.
        """
        pass
    
    @abstractmethod
    def get__property(self) -> 'IFieldProperty':
        """
        The FieldProperty object associated with the current field.
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'int':
        """
        The field database type.
        """
        pass
    
    @abstractmethod
    def is_valid_value(self, value: 'Any', p_object: 'T'):
        """
        Checks if the input value is valid for the field.
        
        :param value: The value to check.
        :param p_object: Any object that implements the IBaseField Interface.
        """
        pass

    