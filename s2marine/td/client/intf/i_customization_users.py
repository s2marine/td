from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser
    


class ICustomizationUsers(metaclass=ABCMeta):
    """
    The collection of all CustomizationUser objects.
    """
    
    @abstractmethod
    def get__user(self, name: 'str') -> 'ICustomizationUser':
        """
        The CustomizationUser object representing the specified user.
        
        :param name: 'The user name.'
        """
        pass
    
    @abstractmethod
    def get__users(self) -> 'List[ICustomizationUser]':
        """
        The list of customization users.
        """
        pass
    
    @abstractmethod
    def add_site_authenticated_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', domain_authentication: 'str', group: 'Any'):
        """
        Adds site user with credentials for LDAP authentication.
        This method creates a CustomizationUser object and adds it to the site and to the current project. The new user is only memory-resident until Customization.Commit is called.
        
        :param user_name: The name of the user to be added.
        :param full_name: The full name of the user to be added.
        :param email: The e-mail address of the user to be added.
        :param description: A description of the user to be added
        :param phone: The phone number of the user to be added.
        :param domain_authentication: The identifier of the user in an external User directory, for example the distinguished name in LDAP.
        :param group: The group to which the added user belongs. Pass either the group name or a CustomizationUsersGroup Object.
        """
        pass

    @abstractmethod
    def add_site_user(self, user_name: 'str', full_name: 'str', email: 'str', description: 'str', phone: 'str', group: 'Any'):
        """
        Creates a new site user and adds it to the current project.
        
        This method creates a CustomizationUser object and adds it to the site and to the current project. The new user is only memory-resident until Customization.Commit is called.
        
        :param user_name: The name of the user to be added.
        :param full_name: The full name of the user to be added.
        :param email: The e-mail address of the user to be added.
        :param description: A description of the user to be added.
        :param phone: The phone number of the user to be added.
        :param group: The group to which the added user belongs. Pass either the group name or a CustomizationUsersGroup Object.
        """
        pass

    @abstractmethod
    def add_user(self, name: 'str') -> 'T':
        """
        Adds an existing site user to the current project.
        
        :param name: The name of the user to be added.
        :return: A CustomizationUser object.
        """
        pass

    @abstractmethod
    def remove_user(self, name: 'str'):
        """
        Removes a user from the collection.
        The deletion takes effect when Customization.Commit is called.
        
        :param name: The name of the user to be removed.
        """
        pass

    