from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IExecEventActionParams(metaclass=ABCMeta):
    """
    Actions to be taken after the completion of a test set run.
    """
    
    @abstractmethod
    def get__on_exec_event_scheduler_action(self) -> 'int':
        """
        The action to be taken.
        """
        pass
    
    