from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IActionPermission(metaclass=ABCMeta):
    """
    Used to determine whether a specified action can be performed by users.
    """
    
    @abstractmethod
    def is__action_enabled(self, action_identity: 'Any', action_target: 'Any') -> 'bool':
        """
        Checks if the user has rights to perform the specified action.
        
        :param action_identity: 'Specifies the action. To get the full list of actions at your site, see the
        ACTIONS table using the Site Administrator UI.
        The most common options are:
        ac_add_bug, ac_add_common_settings, ac_add_cover, ac_add_cycle, ac_add_cyclefolder, ac_add_cycle_to_cyclefolder, ac_add_desstep, ac_add_folder, ac_add_private_settings, ac_add_r&d_comments, ac_add_req ac_add_reqfolder, ac_add_test, ac_add_test_to_testcycl, ac_change_password, ac_clear_history, ac_configure_mail, ac_copy_cycle, ac_copy_folder, ac_copy_cyclefolder, ac_create_rbr_files, ac_create_views, ac_create_wr_script, ac_customize_fields, ac_delete_bug, ac_delete_common_settings, ac_delete_cover, ac_delete_cycle, ac_delete_cycle_from_cyclefolder, ac_delete_cyclefolder, ac_delete_desstep, ac_delete_folder, ac_delete_private_settings, ac_delete_req, ac_delete_run, ac_delete_test, ac_delete_test_from_testcycl, ac_import_bugs_from_file, ac_import_bugs_from_mail, ac_import_bugs_settings, ac_import_wr_tests, ac_modify_bug, ac_modify_common_settings, ac_modify_cycle, ac_modify_desstep, ac_modify_folder, ac_modify_hosts, ac_modify_private_settings, ac_modify_req, ac_modify_reqfolder, ac_modify_run, ac_modify_test, ac_modify_test_in_testcycl, ac_move_folder, ac_move_cyclefolder, ac_remove_reqfolder, ac_reset_cycle, ac_run_auto_test, ac_run_manual_test, ac_send_all_qualified, ac_test_exec_params, ac_users, ac_wr_settings
         '
        :param action_target: 'The object on which the specified action is performed, or the user performing the action.
        To determine whether the action can be performed by the current user on a specific test or other object, the ActionTarget parameter value should be a reference to the object.
        To determine whether the specified action can be performed by a specific user, the ActionTarget parameter value should be the user name (string).'
        """
        pass
    
    