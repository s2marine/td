from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ISettings(metaclass=ABCMeta):
    """
    Represents users' settings in various user-defined categories.
    """
    
    @abstractmethod
    def get__enum_items(self) -> 'List[T]':
        """
        A list of the settings items’ names.
        """
        pass
    
    @abstractmethod
    def is__is_system(self, name: 'str') -> 'bool':
        """
        Checks if the specified setting is built-in and read-only.
        
        :param name: 'The name of the setting item.'
        """
        pass
    
    @abstractmethod
    def get__value(self, name: 'str') -> 'str':
        """
        The value of the specified item in the active category.
        
        :param name: 'The name of the item whose value to set or return.'
        """
        pass
    
    @abstractmethod
    def set__value(self, name: 'str', value: 'str'):
        """
        The value of the specified item in the active category.
        
        :param name: 'The name of the item whose value to set or return.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def close(self):
        """
        Closes and updates the category.
        """
        pass

    @abstractmethod
    def delete_category(self, category: 'str'):
        """
        Deletes the current settings folder.
        
        :param category: The name of the category to delete.
        """
        pass

    @abstractmethod
    def delete_value(self, name: 'str'):
        """
        Deletes the specified item.
        
        :param name: The name of the entry to delete.
        """
        pass

    @abstractmethod
    def open(self, category: 'str'):
        """
        Sets the category to be used in subsequent actions by the client.
        
        This method sets the context locally for subseqent use of the Settings object. The method call does not result in any communications with the server.
        
        :param category: The name of the category to open.  The category name can not contain any of the characters: /\*?=<>|:".
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts the category to the server.
        """
        pass

    @abstractmethod
    def refresh(self, category: 'str'):
        """
        Reads saved values, overwriting values in memory.
        
        :param category: The name of the category to refresh.
        """
        pass

    