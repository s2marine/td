from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition
    
    from s2marine.td.client.intf.i_customization_mail_condition import ICustomizationMailCondition


class ICustomizationMailConditions(metaclass=ABCMeta):
    """
    The collection of CustomizationMailCondition configurations for custom automatic mail notifications.
    """
    
    @abstractmethod
    def get__condition(self, name: 'str', condition_type: 'int') -> 'ICustomizationMailCondition':
        """
        Gets the CustomizationMailCondition specified by the name and type.
        
        :param name: 'The name of the condition.'
        :param condition_type: 'A value of the tagMAIL_CONDITIONS_TYPE Enumeration.'
        """
        pass
    
    @abstractmethod
    def is__condition_exist(self, name: 'str', condition_type: 'int') -> 'bool':
        """
        Checks if the specified mail customization condition exists.
        
        :param name: 'The name of the new condition.'
        :param condition_type: 'A value of the tagMAIL_CONDITIONS_TYPE Enumeration.'
        """
        pass
    
    @abstractmethod
    def get__conditions(self) -> 'List[ICustomizationMailCondition]':
        """
        Gets the list of CustomizationMailCondition objects.
        """
        pass
    
    @abstractmethod
    def add_condition(self, name: 'str', condition_type: 'int', condition_text: 'str') -> 'ICustomizationMailCondition':
        """
        Creates and returns a new CustomizationMailCondition.
        
        :param name: The name of the new condition.
        :param condition_type: A value of the tagMAIL_CONDITIONS_TYPE Enumeration.
        :param condition_text: The actual condition. A string valid for a TDFilter.Text.
        :return: A CustomizationMailCondition Object.
        """
        pass

    @abstractmethod
    def remove_condition(self, name: 'str', condition_type: 'int'):
        """
        Deletes the specified CustomizationMailCondition.
        
        :param name: The name of the new condition.
        :param condition_type: A value of the tagMAIL_CONDITIONS_TYPE Enumeration.
        """
        pass

    