from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    


class IAttachment(metaclass=ABCMeta):
    """
    Represents a single file or Internet address attached to a field object.
    """
    
    @abstractmethod
    def get__attachment_storage(self) -> 'IExtendedStorage':
        """
        The ExtendedStorage object of this attachment. Applies only to file attachments.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__data(self) -> 'Any':
        """
        If the attachment is a file, an array of bytes. If a reference to file, the path or URL.
        """
        pass
    
    @abstractmethod
    def get__description(self) -> 'str':
        """
        The attachment description.
        """
        pass
    
    @abstractmethod
    def set__description(self, value: 'str'):
        """
        The attachment description.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__direct_link(self) -> 'str':
        """
        The link or path to the attachment.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__file_name(self) -> 'str':
        """
        If a file, the file location. If a virtual file, the location for the attachment before it is uploaded to the server. If a URL, the link address.
        """
        pass
    
    @abstractmethod
    def set__file_name(self, value: 'str'):
        """
        If a file, the file location. If a virtual file, the location for the attachment before it is uploaded to the server. If a URL, the link address.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__file_size(self) -> 'int':
        """
        The attachment file size in bytes.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def get__last_modified(self) -> 'float':
        """
        The last modified time.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self, view_format: 'int') -> 'str':
        """
        The attachment name.
        
        :param view_format: 'If 1 is passed, the return value is the name of the file that was attached. For example, SampleAttachment.txt.
        If 0 is passed , the return value is a file name with a prefix indicating its item type and number as stored in the project database. For example, BUG_1_SampleAttachment.txt.'
        """
        pass
    
    @abstractmethod
    def get__server_file_name(self) -> 'str':
        """
        The attachment file name on the server machine.
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'int':
        """
        The attachment type. Either File or URL.
        """
        pass
    
    @abstractmethod
    def set__type(self, value: 'int'):
        """
        The attachment type. Either File or URL.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def load(self, synchronize: 'bool', root_path: 'str'):
        """
        Downloads an attachment file to a client machine.
        
        :param synchronize: If true, program run waits for download to complete. Otherwise, download is asynchronous.
        :param root_path: Output string variable. After the method returns, the value is the path to which the file was downloaded on the client machine.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def rename(self, new_name: 'str'):
        """
        Renames the attachment on the server. The attachment must be uploaded before calling this method.
        
        :param new_name: The new name for the attachment.
        """
        pass

    @abstractmethod
    def save(self, synchronize: 'bool'):
        """
        Uploads a file to the server.
        
        :param synchronize: If true, program run waits for upload to complete. Otherwise, upload is 
        asynchronous.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    