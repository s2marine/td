from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_exec_event_action_params import IExecEventActionParams
    


class IExecSettings(metaclass=ABCMeta):
    """
    The information on the execution of a test set.
    """
    
    @abstractmethod
    def get__on_exec_event_scheduler_action_params(self, event_type: 'int') -> 'IExecEventActionParams':
        """
        The restart action parameters.
        
        :param event_type: 'A member of the tagTDAPI_EXECUTIONEVENT Enumeration.'
        """
        pass
    
    @abstractmethod
    def get__on_exec_event_scheduler_action_type(self, event_type: 'int') -> 'int':
        """
        The action type of the execution event.
        
        :param event_type: 'A member of the tagTDAPI_EXECUTIONEVENT Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__on_exec_event_scheduler_action_type(self, event_type: 'int', value: 'int'):
        """
        The action type of the execution event.
        
        :param event_type: 'A member of the tagTDAPI_EXECUTIONEVENT Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__planned_execution_date(self) -> 'Any':
        """
        The planned test execution date.
        """
        pass
    
    @abstractmethod
    def set__planned_execution_date(self, value: 'Any'):
        """
        The planned test execution date.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__planned_execution_time(self) -> 'Any':
        """
        The planned test execution time.
        """
        pass
    
    @abstractmethod
    def set__planned_execution_time(self, value: 'Any'):
        """
        The planned test execution time.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__planned_run_duration(self) -> 'Any':
        """
        The planned test run duration.
        """
        pass
    
    @abstractmethod
    def set__planned_run_duration(self, value: 'Any'):
        """
        The planned test run duration.
        :param value: 
        """
        pass
    
    