from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ITextParser(metaclass=ABCMeta):
    """
    Utilities for handling parameters in a string.
    """
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        Gets the number of parameters.
        """
        pass
    
    @abstractmethod
    def is__param_exist(self, param_name: 'str') -> 'bool':
        """
        Checks if a parameter with the specified name exists.
        
        :param param_name: 'The name of the parameter.'
        """
        pass
    
    @abstractmethod
    def get__param_name(self, n_position: 'int') -> 'str':
        """
        Gets the parameter name.
        
        :param n_position: 'The  index (zero-based) of parameter in the list of all parameters found in the Text.'
        """
        pass
    
    @abstractmethod
    def get__param_type(self, v_param: 'Any') -> 'str':
        """
        Gets the user-defined Parameter Type.
        
        :param v_param: 'Either the index (zero-based) in the collection, or the name of the parameter.'
        """
        pass
    
    @abstractmethod
    def get__param_value(self, v_param: 'Any') -> 'str':
        """
        The parameter value.
        
        :param v_param: 'Either the index (zero-based) in the collection, or the name of the 
        parameter.'
        """
        pass
    
    @abstractmethod
    def set__param_value(self, v_param: 'Any', value: 'str'):
        """
        The parameter value.
        
        :param v_param: 'Either the index (zero-based) in the collection, or the name of the 
        parameter.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__text(self) -> 'str':
        """
        The text to be parsed.
        """
        pass
    
    @abstractmethod
    def set__text(self, value: 'str'):
        """
        The text to be parsed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__type(self, v_param: 'Any') -> 'int':
        """
        Gets the Parameter Type - predefined, null, or regular.
        
        :param v_param: 'Either the index (zero-based) in the collection, or the name of the 
        parameter.'
        """
        pass
    
    @abstractmethod
    def clear_param(self, v_param: 'Any'):
        """
        Clear the parameter - Set its value to null.
        
        :param v_param: Either the index (zero-based) in the collection, or the name of the 
        parameter.
        """
        pass

    @abstractmethod
    def evaluate_text(self):
        """
        Converts the parameters to their values.
        """
        pass

    @abstractmethod
    def initialize(self, start_close: 'str', end_close: 'str', type_close: 'str', max_len: 'int', default_type: 'str'):
        """
        Initialization is generally performed by the infrastructure. Use this method after confirming the need with Mercury Support.
        Several step parameters can be assigned to a step. The text parser is used to identify each parameter. By default, a parameter has format <?> .
        
        :param start_close: The token that marks the start of a parameter.
        :param end_close: The token that marks the end of a parameter.
        :param type_close: The token that marks a value to be replaced.
        :param max_len: Not in use. The default is always used. -1 means there is no maximum length.
        :param default_type: Not in use. Always "String"
        """
        pass

    