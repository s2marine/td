from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class ICustomizationListNode(metaclass=ABCMeta):
    """
    Represents a node in a list.
    """
    
    @abstractmethod
    def is__can_add_child(self) -> 'bool':
        """
        Checks if sub-nodes can be added to the current node.
        """
        pass
    
    @abstractmethod
    def get__child(self, node_name: 'str') -> 'ICustomizationListNode':
        """
        Gets the specified sub-node.
        
        :param node_name: 'The name of the child node.'
        """
        pass
    
    @abstractmethod
    def get__children(self) -> 'List[T]':
        """
        The list of CustomizationListNode objects that are sub-nodes of the current node.
        """
        pass
    
    @abstractmethod
    def get__children_count(self) -> 'int':
        """
        The number of sub-nodes under the node.
        """
        pass
    
    @abstractmethod
    def is__deleted(self) -> 'bool':
        """
        If true, the node is marked for deletion, but the deletion is not yet committed.
        """
        pass
    
    @abstractmethod
    def set__deleted(self, value: 'bool'):
        """
        If true, the node is marked for deletion, but the deletion is not yet committed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__father(self) -> 'ICustomizationListNode':
        """
        Gets the CustomizationListNode object representing the current node's parent node, or sets a new father, thereby moving the node.
        """
        pass
    
    @abstractmethod
    def set__father(self, value: 'ICustomizationListNode'):
        """
        Gets the CustomizationListNode object representing the current node's parent node, or sets a new father, thereby moving the node.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'int':
        """
        The current node ID.
        """
        pass
    
    @abstractmethod
    def set__id(self, value: 'int'):
        """
        The current node ID.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__list(self) -> 'ICustomizationList':
        """
        The CustomizationList object that contains the current node.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The current node name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The current node name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__order(self) -> 'int':
        """
        The order number of the node within its siblings.
        """
        pass
    
    @abstractmethod
    def set__order(self, value: 'int'):
        """
        The order number of the node within its siblings.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__read_only(self) -> 'bool':
        """
        Checks if the current node is read-only.
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the node was modified since it was loaded from the server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the node was modified since it was loaded from the server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_child(self, node: 'Any') -> 'T':
        """
        Adds a new sub-node to the current node.
        
        :param node: Either the node name of the child or a CustomListNode object.
        :return: The CustomListNode object representing the new child.
        """
        pass

    @abstractmethod
    def remove_child(self, node: 'Any'):
        """
        Removes the specified sub-node from the current node.
        
        :param node: Either the node name of the child or a CustomListNode object.
        """
        pass

    