from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IAlert(metaclass=ABCMeta):
    """
    Notifications sent to the user.
    """
    
    @abstractmethod
    def get__alert_date(self) -> 'float':
        """
        The date that the alert was generated, based on the database server clock.
        """
        pass
    
    @abstractmethod
    def get__alert_type(self) -> 'int':
        """
        The type of alert generated: Alert or Follow-up.
        """
        pass
    
    @abstractmethod
    def get__description(self) -> 'str':
        """
        The description of the alert.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'int':
        """
        The alert or follow-up ID.
        """
        pass
    
    @abstractmethod
    def get__subject(self) -> 'str':
        """
        A short summary appropriate to be used as subject line of the Alert email.
        """
        pass
    
    @abstractmethod
    def is__unread(self) -> 'bool':
        """
        If true, the alert or follow-up has not yet been read.
        """
        pass
    
    @abstractmethod
    def set__unread(self, value: 'bool'):
        """
        If true, the alert or follow-up has not yet been read.
        :param value: 
        """
        pass
    
    