from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_history_filter import IHistoryFilter
    
    from s2marine.td.client.intf.i_history_record import IHistoryRecord


class IHistory(metaclass=ABCMeta):
    """
    Enables retrieval of a list of history records.
    """
    
    @abstractmethod
    def get__filter(self) -> 'IHistoryFilter':
        """
        The filter object for the history item.
        """
        pass
    
    @abstractmethod
    def clear_history(self, filter: 'str'):
        """
        Clears the history records selected by the filter.
        
        :param filter: A TDFilter.Text that defines which history records will be deleted.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[IHistoryRecord]':
        """
        Gets filtered list of history data records.
        
        :param filter: A string containing one of the following:
        
        Any field name. Get all history records for the current object in which the specified field was changed. The format for the string is [FieldName]<name>, for example, [FieldName]BG_STATUS.
        An empty string. Get all history records for the current object.
        :return: A list of all HistoryRecord objects that match the search criteria. The list index is 1-based.
        """
        pass

    