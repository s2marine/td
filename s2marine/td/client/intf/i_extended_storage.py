from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_file_data import IFileData
    


class IExtendedStorage(metaclass=ABCMeta):
    """
    Represents a storage structure used to transfer files between the server and client file system and delete files.
    """
    
    @abstractmethod
    def get__action_finished(self) -> 'int':
        """
        Checkes if the load or save action is finished.
        """
        pass
    
    @abstractmethod
    def get__client_path(self) -> 'str':
        """
        The location on the client to which to download or from which to upload.
        """
        pass
    
    @abstractmethod
    def set__client_path(self, value: 'str'):
        """
        The location on the client to which to download or from which to upload.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__root(self) -> 'IFileData':
        """
        The storage root.
        """
        pass
    
    @abstractmethod
    def get__server_path(self) -> 'str':
        """
        The location on the server from which to download or to which to upload.
        """
        pass
    
    @abstractmethod
    def set__server_path(self, value: 'str'):
        """
        The location on the server from which to download or to which to upload.
        :param value: 
        """
        pass
    
    @abstractmethod
    def cancel(self):
        """
        Cancels the load or save action.
        """
        pass

    @abstractmethod
    def delete(self, f_sys_filter: 'str', n_delete_type: 'int'):
        """
        Deletes files locally or from the server.
        
        :param f_sys_filter: A file system filter. If the filter begins with "-r", it is applied recursively to all sub-directories. If the filter is an empty string, all files with all subdirectories are downloaded to the client.
        :param n_delete_type: The delete type, Local or remote. A member of the tagTDAPI_EXSTOR_DELMODE Enumeration .
        """
        pass

    @abstractmethod
    def get_last_error(self):
        """
        Gets the last error that occurred during asynchronous load and save operations.
        
        Used for asynchronous operations where error handled can not be implemented.
        """
        pass

    @abstractmethod
    def load(self, f_sys_filter: 'str', synchronize: 'bool') -> 'str':
        """
        Downloads the storage structure to the client file system.
        
        If the filter is an empty string, all files with all subdirectories are uploaded to the server.
        
        :param f_sys_filter: A file system filter.
        :param synchronize: If True, the download operation is synchronous.
        :return: The path on the client machine to which the files were downloaded.
        """
        pass

    @abstractmethod
    def load_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]', p_non_fatal_error_occured: 'bool') -> 'str':
        """
        Downloads the storage structure to the client file system.
        
        If the filter is an empty string, all files with all subdirectories are uploaded to the server.
        
        :param f_sys_filter: A file system filter.
        :param synchronize: If True, the download operation is synchronous.
        :param p_val: NULL
        :param p_non_fatal_error_occured: The error that occurred was non-fatal. This value is meaningful only for synchronous transfers.
        :return: The path on the client machine to which the files were downloaded.
        """
        pass

    @abstractmethod
    def progress(self, total: 'int', current: 'int') -> 'str':
        """
        Polls the progress of the last action (Load or Save).
        
        :param total: The total number of bytes to be transferred.
        :param current: The number of bytes transferred so far.
        :return: Null
        """
        pass

    @abstractmethod
    def save(self, f_sys_filter: 'str', synchronize: 'bool'):
        """
        Uploads the storage structure to the server.
        
        If the filter is an empty string, all files with all subdirectories are uploaded to the server.
        
        :param f_sys_filter: A file system filter.
        :param synchronize: If True, the upload operation is synchronous.
        """
        pass

    @abstractmethod
    def save_ex(self, f_sys_filter: 'str', synchronize: 'bool', p_val: 'List[T]') -> 'bool':
        """
        Uploads the storage structure to the server.
        
        If the filter is an empty string, all files with all subdirectories are uploaded to the server.
        
        :param f_sys_filter: A file system filter.
        :param synchronize: If True, the upload operation is synchronous.
        :param p_val: NULL
        :return: The error that occurred was non-fatal (True or False). This value is meaningful only for synchronous transfers.
        """
        pass

    