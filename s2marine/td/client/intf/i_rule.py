from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IRule(metaclass=ABCMeta):
    """
    Represents a rule for generating an alert.
    """
    
    @abstractmethod
    def get__description(self) -> 'str':
        """
        The rule description.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'int':
        """
        The rule ID.
        """
        pass
    
    @abstractmethod
    def is__is_active(self) -> 'bool':
        """
        Indicates if the rule is active.
        """
        pass
    
    @abstractmethod
    def set__is_active(self, value: 'bool'):
        """
        Indicates if the rule is active.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__to_mail(self) -> 'bool':
        """
        Indicates if the alert is to be sent by e-mail.
        """
        pass
    
    @abstractmethod
    def set__to_mail(self, value: 'bool'):
        """
        Indicates if the alert is to be sent by e-mail.
        :param value: 
        """
        pass
    
    @abstractmethod
    def post(self):
        """
        Saves the changes to the Rules object to the server.
        """
        pass

    