from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_design_step_factory import IDesignStepFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_vcs import IVCS
    


class ITest(metaclass=ABCMeta):
    """
    Represents a planning test.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__checkout_path_if_exist(self) -> 'str':
        """
        The checkout path for the test.
        """
        pass
    
    @abstractmethod
    def get__design_step_factory(self) -> 'IDesignStepFactory':
        """
        The design steps factory for the current test.
        """
        pass
    
    @abstractmethod
    def get__des_steps_num(self) -> 'int':
        """
        The number of design steps in the test.
        """
        pass
    
    @abstractmethod
    def get__exec_date(self) -> 'float':
        """
        The test execution date and time.
        """
        pass
    
    @abstractmethod
    def get__exec_status(self) -> 'str':
        """
        The test execution status.
        """
        pass
    
    @abstractmethod
    def get__extended_storage(self) -> 'IExtendedStorage':
        """
        The Extended Storage object for the current test.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__full_path(self) -> 'str':
        """
        The full file system path for the test.
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def is__has_coverage(self) -> 'bool':
        """
        Checks if the test covers at least one requirement.
        """
        pass
    
    @abstractmethod
    def is__has_param(self) -> 'bool':
        """
        Checks if the test has at least one parameter.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def get__last_run(self) -> 'IRun':
        """
        The last Run object for this test.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The test name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The test name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__params(self) -> 'IStepParams':
        """
        The current test parameters.
        """
        pass
    
    @abstractmethod
    def is__template_test(self) -> 'bool':
        """
        Indicates if this test is a template test.
        """
        pass
    
    @abstractmethod
    def set__template_test(self, value: 'bool'):
        """
        Indicates if this test is a template test.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'str':
        """
        The test type.
        """
        pass
    
    @abstractmethod
    def set__type(self, value: 'str'):
        """
        The test type.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__vcs(self) -> 'IVCS':
        """
        The VCS version control object for this test.
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def cover_requirement(self, req: 'Any', order: 'int', recursive: 'bool') -> 'int':
        """
        Checks if this test covers the specified requirement.
        
        :param req: A requirement ID (long) or a Req Object.
        :param order: The ordinal position of the current test in the existing coverage list. Possible values are: 
        
        A number representing the requirement position. 
        TDPOSITION_LAST [-4] - Indicates that the requirement is inserted last.
        :param recursive: If true, all child requirements will be covered by this test.
        :return: The position of this test in the coverage list.
        """
        pass

    @abstractmethod
    def get_cover_list(self) -> 'List[T]':
        """
        Gets the list of all requirements covered by this test.
        :return: A list of Req Objects.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        """
        Mails the IBaseFieldExMail field item.
        
        :param send_to: A Quality Center user name, an e-mail address (not necessarily a Quality Center user), or a list of e-mail addresses. Addresses can be separated by a comma or a semi-colon.
        :param send_cc: The recipients of a carbon copy of the mail. Addresses can be separated by a comma or a semicolon.
        :param option: The mailing options binary mask.Create the mask using values of TDMAIL_FLAGS.
        :param subject: The mail subject line.
        :param comment: A comment on the sent e-mail. If the TDMAIL_COMMENT_AS_BODY option is set, this will be sent as the message body.
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def remove_coverage(self, req: 'Any'):
        """
        Removes a requirement from the list of requirements this test covers.
        
        :param req: The Req Object representing the requirement that this test will no longer cover.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    