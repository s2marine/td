from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_ts_test import ITSTest
    
    from s2marine.td.client.intf.i_ts_test import ITSTest


class ITSTestFactory(metaclass=ABCMeta):
    """
    Manages test instances (TSTest objects) in a test set.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'ITSTest':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'A TSTest.ID. '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'ITSTest':
        """
        Adds one or more tests to the test set.
        
        :param item_data: There are three options for ItemData: 
        
        Null. Initially creating a virtual object with Null ensures that you cannot then Post until all required fields are initialized. 
        A test ID (long or string) 
        An array of variants containing test IDs either as long values or as strings.
        :return: If either Null or a single test ID is passes, AddItem returns a TSTest Object. If an array of test IDs is passed, a List of TSTests is returned.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: The TSTest.ID (long), a reference to the TSTest Object, or a Variant array of TSTest.IDs.
        """
        pass

    