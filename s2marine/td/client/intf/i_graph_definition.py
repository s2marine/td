from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IGraphDefinition(metaclass=ABCMeta):
    """
    Configuration information used to build a graph.
    """
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The filter sets the criteria for which data are included in the graph.
        """
        pass
    
    @abstractmethod
    def set__filter(self, value: 'TDFilter'):
        """
        The filter sets the criteria for which data are included in the graph.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__module(self) -> 'int':
        """
        The module specified when the graph definition was created.
        """
        pass
    
    @abstractmethod
    def get__property(self, prop: 'int') -> 'Any':
        """
        The value of the specified property.
        
        :param prop: 'A value of the tagGRAPH_PROPERTIES Enumeration.'
        """
        pass
    
    @abstractmethod
    def set__property(self, prop: 'int', value: 'Any'):
        """
        The value of the specified property.
        
        :param prop: 'A value of the tagGRAPH_PROPERTIES Enumeration.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'int':
        """
        The graph type
        """
        pass
    
    