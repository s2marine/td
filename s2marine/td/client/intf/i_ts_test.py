from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_exec_settings import IExecSettings
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test import ITest
    from s2marine.td.client.intf.i_test_set import ITestSet
    


class ITSTest(metaclass=ABCMeta):
    """
    Represents a test instance, or execution test, in a test set.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__execution_params(self) -> 'str':
        """
        The execution parameters for this test instance.
        """
        pass
    
    @abstractmethod
    def set__execution_params(self, value: 'str'):
        """
        The execution parameters for this test instance.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__execution_settings(self) -> 'IExecSettings':
        """
        The execution settings for this test instance.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def is__has_coverage(self) -> 'bool':
        """
        Checks if the test instance covers at least one requirement.
        """
        pass
    
    @abstractmethod
    def is__has_steps(self) -> 'bool':
        """
        Checks if the planning test specified by the Test property has design steps.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__host_name(self) -> 'str':
        """
        The name of the host planned to run this test instance.
        """
        pass
    
    @abstractmethod
    def set__host_name(self, value: 'str'):
        """
        The name of the host planned to run this test instance.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def get__instance(self) -> 'int':
        """
        The instance number of this execution test, within the other instances of the same test.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def get__last_run(self) -> 'IRun':
        """
        The Run object of the last run.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The name of this execution test. By convention, this is the test instance number and the test name.
        """
        pass
    
    @abstractmethod
    def get__params(self) -> 'IStepParams':
        """
        The step parameters for this execution test.
        """
        pass
    
    @abstractmethod
    def get__run_factory(self) -> 'IRunFactory':
        """
        The Run Factory for this execution test.
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The execution test status.
        """
        pass
    
    @abstractmethod
    def set__status(self, value: 'str'):
        """
        The execution test status.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__test(self) -> 'ITest':
        """
        The planning test of which this execution test in an instance.
        """
        pass
    
    @abstractmethod
    def get__test_id(self) -> 'Any':
        """
        The ID of the design test of which this is an instance.
        """
        pass
    
    @abstractmethod
    def get__test_name(self) -> 'str':
        """
        The name of the design test of which this is an instance.
        """
        pass
    
    @abstractmethod
    def get__test_set(self) -> 'ITestSet':
        """
        The test set to which the execution test belongs.
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'str':
        """
        The execution test type.
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    