from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    


class ICustomizationAction(metaclass=ABCMeta):
    """
    Represents a type of user action such as adding or deleting a bug. Actions are listed in the AC_ACTION_NAME field of the Actions table.
    """
    
    @abstractmethod
    def get__groups(self) -> 'List[ICustomizationUsersGroup]':
        """
        A list of the user groups for which the current action is allowed.
        """
        pass
    
    @abstractmethod
    def is__is_group_permited(self, group: 'Any') -> 'bool':
        """
        Checks if the group specified by the Group parameter has permission to perform the current action.
        
        :param group: 'Specify the group with either the name of the group or a CustomizationUsersGroup Object. '
        """
        pass
    
    @abstractmethod
    def is__is_owner_group(self, group: 'Any') -> 'bool':
        """
        Checks if the user group specified by the Group parameter is the owner of the current action.
        
        :param group: 'Specify the group with either the name of the group or a CustomizationUsersGroup Object. '
        """
        pass
    
    @abstractmethod
    def is__is_user_permited(self, user: 'Any') -> 'bool':
        """
        Checks if the user specified by the User parameter is permitted to perform the current action.
        
        :param user: 'The user's name or a CustomizationUser Object.'
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The name of the action.
        """
        pass
    
    @abstractmethod
    def get__owner_group(self) -> 'ICustomizationUsersGroup':
        """
        The user group that owns the action.
        """
        pass
    
    @abstractmethod
    def set__owner_group(self, value: 'ICustomizationUsersGroup'):
        """
        The user group that owns the action.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the object has been modified since last synchronized with server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the object has been modified since last synchronized with server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_group(self, group: 'Any'):
        """
        Adds a user group to the list of user groups for which the current action is allowed.
        
        :param group: The user group to be added. Pass either the name of the group or a CustomizationUsersGroup Object.
        """
        pass

    @abstractmethod
    def remove_group(self, group: 'Any'):
        """
        Removes the specified user group from the list of groups for which the current action is allowed.
        
        :param group: The user group to be removed. Pass either the name of the group or 
        a CustomizationUsersGroup Object.
        """
        pass

    