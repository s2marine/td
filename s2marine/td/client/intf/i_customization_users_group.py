from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_customization_user import ICustomizationUser


class ICustomizationUsersGroup(metaclass=ABCMeta):
    """
    Represents a user group for purposes of adding and removing users.
    """
    
    @abstractmethod
    def is__deleted(self) -> 'bool':
        """
        If true, the object is marked for deletion, but the deletion has not been committed.
        """
        pass
    
    @abstractmethod
    def set__deleted(self, value: 'bool'):
        """
        If true, the object is marked for deletion, but the deletion has not been committed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__hide_filter(self, filter_type: 'str') -> 'str':
        """
        The data hiding filter for the entity specified by the FilterType parameter.
        
        :param filter_type: 'BUG, REQ, TEST, or TESTCYCL'
        """
        pass
    
    @abstractmethod
    def set__hide_filter(self, filter_type: 'str', value: 'str'):
        """
        The data hiding filter for the entity specified by the FilterType parameter.
        
        :param filter_type: 'BUG, REQ, TEST, or TESTCYCL'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'int':
        """
        The user group Id.
        """
        pass
    
    @abstractmethod
    def is__is_system(self) -> 'bool':
        """
        Checks if the user group is a built-in system user group.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The user group name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The user group name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the object has been modified since being downloaded from database.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the object has been modified since being downloaded from database.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_user(self, user: 'Any'):
        """
        Adds a new user to the user group.
        
        :param user: The name of the user to be added or a CustomizationUser 
        Object.
        """
        pass

    @abstractmethod
    def remove_user(self, user: 'Any'):
        """
        Removes a user from the user group.
        
        :param user: The name of the user to be removed or a CustomizationUser 
        Object.
        """
        pass

    @abstractmethod
    def users_list(self) -> 'List[ICustomizationUser]':
        """
        A list of all members of the user group.
        :return: A list of CustomizationUser Objects.
        """
        pass

    