from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_run import IRun
    


class IRunFactory(metaclass=ABCMeta):
    """
    Services for managing test runs.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'IRun':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'The run ID (long). '
        """
        pass
    
    @abstractmethod
    def get__unique_run_name(self) -> 'str':
        """
        Gets the run name. The name is unique in the current test and test set.
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'T':
        """
        Creates a new item object.
        
        Following a call to AddItem with an array of parameters, the Run table field RN_PATH contains the value from in the array location (2), Location, if it was passed. If array location (2) was an empty string (""), then field RN_PATH contains "a_b" where "a" is the RN_CYCLE_ID and "b" is the RN_RUN_ID. Field RN_HOST is always empty following a call AddItem.
        
        :param item_data: There are three options for ItemData: 
        
        Null. Creating a virtual Run object with Null ensures that you cannot then Post until the run name and all other required fields are initialized. 
        The name of the run (string). 
        An array consisting of the following elements: 
        (0) Name - The name of the run (string. required).
        (1) Tester - The name of the user responsible (string. optional). 
        (2) Location - The host name (string. optional). The default is the host name of the current machine.
        :return: A Run Object.
        """
        pass

    @abstractmethod
    def delete_duplicate_runs(self, run_name: 'str'):
        """
        Removes duplicate runs for the current test set. Removal is immediate without Post.
        
        :param run_name: The name of the run.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: The Run.ID (long), a reference to the Run Object, or a Variant array of Run.IDs.
        """
        pass

    