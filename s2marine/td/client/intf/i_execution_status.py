from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_exec_status import ITestExecStatus
    
    from s2marine.td.client.intf.i_exec_event_info import IExecEventInfo


class IExecutionStatus(metaclass=ABCMeta):
    """
    Represents the execution status of the scheduler.
    """
    
    @abstractmethod
    def get___new_enum(self) -> 'Unknown':
        """
        Gets an ATL.IEnumOnSTLImpl enumerator for the TestExecStatus objects. For information on the ATL Class and IEnumOnSTLImpl, see Microsoft documentation.
        """
        pass
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of status elements in the object.
        """
        pass
    
    @abstractmethod
    def is__finished(self) -> 'bool':
        """
        Checks if execution is finished or still in progress.
        """
        pass
    
    @abstractmethod
    def get__item(self, index: 'int') -> 'ITestExecStatus':
        """
        Gets a TestExecStatus object by index (1-based).
        
        :param index: 'Specifies the TestExecStatus object in the collection. Index is one-based.'
        """
        pass
    
    @abstractmethod
    def events_list(self) -> 'List[IExecEventInfo]':
        """
        Gets the list of the execution events.
        :return: A list of the ExecEventInfo Objects of the events.
        """
        pass

    @abstractmethod
    def refresh_exec_status_info(self, test_data: 'Any', force: 'bool'):
        """
        Refreshes the execution status from the execution controller.
        
        :param test_data: Either "all" or a comma-separated list of test IDs
        :param force: If True, refresh all requested tests. If False, refresh only those tests whose status has changed since the last call.
        """
        pass

    