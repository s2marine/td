from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    


class ITreeManager(metaclass=ABCMeta):
    """
    Represents the system tree, containing a subject tree and all hierarchical field trees.
    """
    
    @abstractmethod
    def get__node_by_id(self, node_id: 'int') -> 'ISysTreeNode':
        """
        Gets the object having the specified node Id.
        
        :param node_id: 'The Node ID.'
        """
        pass
    
    @abstractmethod
    def get__node_by_path(self, path: 'str') -> 'ISysTreeNode':
        """
        Gets the node at the specified tree path.
        
        :param path: 'The tree path of the node (a string separated by slashes).'
        """
        pass
    
    @abstractmethod
    def get__node_path(self, node_id: 'int') -> 'str':
        """
        Gets the node path, slash separated.
        
        :param node_id: 'The Node ID.'
        """
        pass
    
    @abstractmethod
    def get__root_list(self, val: 'int') -> 'List[T]':
        """
        Gets list of available trees in the specified range.
        
        :param val: 'One of the values TDOLE_ALL, TDOLE_SUBJECT, or TDOLE_NOT_SUBJECT, from tagTDAPI_ROOTLIST Enumeration.'
        """
        pass
    
    @abstractmethod
    def get__tree_root(self, root_name: 'str') -> 'ISysTreeNode':
        """
        The root node specified by the node name.
        
        :param root_name: 'One of the values returned by RootList.'
        """
        pass
    
    