from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_action_permission import IActionPermission
    from s2marine.td.client.intf.i_alert_manager import IAlertManager
    from s2marine.td.client.intf.i_audit_property_factory import IAuditPropertyFactory
    from s2marine.td.client.intf.i_audit_record_factory import IAuditRecordFactory
    from s2marine.td.client.intf.i_bug_factory import IBugFactory
    from s2marine.td.client.intf.i_command import ICommand
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_customization import ICustomization
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_graph_builder import IGraphBuilder
    from s2marine.td.client.intf.i_host_factory import IHostFactory
    from s2marine.td.client.intf.i_host_group_factory import IHostGroupFactory
    from s2marine.td.client.intf.i_td_mail_conditions import ITDMailConditions
    from s2marine.td.client.intf.i_product_info import IProductInfo
    from s2marine.td.client.intf.i_project_properties import IProjectProperties
    from s2marine.td.client.intf.i_req_factory import IReqFactory
    from s2marine.td.client.intf.i_rule_manager import IRuleManager
    from s2marine.td.client.intf.i_run_factory import IRunFactory
    from s2marine.td.client.intf.i_test_factory import ITestFactory
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    from s2marine.td.client.intf.i_test_set_tree_manager import ITestSetTreeManager
    from s2marine.td.client.intf.i_text_parser import ITextParser
    from s2marine.td.client.intf.i_tree_manager import ITreeManager
    from s2marine.td.client.intf.i_ts_test_factory import ITSTestFactory
    from s2marine.td.client.intf.i_settings import ISettings
    from s2marine.td.client.intf.i_vcs import IVCS
    


class ITDConnection(metaclass=ABCMeta):
    """
    Represents a single server connection.
    """
    
    @abstractmethod
    def get__action_permission(self) -> 'IActionPermission':
        """
        The ActionPermission object for this connection.
        """
        pass
    
    @abstractmethod
    def get__alert_manager(self) -> 'IAlertManager':
        """
        The AlertManager object for this connection.
        """
        pass
    
    @abstractmethod
    def get__audit_property_factory(self) -> 'IAuditPropertyFactory':
        """
        The global AuditProperty factory, not associated with a specific entity.
        """
        pass
    
    @abstractmethod
    def get__audit_record_factory(self) -> 'IAuditRecordFactory':
        """
        The global AuditRecord factory, not associated with a specific entity.
        """
        pass
    
    @abstractmethod
    def get__bug_factory(self) -> 'IBugFactory':
        """
        The Bug Factory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__checkout_repository(self) -> 'str':
        """
        The path of the checkout repository.
        """
        pass
    
    @abstractmethod
    def get__command(self) -> 'ICommand':
        """
        The Command object for this connection.
        """
        pass
    
    @abstractmethod
    def get__common_settings(self) -> 'ISettings':
        """
        The Settings object for all users in this connection.
        """
        pass
    
    @abstractmethod
    def is__connected(self) -> 'bool':
        """
        Checks if the Open Test Architecture API server connection is initialized.
        """
        pass
    
    @abstractmethod
    def get__customization(self) -> 'ICustomization':
        """
        The Customization object for this connection.
        """
        pass
    
    @abstractmethod
    def get__db_name(self) -> 'str':
        """
        The name of the database.
        """
        pass
    
    @abstractmethod
    def get__db_type(self) -> 'str':
        """
        The database type.
        """
        pass
    
    @abstractmethod
    def get__directory_path(self, n_type: 'int') -> 'str':
        """
        The path of the server side repository directory for the database repository type.
        
        :param n_type: 'The database repository type, a value of the tagTDAPI_DIRECTORY Enumeration.'
        """
        pass
    
    @abstractmethod
    def get__domain_name(self) -> 'str':
        """
        The current domain.
        """
        pass
    
    @abstractmethod
    def get__domains_list(self) -> 'List[T]':
        """
        The domain names for this site.
        """
        pass
    
    @abstractmethod
    def get__extended_storage(self) -> 'IExtendedStorage':
        """
        The ExtendedStorage object for this connection.
        """
        pass
    
    @abstractmethod
    def get__fields(self, data_type: 'str') -> 'List[ITDField]':
        """
        A list of fields for the table specified in the DataType argument.
        
        :param data_type: 'The name of a system table.'
        """
        pass
    
    @abstractmethod
    def get__graph_builder(self) -> 'IGraphBuilder':
        """
        The GraphBuilder object for the connection.
        """
        pass
    
    @abstractmethod
    def get__host_factory(self) -> 'IHostFactory':
        """
        The Hosts Factory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__host_group_factory(self) -> 'IHostGroupFactory':
        """
        The HostGroupFactory object for this connection.
        """
        pass
    
    @abstractmethod
    def is__ignore_html_format(self) -> 'bool':
        """
        Indicates if formatted memo and description fields are returned as plain text without HTML tags.
        """
        pass
    
    @abstractmethod
    def set__ignore_html_format(self, value: 'bool'):
        """
        Indicates if formatted memo and description fields are returned as plain text without HTML tags.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_search_supported(self) -> 'bool':
        """
        Checks if the search function is enabled for the project.
        """
        pass
    
    @abstractmethod
    def is__logged_in(self) -> 'bool':
        """
        Checks if a user is currently logged in on this TDConnection.
        """
        pass
    
    @abstractmethod
    def get__mail_conditions(self) -> 'ITDMailConditions':
        """
        The TDMailConditions object for this connection.
        """
        pass
    
    @abstractmethod
    def get__product_info(self) -> 'IProductInfo':
        """
        An IProductInfo reference pointing to the current build information.
        """
        pass
    
    @abstractmethod
    def is__project_connected(self) -> 'bool':
        """
        Checks if the TDConnection is connected to a project.
        """
        pass
    
    @abstractmethod
    def get__project_name(self) -> 'str':
        """
        The name of the connected project.
        """
        pass
    
    @abstractmethod
    def get__project_properties(self) -> 'IProjectProperties':
        """
        The ProjectProperties object for this connection.
        """
        pass
    
    @abstractmethod
    def get__projects_list(self) -> 'List[T]':
        """
        The projects that are available in the domain to which Quality Center is connected.
        """
        pass
    
    @abstractmethod
    def get__projects_list_ex(self, domain_name: 'str') -> 'List[T]':
        """
        The projects available within the specified domain.
        
        :param domain_name: 'The name of a domain of projects defined in the Quality Center site.'
        """
        pass
    
    @abstractmethod
    def get__report_role(self) -> 'str':
        """
        The report role of the connected user.
        """
        pass
    
    @abstractmethod
    def get__req_factory(self) -> 'IReqFactory':
        """
        The ReqFactory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__rules(self) -> 'IRuleManager':
        """
        The RuleManager object for this connection.
        """
        pass
    
    @abstractmethod
    def get__run_factory(self) -> 'IRunFactory':
        """
        The RunFactory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__server_name(self) -> 'str':
        """
        The name of the connected Open Test Architecture API server.
        """
        pass
    
    @abstractmethod
    def get__server_time(self) -> 'float':
        """
        The time and date of the currently connected database server.
        """
        pass
    
    @abstractmethod
    def get__server_url(self) -> 'str':
        """
        The URL of the connected server.
        """
        pass
    
    @abstractmethod
    def get__td_params(self, request: 'str') -> 'str':
        """
        Returns the value of the parameter whose name is specified by Request.
        
        :param request: 'The name of a parameter from the Site Configuration tab in the Site Administrator UI.'
        """
        pass
    
    @abstractmethod
    def get__test_factory(self) -> 'ITestFactory':
        """
        The Test Factory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__test_repository(self) -> 'str':
        """
        The path of the tests directory of the connected project.
        """
        pass
    
    @abstractmethod
    def get__test_set_factory(self) -> 'ITestSetFactory':
        """
        The Test Set Factory object for this connection.
        """
        pass
    
    @abstractmethod
    def get__test_set_tree_manager(self) -> 'ITestSetTreeManager':
        """
        The TestSetTreeManager object for this connection.
        """
        pass
    
    @abstractmethod
    def get__text_param(self) -> 'ITextParser':
        """
        The TextParser object.
        """
        pass
    
    @abstractmethod
    def get__tree_manager(self) -> 'ITreeManager':
        """
        The TreeManager object for the system tree.
        """
        pass
    
    @abstractmethod
    def get__ts_test_factory(self) -> 'ITSTestFactory':
        """
        The factory that manages test instances in test sets.
        """
        pass
    
    @abstractmethod
    def get__user_groups_list(self) -> 'List[T]':
        """
        The names of the user groups of the currently connected project.
        """
        pass
    
    @abstractmethod
    def get__user_name(self) -> 'str':
        """
        The user connected to the project.
        """
        pass
    
    @abstractmethod
    def get__user_settings(self) -> 'ISettings':
        """
        The Settings object for the logged-on user.
        """
        pass
    
    @abstractmethod
    def get__users_list(self) -> 'List[T]':
        """
        The names of the users of the currently connected project.
        """
        pass
    
    @abstractmethod
    def is__using_progress(self) -> 'bool':
        """
        Indicates if a progress bar is displayed.
        """
        pass
    
    @abstractmethod
    def set__using_progress(self, value: 'bool'):
        """
        Indicates if a progress bar is displayed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__vcs(self) -> 'IVCS':
        """
        The Version Control System (VCS) object for this connection.
        """
        pass
    
    @abstractmethod
    def get__vcs_db_repository(self) -> 'str':
        """
        The path of the VCS database of the connected project.
        """
        pass
    
    @abstractmethod
    def get__visible_domains(self) -> 'List[T]':
        """
        The list of domain names that the current logged in user is permitted to view.
        """
        pass
    
    @abstractmethod
    def get__visible_projects(self, domain_name: 'str') -> 'List[T]':
        """
        The list of projects in the specified domain that the current logged in user is allowed to connect to.
        
        :param domain_name: 'The logical group of projects in the Quality Center data base.'
        """
        pass
    
    @abstractmethod
    def change_password(self, old_password: 'str', new_password: 'str'):
        """
        Changes the password for the currently logged in user.
        
        :param old_password: The current password before the change.
        :param new_password: The new password.
        """
        pass

    @abstractmethod
    def connect(self, domain_name: 'str', project_name: 'str'):
        """
        Connects the logged-in user to the specified project in the  domain.
        Call Connect only if there is a logged-in user (LoggedIn is True). After a successful Connect, both the LoggedIn property and the Connected property are True.
        
        :param domain_name: The logical group of projects in the Quality Center data base.
        :param project_name: The name of the project in the Quality Center data base.
        """
        pass

    @abstractmethod
    def connect_project(self, project_name: 'str', user_name: 'str', password: 'str'):
        """
        Connects to the specified project.
        
        This method is deprecated from version 9.0.
        
        :param project_name: The name of the project in the Quality Center data base.
        :param user_name: The user name defined in the Quality Center data base.
        :param password: The user's password.
        """
        pass

    @abstractmethod
    def connect_project_ex(self, domain_name: 'str', project_name: 'str', user_name: 'str', password: 'str'):
        """
        Connects to the specified project in the specified domain.
        
        This method is deprecated from version 9.0.
        
        :param domain_name: The logical group of projects in the Quality Center data base.
        :param project_name: The name of the project in the Quality Center data base.
        :param user_name: The name of a user defined in the specified project.
        :param password: The user password.
        """
        pass

    @abstractmethod
    def disconnect(self):
        """
        Disconnects the user from the currently connected project.
        
        Disconnect is called when the logged-in user is connected to a project. After Disconnect, the user is still authenticated on the server and can Connect to a different project without logging in again.
        """
        pass

    @abstractmethod
    def disconnect_project(self):
        """
        Disconnects from the project.
        
        This method is deprecated from version 9.0.
        """
        pass

    @abstractmethod
    def get_licenses(self, licenses_type: 'int', p_val: 'str'):
        """
        Allocates multiple licenses types according to the LicensesType parameter.
        For details of the return string, see the example. Error codes in the return string are from  the tagTDAPI_ERRORCODES Enumeration.
        
        :param licenses_type: A bit mask composed of members of the tagTDLICENSE_CLIENT Enumeration combined with a bit-wise OR. Each bit set in the mask indicates that the description of the license of that type is to be returned in the output argument.
        :param p_val: The output description of the licenses: Type code, approval status, and UID.
        """
        pass

    @abstractmethod
    def get_license_status(self, client_type: 'int', in_use: 'int', max: 'int'):
        """
        The license status for the specified client type.
        
        :param client_type: A value of the tagTDAPI_LICENSE Enumeration.
        :param in_use: The number of modules of the specified type currently in use.
        :param max: The maximum number of modules of the specified type that can be used simultaneously.
        """
        pass

    @abstractmethod
    def get_td_version(self, pbs_major_version: 'str', pbs_build_num: 'str'):
        """
        The major and minor versions of the OTA API.
        
        :param pbs_major_version: The release number.
        :param pbs_build_num: The intermediate release number.
        """
        pass

    @abstractmethod
    def init_connection_ex(self, server_name: 'str'):
        """
        Initializes the  connection.
        InitConnectionEx must be called before a user can be authenticated with Login.
        
        :param server_name: The Quality Center URL: http://<server>[:port]/qcbin
        """
        pass

    @abstractmethod
    def login(self, user_name: 'str', password: 'str'):
        """
        Confirms that the user is authorized to use Quality Center. If authorized, the user is logged in and can connect to projects.
        
        Before authenticating a user with Login, connect to the server with InitConnectionEx.
        
        :param user_name: The Quality Center use name.
        :param password: The user's password.
        """
        pass

    @abstractmethod
    def logout(self):
        """
        Terminates the user's session on this TDConnection.
        """
        pass

    @abstractmethod
    def purge_runs(self, test_set_filter: 'str', keep_last: 'int', date_unit: 'Any', unit_count: 'int', steps_only: 'bool'):
        """
        Deletes the runs in this connection that match the input arguments.
        KeepLast overrides OlderThan. For example, if KeepLast is 10, OlderThan is 2 and UnitCount is 5, the meaning is "Purge all runs older than 5 weeks, but keep the last 10 runs for each test instance regardless of age".
        
        :param test_set_filter: The filter specifying the test sets from which the runs should be deleted. The filter can be a list of cycle IDs like "1,5,78,14", a status like  "CY_STATUS='Open'", or a TDFilter.Text.
        :param keep_last: The number of most recent test instance runs not to purge.
        :param date_unit: Either a date string, in which case runs before this date are purged, or a unit code used to interpret the unit count. A unit code is one of:
        1 for Days2 for Weeks3 for Months 4 for Years
        :param unit_count: If DateUnit is not a date string, this is the number of units. For example, if DateUnit is 1 and UnitCount is 5, purge all runs older than 5 days.
        :param steps_only: If True, purge only the steps, but not the runs. If False, purge both the runs and steps.
        """
        pass

    @abstractmethod
    def release_connection(self):
        """
        ReleaseConnection must be called for each call of an initialization method after finishing work with the Open Test Architecture API.
        """
        pass

    @abstractmethod
    def send_mail(self, send_to: 'str', send_from: 'str', subject: 'str', message: 'str', attach_array: 'Any', bs_format: 'str'):
        """
        Sends Mail.
        
        :param send_to: A Quality Center user name, an e-mail address (not necessarily a Quality Center user), or a list of e-mail addresses. Addresses can be separated by a comma or a semi-colon.
        :param send_from: No longer in use. The actual "From" address is the email associated with the connected user. Pass an empty string ("").
        :param subject: The subject of the e-mail.
        :param message: The body of the e-mail.
        :param attach_array: A Variant Array, each element of which is an Attachment.ServerFileName
        :param bs_format: The mail format, either "HTML", "TEXT" or any other string. Any string other than "HTML" is interpreted as text.
        """
        pass

    