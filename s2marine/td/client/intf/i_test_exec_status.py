from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ITestExecStatus(metaclass=ABCMeta):
    """
    Represents the execution status of the test currently running.
    """
    
    @abstractmethod
    def get__message(self) -> 'str':
        """
        The test execution message displayed in the Execution Controller grid.
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The test execution status.
        """
        pass
    
    @abstractmethod
    def get__test_id(self) -> 'int':
        """
        The test Id.
        """
        pass
    
    @abstractmethod
    def get__test_instance(self) -> 'int':
        """
        The number of this instance of the design test in the test set.
        """
        pass
    
    @abstractmethod
    def get__ts_test_id(self) -> 'Any':
        """
        The ID of the test instance for the current run.
        """
        pass
    
    