from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_extended_storage import IExtendedStorage
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_step_params import IStepParams
    from s2marine.td.client.intf.i_step_factory import IStepFactory
    


class IRun(metaclass=ABCMeta):
    """
    Represents a test run.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__bp_step_param_factory(self) -> 'T':
        """
        The BPStepParamFactory for the current run.
        """
        pass
    
    @abstractmethod
    def get__extended_storage(self) -> 'IExtendedStorage':
        """
        The ExtendedStorage object for the current run.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The run name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The run name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__params(self, source_mode: 'int') -> 'IStepParams':
        """
        The step parameters of this run.
        
        :param source_mode: 'A value of the tagTDAPI_RUN_PARAM_MODE Enumeration.'
        """
        pass
    
    @abstractmethod
    def get__result_location(self) -> 'str':
        """
        The run result location, a UNC path.
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The run status.
        """
        pass
    
    @abstractmethod
    def set__status(self, value: 'str'):
        """
        The run status.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__step_factory(self) -> 'IStepFactory':
        """
        The Step Factory for the current run.
        """
        pass
    
    @abstractmethod
    def get__test_id(self) -> 'int':
        """
        The test ID of the test that has been run.
        """
        pass
    
    @abstractmethod
    def get__test_instance(self) -> 'int':
        """
        The number of this instance of the design test in the test set.
        """
        pass
    
    @abstractmethod
    def get__test_instance_id(self) -> 'int':
        """
        The ID of the test instance for the current run.
        """
        pass
    
    @abstractmethod
    def get__test_set_id(self) -> 'int':
        """
        The ID of the test set to which the run belongs.
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def cancel_run(self):
        """
        Cancels the run.
        """
        pass

    @abstractmethod
    def copy_design_steps(self):
        """
        Copies design steps into the test run of an executed test.
        
        Call this method immediately after creating the Run object to fill the test run with copies of the design steps.
        """
        pass

    @abstractmethod
    def copy_steps_to_test(self):
        """
        Copies all run execution steps, including new added steps, into the design steps of the corresponding planning test.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def resolve_steps_parameters(self, update_local_cache: 'bool'):
        """
        Updates the texts of the run's steps by resolving the parameter values at run time.
        
        :param update_local_cache: If True, the client cache is updated with the changed 
        values.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    