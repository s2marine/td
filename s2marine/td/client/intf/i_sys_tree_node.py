from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode


class ISysTreeNode(metaclass=ABCMeta):
    """
    Represents a system folder, that is, a tree node in the TreeManager object.
    """
    
    @abstractmethod
    def get__attribute(self) -> 'int':
        """
        Gets the node attribute.
        """
        pass
    
    @abstractmethod
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        """
        Gets a sub-folder of the current folder.
        
        :param index: 'The index (1-based) represents the child level of the folder.'
        """
        pass
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of child nodes.
        """
        pass
    
    @abstractmethod
    def get__depth_type(self) -> 'int':
        """
        The sub-tree depth type.
        """
        pass
    
    @abstractmethod
    def get__father(self) -> 'ISysTreeNode':
        """
        The current folder's parent folder.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The node name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The node name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__node_id(self) -> 'int':
        """
        The node ID number.
        """
        pass
    
    @abstractmethod
    def get__path(self) -> 'str':
        """
        The folder tree path starting from the tree root.
        """
        pass
    
    @abstractmethod
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        """
        Adds a new child node.
        
        :param node_name: The new folder name.
        :return: A SysTreeNode Object.
        """
        pass

    @abstractmethod
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        """
        Finds a child node by node name.
        
        :param child_name: The folder name to find.
        :return: A SysTreeNode Object.
        """
        pass

    @abstractmethod
    def find_children(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        """
        Finds node children according to specified search pattern.
        
        :param pattern: The search string or pattern.
        :param match_case: If True, the search is case-sensitive.
        :param filter: A logical filter. The filter can be retrieved from TDFilter.Text.
        :return: A list of SysTreeNode objects that match the pattern and filter.
        """
        pass

    @abstractmethod
    def new_list(self) -> 'List[T]':
        """
        Gets a list of the node's immediate children.
        The list index is 1-based.
        :return: A list of SysTreeNode objects in the first level below the current node.
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values to the database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads the saved node data, overwriting values in memory.
        """
        pass

    @abstractmethod
    def remove_node(self, node: 'Any'):
        """
        Deletes the specified node.
        
        :param node: The node name (string), index (long) of the folder, or the SysTreeNode object representing the folder.
        """
        pass

    