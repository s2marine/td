from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IProductInfo(metaclass=ABCMeta):
    """
    Information about the current version.
    """
    
    @abstractmethod
    def get__bpt_version(self) -> 'int':
        """
        The Business Process Test versions.
        """
        pass
    
    @abstractmethod
    def get__qc_version(self, pn_major_version: 'int', pn_minor_version: 'int') -> 'int':
        """
        The build information for the installed version.
        
        :param pn_major_version: 'The release number.'
        :param pn_minor_version: 'The intermediate release number.'
        """
        pass
    
    