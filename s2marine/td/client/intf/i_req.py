from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    
    from s2marine.td.client.intf.i_test import ITest


class IReq(metaclass=ABCMeta):
    """
    Represents a requirement for which testing must be performed.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def get__author(self) -> 'str':
        """
        The name of the requirement author.
        """
        pass
    
    @abstractmethod
    def set__author(self, value: 'str'):
        """
        The name of the requirement author.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__comment(self) -> 'str':
        """
        The comment associated with this requirement.
        """
        pass
    
    @abstractmethod
    def set__comment(self, value: 'str'):
        """
        The comment associated with this requirement.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of direct children.
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def is__has_coverage(self) -> 'bool':
        """
        The requirement is covered by at least one test.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The Requirement name as seen in the tree.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The Requirement name as seen in the tree.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__paragraph(self) -> 'str':
        """
        The paragraph number or numeration of the requirement.
        """
        pass
    
    @abstractmethod
    def get__path(self) -> 'str':
        """
        The path of the node containing this requirement.
        """
        pass
    
    @abstractmethod
    def get__priority(self) -> 'str':
        """
        The requirement priority.
        """
        pass
    
    @abstractmethod
    def set__priority(self, value: 'str'):
        """
        The requirement priority.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__product(self) -> 'str':
        """
        The product name for which the requirement is set.
        """
        pass
    
    @abstractmethod
    def set__product(self, value: 'str'):
        """
        The product name for which the requirement is set.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__reviewed(self) -> 'str':
        """
        The review status.
        """
        pass
    
    @abstractmethod
    def set__reviewed(self, value: 'str'):
        """
        The review status.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__status(self) -> 'str':
        """
        The status of the tests that cover this requirement.
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'str':
        """
        The requirement type.
        """
        pass
    
    @abstractmethod
    def set__type(self, value: 'str'):
        """
        The requirement type.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def add_coverage(self, test_id: 'int', order: 'int') -> 'int':
        """
        Assigns a test to cover this requirement.
        The actual position of the test in the requirement coverage list.
        
        :param test_id: The ID of the test assigned to the requirement.
        :param order: The position of the test in the requirement coverage list. Possible values are: 
        
        A number representing the position. 
        TDPOSITION_LAST [-4] - Indicates that the test is inserted last
        :return: Zero on success, or -1 on error.
        """
        pass

    @abstractmethod
    def add_coverage_by_filter(self, subject_id: 'int', order: 'int', test_filter: 'str') -> 'int':
        """
        Adds the tests from the specified subject that match the input filter to the list of tests that cover the current requirement.
        
        :param subject_id: The ID of the folder in the test plan module.
        :param order: The desired position in the list of tests.
        :param test_filter: A TDFilter.Text that selects the tests to cover the requirement.
        :return: 
        """
        pass

    @abstractmethod
    def add_coverage_ex(self, subject_id: 'int', order: 'int') -> 'int':
        """
        Assigns all the tests in the specified subject folder to cover this current requirement.
        The actual position of the first test in the folder in the requirement coverage list.
        
        :param subject_id: The ID of the subject folder containing the tests to be added to the requirement.
        :param order: The position of the first test in the folder in the requirement coverage list. Possible values are: 
        
        A number representing the position. 
        TDPOSITION_LAST [-4] - Indicates that the test is inserted last
        :return: Zero on success, or -1 on error.
        """
        pass

    @abstractmethod
    def get_cover_list(self, recursive: 'bool') -> 'List[ITest]':
        """
        Gets a list of the tests that cover this requirement.
        
        :param recursive: If TRUE, GetCoverList returns a list of coverage tests for current requirement and all its children. Otherwise, the method returns a list of coverage tests only for the current requirement.
        :return: A list of Test Objects.
        """
        pass

    @abstractmethod
    def get_cover_list_by_filter(self, test_filter: 'str', recursive: 'bool') -> 'List[T]':
        """
        Gets the list of all tests that cover the current requirement and match the filter.
        
        :param test_filter: A TDFilter.Text  string.
        :param recursive: If TRUE, GetCoverList returns a list of coverage tests for current requirement and all its children. Otherwise, the method returns a list of coverage tests only for the current requirement.
        :return: 
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def mail(self, send_to: 'str', send_cc: 'str', option: 'int', subject: 'str', comment: 'str'):
        """
        Mails the IBaseFieldExMail field item.
        
        :param send_to: The recipient address or addresses. Addresses can be separated by a comma or a semicolon.
        :param send_cc: The recipients of a carbon copy of the mail. Addresses can be separated by a comma or a semicolon.
        :param option: The mailing options binary mask.Create the mask using values of TDMAIL_FLAGS.
        :param subject: The mail subject line.
        :param comment: A comment on the sent e-mail. If the TDMAIL_COMMENT_AS_BODY option is set, this will be sent as the message body.
        """
        pass

    @abstractmethod
    def move(self, new_father_id: 'int', new_order: 'int'):
        """
        Moves a requirement to being a child of a specified father in the requirements tree.
        
        :param new_father_id: The ID of the designated new father requirement.
        :param new_order: The position of the requirement under the new parent. Possible values are: 
        
        A number representing the position. 
        TDPOSITION_LAST [-4] - Indicates that the test is inserted last
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def remove_coverage(self, v_test: 'Any', recursive: 'bool'):
        """
        Removes a test from the list of tests that cover this requirement.
        
        :param v_test: A Test Object or array of test objects.
        :param recursive: If TRUE, the specified coverage is removed for all the requirements under the current one. If FALSE, only the coverage at the current requirement is removed.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    