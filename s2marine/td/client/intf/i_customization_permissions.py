from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_transition_rules import ICustomizationTransitionRules
    


class ICustomizationPermissions(metaclass=ABCMeta):
    """
    Properties that define the ability of user groups to add, remove, and modify entities.
    """
    
    @abstractmethod
    def is__can_add_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        """
        Indicates if members of the specified group can add the specified type of entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__can_add_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        """
        Indicates if members of the specified group can add the specified type of entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__can_allow_attachment(self, entity_name: 'str', group: 'Any') -> 'int':
        """
        Indicates if members owners of the specified group can handle the specified type of entity attachments.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__can_allow_attachment(self, entity_name: 'str', group: 'Any', value: 'int'):
        """
        Indicates if members owners of the specified group can handle the specified type of entity attachments.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'int':
        """
        Indicates if members or owners of the specified group can modify the specified field.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param field: 'The name of the field or a CustomizationField Object.'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__can_modify_field(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'int'):
        """
        Indicates if members or owners of the specified group can modify the specified field.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param field: 'The name of the field or a CustomizationField Object.'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__can_modify_item(self, entity_name: 'str', group: 'Any') -> 'bool':
        """
        Indicates if members of the specified group can modify the specified entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__can_modify_item(self, entity_name: 'str', group: 'Any', value: 'bool'):
        """
        Indicates if members of the specified group can modify the specified entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__can_remove_item(self, entity_name: 'str', group: 'Any') -> 'int':
        """
        Indicates if members or owners of the specified group can remove the specified type of entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__can_remove_item(self, entity_name: 'str', group: 'Any', value: 'int'):
        """
        Indicates if members or owners of the specified group can remove the specified type of entity.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__can_use_owner_sensible(self, entity_name: 'str') -> 'bool':
        """
        Checks if the fields of the specified entity can be restricted for owner modifications only.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        """
        pass
    
    @abstractmethod
    def is__has_attachment_field(self, entity_name: 'str') -> 'bool':
        """
        Checks if the entity specified by the EntityName parameter can have attachments.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        """
        pass
    
    @abstractmethod
    def is__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'bool':
        """
        Indicates if the specified Bug field is visible for the specified group in a new defect form.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param field: 'The name of the field or a CustomizationField Object.'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    @abstractmethod
    def set__is_visible_in_new_bug(self, entity_name: 'str', field: 'Any', group: 'Any', value: 'bool'):
        """
        Indicates if the specified Bug field is visible for the specified group in a new defect form.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param field: 'The name of the field or a CustomizationField Object.'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__transition_rules(self, entity_name: 'str', field: 'Any', group: 'Any') -> 'ICustomizationTransitionRules':
        """
        The transition rules for the specified field and group.
        
        :param entity_name: 'One of : ALL_LISTS, BUG, CROS_REF, CYCLE, CYCL_FOLD, DESSTEPS, GROUPS, HISTORY, HOSTS, HOST_GROUP, HOST_IN_GROUP, MAILCOND, REQ, RUN, STEP, TEST, TESTCYCL, TRAN_RULES, or USERS'
        :param field: 'The name of the field or a CustomizationField Object.'
        :param group: 'The name of the group or a CustomizationUsersGroup Object.'
        """
        pass
    
    