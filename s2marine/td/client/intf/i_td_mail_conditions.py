from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ITDMailConditions(metaclass=ABCMeta):
    """
    Services for managing automatic mail notifications.
    """
    
    @abstractmethod
    def get__condition(self, name: 'str', is_user_condition: 'bool') -> 'str':
        """
        The defect-matching rule.
        
        :param name: 'The mail recipient.'
        :param is_user_condition: 'Pass True if Name is a user name. Pass False if Name is the field containing the name.'
        """
        pass
    
    @abstractmethod
    def set__condition(self, name: 'str', is_user_condition: 'bool', value: 'str'):
        """
        The defect-matching rule.
        
        :param name: 'The mail recipient.'
        :param is_user_condition: 'Pass True if Name is a user name. Pass False if Name is the field containing the name.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def get__item_list(self, get_real_names: 'bool') -> 'List[T]':
        """
        Gets the list of Mail Condition Users.
        
        :param get_real_names: 'If True, return only recipients specified directly in the conditions. If False, return all recipients, regardless of how specified.'
        """
        pass
    
    @abstractmethod
    def close(self, need_to_save: 'bool'):
        """
        Closes and updates Mail Condition.
        
        :param need_to_save: If true, post changes to project when Close is called.
        """
        pass

    @abstractmethod
    def delete_condition(self, name: 'str', is_user_condition: 'bool'):
        """
        Deletes the Mail Condition.
        
        :param name: Either a user name or a field name.
        :param is_user_condition: Pass True if Name specifies a user name. Pass False if Name specifies a field.
        """
        pass

    @abstractmethod
    def load(self):
        """
        Loads Mail Conditions.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    