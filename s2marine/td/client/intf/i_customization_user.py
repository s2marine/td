from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ICustomizationUser(metaclass=ABCMeta):
    """
    Represents a user for purposes of adding and removing the user to and from user groups.
    """
    
    @abstractmethod
    def get__address(self) -> 'str':
        """
        A text field that can be used to record user information as appropriate for the project.
        """
        pass
    
    @abstractmethod
    def set__address(self, value: 'str'):
        """
        A text field that can be used to record user information as appropriate for the project.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__deleted(self) -> 'bool':
        """
        If true, this user is marked for deletion, but the deletion is not yet committed.
        """
        pass
    
    @abstractmethod
    def set__deleted(self, value: 'bool'):
        """
        If true, this user is marked for deletion, but the deletion is not yet committed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__domain_authentication(self) -> 'str':
        """
        The user credentials for LDAP authentication.
        """
        pass
    
    @abstractmethod
    def set__domain_authentication(self, value: 'str'):
        """
        The user credentials for LDAP authentication.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__email(self) -> 'str':
        """
        The user's e-mail address.
        """
        pass
    
    @abstractmethod
    def set__email(self, value: 'str'):
        """
        The user's e-mail address.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__full_name(self) -> 'str':
        """
        The user's full name.
        """
        pass
    
    @abstractmethod
    def set__full_name(self, value: 'str'):
        """
        The user's full name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__in_group(self, group_name: 'Any') -> 'bool':
        """
        Indicates if this user is a member of the specified group.
        
        :param group_name: 'The name of the group or a CustomizationUsersGroup 
        Object.'
        """
        pass
    
    @abstractmethod
    def set__in_group(self, group_name: 'Any', value: 'bool'):
        """
        Indicates if this user is a member of the specified group.
        
        :param group_name: 'The name of the group or a CustomizationUsersGroup 
        Object.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        A unique name by which to identify this user.
        """
        pass
    
    @abstractmethod
    def get__password(self, rhs: 'str') -> 'str':
        """
        Sets the user's password.
        
        :param rhs: 'The password.'
        """
        pass
    
    @abstractmethod
    def set__password(self, rhs: 'str', value: 'str'):
        """
        Sets the user's password.
        
        :param rhs: 'The password.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__phone(self) -> 'str':
        """
        The user's telephone number.
        """
        pass
    
    @abstractmethod
    def set__phone(self, value: 'str'):
        """
        The user's telephone number.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the user's data was updated since the last synchronization with the server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the user's data was updated since the last synchronization with the server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_to_group(self, group: 'Any'):
        """
        Adds this user to the specified user group.
        
        :param group: The name of the group or a CustomizationUsersGroup 
        Object.
        """
        pass

    @abstractmethod
    def groups_list(self) -> 'List[T]':
        """
        The names of all the user groups to which this user belongs.
        :return: 
        """
        pass

    @abstractmethod
    def remove_from_group(self, group: 'Any'):
        """
        Removes this user from the specified user group.
        
        :param group: The name of the group or a CustomizationUsersGroup 
        Object.
        """
        pass

    