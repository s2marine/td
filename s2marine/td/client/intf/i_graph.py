from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IGraph(metaclass=ABCMeta):
    """
    Represents a graph built through a method.
    """
    
    @abstractmethod
    def get__col_count(self) -> 'int':
        """
        The number of columns.
        """
        pass
    
    @abstractmethod
    def get__col_name(self, col: 'int') -> 'str':
        """
        Gets the name of the specified column. Column numbers are zero-based.
        
        :param col: 'The column number (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__col_total(self, col: 'int') -> 'int':
        """
        The sum of the values in the specified graph data column. The column number is zero-based.
        
        :param col: 'The column number (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__data(self, col: 'int', row: 'int') -> 'int':
        """
        The data in the specified cell. Column and Row numbers are zero-based.
        
        :param col: 'The column number (zero-based).'
        :param row: 'The row number (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__graph_total(self) -> 'int':
        """
        The total number of items represented in the graph.
        """
        pass
    
    @abstractmethod
    def get__max_value(self) -> 'int':
        """
        The maximum value in the graph.
        """
        pass
    
    @abstractmethod
    def get__row_count(self) -> 'int':
        """
        The number of rows.
        """
        pass
    
    @abstractmethod
    def get__row_name(self, row: 'int') -> 'str':
        """
        Gets the row name of the specified row. Row numbers are zero-based.
        
        :param row: 'The row number (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__row_total(self, row: 'int') -> 'int':
        """
        The sum of the values in the specified graph data row. The row number is zero-based.
        
        :param row: 'The row number (zero-based).'
        """
        pass
    
    @abstractmethod
    def get__start_date(self) -> 'float':
        """
        The start date for the graph.
        """
        pass
    
    @abstractmethod
    def drill_down(self, p_areas: 'Any', m_areas: 'Any') -> 'List[T]':
        """
        Drills down to graph data.
        pAreas is a two-dimensional variant array representing lists of rectangular areas in data grid. The first array dimension size must be 4.Such an array can be logically modeled as a table with four columns where each row represents one rectangular area. Each rectangular area is a set of four coordinates: TopLeftColNum, TopLeftRowNum, BottomRightColNum, and BottomRightRowNum.
        
        :param p_areas: The area of the graph to retrieve, base on its data grid view.
        :param m_areas: No longer in use. Argument preserved for backward compatibility. Pass an Empty variant.
        :return: A list of objects representing the data in the specified cell of the graph. For example, in a defect graph, DrillDown returns a list of Bug objects.
        """
        pass

    @abstractmethod
    def multi_drill_down(self, areas: 'Any') -> 'List[T]':
        """
        Gets detailed information about a graph area consisting of four sets of coordinates.
        Areas is a two-dimensional variant array representing a list of rectangular areas in data grid. The first array dimension size must be 4.Such an array can be logically modeled as a table with four columns where each row represents one rectangular area. Each rectangular area is a set of four coordinates: TopLeftColNum, TopLeftRowNum, BottomRightColNum, and BottomRightRowNum.
        
        :param areas: The area of the graph to retrieve, based on its data grid view.
        :return: A list of objects representing the data in the specified cell of the graph. For example, in a defect graph, DrillDown returns a list of Bug objects.
        """
        pass

    