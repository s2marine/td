from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_history import IHistory
    


class IHostGroup(metaclass=ABCMeta):
    """
    Services for managing a group of host servers.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The Attachment factory for the object.
        """
        pass
    
    @abstractmethod
    def is__auto_post(self) -> 'bool':
        """
        If true, the database is updated immediately when the field value changes.
        """
        pass
    
    @abstractmethod
    def set__auto_post(self, value: 'bool'):
        """
        If true, the database is updated immediately when the field value changes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__field(self, field_name: 'str') -> 'Any':
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__field(self, field_name: 'str', value: 'Any'):
        """
        The value of the specified field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if the object has one or more attachments.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for the object.
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The item ID.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked.
        """
        pass
    
    @abstractmethod
    def is__modified(self) -> 'bool':
        """
        Checks if the item has been modified since last refresh or post operation. If true, the field properties on the server side are not up to date.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        Gets the host group name.
        """
        pass
    
    @abstractmethod
    def is__virtual(self) -> 'bool':
        """
        Checks if this is a virtual item, that is, an item that does not have a corresponding database record.
        """
        pass
    
    @abstractmethod
    def add_host(self, val: 'Any'):
        """
        Adds a host to the group.
        
        :param val: Either a Host Object, the host name, or a string array 
        containing the names of the hosts to add.
        """
        pass

    @abstractmethod
    def lock_object(self) -> 'bool':
        """
        Locks the object. Returns True if lock successful.
        :return: 
        """
        pass

    @abstractmethod
    def new_list(self) -> 'List[T]':
        """
        Gets a list of all hosts in the group.
        :return: 
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values into database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads saved values, overwriting values in memory.
        """
        pass

    @abstractmethod
    def remove_host(self, val: 'Any'):
        """
        Removes the specified host from the group.
        
        :param val: Either a Host Object, the host name, or a string array 
        containing the names of the hosts to add.
        """
        pass

    @abstractmethod
    def undo(self):
        """
        Undoes changes to field values that have not been posted.
        """
        pass

    @abstractmethod
    def un_lock_object(self):
        """
        Unlocks the object.
        """
        pass

    