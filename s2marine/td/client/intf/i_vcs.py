from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    from s2marine.td.client.intf.i_version_item import IVersionItem
    


class IVCS(metaclass=ABCMeta):
    """
    Represents a Version Control System connection.
    """
    
    @abstractmethod
    def get__checkout_info(self) -> 'IVersionItem':
        """
        A reference to a VersionItem object, containing the checkout information of the current object.
        """
        pass
    
    @abstractmethod
    def get__current_version(self) -> 'str':
        """
        The current object latest version.
        """
        pass
    
    @abstractmethod
    def is__is_checked_out(self) -> 'bool':
        """
        Checks if the object is in CHECKED-OUT or GET status.
        """
        pass
    
    @abstractmethod
    def is__is_locked(self) -> 'bool':
        """
        Checks if object is locked by the version control system.
        """
        pass
    
    @abstractmethod
    def get__locked_by(self) -> 'str':
        """
        The name of the locking user.
        """
        pass
    
    @abstractmethod
    def get__version(self) -> 'str':
        """
        The version the current user is viewing.
        """
        pass
    
    @abstractmethod
    def get__version_info(self, version: 'str') -> 'IVersionItem':
        """
        A reference to a VersionItem object containing information about a specific version, specified by the Version argument.
        
        :param version: 'The version number of the object: <major version>.<minor version>.<build>.'
        """
        pass
    
    @abstractmethod
    def get__versions(self) -> 'List[T]':
        """
        A list of version strings, representing all the versions in the object.
        """
        pass
    
    @abstractmethod
    def get__versions_ex(self) -> 'List[IVersionItem]':
        """
        A list of IVersionItem references for accessing the details of each version in the object. .
        """
        pass
    
    @abstractmethod
    def add_object_to_vcs(self, version: 'str', comments: 'str'):
        """
        Adds an object to the Version control database.
        When using an automated test, this method must be used after the test files are uploaded from the client to the server.
        
        :param version: Specifies the starting version number of the test.
        :param comments: Comments pertaining to the object version.
        """
        pass

    @abstractmethod
    def check_in(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool'):
        """
        Checks in an object.
        This method can also be used in case of a conflict caused by a change in the StepParams value.
        
        :param version: The starting version number of the object.
        :param comments: Comment on the object version.
        :param remove: If True, delete the files from the file system. It is recommended not to use this option.
        :param set_current: Sets the version you are currently working with as the current one. It is recommended not to use this option.
        """
        pass

    @abstractmethod
    def check_in_ex(self, version: 'str', comments: 'str', remove: 'bool', set_current: 'bool', force_checkin: 'bool', reserved: 'int'):
        """
        Checks in an object.
        This method can also be used in case of a conflict caused by a change in the StepParams value.
        
        :param version: The starting version number of the object.
        :param comments: Comment on the object version.
        :param remove: If True, delete the files from the file system. It is recommended not to use this option.
        :param set_current: Sets the version you are currently working with as the current one. It is recommended not to use this option.
        :param force_checkin: If True, ignores conflict. Otherwise, checks for conflict.
        :param reserved: Not in use.
        """
        pass

    @abstractmethod
    def check_out(self, version: 'str', comment: 'str', lock: 'bool', read_only: 'bool', sync: 'bool'):
        """
        Checks out an object
        
        :param version: The version number of the object.  Pass an empty string to check out the latest version.
        :param comment: Comment pertaining to the object version.
        :param lock: If True, check out and lock the object. Otherwise, just get copy.
        :param read_only: If True, the checked out files will be read only. It is recommended not to use this option.
        :param sync: For internal use.
        """
        pass

    @abstractmethod
    def delete_object_from_vcs(self):
        """
        Deletes the object from the Version Control database.
        """
        pass

    @abstractmethod
    def lock_vcs_object(self):
        """
        Changes VCS object status from GET to CHECK-OUT. The object must be in a GET status to use this method.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Refreshes the VCS object from the server to the client, overwriting values in memory.
        """
        pass

    @abstractmethod
    def set_current_version(self, version: 'str'):
        """
        This method is potentially destructive. Use only if necessary and with care. Sets the VCS object current version.
        
        :param version: The version number to make current.
        """
        pass

    @abstractmethod
    def undo_checkout(self, remove: 'bool'):
        """
        Undoes the check out operation, falling back to the current version on the server.
        The local copy of the file is removed when UndoCheckout is called.
        
        :param remove: Always pass "True".
        """
        pass

    