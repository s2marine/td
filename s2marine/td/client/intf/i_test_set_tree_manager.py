from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_folder import ITestSetFolder
    


class ITestSetTreeManager(metaclass=ABCMeta):
    """
    Manages the test set tree and its related test set folders.
    """
    
    @abstractmethod
    def get__node_by_id(self, node_id: 'int') -> 'ITestSetFolder':
        """
        Gets the test set tree node object corresponding to the specified node Id.
        
        :param node_id: 'The ID of the node in the project database.'
        """
        pass
    
    @abstractmethod
    def get__node_by_path(self, path: 'str') -> 'ITestSetFolder':
        """
        Gets the test set tree node from the specified tree path.
        
        :param path: 'The folder tree path starting from the tree root. For example, "Root\OTA_Demo_TestSets".'
        """
        pass
    
    @abstractmethod
    def get__root(self) -> 'ISysTreeNode':
        """
        The test set tree root node.
        """
        pass
    
    @abstractmethod
    def get__unattached(self) -> 'ITestSetFolder':
        """
        Returns an unattached folder.
        """
        pass
    
    