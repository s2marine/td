from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ICondition(metaclass=ABCMeta):
    """
    Represents a condition for a test to be executed.
    """
    
    @abstractmethod
    def get__description(self) -> 'str':
        """
        The Condition description.
        """
        pass
    
    @abstractmethod
    def set__description(self, value: 'str'):
        """
        The Condition description.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__id(self) -> 'Any':
        """
        The Condition ID.
        """
        pass
    
    @abstractmethod
    def get__source(self) -> 'Any':
        """
        For a run condition, the source condition ID. For a time condition, a date-time string.
        """
        pass
    
    @abstractmethod
    def get__target(self) -> 'Any':
        """
        The target test instance number of the condition.
        """
        pass
    
    @abstractmethod
    def get__value(self) -> 'Any':
        """
        The Condition status value.
        """
        pass
    
    @abstractmethod
    def set__value(self, value: 'Any'):
        """
        The Condition status value.
        :param value: 
        """
        pass
    
    