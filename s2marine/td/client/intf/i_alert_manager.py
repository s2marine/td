from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_alert import IAlert
    from s2marine.td.client.intf.i_alert import IAlert
    


class IAlertManager(metaclass=ABCMeta):
    """
    Services for managing alerts.
    """
    
    @abstractmethod
    def get__alert(self, id: 'int') -> 'IAlert':
        """
        Gets the Alert object for the alert with the specified ID.
        
        :param id: 'An Alert.ID.'
        """
        pass
    
    @abstractmethod
    def get__alert_list(self, entity_type: 'str', need_refresh: 'bool') -> 'List[IAlert]':
        """
        The list of user alerts.
        
        :param entity_type: 'One of: "ALL", "TEST", "TESTCYCL", or "BUG".'
        :param need_refresh: 'If True, refresh the local cache from the project database.'
        """
        pass
    
    @abstractmethod
    def clean_all_alerts(self):
        """
        Removes all current alerts from the database.
        """
        pass

    @abstractmethod
    def delete_alert(self, i_ds: 'Any'):
        """
        Removes an alert or alerts related to the current object from the database.
        
        :param i_ds: An Alert.ID or "[ALL]", "[TEST]", "[TESTCYCL]" or "[BUG]".
        """
        pass

    