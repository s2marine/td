from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_td_field import ITDField
    from s2marine.td.client.intf.i_history import IHistory
    from s2marine.td.client.intf.i_test_set import ITestSet
    
    from s2marine.td.client.intf.i_test_set import ITestSet


class ITestSetFactory(metaclass=ABCMeta):
    """
    Services for managing test sets.
    """
    
    @abstractmethod
    def get__fetch_level(self, field_name: 'str') -> 'int':
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        """
        pass
    
    @abstractmethod
    def set__fetch_level(self, field_name: 'str', value: 'int'):
        """
        The Fetch level for a field.
        
        :param field_name: 'The name of the field in the project database.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__fields(self) -> 'List[ITDField]':
        """
        The list of all available fields for the entity managed by the factory.
        """
        pass
    
    @abstractmethod
    def get__filter(self) -> 'TDFilter':
        """
        The TDFilter object for the factory.
        """
        pass
    
    @abstractmethod
    def get__history(self) -> 'IHistory':
        """
        The History object for this entity.
        """
        pass
    
    @abstractmethod
    def get__item(self, item_key: 'Any') -> 'ITestSet':
        """
        Gets an object managed by the factory by its key.
        
        :param item_key: 'A TestSet.ID (long). '
        """
        pass
    
    @abstractmethod
    def add_item(self, item_data: 'Any') -> 'ITestSet':
        """
        Creates a new item object.
        
        Passing NULL as the ItemData argument creates a virtual object, one that does not appear in the project database. After creating the item, use the relevant object properties to fill the object, then use the Post method to save the object in the database.
        
        :param item_data: There are three options for ItemData: 
        
        Null. Creating a virtual TestSet object with Null ensures that you cannot then Post until all required fields are initialized. 
        The test set name. 
        An array consisting of the following elements: 
        (0) Name - The test set name (string, required). 
        (1) Test set ID - The ID of the test set folder in which to create the test set (long, optional).
        :return: A TestSet Object.
        """
        pass

    @abstractmethod
    def build_perf_graph(self, test_set_id: 'int', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates Performance Graph.
        
        :param test_set_id: The test set ID.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph(self, test_set_id: 'int', group_by_field: 'str', sum_of_field: 'str', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates graph showing number of tests in all test sets executed over a period of time.
        
        :param test_set_id: The test set ID.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_progress_graph_ex(self, test_set_id: 'int', group_by_field: 'str', sum_of_field: 'str', by_history: 'bool', major_skip: 'int', minor_skip: 'int', max_cols: 'int', filter: 'Any', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates progress graph for specified test set.
        
        :param test_set_id: The test set ID.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param by_history: Determines whether to use history information to create the graph. Possible values are: 
        TRUE - Use history information. 
        FALSE - Do not use history information.
        :param major_skip: Determines the interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param minor_skip: Determines the second level interval type shown in the graph. Use the values of tagTDAPI_SKIP Enumeration.
        :param max_cols: The maximum number of groups represented in the graph. Zero indicates no limit.
        :param filter: The TDFilter Object that sets the criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are:
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_summary_graph(self, test_set_id: 'int', x_axis_field: 'str', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates graph showing the number of requirements reported according to the defect tracking information specified.
        
        :param test_set_id: The test set ID.
        :param x_axis_field: A database field representing the name of the graph x-axis.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def build_trend_graph(self, test_set_id: 'int', group_by_field: 'str', sum_of_field: 'str', max_cols: 'int', filter: 'TDFilter', fr_date: 'Any', force_refresh: 'bool', show_full_path: 'bool') -> 'T':
        """
        Creates graph showing the number of status changes over a time period.
        
        :param test_set_id: The test set ID.
        :param group_by_field: A database field representing the name of the graph y-axis. All data in the graph is grouped according to this parameter.
        :param sum_of_field: For internal use.
        :param max_cols: The maximum number of groups represented in the graph. A value of 0 signifies no limit.
        :param filter: A TDFilter Object that sets the filter criteria for the graph.
        :param fr_date: Only items with a change date after this date are reflected in the graph.
        :param force_refresh: Determines whether or not to refresh graph data on the server side. Possible values are: 
        
        TRUE - The graph is refreshed. 
        FALSE - The graph is not refreshed
        :param show_full_path: Group instances by name or show individually based on location (path), in the created graph. 
        A value of True shows the full path of each instance, thus treating them as unique.
        :return: A new Graph Object.
        """
        pass

    @abstractmethod
    def new_list(self, filter: 'str') -> 'List[T]':
        """
        Creates a list of objects according to the specified filter.
        
        :param filter: A TDFilter.Text defining the criteria for filtering items in the factory. If an empty string is passed, the returned list contains all the child items of the current factory object.
        :return: A reference to the list created. The list index is 1-based.
        """
        pass

    @abstractmethod
    def remove_item(self, item_key: 'Any'):
        """
        Removes item from the database. Removal takes place immediately, without a Post.
        
        :param item_key: The TestSet.ID (long), a reference to the TestSet Object, or a Variant array of TestSet.IDs.
        """
        pass

    