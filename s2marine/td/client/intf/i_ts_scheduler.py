from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_execution_status import IExecutionStatus
    


class ITSScheduler(metaclass=ABCMeta):
    """
    Responsible for executing selected automated tests.
    """
    
    @abstractmethod
    def get__execution_log(self) -> 'str':
        """
        The execution error or log message sent by the remote agent.
        """
        pass
    
    @abstractmethod
    def get__execution_status(self) -> 'IExecutionStatus':
        """
        The execution status object for the scheduler execution.
        """
        pass
    
    @abstractmethod
    def get__host_time_out(self) -> 'int':
        """
        The time to wait for response before either sending test to another host in group or failing.
        """
        pass
    
    @abstractmethod
    def set__host_time_out(self, value: 'int'):
        """
        The time to wait for response before either sending test to another host in group or failing.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__log_enabled(self) -> 'bool':
        """
        Indicates if logging is enabled.
        """
        pass
    
    @abstractmethod
    def set__log_enabled(self, value: 'bool'):
        """
        Indicates if logging is enabled.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__run_all_locally(self) -> 'bool':
        """
        Indicates if all tests are to be run on the local host.
        """
        pass
    
    @abstractmethod
    def set__run_all_locally(self, value: 'bool'):
        """
        Indicates if all tests are to be run on the local host.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__run_on_host(self, ts_test_id: 'Any') -> 'str':
        """
        The host on which to execute the test instance.
        
        :param ts_test_id: 'The ID of the test instance (TSTest).'
        """
        pass
    
    @abstractmethod
    def set__run_on_host(self, ts_test_id: 'Any', value: 'str'):
        """
        The host on which to execute the test instance.
        
        :param ts_test_id: 'The ID of the test instance (TSTest).'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__td_host_name(self) -> 'str':
        """
        The host on which to run all tests in the current scheduler execution.
        """
        pass
    
    @abstractmethod
    def set__td_host_name(self, value: 'str'):
        """
        The host on which to run all tests in the current scheduler execution.
        :param value: 
        """
        pass
    
    @abstractmethod
    def run(self, test_data: 'Any'):
        """
        Starts execution the test set or of the specified tests.
        A TSScheduler can perform only one run. If you want to perform more than one run, then after calling Run, call TestSet.StartExecution again to get a new TSScheduler object.
        
        :param test_data: A comma-separated list of the IDs (string) of the test instances to run, or a List of TSTest objects. If no value is passed, the entire test set is run.
        """
        pass

    @abstractmethod
    def stop(self, test_data: 'Any'):
        """
        Stops execution of the test set or the specified tests.
        
        :param test_data: A comma-separated list of the IDs (string) of the test instances to stop. If no value is passed, all the test instances in the test set are stopped/
        """
        pass

    