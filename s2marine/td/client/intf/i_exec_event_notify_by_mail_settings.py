from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IExecEventNotifyByMailSettings(metaclass=ABCMeta):
    """
    Represents the notification to be sent by email after a test has completed its run.
    """
    
    @abstractmethod
    def get__e_mail_to(self) -> 'str':
        """
        The recipients' email addresses.
        """
        pass
    
    @abstractmethod
    def set__e_mail_to(self, value: 'str'):
        """
        The recipients' email addresses.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__enabled(self, event_type: 'int') -> 'bool':
        """
        Indicates if mail is enabled for the specified event type.
        
        :param event_type: 'The event type for which to enable the notification. A member of the tagTDAPI_EXECUTIONEVENT Enumeration'
        """
        pass
    
    @abstractmethod
    def set__enabled(self, event_type: 'int', value: 'bool'):
        """
        Indicates if mail is enabled for the specified event type.
        
        :param event_type: 'The event type for which to enable the notification. A member of the tagTDAPI_EXECUTIONEVENT Enumeration'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__user_message(self) -> 'str':
        """
        The message body of the email.
        """
        pass
    
    @abstractmethod
    def set__user_message(self, value: 'str'):
        """
        The message body of the email.
        :param value: 
        """
        pass
    
    @abstractmethod
    def save(self, auto_post: 'bool'):
        """
        Uploads the notification settings to the server.
        
        :param auto_post: If TRUE, the notification is updated immediately when the field is changed.
        """
        pass

    