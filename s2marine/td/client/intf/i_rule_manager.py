from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    
    from s2marine.td.client.intf.i_rule import IRule


class IRuleManager(metaclass=ABCMeta):
    """
    Services for managing the notification rules.
    """
    
    @abstractmethod
    def get__rules(self, need_refresh: 'bool') -> 'List[T]':
        """
        A list of all the Rule objects.
        
        :param need_refresh: 'If True, the rules in the local cache are reloaded from the project.'
        """
        pass
    
    @abstractmethod
    def get_rule(self, id: 'int') -> 'IRule':
        """
        Gets a traceability notification rule by its ID.
        
        :param id: A Rule.ID.
        :return: A Rule Object.
        """
        pass

    