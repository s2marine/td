from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class ICustomizationTransitionRule(metaclass=ABCMeta):
    """
    Represents a single transition rule.
    """
    
    @abstractmethod
    def get__destination_value(self) -> 'str':
        """
        The destination value of the transition rule.
        """
        pass
    
    @abstractmethod
    def set__destination_value(self, value: 'str'):
        """
        The destination value of the transition rule.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__source_value(self) -> 'str':
        """
        The source value of the transition rule.
        """
        pass
    
    @abstractmethod
    def set__source_value(self, value: 'str'):
        """
        The source value of the transition rule.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the object has been modified since last synchronized with server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the object has been modified since last synchronized with server.
        :param value: 
        """
        pass
    
    