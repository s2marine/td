from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_attachment_factory import IAttachmentFactory
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_test_set_factory import ITestSetFactory
    
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode
    from s2marine.td.client.intf.i_sys_tree_node import ISysTreeNode


class ITestSetFolder(metaclass=ABCMeta):
    """
    Manages the tests and test sets included in a particular test set folder.
    """
    
    @abstractmethod
    def get__attachments(self) -> 'IAttachmentFactory':
        """
        The folder's AttachmentFactory object.
        """
        pass
    
    @abstractmethod
    def get__attribute(self) -> 'int':
        """
        Gets the node attribute.
        """
        pass
    
    @abstractmethod
    def get__child(self, index: 'int') -> 'ISysTreeNode':
        """
        Gets a sub-folder of the current folder.
        
        :param index: 'The position in the list of the child folder to return.'
        """
        pass
    
    @abstractmethod
    def get__count(self) -> 'int':
        """
        The number of child nodes.
        """
        pass
    
    @abstractmethod
    def get__depth_type(self) -> 'int':
        """
        The sub-tree depth type.
        """
        pass
    
    @abstractmethod
    def get__description(self) -> 'str':
        """
        The test set folder description.
        """
        pass
    
    @abstractmethod
    def set__description(self, value: 'str'):
        """
        The test set folder description.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__father(self) -> 'ISysTreeNode':
        """
        The current folder's parent folder.
        """
        pass
    
    @abstractmethod
    def get__father_disp(self) -> 'T':
        """
        The parent node.
        """
        pass
    
    @abstractmethod
    def get__father_id(self) -> 'int':
        """
        The unique parent ID.
        """
        pass
    
    @abstractmethod
    def is__has_attachment(self) -> 'bool':
        """
        Checks if this folder has at least one attachment.
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The node name.
        """
        pass
    
    @abstractmethod
    def set__name(self, value: 'str'):
        """
        The node name.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__node_id(self) -> 'int':
        """
        The node ID number.
        """
        pass
    
    @abstractmethod
    def get__path(self) -> 'str':
        """
        The folder tree path starting from the tree root.
        """
        pass
    
    @abstractmethod
    def get__sub_nodes(self) -> 'List[T]':
        """
        The list of child nodes.
        """
        pass
    
    @abstractmethod
    def get__test_set_factory(self) -> 'ITestSetFactory':
        """
        Gets the TestSetFactory for the folder.
        """
        pass
    
    @abstractmethod
    def get__view_order(self) -> 'int':
        """
        The index of this folder in its parent's list of sub-nodes.
        """
        pass
    
    @abstractmethod
    def set__view_order(self, value: 'int'):
        """
        The index of this folder in its parent's list of sub-nodes.
        :param value: 
        """
        pass
    
    @abstractmethod
    def add_node(self, node_name: 'str') -> 'ISysTreeNode':
        """
        Adds a new child node.
        
        :param node_name: The name of the folder to add.
        :return: 
        """
        pass

    @abstractmethod
    def add_node_disp(self, node_name: 'str') -> 'T':
        """
        Adds a new child node.
        
        :param node_name: The name of the folder to add.
        :return: A TestSetFolder object.
        """
        pass

    @abstractmethod
    def find_child_node(self, child_name: 'str') -> 'ISysTreeNode':
        """
        Finds a child node by node name.
        
        :param child_name: The name of the child node.
        :return: 
        """
        pass

    @abstractmethod
    def find_children(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        """
        Finds node children according to specified search pattern.
        
        :param pattern: The search text pattern. Pass an empty string ("") to find all. The pattern is matched if it occcurs anywhere in the name. Do not use wildcards.
        :param match_case: If TRUE, search is case-sensitive.  MatchCase has no effect if the database manager is not case sensitive. For example, if the project is hosted on an MS-SQL database, this flag has no effect.
        :param filter: A TDFilter.Text to select the items to be returned.
        :return: A list of TestSetFolder objects.
        """
        pass

    @abstractmethod
    def find_test_instances(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        """
        Gets the list of test instances in all test sets in given folder and its subfolders that match the specified pattern.
        
        :param pattern: The search text pattern. Pass an empty string ("") to find all. The pattern is matched if it occcurs anywhere in the name. Do not use wildcards.
        :param match_case: If TRUE, search is case-sensitive.  MatchCase has no effect if the database manager is not case sensitive. For example, if the project is hosted on an MS-SQL database, this flag has no effect.
        :param filter: A TDFilter.Text to select the items to be returned.
        :return: A list of TSTest Objects.
        """
        pass

    @abstractmethod
    def find_test_sets(self, pattern: 'str', match_case: 'bool', filter: 'str') -> 'List[T]':
        """
        Gets a list of test sets contained in the folder that match the specified pattern.
        
        :param pattern: The search text pattern. Pass an empty string ("") to find all. The pattern is matched if it occcurs anywhere in the name. Do not use wildcards.
        :param match_case: If TRUE, search is case-sensitive.  MatchCase has no effect if the database manager is not case sensitive. For example, if the project is hosted on an MS-SQL database, this flag has no effect.
        :param filter: A TDFilter.Text to select the items to be returned.
        :return: A list of TestSet Objects.
        """
        pass

    @abstractmethod
    def move(self, father: 'Any'):
        """
        Moves folder node under new father.
        
        :param father: Either a TestSetFolder object or NodeID.
        """
        pass

    @abstractmethod
    def new_list(self) -> 'List[T]':
        """
        Gets a list of the node's immediate children.
        :return: A list of TestSetFolder objects.
        """
        pass

    @abstractmethod
    def post(self):
        """
        Posts all changed values to the database.
        """
        pass

    @abstractmethod
    def refresh(self):
        """
        Reads the saved node data, overwriting values in memory.
        """
        pass

    @abstractmethod
    def remove_node(self, node: 'Any'):
        """
        Deletes the specified node.
        
        :param node: The folder to delete. One of: a TestSetFolder object; the name of 
        TestSetFolder as returned by the Name property; or the index of the 
        TestSetFolder in its parent's children list.
        """
        pass

    @abstractmethod
    def remove_node_ex(self, node: 'Any', delete_test_sets: 'bool'):
        """
        Deletes a test set folder node.
        
        :param node: The folder to delete. One of: a TestSetFolder object; the name of 
        TestSetFolder as returned by the Name property; or the index of the 
        TestSetFolder in its parent's children list.
        :param delete_test_sets: If TRUE, delete the test sets with the folder.
        """
        pass

    