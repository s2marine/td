from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    


class IHistoryRecord(metaclass=ABCMeta):
    """
    Represents a single change.
    """
    
    @abstractmethod
    def get__change_date(self) -> 'float':
        """
        The change date according to database server.
        """
        pass
    
    @abstractmethod
    def get__changer(self) -> 'str':
        """
        The name of the user who made the change.
        """
        pass
    
    @abstractmethod
    def get__field_name(self) -> 'str':
        """
        The name of the changed field.
        """
        pass
    
    @abstractmethod
    def get__item_key(self) -> 'Any':
        """
        The item key for the changed item.
        """
        pass
    
    @abstractmethod
    def get__new_value(self) -> 'Any':
        """
        The value after the change.
        """
        pass
    
    @abstractmethod
    def get__old_value(self) -> 'Any':
        """
        The value before the change.
        """
        pass
    
    