from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode
    
    from s2marine.td.client.intf.i_customization_list_node import ICustomizationListNode


class ICustomizationList(metaclass=ABCMeta):
    """
    Represents a user-defined list associated with a field.
    """
    
    @abstractmethod
    def is__deleted(self) -> 'bool':
        """
        If true, the node is marked for deletion, but the deletion is not yet committed.
        """
        pass
    
    @abstractmethod
    def set__deleted(self, value: 'bool'):
        """
        If true, the node is marked for deletion, but the deletion is not yet committed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__name(self) -> 'str':
        """
        The name of the list.
        """
        pass
    
    @abstractmethod
    def get__root_node(self) -> 'ICustomizationListNode':
        """
        The root CustomizationListNode of the current list.
        """
        pass
    
    @abstractmethod
    def find(self, val: 'Any') -> 'ICustomizationListNode':
        """
        Finds a CustomizationListNode of the current list by the node name or ID.
        
        :param val: The node name or ID.
        :return: A CustomizationListNode Object.
        """
        pass

    