from abc import ABCMeta, abstractmethod
from typing import List, Any, TYPE_CHECKING

if TYPE_CHECKING:
    pass
    
    from s2marine.td.client.intf.i_customization_users_group import ICustomizationUsersGroup
    from s2marine.td.client.intf.i_customization_list import ICustomizationList
    


class ICustomizationField(metaclass=ABCMeta):
    """
    Represents a user-defined field.
    """
    
    @abstractmethod
    def get__authorized_modify_for_groups(self) -> 'List[ICustomizationUsersGroup]':
        """
        The list of groups whose members can modify this field.
        """
        pass
    
    @abstractmethod
    def get__authorized_owner_sensible_for_groups(self) -> 'List[T]':
        """
        The list of CustomizationUsersGroup objects of the groups for which only the owner can modify the field.
        """
        pass
    
    @abstractmethod
    def get__authorized_visible_for_groups(self) -> 'List[T]':
        """
        The list of CustomizationUsersGroup objects of the groups for which the field is visible in customization dialogs.
        """
        pass
    
    @abstractmethod
    def get__authorized_visible_in_new_bug_for_groups(self) -> 'List[T]':
        """
        The list of CustomizationUsersGroup objects of the groups for which the field is visible in the new defect dialog.
        """
        pass
    
    @abstractmethod
    def is__can_make_multi_value(self) -> 'bool':
        """
        Checks if the field can be configured to store multiple values. If False, IsMultiValue cannot be set to True.
        """
        pass
    
    @abstractmethod
    def is__can_make_searchable(self) -> 'bool':
        """
        Checks if search can be enabled for the field.
        """
        pass
    
    @abstractmethod
    def get__column_name(self) -> 'str':
        """
        The column name.
        """
        pass
    
    @abstractmethod
    def get__column_type(self) -> 'str':
        """
        The database column type: char, date, memo, number, or time.
        """
        pass
    
    @abstractmethod
    def set__column_type(self, value: 'str'):
        """
        The database column type: char, date, memo, number, or time.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__default_value(self) -> 'str':
        """
        The default value of the field.
        """
        pass
    
    @abstractmethod
    def set__default_value(self, value: 'str'):
        """
        The default value of the field.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__edit_mask(self) -> 'str':
        """
        The input mask for string fields.
        """
        pass
    
    @abstractmethod
    def set__edit_mask(self, value: 'str'):
        """
        The input mask for string fields.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__field_size(self) -> 'int':
        """
        The size of the  field in the database. For BLOB fields, the size is  -1.
        """
        pass
    
    @abstractmethod
    def set__field_size(self, value: 'int'):
        """
        The size of the  field in the database. For BLOB fields, the size is  -1.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__grant_modify_for_group(self, group: 'Any') -> 'bool':
        """
        Indicates whether members of the group can change the value of this field.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        """
        pass
    
    @abstractmethod
    def set__grant_modify_for_group(self, group: 'Any', value: 'bool'):
        """
        Indicates whether members of the group can change the value of this field.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__grant_modify_mask(self) -> 'int':
        """
        The bit mask that indicates which user groups can modify the field.
        """
        pass
    
    @abstractmethod
    def set__grant_modify_mask(self, value: 'int'):
        """
        The bit mask that indicates which user groups can modify the field.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_active(self) -> 'bool':
        """
        Indicates if the field is active. Inactive user-defined fields are not visible in the user interface.
        """
        pass
    
    @abstractmethod
    def set__is_active(self, value: 'bool'):
        """
        Indicates if the field is active. Inactive user-defined fields are not visible in the user interface.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_by_code(self) -> 'bool':
        """
        Indicates if the field stores the Tree Node ID, rather than the passed value.
        """
        pass
    
    @abstractmethod
    def set__is_by_code(self, value: 'bool'):
        """
        Indicates if the field stores the Tree Node ID, rather than the passed value.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_can_group(self) -> 'bool':
        """
        Indicates if items of this type can form groups with others of the same type.
        """
        pass
    
    @abstractmethod
    def set__is_can_group(self, value: 'bool'):
        """
        Indicates if items of this type can form groups with others of the same type.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_customizable(self) -> 'bool':
        """
        Indicates if the field is displayed in the customization user interface.
        """
        pass
    
    @abstractmethod
    def set__is_customizable(self, value: 'bool'):
        """
        Indicates if the field is displayed in the customization user interface.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_edit(self) -> 'bool':
        """
        Indicates if the field can be edited.
        """
        pass
    
    @abstractmethod
    def set__is_edit(self, value: 'bool'):
        """
        Indicates if the field can be edited.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_history(self) -> 'bool':
        """
        Indicates if change history is stored for the field.
        """
        pass
    
    @abstractmethod
    def set__is_history(self, value: 'bool'):
        """
        Indicates if change history is stored for the field.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_mail(self) -> 'bool':
        """
        Indicates if Users on the list to be notified of changes are notified when this field is changed.
        """
        pass
    
    @abstractmethod
    def set__is_mail(self, value: 'bool'):
        """
        Indicates if Users on the list to be notified of changes are notified when this field is changed.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_multi_value(self) -> 'bool':
        """
        Indicates whether the field can store more than one value.
        """
        pass
    
    @abstractmethod
    def set__is_multi_value(self, value: 'bool'):
        """
        Indicates whether the field can store more than one value.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_required(self) -> 'bool':
        """
        Checks if the field is required.
        """
        pass
    
    @abstractmethod
    def set__is_required(self, value: 'bool'):
        """
        Checks if the field is required.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_searchable(self) -> 'bool':
        """
        Indicates whether the content of the field is searched by the Search method.
        """
        pass
    
    @abstractmethod
    def set__is_searchable(self, value: 'bool'):
        """
        Indicates whether the content of the field is searched by the Search method.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_transition_logic(self) -> 'bool':
        """
        Indicates if the field has transition logic.
        """
        pass
    
    @abstractmethod
    def set__is_transition_logic(self, value: 'bool'):
        """
        Indicates if the field has transition logic.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_verify(self) -> 'bool':
        """
        Indicates if the field requires verification.
        """
        pass
    
    @abstractmethod
    def set__is_verify(self, value: 'bool'):
        """
        Indicates if the field requires verification.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__is_virtual(self) -> 'bool':
        """
        Checks if the field has a physical representation in the database. If True, there is no physical representation.
        """
        pass
    
    @abstractmethod
    def get__is_visible_in_new_bug(self) -> 'int':
        """
        Indicates if the field is visible for the specified groups in a new defect form.
        """
        pass
    
    @abstractmethod
    def set__is_visible_in_new_bug(self, value: 'int'):
        """
        Indicates if the field is visible for the specified groups in a new defect form.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__list(self) -> 'ICustomizationList':
        """
        The CustomizationList assigned to the field.
        """
        pass
    
    @abstractmethod
    def set__list(self, value: 'ICustomizationList'):
        """
        The CustomizationList assigned to the field.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__new_created(self) -> 'bool':
        """
        Indicates if the field was created but not posted to the server.
        """
        pass
    
    @abstractmethod
    def set__new_created(self, value: 'bool'):
        """
        Indicates if the field was created but not posted to the server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__owner_sensible_for_group(self, group: 'Any') -> 'bool':
        """
        Indicates whether the field can be modified by the owner if the owner belongs to the specified group.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        """
        pass
    
    @abstractmethod
    def set__owner_sensible_for_group(self, group: 'Any', value: 'bool'):
        """
        Indicates whether the field can be modified by the owner if the owner belongs to the specified group.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__owner_sensible_mask(self) -> 'int':
        """
        The bit mask that indicates for which groups this field can be modified only by its owner.
        """
        pass
    
    @abstractmethod
    def set__owner_sensible_mask(self, value: 'int'):
        """
        The bit mask that indicates for which groups this field can be modified only by its owner.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__root_id(self) -> 'Any':
        """
        If the field contains a list, the ID of the list root node. If not, RootId is NULL.
        """
        pass
    
    @abstractmethod
    def set__root_id(self, value: 'Any'):
        """
        If the field contains a list, the ID of the list root node. If not, RootId is NULL.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__table_name(self) -> 'str':
        """
        The name of the table that stores the active field.
        """
        pass
    
    @abstractmethod
    def get__type(self) -> 'int':
        """
        The Field type.
        """
        pass
    
    @abstractmethod
    def set__type(self, value: 'int'):
        """
        The Field type.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__updated(self) -> 'bool':
        """
        Indicates if the object has been modified since the last download from the server.
        """
        pass
    
    @abstractmethod
    def set__updated(self, value: 'bool'):
        """
        Indicates if the object has been modified since the last download from the server.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__user_column_type(self) -> 'str':
        """
        The user column type: char, number, date, memo, or empty string.
        """
        pass
    
    @abstractmethod
    def set__user_column_type(self, value: 'str'):
        """
        The user column type: char, number, date, memo, or empty string.
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__user_label(self) -> 'str':
        """
        A user-defined field label.
        """
        pass
    
    @abstractmethod
    def set__user_label(self, value: 'str'):
        """
        A user-defined field label.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__version_control(self) -> 'bool':
        """
        Indicates if the field is under version control.
        """
        pass
    
    @abstractmethod
    def set__version_control(self, value: 'bool'):
        """
        Indicates if the field is under version control.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__visible_for_group(self, group: 'Any') -> 'bool':
        """
        Indicates whether the field is visible in the New Defect dialog to the specified group.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        """
        pass
    
    @abstractmethod
    def set__visible_for_group(self, group: 'Any', value: 'bool'):
        """
        Indicates whether the field is visible in the New Defect dialog to the specified group.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        :param value: 
        """
        pass
    
    @abstractmethod
    def get__visible_for_groups(self) -> 'int':
        """
        The bit mask that indicates which user groups can see this field.
        """
        pass
    
    @abstractmethod
    def set__visible_for_groups(self, value: 'int'):
        """
        The bit mask that indicates which user groups can see this field.
        :param value: 
        """
        pass
    
    @abstractmethod
    def is__visible_in_new_bug_for_group(self, group: 'Any') -> 'bool':
        """
        Indicates whether the field is visible to the specified group in the new defect dialog.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        """
        pass
    
    @abstractmethod
    def set__visible_in_new_bug_for_group(self, group: 'Any', value: 'bool'):
        """
        Indicates whether the field is visible to the specified group in the new defect dialog.
        
        :param group: 'The CustomizationUsersGroup.ID.'
        :param value: 
        """
        pass
    
    