from typing import Union

from s2marine.td.client.intf.i_command import ICommand
from s2marine.td.client.intf.i_recordset import IRecordset
from s2marine.td.client.utils.logger import td_logger


class Command:
    def __init__(self, impl: 'ICommand'):
        self.impl = impl

    @property
    def sql(self) -> 'str':
        return self.impl.get__command_text()

    @sql.setter
    def sql(self, value: 'str'):
        self.impl.set__command_text(value)

    def execute(self) -> 'int':
        td_logger.debug(f"Command.execute - sql: \n{self.sql}")
        self.impl.execute()
        affected = self.impl.get__affected_rows()
        td_logger.debug(f"Command.execute - affected: {affected}")
        return affected

    def query(self) -> 'CommandRowSet':
        td_logger.debug(f"Command.query - sql: \n{self.sql}")
        return CommandRowSet(self.impl.execute())


class CommandRowSet:
    def __init__(self, recordset: 'IRecordset'):
        self.recordset = recordset

    def __iter__(self):
        self.recordset.first()
        self.curr_row = 0
        return self

    def __next__(self):
        if self.curr_row == 0:
            self.curr_row += 1
            return CommandRow(self.recordset)

        self.recordset.next()
        if self.recordset.is__eor():
            raise StopIteration
        return CommandRow(self.recordset)


class CommandRow:
    def __init__(self, recordset: 'IRecordset'):
        self.recordset = recordset

    def __iter__(self):
        self.curr_col = -1
        return self

    def __next__(self):
        self.curr_col += 1
        if self.curr_col > self.recordset.get__col_count():
            raise StopIteration
        return self.recordset.get__field_value(self.curr_col)

    def __getitem__(self, key: 'Union[int, str]') -> 'any':
        return self.recordset.get__field_value(key)

    def __setitem__(self, key: 'Union[int, str]', value: 'any'):
        self.recordset.set__field_value(key, value)

    def __str__(self):
        ret = []
        for i in range(self.recordset.get__col_count()):
            ret.append(f"{self.recordset.get__col_name(i)}: {self.recordset.get__field_value(i)}")
        return ', '.join(ret)
